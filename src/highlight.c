/* Katoob
 * Copyright (c) 2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef ENABLE_HIGHLIGHT
#include "katoob.h"
#include <string.h>
#include <gtk/gtk.h>
#include "katoobdocument.h"
#include "highlight.h"
#include "mdi.h"
#include "misc.h"

void
katoob_set_highlight (GtkCheckMenuItem * menuitem, gpointer data)
{
  extern UI *katoob;
  KatoobDocument *doc = katoob_get_active_doc ();
KatoobHighlightType t = (KatoobHighlightType)GPOINTER_TO_INT(data);
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return;
    }
  if (!menuitem->active)
    {
      return;
    }
  katoob_document_set_highlight (doc, katoob->hl_manager, t);
}

/* TODO: better implementation */
KatoobHighlightType
katoob_get_highlight_type (gchar * file)
{
  gint x = strlen (file);

  KATOOB_DEBUG_FUNCTION;

/* C */
  if (string_with_suffix (file, ".c", x, 2))
    {
      return KATOOB_HIGHLIGHT_C;
    }
  else if (string_with_suffix (file, ".h", x, 2))
    {
      return KATOOB_HIGHLIGHT_C;
    }

/* C++ */
  else if (string_with_suffix (file, ".C", x, 2))
    {
      return KATOOB_HIGHLIGHT_CPP;
    }
  else if (string_with_suffix (file, ".hh", x, 3))
    {
      return KATOOB_HIGHLIGHT_CPP;
    }
  else if (string_with_suffix (file, ".cpp", x, 4))
    {
      return KATOOB_HIGHLIGHT_CPP;
    }
  else if (string_with_suffix (file, ".cxx", x, 4))
    {
      return KATOOB_HIGHLIGHT_CPP;
    }

/* HTML */
  else if (string_with_suffix (file, ".html", x, 5))
    {
      return KATOOB_HIGHLIGHT_HTML;
    }
  else if (string_with_suffix (file, ".htm", x, 4))
    {
      return KATOOB_HIGHLIGHT_HTML;
    }
  else if (string_with_suffix (file, ".shtml", x, 6))
    {
      return KATOOB_HIGHLIGHT_HTML;
    }

/* Perl */
  else if (string_with_suffix (file, ".pl", x, 3))
    {
      return KATOOB_HIGHLIGHT_PERL;
    }
  else if (string_with_suffix (file, ".cgi", x, 4))
    {
      return KATOOB_HIGHLIGHT_PERL;
    }

/* Python */
  else if (string_with_suffix (file, ".py", x, 3))
    {
      return KATOOB_HIGHLIGHT_PYTHON;
    }

/* Desktop */
  else if (string_with_suffix (file, ".desktop", x, 8))
    {
      return KATOOB_HIGHLIGHT_DESKTOP;
    }

/* Diff */
  else if (string_with_suffix (file, ".diff", x, 5))
    {
      return KATOOB_HIGHLIGHT_DIFF;
    }
  else if (string_with_suffix (file, ".patch", x, 6))
    {
      return KATOOB_HIGHLIGHT_DIFF;
    }

/* XML */
  else if (string_with_suffix (file, ".xml", x, 4))
    {
      return KATOOB_HIGHLIGHT_XML;
    }

/* po */
  else if (string_with_suffix (file, ".po", x, 3))
    {
      return KATOOB_HIGHLIGHT_PO;
    }
  else if (string_with_suffix (file, ".pot", x, 4))
    {
      return KATOOB_HIGHLIGHT_PO;
    }

/* Java */
  else if (string_with_suffix (file, ".java", x, 5))
    {
      return KATOOB_HIGHLIGHT_JAVA;
    }
  else if (string_with_suffix (file, ".class", x, 6))
    {
      return KATOOB_HIGHLIGHT_JAVA;
    }

/* TeX */
  else if (string_with_suffix (file, ".tex", x, 4))
    {
      return KATOOB_HIGHLIGHT_LATEX;
    }

/* Ada */
  else if (string_with_suffix (file, ".ada", x, 4))
    {
      return KATOOB_HIGHLIGHT_ADA;
    }
/* IDL */
  else if (string_with_suffix (file, ".idl", x, 4))
    {
      return KATOOB_HIGHLIGHT_IDL;
    }
  else
    {
/* Text */
      return KATOOB_HIGHLIGHT_NONE;
    }
}

#endif /* ENABLE_HIGHLIGHT */
