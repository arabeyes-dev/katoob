/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ENCODINGSLIST_H__
#define __ENCODINGSLIST_H__

/******************************************************************************
 * I had to modify the languages struct names cause the original names caused *
 * the strings to be disconnected when rendered if GDK_USE_XFT != 0           *
 *****************************************************************************/

typedef struct _Lang Lang;
struct _Lang
{
  gchar *name;
  gchar *encoding;
  Lang *children;
  /* This function should be called before converting the encoding from urf8 "internal gtk encoding" to the given encoding using g_convert() */
  gchar *(*to_encoding)(gchar *buffer);
  /* If we don't have a function to run before the conversion, call katoob_strip_codes() on the buffer ?  */
  gboolean strip_codes;
};

Lang __arabic[] = {
  {"ISO 8859-6 (Arabic)", "ISO_8859-6", NULL, NULL, TRUE},
  {"WINDOWS-1256 (Arabic)", "WINDOWS-1256", NULL, katoob_encodings_to_cp1256, FALSE},
  {NULL}
};

Lang __baltic[] = {
  {"ISO_8859-4 (Baltic)", "ISO_8859-4", NULL, NULL, TRUE},
  {"ISO_8859-13 (Baltic)", "ISO_8859-13", NULL, NULL, TRUE},
  {"WINDOWS-1257 (Baltic)", "WINDOWS-1257", NULL, NULL, TRUE},
  {NULL}
};

Lang __central_european[] = {
  {"ISO 8859-2 (Central European)", "ISO_8859-2", NULL, NULL, TRUE},
  {"WINDOWS-1250 (Central European)", "WINDOWS-1250", NULL, NULL, TRUE},
  {NULL}
};

Lang __cyrillic[] = {
  {"ISO_8859-5 (Cyrillic)", "ISO_8859-5", NULL, NULL, TRUE},
  {"WINDOWS-1251 (Cyrillic)", "WINDOWS-1251", NULL, NULL, TRUE},
  {NULL}
};

Lang __greek[] = {
  {"ISO 8859-7 (Greek)", "ISO_8859-7", NULL, NULL, TRUE},
  {"WINDOWS-1253 (Greek)", "WINDOWS-1253", NULL, NULL, TRUE},
  {NULL}
};

Lang __hebrew[] = {
  /* {"ISO 8859-8 (Hebrew - visual ordering)", "ISO_8859-8", NULL}, */
  {"ISO 8859-8-i (Hebrew - logical ordering)", "ISO_8859-8", NULL, NULL, TRUE},
  /*   {"ISO 8859-8 (Hebrew)", "ISO_8859-8", NULL}, */
  {"WINDOWS-1255 (Hebrew)", "WINDOWS-1255", NULL, NULL, TRUE},
  {NULL}
};

Lang __japanese[] = {
  {"EUC-JP (Japanese)", "EUC-JP", NULL, NULL, TRUE},
  {"SHIFT_JIS (Japanese)", "SHIFT_JIS", NULL, NULL, TRUE},
  {"ISO-2022-JP (Japanese)", "ISO-2022-JP", NULL, NULL, TRUE},
  {NULL}
};

Lang __romanian[] = {
  {"ISO_8859-16 (Romanian)", "ISO-8859-16", NULL, NULL, TRUE},
  {NULL}
};

Lang __turkish[] = {
  {"ISO 8859-9 (Turkish)", "ISO_8859-9", NULL, NULL, TRUE},
  {"WINDOWS-1254 (Turkish)", "WINDOWS-1254", NULL, NULL, TRUE},
  {NULL}
};

Lang __western[] = {
  {"ISO 8859-1 (Western European)", "ISO_8859-1", NULL, NULL, TRUE},
  {"ISO_8859-15 (Western, New)", "ISO_8859-15", NULL, NULL, TRUE},
  {"WINDOWS-1252 (Western)", "WINDOWS-1252", NULL, NULL, TRUE},
  {NULL}
};

Lang __other[] = {
  {"ISO_8859-3 (South European)", "ISO_8859-3", NULL, NULL, TRUE},
  {"ISO_8859-10 (Nordic)", "ISO_8859-10", NULL, NULL, TRUE},
  {"ISO_8859-11", "ISO_8859-11", NULL, NULL, TRUE},
  {"ISO_8859-12", "ISO_8859-12", NULL, NULL, TRUE},
  {"ISO_8859-14", "ISO_8859-14", NULL, NULL},
  {"WINDOWS-1258 (Vietnamese)", "WINDOWS-1258", NULL, NULL, TRUE},
  {"PLAIN TEXT (No Unicode Control Characters)", "PLAIN TEXT", NULL, NULL, TRUE},
  {"UTF-8", "UTF-8", NULL, NULL, TRUE},
  {NULL}
};

Lang languages[] = {
  {"Arabic", NULL, __arabic, NULL, TRUE},
  {"Baltic", NULL, __baltic, NULL, TRUE},
  {"Central European", NULL, __central_european, NULL, TRUE},
  {"Cyrillic", NULL, __cyrillic, NULL, TRUE},
  {"Greek", NULL, __greek, NULL, TRUE},
  {"Hebrew", NULL, __hebrew, NULL, TRUE},
  {"Japanese", NULL, __japanese, NULL, TRUE},
  {"Romanian", NULL, __romanian, NULL, TRUE},
  {"Turkish", NULL, __turkish, NULL, TRUE},
  {"Western", NULL, __western, NULL, TRUE},
  {"Other", NULL, __other, NULL, TRUE},
  {NULL}
};

Lang *encodings[ENCODINGS];

#endif /* __ENCODINGSLIST_H__ */
