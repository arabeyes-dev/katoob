/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include "toolbar.h"
#include "mdi.h"
#include "save.h"
#include "emulator.h"
#include "katoobwindow.h"
#include "misc.h"
#include "undoredo.h"
#include "emulator.h"
#include "emulator.h"
#include "search.h"
#include "open.h"

#ifdef ENABLE_PRINT
#include "print-gui.h"
#endif /* ENABLE_PRINT */

#ifdef HAVE_SPELL
#include "spelldlg.h"
#endif /* HAVE_SPELL */

extern conf *config;
extern UI *katoob;

GtkWidget *
katoob_toolbar_new (GtkWidget * win)
{
  GtkWidget *toolbar;
  GtkWidget *menu;

#ifndef _WIN32
  if (katoob->xkb_grp == -1)
    {
      katoob->xkb = gtk_button_new_with_label (_("Unknown"));
    }
  else
    {
      katoob->xkb =
	gtk_button_new_with_label (katoob->xkb_grps[katoob->xkb_grp]);
    }
#else
  katoob->xkb = gtk_button_new_with_label (_("Emulator"));
#endif /* _WIN32 */

  gtk_button_set_relief (GTK_BUTTON (katoob->xkb), GTK_RELIEF_NONE);

  menu = katoob_emulator_build_menu ();
  if (menu)
    {
      g_signal_connect (G_OBJECT (katoob->xkb), "clicked",
			G_CALLBACK (katoob_handle_xkb_button), menu);
    }

  toolbar = gtk_toolbar_new ();
  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_NEW,
			    _("Create a new file"), NULL,
			    G_CALLBACK (katoob_new), win, -1);
  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_OPEN,
			    _("Open a file for editing"), NULL,
			    G_CALLBACK (katoob_open), win, -1);
  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_SAVE,
			    _("Save the existing file"), NULL,
			    G_CALLBACK (katoob_save), win, -1);

#ifdef ENABLE_PRINT
  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_PRINT,
			    _("Print this document"), NULL,
			    G_CALLBACK (katoob_create_print_dialog), win, -1);
#endif /* ENABLE_PRINT */

  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));
  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_CLOSE,
			    _("Close the active file"), NULL,
			    G_CALLBACK (katoob_close_active_doc), win, -1);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));
  katoob->toolbar_undo =
    gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_UNDO,
			      _("Undo"), NULL, G_CALLBACK (katoob_undo),
			      win, -1);
  katoob->toolbar_redo =
    gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_REDO,
			      _("Redo"), NULL, G_CALLBACK (katoob_redo),
			      win, -1);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_CUT,
			    _("Cut"), NULL, G_CALLBACK (katoob_cut), win, -1);
  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_COPY,
			    _("Copy"), NULL, G_CALLBACK (katoob_copy),
			    win, -1);
  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_PASTE,
			    _("Paste"), NULL, G_CALLBACK (katoob_paste),
			    win, -1);
  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_DELETE,
			    _("Delete current selection"), NULL,
			    G_CALLBACK (katoob_delete), win, -1);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));
  gtk_toolbar_append_widget (GTK_TOOLBAR (toolbar), katoob->xkb,
			     _("Change the typing language"), NULL);
  return toolbar;
}

gboolean
katoob_popup_toolbar_menu (GtkWidget * widget, GdkEvent * event)
{
  extern UI *katoob;
  if (event->type == GDK_BUTTON_PRESS)
    {
      GdkEventButton *bevent = (GdkEventButton *) event;
      if (bevent->button == 3)
	{
	  gtk_menu_popup (GTK_MENU (katoob->toolbar_menu), NULL, NULL, NULL,
			  NULL, bevent->button, bevent->time);
	  return TRUE;
	}
    }
  return FALSE;
}

void
katoob_toggle_toolbar (GtkCheckMenuItem * checkmenuitem)
{
  extern conf *config;
  extern UI *katoob;
  if (checkmenuitem->active)
    {
      gtk_widget_show (GTK_WIDGET (KATOOB_WINDOW (katoob->win)->toolbar));
    }
  else
    {
      gtk_widget_hide (GTK_WIDGET (KATOOB_WINDOW (katoob->win)->toolbar));
    }
  config->toolbar = checkmenuitem->active;
}

void
katoob_toggle_extended_toolbar (GtkCheckMenuItem * checkmenuitem)
{
  extern conf *config;
  extern UI *katoob;
  if (checkmenuitem->active)
    {
      gtk_widget_show (GTK_WIDGET
		       (KATOOB_WINDOW (katoob->win)->extended_toolbar));
    }
  else
    {
      gtk_widget_hide (GTK_WIDGET
		       (KATOOB_WINDOW (katoob->win)->extended_toolbar));
    }
  config->extended_toolbar = checkmenuitem->active;
}

void
katoob_toolbar_icons ()
{
  extern conf *config;
  extern UI *katoob;

  g_free (config->toolbartype);
  config->toolbartype = g_strdup ("icons");
  gtk_toolbar_set_style (GTK_TOOLBAR (KATOOB_WINDOW (katoob->win)->toolbar),
			 GTK_TOOLBAR_ICONS);
}

void
katoob_toolbar_text ()
{
  extern conf *config;
  extern UI *katoob;

  g_free (config->toolbartype);
  config->toolbartype = g_strdup ("text");
  gtk_toolbar_set_style (GTK_TOOLBAR (KATOOB_WINDOW (katoob->win)->toolbar),
			 GTK_TOOLBAR_TEXT);
}

void
katoob_toolbar_both ()
{
  extern conf *config;
  extern UI *katoob;

  g_free (config->toolbartype);
  config->toolbartype = g_strdup ("both");
  gtk_toolbar_set_style (GTK_TOOLBAR (KATOOB_WINDOW (katoob->win)->toolbar),
			 GTK_TOOLBAR_BOTH);
}

void
katoob_toolbar_bothhoriz ()
{
  extern conf *config;
  extern UI *katoob;

  g_free (config->toolbartype);
  config->toolbartype = g_strdup ("both_horiz");
  gtk_toolbar_set_style (GTK_TOOLBAR (KATOOB_WINDOW (katoob->win)->toolbar),
			 GTK_TOOLBAR_BOTH_HORIZ);
}

void
katoob_populate_extended_toolbar (GtkWidget * toolbar, GtkWidget * win)
{
  GtkWidget *search_l;
  GtkWidget *search_e;
  GtkWidget *goto_l;
  GtkWidget *goto_e;
#ifdef HAVE_SPELL
  GtkWidget *spell_l;
#endif /* HAVE_SPELL */

  KATOOB_DEBUG_FUNCTION;

  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

  search_l = gtk_label_new (_("Search"));
  gtk_toolbar_append_widget (GTK_TOOLBAR (toolbar), search_l, NULL, NULL);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

  search_e = gtk_entry_new ();
  gtk_toolbar_append_widget (GTK_TOOLBAR (toolbar), search_e, NULL, NULL);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

  g_signal_connect (G_OBJECT (search_e), "activate",
		    G_CALLBACK (extended_toolbar_search), NULL);

  goto_l = gtk_label_new (_("Goto Line"));
  gtk_toolbar_append_widget (GTK_TOOLBAR (toolbar), goto_l, NULL, NULL);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

  goto_e = gtk_entry_new ();
  gtk_toolbar_append_widget (GTK_TOOLBAR (toolbar), goto_e, NULL, NULL);
  gtk_entry_set_width_chars (GTK_ENTRY (goto_e), 6);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

  g_signal_connect (G_OBJECT (goto_e), "insert_text",
		    G_CALLBACK (filter_goto), NULL);
  g_signal_connect (G_OBJECT (goto_e), "activate",
		    G_CALLBACK (extended_toolbar_goto_line), NULL);

#ifdef HAVE_SPELL
  spell_l = gtk_label_new (_("Spelling Dictionary"));
  gtk_toolbar_append_widget (GTK_TOOLBAR (toolbar), spell_l, NULL, NULL);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

  katoob->dicts_menu = katoob_spell_generate_option_menu ();
  gtk_toolbar_append_widget (GTK_TOOLBAR (toolbar), katoob->dicts_menu, NULL,
			     NULL);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

  gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_SPELL_CHECK,
			    _("Check Spelling"), NULL,
			    G_CALLBACK (katoob_create_spelldlg), win, -1);
  gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));
#endif /* HAVE_SPELL */

  gtk_toolbar_set_icon_size (GTK_TOOLBAR (toolbar), GTK_ICON_SIZE_MENU);
  gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
}
