/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "katoob.h"
#include <gtk/gtk.h>

#ifndef __KATOOB_WINDOW_H__
#define __KATOOB_WINDOW_H__

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define KATOOB_TYPE_WINDOW                  (katoob_window_get_type ())
#define KATOOB_WINDOW(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), KATOOB_TYPE_WINDOW, KatoobWindow))
#define KATOOB_WINDOW_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), KATOOB_TYPE_WINDOW, KatoobWindowClass))
#define KATOOB_IS_WINDOW(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KATOOB_TYPE_WINDOW))
#define KATOOB_IS_WINDOW_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), KATOOB_TYPE_WINDOW))
#define KATOOB_WINDOW_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), KATOOB_TYPE_WINDOW, KatoobWindowClass))


  typedef struct _KatoobWindow KatoobWindow;
  typedef struct _KatoobWindowClass KatoobWindowClass;

  struct _KatoobWindow
  {
    GtkWindow window;
    GtkWidget *toolbar;
    GtkWidget *extended_toolbar;
    GtkWidget *notebook;
    GtkWidget *statusbar;
  };

  struct _KatoobWindowClass
  {
    GtkWindowClass parent_class;

    /* Padding for future expansion */
    void (*_gtk_reserved1) (void);
    void (*_gtk_reserved2) (void);
    void (*_gtk_reserved3) (void);
    void (*_gtk_reserved4) (void);
  };


  GType katoob_window_get_type (void) G_GNUC_CONST;
  GtkWidget *katoob_window_new (void);
  void katoob_window_set_title (GtkWidget * win, gchar * title);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* __KATOOB_WINDOW_H__ */
