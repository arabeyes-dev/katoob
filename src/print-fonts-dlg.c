/* Katoob
 * Copyright (c) 2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef ENABLE_PRINT

#include "katoob.h"
#include <string.h>
#include <gtk/gtk.h>
#include <pango/pango.h>
#include <pango/pangoft2.h>
#include "print-gui.h"
#include "print.h"

#define FONT_LIST_HEIGHT        136
#define FONT_LIST_WIDTH         190
#define FONT_STYLE_LIST_WIDTH   170

GtkWidget *family_treeview = NULL;
GtkWidget *style_treeview = NULL;
GtkWidget *size_treeview = NULL;
GtkWidget *preview_label;
GtkTreeModel *family_model;
GtkTreeModel *style_model;
GtkTreeModel *size_model;

PangoFontFamily **family = NULL;

gint n_families;

gchar *cur_family;
gchar *cur_style;
gchar *cur_size;

static GtkTreeModel *create_model ();
static void update_preview_label ();

void fill_fonts ();
void fill_styles (GtkTreeSelection * selection, gpointer data);
void clear_styles ();
void fill_sizes ();
void update_family_preview (GtkTreeSelection * selection, gpointer data);
void update_style_preview (GtkTreeSelection * selection, gpointer data);
void update_size_preview (GtkTreeSelection * selection, gpointer data);

extern PConfig *PConf;

void
katoob_print_set_fonts (gpointer data)
{
  extern UI *katoob;

  GtkWidget *dialog;
  GtkWidget *dialog_vbox;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *family_frame;
  GtkWidget *family_scrolledwindow;
  GtkWidget *family_frame_label;
  GtkWidget *style_frame;
  GtkWidget *style_scrolledwindow;
  GtkWidget *style_frame_label;
  GtkWidget *size_frame;
  GtkWidget *size_scrolledwindow;
  GtkWidget *size_frame_label;
  GtkWidget *dialog_action_area;
  GtkWidget *cancelbutton;
  GtkWidget *okbutton;

  GtkTreeViewColumn *column;
  GtkCellRenderer *renderer;
  GtkTreeSelection *selection;

  GtkWidget *fonts_label = (GtkWidget *) data;

  gint r;
  gchar *txt;

  cur_family = g_strdup (PConf->font_family);
  cur_style = g_strdup (PConf->font_style);
  cur_size = g_strdup (PConf->font_scale);

  family_model = create_model ();
  style_model = create_model ();
  size_model = create_model ();

  dialog = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (dialog), _("Choose Font"));

  dialog_vbox = GTK_DIALOG (dialog)->vbox;

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (dialog_vbox), vbox, TRUE, TRUE, 0);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);

  family_frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (hbox), family_frame, TRUE, TRUE, 0);

  family_scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_set_size_request (family_scrolledwindow, FONT_LIST_WIDTH,
			       FONT_LIST_HEIGHT);

  gtk_container_add (GTK_CONTAINER (family_frame), family_scrolledwindow);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (family_scrolledwindow),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  family_treeview = gtk_tree_view_new_with_model (family_model);
  g_object_unref (G_OBJECT (family_model));

  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Family"),
						     renderer,
						     "text", 0, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (family_treeview), column);

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (family_treeview));
  gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

  g_signal_connect (G_OBJECT (selection), "changed",
		    G_CALLBACK (fill_styles), NULL);

  g_signal_connect (G_OBJECT (selection), "changed",
		    G_CALLBACK (update_family_preview), NULL);

  gtk_container_add (GTK_CONTAINER (family_scrolledwindow), family_treeview);
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (family_treeview), FALSE);

  family_frame_label = gtk_label_new (_("Family"));
  gtk_frame_set_label_widget (GTK_FRAME (family_frame), family_frame_label);
  gtk_label_set_justify (GTK_LABEL (family_frame_label), GTK_JUSTIFY_LEFT);

  style_frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (hbox), style_frame, TRUE, TRUE, 0);

  style_scrolledwindow = gtk_scrolled_window_new (NULL, NULL);

  gtk_widget_set_size_request (style_scrolledwindow, FONT_STYLE_LIST_WIDTH,
			       -1);

  gtk_container_add (GTK_CONTAINER (style_frame), style_scrolledwindow);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (style_scrolledwindow),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  style_treeview = gtk_tree_view_new_with_model (style_model);
  g_object_unref (G_OBJECT (style_model));

  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Style"),
						     renderer,
						     "text", 0, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (style_treeview), column);

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (style_treeview));
  gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

  g_signal_connect (G_OBJECT (selection), "changed",
		    G_CALLBACK (update_style_preview), NULL);

  gtk_container_add (GTK_CONTAINER (style_scrolledwindow), style_treeview);
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (style_treeview), FALSE);

  style_frame_label = gtk_label_new (_("Style"));
  gtk_frame_set_label_widget (GTK_FRAME (style_frame), style_frame_label);
  gtk_label_set_justify (GTK_LABEL (style_frame_label), GTK_JUSTIFY_LEFT);

  size_frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (hbox), size_frame, FALSE, FALSE, 0);

  size_scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (size_frame), size_scrolledwindow);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (size_scrolledwindow),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  size_treeview = gtk_tree_view_new_with_model (size_model);
  g_object_unref (G_OBJECT (size_model));

  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Size"),
						     renderer,
						     "text", 0, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (size_treeview), column);

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (size_treeview));
  gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

  g_signal_connect (G_OBJECT (selection), "changed",
		    G_CALLBACK (update_size_preview), NULL);

  gtk_container_add (GTK_CONTAINER (size_scrolledwindow), size_treeview);
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (size_treeview), FALSE);

  size_frame_label = gtk_label_new (_("Size"));
  gtk_frame_set_label_widget (GTK_FRAME (size_frame), size_frame_label);
  gtk_label_set_justify (GTK_LABEL (size_frame_label), GTK_JUSTIFY_LEFT);

  preview_label = gtk_label_new ("");
  gtk_box_pack_start (GTK_BOX (vbox), preview_label, TRUE, TRUE, 0);
  gtk_label_set_use_markup (GTK_LABEL (preview_label), TRUE);

  dialog_action_area = GTK_DIALOG (dialog)->action_area;
  gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area),
			     GTK_BUTTONBOX_END);

  cancelbutton = gtk_button_new_from_stock ("gtk-cancel");
  gtk_dialog_add_action_widget (GTK_DIALOG (dialog), cancelbutton,
				GTK_RESPONSE_CANCEL);
  GTK_WIDGET_SET_FLAGS (cancelbutton, GTK_CAN_DEFAULT);

  okbutton = gtk_button_new_from_stock ("gtk-ok");
  gtk_dialog_add_action_widget (GTK_DIALOG (dialog), okbutton,
				GTK_RESPONSE_OK);
  GTK_WIDGET_SET_FLAGS (okbutton, GTK_CAN_DEFAULT);

  fill_fonts ();
  fill_sizes ();

  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (katoob->win));
  gtk_widget_show_all (dialog);
  r = gtk_dialog_run (GTK_DIALOG (dialog));

  gtk_widget_destroy (dialog);

  g_free (family);

  if (r == GTK_RESPONSE_OK)
    {
      g_free (PConf->font_family);
      g_free (PConf->font_style);
      g_free (PConf->font_scale);
      PConf->font_family = g_strdup (cur_family);
      PConf->font_style = g_strdup (cur_style);
      PConf->font_scale = g_strdup (cur_size);
      txt = g_strdup_printf
	("<span font_desc=\"%s %s %s\">%s %s %s</span>",
	 PConf->font_family, PConf->font_style,
	 PConf->font_scale, PConf->font_family,
	 PConf->font_style, PConf->font_scale);

      gtk_label_set_markup (GTK_LABEL (fonts_label), txt);
      g_free (txt);

    }
  g_free (cur_family);
  g_free (cur_style);
  g_free (cur_size);
}

static GtkTreeModel *
create_model ()
{
  KATOOB_DEBUG_FUNCTION;
  return GTK_TREE_MODEL (gtk_list_store_new (1, G_TYPE_STRING));
}

void
fill_sizes ()
{
  gchar *sizes[] =
    { "8", "9", "10", "11", "12", "13", "14", "16", "18", "20", "22", "24",
    "26", "28", "32", "36", "40", "48", "56", "64", "72"
  };
  gint n_sizes = 21;
  gint x;
  GtkListStore *store;
  GtkTreeIter iter;
  KATOOB_DEBUG_FUNCTION;
  store = GTK_LIST_STORE
    (gtk_tree_view_get_model (GTK_TREE_VIEW (size_treeview)));

  for (x = 0; x < n_sizes; x++)
    {
      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter, 0, sizes[x], -1);
      if (!strcmp (sizes[x], PConf->font_scale))
	{
	  GtkTreeSelection *selection =
	    gtk_tree_view_get_selection (GTK_TREE_VIEW (size_treeview));
	  gtk_tree_selection_select_iter (selection, &iter);
	}
    }
}

void
fill_fonts ()
{
  GtkListStore *store;
  GtkTreeIter iter;

  PangoFontMap *map;
  gint x;

  KATOOB_DEBUG_FUNCTION;

  store =
    GTK_LIST_STORE (gtk_tree_view_get_model
		    (GTK_TREE_VIEW (family_treeview)));

  map = pango_ft2_font_map_for_display ();
  pango_font_map_list_families (map, &family, &n_families);

  for (x = 0; x < n_families; x++)
    {
      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter, 0,
			  pango_font_family_get_name (family[x]), -1);

      if (!strcmp
	  (pango_font_family_get_name (family[x]), PConf->font_family))
	{
	  GtkTreeSelection *selection =
	    gtk_tree_view_get_selection (GTK_TREE_VIEW (family_treeview));
	  gtk_tree_selection_select_iter (selection, &iter);
#warning THIS DOES NOT SELECT !
	}
    }
}

void
update_family_preview (GtkTreeSelection * selection, gpointer data)
{
  GtkListStore *store;
  GtkTreeIter iter;
  gchar *word = NULL;

  KATOOB_DEBUG_FUNCTION;

  store = GTK_LIST_STORE (family_model);

  if (!gtk_tree_selection_get_selected (selection, &family_model, &iter))
    {
      return;
    }

  gtk_tree_model_get (family_model, &iter, 0, &word, -1);
  if (!strcmp (word, cur_family))
    {
      g_free (word);
      return;
    }

  g_free (cur_family);
  cur_family = g_strdup (word);

  update_preview_label ();

  g_free (word);
}

void
update_style_preview (GtkTreeSelection * selection, gpointer data)
{
  GtkListStore *store;
  GtkTreeIter iter;
  gchar *word = NULL;

  KATOOB_DEBUG_FUNCTION;

  store = GTK_LIST_STORE (style_model);

  if (!gtk_tree_selection_get_selected (selection, &style_model, &iter))
    {
      return;
    }

  gtk_tree_model_get (style_model, &iter, 0, &word, -1);
  if (!strcmp (word, cur_style))
    {
      g_free (word);
      return;
    }
  g_free (cur_style);
  cur_style = g_strdup (word);

  update_preview_label ();

  g_free (word);
}

void
update_size_preview (GtkTreeSelection * selection, gpointer data)
{
  GtkListStore *store;
  GtkTreeIter iter;
  gchar *word = NULL;

  KATOOB_DEBUG_FUNCTION;

  store = GTK_LIST_STORE (size_model);

  if (!gtk_tree_selection_get_selected (selection, &size_model, &iter))
    {
      return;
    }

  gtk_tree_model_get (size_model, &iter, 0, &word, -1);
  if (!strcmp (word, cur_size))
    {
      g_free (word);
      return;
    }
  g_free (cur_size);
  cur_size = g_strdup (word);

  update_preview_label ();

  g_free (word);
}

void
fill_styles (GtkTreeSelection * selection, gpointer data)
{
  GtkListStore *store;
  GtkTreeIter iter;
  gchar *word = NULL;
  gint x;
  gint i;

  KATOOB_DEBUG_FUNCTION;

  store = GTK_LIST_STORE (style_model);

  if (!gtk_tree_selection_get_selected (selection, &family_model, &iter))
    {
      return;
    }

  clear_styles ();

  gtk_tree_model_get (family_model, &iter, 0, &word, -1);

  for (x = 0; x < n_families; x++)
    {
      if (!strcmp (word, pango_font_family_get_name (family[x])))
	{
	  PangoFontFace **faces;
	  gint n_faces;
	  pango_font_family_list_faces (family[x], &faces, &n_faces);

	  for (i = 0; i < n_faces; i++)
	    {
	      gtk_list_store_append (store, &iter);
	      gtk_list_store_set (store, &iter, 0,
				  pango_font_face_get_face_name (faces[i]),
				  -1);
	      if (i == 0)
		{
		  GtkTreeSelection *selection2 =
		    gtk_tree_view_get_selection
		    (GTK_TREE_VIEW (style_treeview));
		  gtk_tree_selection_select_iter (selection2, &iter);
		}
	      if (!strcmp
		  (pango_font_face_get_face_name (faces[i]),
		   PConf->font_style))
		{
		  GtkTreeSelection *selection2 =
		    gtk_tree_view_get_selection
		    (GTK_TREE_VIEW (style_treeview));
		  gtk_tree_selection_select_iter (selection2, &iter);
		}
	    }
	  break;
	}
    }
  g_free (word);
}

void
clear_styles ()
{
  KATOOB_DEBUG_FUNCTION;
  gtk_list_store_clear (GTK_LIST_STORE (style_model));
}

static void
update_preview_label ()
{
  gchar *txt;
  KATOOB_DEBUG_FUNCTION;
  txt = g_strdup_printf
    ("<span font_desc=\"%s %s %s\">%s</span>",
     cur_family, cur_style, cur_size, PREVIEW_TEXT);
  gtk_label_set_markup (GTK_LABEL (preview_label), txt);
  g_free (txt);
}

#endif /* ENABLE_PRINT */
