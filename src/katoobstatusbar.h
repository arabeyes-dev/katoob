/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __KATOOBSTATUSBAR_H__
#define __KATOOBSTATUSBAR_H__

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#include <gtk/gtk.h>

#define KATOOB_TYPE_STATUSBAR                  (katoob_statusbar_get_type ())
#define KATOOB_STATUSBAR(obj)                  (GTK_CHECK_CAST ((obj), KATOOB_TYPE_STATUSBAR, KatoobStatusbar))
#define KATOOB_STATUSBAR_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), KATOOB_TYPE_STATUSBAR, KatoobStatusbarClass))
#define KATOOB_IS_STATUSBAR(obj)               (GTK_CHECK_TYPE ((obj), KATOOB_TYPE_STATUSBAR))
#define KATOOB_IS_STATUSBAR_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), KATOOB_TYPE_STATUSBAR))
#define KATOOB_STATUSBAR_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), KATOOB_TYPE_STATUSBAR, KatoobStatusbarClass))

  typedef struct _KatoobStatusbar KatoobStatusbar;
  typedef struct _KatoobStatusbarClass KatoobStatusbarClass;
  typedef struct _KatoobStatusbarPrivate KatoobStatusbarPrivate;

  struct _KatoobStatusbar
  {
    GtkHBox hbox;
    KatoobStatusbarPrivate *priv;
  };

  struct _KatoobStatusbarClass
  {
    GtkHBoxClass parent_class;
  };

  GType katoob_statusbar_get_type (void) G_GNUC_CONST;
  GtkWidget *katoob_statusbar_new ();
  void katoob_statusbar_set_overwrite (KatoobStatusbar * sbar,
				       gboolean overwrite);
  void katoob_statusbar_set_modified (KatoobStatusbar * sbar,
				      gboolean modified);
  void katoob_statusbar_set_cursor_position (KatoobStatusbar * sbar, gint col,
					     gint row);
  void katoob_statusbar_show_hint (KatoobStatusbar * sbar, gchar * text);
  void katoob_statusbar_hide_hint (KatoobStatusbar * sbar);
  void katoob_statusbar_show (KatoobStatusbar * sbar);
  void katoob_statusbar_hide (KatoobStatusbar * sbar);
  void katoob_statusbar_set_encoding (KatoobStatusbar * sbar, gint encoding);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* __KATOOBSTATUSBAR_H__ */
