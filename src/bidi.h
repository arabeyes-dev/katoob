/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "fribidi.h"

#ifndef __BIDI_H__
#define __BIDI_H__

/*
#define PARAGRAPH_SEPARATOR 0x2029
*/

FriBidiCharType katoob_bidi_get_line_dir (gunichar * ch, glong l);
void katoob_bidi_modify_line_dir_from_utf8_string (KatoobDocument * doc,
						   gchar * text,
						   GtkTextIter start,
						   GtkTextIter end);

#endif /* __BIDI_H__ */
