/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*********************************
 * From gtkspell, Thanks Thanks! *
 *********************************/

#ifndef __SPELL_PRIVATE_H__
#define __SPELL_PRIVATE_H__

#ifdef HAVE_SPELL

#ifdef HAVE_NEW_SPELL
#include <aspell.h>

#else /* HAVE_OLD_SPELLL */
#include <pspell/pspell.h>
#define AspellSpeller PspellManager
#define speller manager
#define aspell_speller_check pspell_manager_check
#define aspell_speller_add_to_session pspell_manager_add_to_session
#define aspell_speller_store_replacement pspell_manager_store_replacement
#define AspellWordList PspellWordList
#define AspellStringEnumeration PspellStringEmulation
#define aspell_speller_suggest pspell_manager_suggest
#define aspell_word_list_elements pspell_word_list_elements
#define aspell_string_enumeration_next pspell_string_emulation_next
#define delete_aspell_string_enumeration delete_pspell_string_emulation
#define AspellConfig PspellConfig
#define AspellCanHaveError PspellCanHaveError
#define new_aspell_config new_pspell_config
#define aspell_config_replace pspell_config_replace
#define new_aspell_speller new_pspell_manager
#define delete_aspell_config delete_pspell_config
#define aspell_error_message pspell_error_message
#define delete_aspell_speller delete_pspell_manager
#define to_aspell_speller to_pspell_manager
#define aspell_error_number pspell_error_number
#define aspell pspell
#define aspell_speller_add_to_personal pspell_manager_add_to_personal
#endif /* HAVE_OLD_SPELL */

#endif /* HAVE_SPELL */

#endif /* __SPELL_PRIVATE_H__ */
