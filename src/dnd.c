/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include <gtk/gtk.h>
#include <string.h>
#include "dnd.h"
#include "mdi.h"
#include "katoobdocument.h"

static void
katoob_dnd_handle_drag_data_received (GtkWidget * widget, GdkDragContext
				      * context, gint x, gint y,
				      GtkSelectionData * selection_data,
				      guint info, guint time);

#if 0
static void
katoob_drag_data_get (GtkWidget * widget, GdkDragContext * dc,
		      GtkSelectionData * selection_data, guint info, guint t,
		      gpointer data);
/*
static void  katoob_drag_data_delete(GtkWidget *widget, GdkDragContext *context,
                                gpointer data);
*/
#endif

static GtkTargetEntry drag_types[] = {
  {"text/uri-list", 0, 0},
};

static gint n_drag_types = sizeof (drag_types) / sizeof (drag_types[0]);

void
katoob_dnd_init ()
{
  extern UI *katoob;
/* Drop */
  KATOOB_DEBUG_FUNCTION;

  gtk_drag_dest_set (katoob->win, GTK_DEST_DEFAULT_ALL, drag_types,
		     n_drag_types, GDK_ACTION_COPY);
  g_signal_connect (G_OBJECT (katoob->win), "drag_data_received",
		    G_CALLBACK (katoob_dnd_handle_drag_data_received), NULL);
}

#if 0
void
katoob_set_widget_dnd (GtkWidget * widget, KatoobDocument * doc)
{
  KATOOB_DEBUG_FUNCTION;
  gtk_drag_source_set (widget, GDK_BUTTON1_MASK, drag_types, n_drag_types,
		       GDK_ACTION_COPY | GDK_ACTION_MOVE);
  g_signal_connect (G_OBJECT (widget), "drag_data_get",
		    G_CALLBACK (katoob_drag_data_get), doc);
/*
  g_signal_connect (G_OBJECT (widget), "drag_data_delete",
                    G_CALLBACK (katoob_drag_data_delete), doc);
*/
}
#endif

static void
katoob_dnd_handle_drag_data_received (GtkWidget * widget, GdkDragContext
				      * context, gint x, gint y,
				      GtkSelectionData * selection_data,
				      guint info, guint time)
{
  extern conf *config;
  gint i = 0;
  gint j = 0;

/* FIXME: If this is guchar both strncmp & strndup'll break under g++ ! */
  gchar *str;

  KATOOB_DEBUG_FUNCTION;

/* NOTE: If length field is negative, Then Gtk couldn't obtain the selection 
data. */
  if (selection_data->length < 0)
    {
      katoob_debug ("Couldn't obtain selection data");
      return;
    }
#warning WE NEED TO KNOW WHEN THE DATA FIELD IS UNSIGNED AS CASTING HERE MAY BE A SECURITY ISSUE
  str = (gchar *) selection_data->data;

  katoob_udebug (selection_data->data);

  while (1)
    {
      if (str[i] == '\0')
	{
	  break;
	}

      if (str[i] == '\n')
	{
	  if (!strncmp (&str[j], "file://", 7))
	    {
	      gchar *str2 = g_strndup (&str[j + 7], (i - j) - 8);
	      katoob_debug (str2);
	      katoob_create_doc_from_file (str2, TRUE, config->defenc);
	      g_free (str2);
	    }
	  ++i;			/* skip \r */
	  j = i;
	}
      ++i;
    }
}

#if 0
static void
katoob_drag_data_get (GtkWidget * widget, GdkDragContext * dc,
		      GtkSelectionData * selection_data, guint info, guint t,
		      gpointer data)
{
  gchar *file;
  gchar *url;
  KatoobDocument *doc = (KatoobDocument *) data;

  KATOOB_DEBUG_FUNCTION;

  file = katoob_document_get_file (doc);
  if (file)
    {
      katoob_debug ("Found a file.");

/* URL: file:// + /full/path/to/foo + \r\n */

      url = g_strdup_printf ("file://%s\r\n", file);
      katoob_debug (file);
      katoob_debug (url);
      gtk_selection_data_set (selection_data, selection_data->target,
			      8, (guchar *) url, strlen (url));
      g_free (url);
    }
  else
    {
      katoob_debug ("Didn't find a file.");
      gtk_selection_data_set (selection_data, selection_data->target,
			      8, NULL, 0);
    }
}

/*
static void  katoob_drag_data_delete(GtkWidget *widget, GdkDragContext 
*context,
                                gpointer data)
{
katoob_debug(__FUNCTION__);
}

*/
#endif
