/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __PRINT_GUI_H__
#define __PRINT_GUI_H__

#ifdef ENABLE_PRINT

void katoob_create_print_dialog ();
void katoob_print_set_fonts (gpointer data);

/*
void katoob_print_create_options_dialog();
*/

#endif /* ENABLE_PRINT */

#endif /* __PRINT_GUI_H__ */
