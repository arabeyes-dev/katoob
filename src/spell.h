/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __SPELL_H__
#define __SPELL_H__

#ifdef HAVE_SPELL

/**************************************************************************
 * If the user is using his distro packages, Then this should be fine. If *
 * not, Then he should be an experienced user and can tell us where they  *
 * are from the preferences dialog.                                       *
 **************************************************************************/
#define DICTS_DIR "/usr/share/pspell"

GSList *katoob_spell_get_available_dicts ();
GtkWidget *katoob_spell_generate_option_menu ();
gchar *katoob_spell_get_default_dict ();
void spell_replace_word (GtkWidget * menuitem, gpointer _doc);
void spell_add_to_dictionary (GtkWidget * menuitem, gpointer _doc);

#endif /* HAVE_SPELL */

#endif /* __SPELL_H__ */
