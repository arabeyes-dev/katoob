/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>		/* strcmp() */
#include <gtk/gtk.h>
#include <glib.h>
#include "recent.h"
#include "open.h"		/* real_open() */
#include "mdi.h"
#include "misc.h"
#include "conf.h"

static void recent_real_open (gchar * file);
static void katoob_create_recent_menu_item (gchar * file);
static void katoob_regenerate_recent (GQuark key_id, gpointer data,
				      gpointer user_data);

void recent_fill_structure(gchar *str);

void
recent_init ()
{
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;
  g_datalist_init (&katoob->recent_files);
}

gboolean
recent_load ()
{
  KATOOB_DEBUG_FUNCTION;
  return katoob_load_strings_from_file("recent", recent_fill_structure);
}

void recent_fill_structure(gchar *str)
{
  extern conf *config;
  KATOOB_DEBUG_FUNCTION;
  config->recent_files =
    g_slist_append (config->recent_files, g_strdup (str));
}

void
recent_open (GtkWidget * menu)
{
/* recent_real_open() frees the argument for us. */
  KATOOB_DEBUG_FUNCTION;

  recent_real_open (g_strdup
		    (gtk_label_get_text
		     (GTK_LABEL (GTK_BIN (GTK_MENU_ITEM (menu))->child))));
}

static void
recent_real_open (gchar * file)
{
  extern conf *config;
  KATOOB_DEBUG_FUNCTION;

  recent_append (file);
  katoob_create_doc_from_file (file, FALSE, config->defenc);
  g_free (file);
}

void
recent_append (gchar * file)
{
  extern conf *config;
  extern UI *katoob;
  GtkWidget *item;
  KATOOB_DEBUG_FUNCTION;

  item = (GtkWidget *)g_datalist_get_data (&katoob->recent_files, file);
  if (item)
    {
      g_datalist_remove_data (&katoob->recent_files, file);
      gtk_widget_hide (GTK_WIDGET (item));
      gtk_widget_destroy (item);
/*  if ((katoob->recentno <= config->recentno) || (config->recentno == 0))
	{ */
/****************************************************************************
 * Since the item is here, We'll just move it to the top of the menu. Since *
 * katoob->recentno MUST always equal or less than config->recentno         *
 ****************************************************************************/
      katoob_create_recent_menu_item (file);
/*	}
      else
	{
	  item = gtk_menu_item_new_with_label (file);
	  g_signal_connect (G_OBJECT (item), "activate",
				    G_CALLBACK (recent_open),
				    NULL);
	  gtk_widget_show (GTK_WIDGET (item));
	  gtk_menu_shell_prepend (GTK_MENU_SHELL (katoob->recent_menu), item);
	  g_datalist_set_data (&katoob->recent_files, file, item);
	} */
    }
  else
    {
      if ((katoob->recentno <= config->recentno) || (config->recentno == 0))
	{
	  katoob_create_recent_menu_item (file);
	  katoob->recentno++;
	}
      else
	{
	  GtkMenuShell *ms = GTK_MENU_SHELL (katoob->recent_menu);
/*	  gint x = g_list_length (ms->children);;
	  x--;
	  item = g_list_nth_data (ms->children, x); */
	  item = (GtkWidget *)(g_list_last (ms->children))->data;
	  gtk_widget_hide (item);
	  gtk_widget_destroy (item);
	  katoob_create_recent_menu_item (file);
	}
    }
}

static void
katoob_create_recent_menu_item (gchar * file)
{
  GtkWidget *item;
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  item = gtk_menu_item_new_with_label (file);
  g_signal_connect (G_OBJECT (item),
		    "activate", G_CALLBACK (recent_open), NULL);
  gtk_widget_show (GTK_WIDGET (item));
  gtk_menu_shell_prepend (GTK_MENU_SHELL (katoob->recent_menu), item);
  g_datalist_set_data (&katoob->recent_files, file, item);
}

void
recent_save ()
{
  extern conf *config;
  extern UI *katoob;
  FILE *tmp;
  gint i;
  GSList *list = NULL;
  gchar *file =
    g_strdup_printf ("%s%s.katoob%srecent", g_get_home_dir (),
		     G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S);
  KATOOB_DEBUG_FUNCTION;

  config->recent_files = NULL;

  g_datalist_foreach (&katoob->recent_files, katoob_regenerate_recent, NULL);

  if (!config->recent_files)
    {
      g_free (file);
      return;
    }

  tmp = fopen (file, "w");
  if (!tmp)
    {
      katoob_error (_("Can't open the recent documents file"));
      g_free (file);
      return;
    }

  list = g_slist_reverse (config->recent_files);
/* TODO: reimplement with a GSList. */
  i = g_slist_length (list);
  i--;
  while (0 <= i)
    {
      fprintf (tmp, "%s\n", (gchar *) g_slist_nth_data (list, i));
      i--;
    }
  fclose (tmp);
/*  g_slist_foreach (list, (GFunc) g_free, NULL); DON'T FREE */

  g_slist_free (list);
  g_slist_free (config->recent_files);
  config->recent_files = NULL;
  g_free (file);
  return;
}

static void
katoob_regenerate_recent (GQuark key_id, gpointer data, gpointer user_data)
{
  extern conf *config;
  GtkWidget *tmp;
  KATOOB_DEBUG_FUNCTION;

  tmp = GTK_WIDGET (data);
  if (GTK_BIN (tmp)->child)
    {
      GtkWidget *child = GTK_BIN (tmp)->child;
      if (GTK_IS_LABEL (child))
	{
	  config->recent_files =
	    g_slist_append (config->recent_files,
			    (gchar *) gtk_label_get_text (GTK_LABEL (child)));
	  katoob_debug ((gchar *) gtk_label_get_text (GTK_LABEL (child)));
	}
    }
}
