/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>

#ifndef __FILE_H__
#define __FILE_H__

typedef struct _File
{
  gint result;
  gint encoding;
  gchar **file;
}
File;

File *katoob_create_file_selection (gchar * title, gboolean is_open);
gboolean katoob_write (gchar * buff, gint encoding, FILE * fp, gchar * file);
gboolean katoob_real_write (gchar * buff, FILE * fp, gchar * file);
gboolean katoob_create_file_if_required (gchar * file);
gboolean katoob_check_file_access (gchar * file);
gboolean katoob_file_get_contents (gchar * file, gchar ** content);
FILE *katoob_create_temp_file (gchar** tmp_file);
gchar* katoob_read_from_stream (FILE *fp);

#endif /* __FILE_H__ */
