
/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include <unistd.h>
#include "exec-cmd.h"
#include "mdi.h"
#include "misc.h"
#include "file.h"
#include "conf.h"

void exec_cmd_fill(gchar *line);
GtkWidget *exec_dialog = NULL;
GtkWidget *new_tab;
GtkWidget *commands;

void
destroy_dialog ()
{
  KATOOB_DEBUG_FUNCTION;
  gtk_widget_destroyed (exec_dialog, &exec_dialog);
}

void
on_ok_clicked ()
{
  extern conf *config;

  FILE *fp;
  gint encoding;
  gchar *c = NULL;
  KatoobDocument *doc;
  gchar *temp_file = NULL;
  gboolean r;
  gchar *command = NULL;
  gchar *cmd = NULL;
  gchar *result = NULL;
  gboolean in_new;
  gint utf8_encoding = katoob_encodings_get_by_name ("UTF-8");

  KATOOB_DEBUG_FUNCTION;

  cmd = g_strdup (gtk_entry_get_text
		  (GTK_ENTRY (GTK_COMBO (commands)->entry)));
  in_new = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (new_tab));

  if (!cmd)
    {
      g_signal_emit_by_name (G_OBJECT (exec_dialog), "destroy");
      return;
    }

  katoob_debug (cmd);
  /* Destroy the dialog. */
  g_signal_emit_by_name (G_OBJECT (exec_dialog), "destroy");

  doc = katoob_get_active_doc ();
  if (!doc)
    {
      g_free (cmd);
      return;
    }

  encoding = katoob_document_get_encoding (doc);

  fp = katoob_create_temp_file (&temp_file);
  katoob_debug (temp_file);
  if (!fp)
    {
      katoob_error (_("Could not create temporary file"));
      g_free (cmd);
      return;
    }

  c = katoob_document_get_text (doc);

  r = katoob_write (c, utf8_encoding, fp, temp_file);
  /* DON'T close fp cause katoob_real_write() close it */

  if (!r)
    {
      g_free (temp_file);
      g_free (cmd);
      g_free (c);
      return;
    }

  command = g_strdup_printf ("cat %s | %s", temp_file, cmd);
  /*  g_free (cmd); */
  katoob_debug (command);
  /*  system(command); */
  fp = popen (command, "r");
  if (!fp)
    {
      katoob_error (_("Couldn't execute the command."));
      g_free (temp_file);
      unlink (temp_file);
      g_free (command);
      g_free (cmd);
    }

  result = katoob_read_from_stream (fp);
  pclose (fp);
  if (!result)
    {
      g_free (temp_file);
      unlink (temp_file);
      g_free (cmd);
      return;
    }

  if (in_new)
    {
      /* We'll create a new document. */
      KatoobDocument *new_doc;
      katoob_create_doc ();
      new_doc = katoob_get_active_doc ();
      if (doc)
	{
	  katoob_document_set_text (new_doc, result);
	}
    }
  else
    {
      /* We'll clear the current document. */
      katoob_document_set_text_with_undo (doc, result);
    }
  g_free (temp_file);
  unlink (temp_file);
  /*  unlink (result); */
  g_free (result);
  g_free (command);
  /* Now to update the global configuration variables. */
  config->exec_cmd_in_new = in_new;

  /* prepend the new command. */
  /* TODO: Check if it's already there. */
  katoob_exec_cmd_set_size(config->exec_cmd_size-1);
  config->exec_cmd = g_list_prepend(config->exec_cmd,cmd);
  /*  g_free (cmd); */
}

void
on_cancel_clicked ()
{
  KATOOB_DEBUG_FUNCTION;
  g_signal_emit_by_name (G_OBJECT (exec_dialog), "destroy");
}

void
exec_cmd_cb ()
{
  extern conf *config;

  GtkWidget *dialog_vbox, *vbox;
  GtkWidget *ok, *cancel;

  KATOOB_DEBUG_FUNCTION;

  if (exec_dialog)
    {
      gtk_window_present (GTK_WINDOW (exec_dialog));
      return;
    }

  exec_dialog = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (exec_dialog), _("Execute Command"));

  gtk_container_set_border_width (GTK_CONTAINER (exec_dialog), 5);
  gtk_window_set_position (GTK_WINDOW (exec_dialog), GTK_WIN_POS_CENTER);

  dialog_vbox = GTK_DIALOG (exec_dialog)->vbox;

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 10);
  gtk_box_pack_start (GTK_BOX (dialog_vbox), vbox, TRUE, TRUE, 0);

  commands = gtk_combo_new ();
  if (config->exec_cmd)
    {
      gtk_combo_set_popdown_strings (GTK_COMBO (commands), config->exec_cmd);
    }
  gtk_combo_set_value_in_list (GTK_COMBO (commands), FALSE, FALSE);
  gtk_combo_set_case_sensitive (GTK_COMBO (commands), TRUE);
  gtk_box_pack_start (GTK_BOX (vbox), commands, FALSE, FALSE, 0);

  new_tab =
    gtk_check_button_new_with_mnemonic (_("_Show the results in a new tab"));
  gtk_box_pack_start (GTK_BOX (vbox), new_tab, FALSE, FALSE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (new_tab), config->exec_cmd_in_new);

  cancel = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
  gtk_dialog_add_action_widget (GTK_DIALOG (exec_dialog), cancel,
				GTK_RESPONSE_CANCEL);
  GTK_WIDGET_SET_FLAGS (cancel, GTK_CAN_DEFAULT);

  ok = gtk_button_new_from_stock (GTK_STOCK_OK);
  gtk_dialog_add_action_widget (GTK_DIALOG (exec_dialog), ok,
				GTK_RESPONSE_OK);
  GTK_WIDGET_SET_FLAGS (ok, GTK_CAN_DEFAULT);

  g_signal_connect (G_OBJECT (exec_dialog), "destroy",
		    G_CALLBACK (destroy_dialog), NULL);
  g_signal_connect (G_OBJECT (cancel), "clicked",
		    G_CALLBACK (on_cancel_clicked), NULL);
  g_signal_connect (G_OBJECT (ok), "clicked",
		    G_CALLBACK (on_ok_clicked), NULL);

  gtk_widget_show_all (exec_dialog);
}

gboolean exec_cmd_load()
{
  KATOOB_DEBUG_FUNCTION;
  return katoob_load_strings_from_file("exec-cmd", exec_cmd_fill);
}

void exec_cmd_fill(gchar *line)
{
  extern conf *config;
  KATOOB_DEBUG_FUNCTION;
  katoob_debug(line);
  config->exec_cmd = g_list_append(config->exec_cmd, g_strdup(line));
}

void exec_cmd_save()
{
  extern conf *config;
  FILE *fp;
  gchar *path;
  GList *tmp;

  KATOOB_DEBUG_FUNCTION;

  path = g_strdup_printf("%s%s.katoob%sexec-cmd",g_get_home_dir(),G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S);
  katoob_debug(path);
  fp = fopen(path, "w");
  g_free(path);
  if (!fp)
    {
      g_warning("Can't open exec-cmd file.");
      return;
    }
  tmp = config->exec_cmd;
  while (tmp)
    {
      gchar *str = (gchar *)tmp->data;
      katoob_debug(str);
      fprintf(fp,"%s\n", str);
      g_free(tmp->data);
      tmp = g_list_next(tmp);
    }
  g_list_free(config->exec_cmd);
  fclose(fp);
}

void
katoob_exec_cmd_set_size(gint size)
{
  extern conf *config;
  GList *tmp;
  gint x = g_list_length(config->exec_cmd);
  if (x > size)
    {
      int i = x - size;
      tmp = g_list_last(config->exec_cmd);
      for (; i > 0; i--)
	{
	  config->exec_cmd = g_list_remove_link(config->exec_cmd, tmp);
	  g_free(tmp->data);
	  g_list_free_1(tmp);
	  tmp = g_list_last(config->exec_cmd);
	}
    }
}
