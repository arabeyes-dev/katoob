/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#ifndef _WIN32
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <gdk/gdkx.h>		/* for GDK_DISPLAY() */
#endif /* _WIN32 */
#include <string.h>
#include "misc.h"
#include "encodings.h"

/* TODO: combine config_init() here */
void
katoob_init (conf ** config, UI ** katoob)
{
  KATOOB_DEBUG_FUNCTION;

  *config = (conf *) g_malloc (sizeof (conf));
  *katoob = (UI *) g_malloc (sizeof (UI));
  (*config)->recent_files = NULL;
  (*config)->files_toopen = NULL;
  (*config)->dicts_dir = NULL;
  (*config)->filesno = 0;
  (*config)->linenumbers = FALSE;
#ifndef _WIN32
  (*config)->xkb = TRUE;
#endif /* _WIN32 */
  (*config)->emulator = TRUE;
#ifndef _WIN32
  (*katoob)->xkb_grp = -1;
#endif /* _WIN32 */
  (*config)->font = NULL;
  (*config)->default_dict = NULL;
  (*config)->saved_enc = NULL;
  (*katoob)->win = NULL;
#ifndef _WIN32
  bzero ((*katoob)->xkb_grps, XkbNumKbdGroups);
#endif /* _WIN32 */
  (*config)->exec_cmd = NULL;

  return;
}

#ifndef _WIN32
void
xkb_init (UI * katoob)
{
  int xkbError;
  XkbDescPtr kbinfo;
  Status status;
  int grpnum = 0;
  XkbStateRec state;
  KATOOB_DEBUG_FUNCTION;

  if (XkbQueryExtension
      (GDK_DISPLAY (), NULL, &(katoob)->xkbEventType, &xkbError, NULL,
       NULL) != True)
    {
      return;
    }
  if (False ==
      XkbSelectEventDetails (GDK_DISPLAY (), XkbUseCoreKbd, XkbStateNotify,
			     0xFFF, XkbGroupLockMask))
    {
      return;
    }

  kbinfo = XkbGetMap (GDK_DISPLAY (), 0, XkbUseCoreKbd);
  if (kbinfo)
    {
      status = XkbGetControls (GDK_DISPLAY (), XkbSlowKeysMask, kbinfo);

      if (kbinfo->ctrls != NULL && status == Success)
	{
	  grpnum = kbinfo->ctrls->num_groups;
	}
      status = XkbGetNames (GDK_DISPLAY (), XkbGroupNamesMask, kbinfo);

      if (kbinfo->names != NULL && status == Success)
	{
	  int i;

	  for (i = 0; i < grpnum; i++)
	    {
	      if (kbinfo->names->groups[i] != 0L)
		{
		  katoob->xkb_grps[i] =
		    XGetAtomName (GDK_DISPLAY (), kbinfo->names->groups[i]);
		}
	      else
		{
		  katoob->xkb_grps[i] = NULL;
		}
	    }
	}

/* Let's NULL all the remaining array. */
      while (grpnum < XkbNumKbdGroups)
	{
	  katoob->xkb_grps[grpnum] = NULL;
	  ++grpnum;
	}

      XkbGetControls (GDK_DISPLAY (), XkbAllControlsMask, kbinfo);

      XkbFreeKeyboard (kbinfo, XkbAllControlsMask, True);
      if (Success == XkbGetState (GDK_DISPLAY (), XkbUseCoreKbd, &state))
	{
	  katoob->xkb_grp = state.locked_group;
	}
      else
	{
	  katoob->xkb_grp = -1;
	}
    }
  gdk_window_add_filter (NULL, (GdkFilterFunc) event_filter, NULL);
  return;
}
#endif /* _WIN32 */
