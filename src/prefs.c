/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"

#include <string.h>		/* strcmp() */
#include <gtk/gtk.h>
#include "katoobwindow.h"
#include "katoobstatusbar.h"
#include "prefs.h"
#include "misc.h"
#include "mdi.h"

/******************************************************************************
 * This function sets all the main window attributes EXCEPT the size &        *
 * position which is handled in the widget implementation! to avoid changing  *
 * them when the user hits "Apply" or "Ok" in the preferences dialog          *
 ******************************************************************************/
void
katoob_update_main_interface (KatoobWindow * win)
{
  extern conf *config;
  extern UI *katoob;
  KatoobDocument *active = katoob_get_active_doc ();
  if (config->toolbar)
    {
      gtk_widget_show (GTK_WIDGET (win->toolbar));
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->show_toolbar), TRUE);
    }
  else
    {
      gtk_widget_hide (GTK_WIDGET (win->toolbar));
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->show_toolbar), FALSE);
    }

  if (config->extended_toolbar)
    {
      gtk_widget_show (GTK_WIDGET (win->extended_toolbar));
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->extended_toolbar), TRUE);
    }
  else
    {
      gtk_widget_hide (GTK_WIDGET (win->extended_toolbar));
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->extended_toolbar), FALSE);
    }

  if (config->statusbar)
    {
      katoob_statusbar_show (KATOOB_STATUSBAR
			     (KATOOB_WINDOW (katoob->win)->statusbar));
    }
  else
    {
      katoob_statusbar_hide (KATOOB_STATUSBAR
			     (KATOOB_WINDOW (katoob->win)->statusbar));
    }

  if (config->recent)
    {
      gtk_widget_show (GTK_WIDGET (katoob->recent));
    }
  else
    {
      gtk_widget_hide (GTK_WIDGET (katoob->recent));
    }

  if (!strcmp (config->toolbartype, "icons"))
    {
      gtk_toolbar_set_style (GTK_TOOLBAR (win->toolbar), GTK_TOOLBAR_ICONS);
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (katoob->iconsonly),
				      TRUE);
    }
  else if (!strcmp (config->toolbartype, "text"))
    {
      gtk_toolbar_set_style (GTK_TOOLBAR (win->toolbar), GTK_TOOLBAR_TEXT);
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->textonly), TRUE);
    }
  else if (!strcmp (config->toolbartype, "both_horiz"))
    {
      gtk_toolbar_set_style (GTK_TOOLBAR (win->toolbar),
			     GTK_TOOLBAR_BOTH_HORIZ);
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->bothhoriz), TRUE);
    }
  else
    {
      gtk_toolbar_set_style (GTK_TOOLBAR (win->toolbar), GTK_TOOLBAR_BOTH);
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (katoob->both),
				      TRUE);
    }

  gtk_notebook_set_show_tabs (GTK_NOTEBOOK (win->notebook), config->showtabs);
  switch (config->tabspos)
    {
    case TABS_POS_TOP:
      gtk_notebook_set_tab_pos (GTK_NOTEBOOK (win->notebook), GTK_POS_TOP);
      break;
    case TABS_POS_BOTTOM:
      gtk_notebook_set_tab_pos (GTK_NOTEBOOK (win->notebook), GTK_POS_BOTTOM);
      break;
    case TABS_POS_RIGHT:
      gtk_notebook_set_tab_pos (GTK_NOTEBOOK (win->notebook), GTK_POS_RIGHT);
      break;
    case TABS_POS_LEFT:
      gtk_notebook_set_tab_pos (GTK_NOTEBOOK (win->notebook), GTK_POS_LEFT);
      break;
    default:
      gtk_notebook_set_tab_pos (GTK_NOTEBOOK (win->notebook), GTK_POS_TOP);
      break;
    }

  if (config->tabsmenu)
    {
      gtk_notebook_popup_enable (GTK_NOTEBOOK (win->notebook));
    }
  else
    {
      gtk_notebook_popup_disable (GTK_NOTEBOOK (win->notebook));
    }

  gtk_notebook_set_scrollable (GTK_NOTEBOOK (win->notebook),
			       config->scrolltabs);

  if (config->undo)
    {
      gtk_widget_show (katoob->undo);
      gtk_widget_show (katoob->redo);
      gtk_widget_show (katoob->toolbar_undo);
      gtk_widget_show (katoob->toolbar_redo);
    }
  else
    {
      gtk_widget_hide (katoob->undo);
      gtk_widget_hide (katoob->redo);
      gtk_widget_hide (katoob->toolbar_undo);
      gtk_widget_hide (katoob->toolbar_redo);
    }

  if (config->showclose)
    {
      gtk_container_foreach (GTK_CONTAINER (KATOOB_WINDOW (win)->notebook),
			     (GtkCallback) katoob_show_close_buttons, NULL);
    }
  else
    {
      gtk_container_foreach (GTK_CONTAINER (KATOOB_WINDOW (win)->notebook),
			     (GtkCallback) katoob_hide_close_buttons, NULL);
    }

  if (config->linenumbers)
    {
      gtk_container_foreach (GTK_CONTAINER (KATOOB_WINDOW (win)->notebook),
			     (GtkCallback) katoob_show_line_numbers, NULL);
    }
  else
    {
      gtk_container_foreach (GTK_CONTAINER (KATOOB_WINDOW (win)->notebook),
			     (GtkCallback) katoob_hide_line_numbers, NULL);
    }

  if (active)
    {
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->line_numbers),
				      katoob_document_get_linenumbers
				      (active));
    }
  else
    {
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->line_numbers),
				      config->linenumbers);
    }
  if (!config->default_font && (config->font))
    {
      gtk_container_foreach (GTK_CONTAINER (KATOOB_WINDOW (win)->notebook),
			     (GtkCallback) katoob_set_custom_font, NULL);
    }
  else
    {
      gtk_container_foreach (GTK_CONTAINER (KATOOB_WINDOW (win)->notebook),
			     (GtkCallback) katoob_set_default_font, NULL);
    }
}
