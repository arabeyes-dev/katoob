/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* TODO: clean this file */

#ifndef _WIN32
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#endif /* _WIN32 */

#ifndef __KATOOB_H__
#define __KATOOB_H__
/*
 * It won't compile under Gtk+ >= 2.4 when enabling these
 */
/*
#ifndef GTK_2_4
#define G_DISABLE_DEPRECATED 1
#define GDK_DISABLE_DEPRECATED 1
#define GDK_PIXBUF_DISABLE_DEPRECATED 1
#define GTK_DISABLE_DEPRECATED 1
#endif
*/

#ifdef DEBUG
#include <stdio.h>
#endif /* DEBUG */

#include <gtk/gtk.h>
/* TODO: More clean way ?? */
#ifdef _WIN32
#ifdef ftruncate
#undef ftruncate
#endif /* ftruncate */
#endif /* _WIN32 */

#if GTK_MINOR_VERSION >= 4
#define GTK_2_4
#endif

#include "encodings.h"
#ifdef ENABLE_HIGHLIGHT
#include <gtksourceview/gtksourcelanguagesmanager.h>
#endif /* ENABLE_HIGHLIGHT */

#ifdef DEBUG
/*
#define k_malloc(n) _k_malloc(n, __FILE__, __FUNCTION__, __LINE__)
#define k_free(n) _k_free(n, __FILE__, __FUNCTION__, __LINE__)
#define k_strdup(n) _k_strdup(n, __FILE__, __FUNCTION__, __LINE__)
*/
#define katoob_debug(x) _katoob_debug(x)
#define katoob_udebug(x) _katoob_udebug(x)
#define KATOOB_DEBUG_FUNCTION fprintf(stdout, "%s\n", __FUNCTION__);fflush(stdout);
/*
void *_k_malloc (int size, gchar *file, gchar *func, int line);
void *_k_strdup (gchar *str, gchar *file, gchar *func, int line);
void _k_free (void *buf, gchar *file, gchar *func, int line);
*/
#else
/*
#define k_malloc g_malloc
#define k_free g_free
#define k_strdup g_strdup
*/
#define katoob_debug(x)
#define katoob_udebug(x)
#define KATOOB_DEBUG_FUNCTION
#endif /* DEBUG */

/*
** Standard gettext macros.
** From any glade generated app.
*/

#ifdef ENABLE_NLS
#  include <locale.h>
#  include <libintl.h>
#  define _(String) gettext (String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define bind_textdomain_codeset(Domain,Codeset) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif /* ENABLE_NLS */

#define TABS_POS_TOP			101
#define TABS_POS_BOTTOM			102
#define TABS_POS_RIGHT			103
#define TABS_POS_LEFT			104

#define KATOOB_BIDI_LTR                 105
#define KATOOB_BIDI_RTL                 106
#define KATOOB_BIDI_AUT                 107

/* FIXME: How to store the preview text ?? */
#define PREVIEW_TEXT "AaBbCc ا ب ت"

#define IS_LAM_ALEF(x) ((x == 0x000fef7) || (x == 0x000fefb) || (x == 0x000fef5) || (x == 0x000fef9))
typedef struct _conf
{
  gint x;
  gint y;
  gint w;
  gint h;
  gint recentno;		/* No. of configured recent items to show */
  gboolean toolbar;
  gboolean extended_toolbar;
  gboolean statusbar;
  gboolean savewinpos;
  gboolean saveonexit;
  gboolean textwrap;
  guint text_dir;
  gboolean recent;
  gboolean tabsmenu;
  gboolean showtabs;
  gboolean showclose;
  gboolean scrolltabs;
  gboolean undo;
  gboolean linenumbers;
#ifndef _WIN32
  gboolean disable_xft;
#endif				/* _WIN32 */
  guint undono;
  guint tabspos;
  gchar *toolbartype;
  gchar *lang;
  gchar *font;
  gchar *dicts_dir;
  gchar *default_dict;
  gint defenc;
  guint filesno;
#ifndef _WIN32
  gboolean xkb;
  gboolean xkb_err_dlg;
#endif				/* _WIN32 */
  gboolean emulator;
#ifndef _WIN32
  gboolean locale_enc;
#endif				/* _WIN32 */
  gboolean special_enc;
  gboolean default_font;
  gchar *saved_enc;
  GSList *recent_files;
  GSList *files_toopen;
  GList *enc;
#ifdef HAVE_SPELL
  gboolean spell_check;
  guint mispelled_red;
  guint mispelled_blue;
  guint mispelled_green;
  GSList *dicts;
#endif				/* HAVE_SPELL */
#ifdef ENABLE_HIGHLIGHT
  gboolean highlight;
#endif				/* ENABLE_HIGHLIGHT */
/* Backup before saving */
  gboolean backup;
  gboolean backup_in_same_dir;
  gchar *backup_ext;
  gchar *backup_dir;
  gchar *filesel_path;
/* Dict stuff. */
  gboolean dict;
  gchar *dict_host;
  guint dict_port;
  guint dict_timeout;
  gchar *dict_user;
  gchar *dict_pw;
  gchar *dict_db;
  GList *exec_cmd; /* GList cause gtk_combo_set_popdown_strings() takes a GList */
  gboolean exec_cmd_in_new;
  gint exec_cmd_size;
}
conf;

typedef struct _UI
{
  GtkWidget *win;
  GtkWidget *show_toolbar;
  GtkWidget *extended_toolbar;
  GtkWidget *toolbar_menu;
  GtkWidget *iconsonly;
  GtkWidget *textonly;
  GtkWidget *both;
  GtkWidget *bothhoriz;
  GtkWidget *wrap_text;
  GtkWidget *show_statusbar;
  GtkWidget *recent_menu;
  GtkWidget *recent;
  GtkWidget *documents_menu;
  GtkWidget *encodings_menu;
  GtkWidget *xkb;
  GtkWidget *undo;
  GtkWidget *redo;
  GtkWidget *toolbar_undo;
  GtkWidget *toolbar_redo;
  GtkWidget *line_numbers;
#ifdef HAVE_SPELL
  GtkWidget *dicts_menu;
  GtkWidget *spell_check;
  GdkColor mispelled_color;
#endif				/* HAVE_SPELL */
  GtkClipboard *clipboard;
  GData *recent_files;
  gint recentno;		/* No. of actually shown recent */
  gboolean emulator;		/* Emulator is active ? */
  GtkWidget *encodings[ENCODINGS];
#ifndef _WIN32
  char *xkb_grps[XkbNumKbdGroups];
  gint xkb_grp;			/* This is equal to the xkb group ONLY not the emulator one! */
  gint xkbEventType;
#endif				/* _WIN32 */
#ifdef ENABLE_HIGHLIGHT
  GtkWidget *hl_none;
  GtkWidget *hl_ada;
  GtkWidget *hl_c;
  GtkWidget *hl_cpp;
  GtkWidget *hl_desktop;
  GtkWidget *hl_diff;
  GtkWidget *hl_html;
  GtkWidget *hl_idl;
  GtkWidget *hl_java;
  GtkWidget *hl_latex;
  GtkWidget *hl_perl;
  GtkWidget *hl_po;
  GtkWidget *hl_python;
  GtkWidget *hl_xml;
  GtkSourceLanguagesManager *hl_manager;
#endif				/* ENABLE_HIGHLIGHT */
}
UI;

gboolean katoob_exit ();
void katoob_new ();
void _katoob_debug (gchar * str);
void _katoob_udebug(guchar *str);

#endif /* __KATOOB_H__ */
