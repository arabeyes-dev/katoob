/* FriBidi - Library of BiDi algorithm
 * Copyright (C) 2001,2002 Behdad Esfahbod. 
 * 
 * This library is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU Lesser General Public 
 * License as published by the Free Software Foundation; either 
 * version 2.1 of the License, or (at your option) any later version. 
 * 
 * This library is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library, in a file named COPYING; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA  
 * 
 * For licensing issues, contact <fwpg@sharif.edu>. 
 */

/* 
 * Adapted for Katoob Copyright (c) 2002, 2003 Arabeyes, Mohammed Sameer.
 */

#include "fribidi.h"

/* Map fribidi_prop_types to fribidi_types, the order of types here should
   be the same as enum FriBidiPropEnum in fribidi_types.h */
static FriBidiCharType fribidi_prop_to_type_array[] = {
  FRIBIDI_TYPE_LTR,		/* Strong left to right */
  FRIBIDI_TYPE_RTL,		/* Right to left characters */
  FRIBIDI_TYPE_AL,		/* Arabic characters */
  FRIBIDI_TYPE_LRE,		/* Left-To-Right embedding */
  FRIBIDI_TYPE_RLE,		/* Right-To-Left embedding */
  FRIBIDI_TYPE_LRO,		/* Left-To-Right override */
  FRIBIDI_TYPE_RLO,		/* Right-To-Left override */
  FRIBIDI_TYPE_PDF,		/* Pop directional override */
  FRIBIDI_TYPE_EN,		/* European digit */
  FRIBIDI_TYPE_AN,		/* Arabic digit */
  FRIBIDI_TYPE_ES,		/* European number separator */
  FRIBIDI_TYPE_ET,		/* European number terminator */
  FRIBIDI_TYPE_CS,		/* Common Separator */
  FRIBIDI_TYPE_NSM,		/* Non spacing mark */
  FRIBIDI_TYPE_BN,		/* Boundary neutral */
  FRIBIDI_TYPE_BS,		/* Block separator */
  FRIBIDI_TYPE_SS,		/* Segment separator */
  FRIBIDI_TYPE_WS,		/* Whitespace */
  FRIBIDI_TYPE_ON,		/* Other Neutral */
  FRIBIDI_TYPE_WL,		/* Weak left to right */
  FRIBIDI_TYPE_WR,		/* Weak right to left */
};

FriBidiCharType *fribidi_prop_to_type = fribidi_prop_to_type_array;
