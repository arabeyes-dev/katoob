/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "katoobdocument.h"

#ifndef __ENCODINGS_H__
#define __ENCODINGS_H__

#define ENCODINGS 30

void encodings_init ();
gchar *katoob_encodings_get_by_number (gint no);
gchar *katoob_encodings_get_name_by_number (gint no);
gint katoob_encodings_get_by_name (gchar * name);
gint katoob_encodings_get_name_from_string (gchar * name);
void katoob_encoding_changed_cb (KatoobDocument * doc, gint enc);
gchar *katoob_encoding_convert (gchar * from, gchar * to, gchar * buff,
				gint len);
gchar *katoob_encodings_to_cp1256(gchar *buff);
gboolean katoob_utf8_strip_codes (gchar * src, gchar ** dist);
gunichar *katoob_unicode_strip_codes (gunichar * src, glong len);
gchar *katoob_encodings_convert_lam_alef(gunichar ch);
gunichar *katoob_encodings_get_lam_alef_unicode (gunichar ch);
gunichar *katoob_unicode_convert_lam_alef(gunichar *src, glong len);
gchar *katoob_utf8_convert_lam_alef(gchar *src);

#endif /*__ ENCODINGS_H__ */
