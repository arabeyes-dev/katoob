/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/* This should be included after config.h, to allow gettext stuff in config.h 
 * to be effective, and because we removed G*_DISABLE_DEPRECATED from the 
 * Makefile to katoob.h */

#include "katoob.h"

#include <stdio.h>
#include <limits.h>		/* realpath() */
#include <stdlib.h>		/* realpath() */
#include <string.h>		/* strcmp() */
#include <unistd.h>		/* unlink() */
#include <signal.h>
#include <gtk/gtk.h>
#include <sys/param.h>
#include "katoobwindow.h"
#include "recent.h"
#include "init.h"
#include "conf.h"
#include "misc.h"
#include "emulator.h"
#include "mdi.h"
#include "encodings.h"
#include "misc.h"
#include "prefs.h"
#include "dnd.h"
#include "crash.h"
#include "file.h"
#include "exec-cmd.h"

#ifdef ENABLE_PRINT
#include "print.h"
#endif /* ENABLE_PRINT */

#ifdef HAVE_SPELL
#include "spelldlg.h"		/* katoob_create_spelldlg() */
#endif /* HAVE_SPELL */

conf *config;			/* our configuration struct     */
UI *katoob;			/* Initialised in katoob_init() */

#ifdef ENABLE_PRINT
PConfig *PConf;
#endif /* ENABLE_PRINT */

#ifndef _WIN32
char *_name;
unsigned long _pid;

/* Our signal handling function */
void signal_handler (const int sig);
static void setup_signal_handler ();
#endif /* _WIN32 */

int
main (int argc, char *argv[])
{
  gchar c;
  gchar *tmp = NULL;
  FILE *tmp_file;
  gchar *file;
  gchar file2[PATH_MAX + 1];
  gchar *tmp_str;

#ifdef ENABLE_NLS
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (PACKAGE, "UTF-8");
  textdomain (PACKAGE);
#endif

  gtk_set_locale ();

  gtk_init (&argc, &argv);

/*
 * NOTE: We are ignoring sigpipe because when we run lpr via popen() to print
 * and no printer is configured or cups is not running lpr will exit and print
 * an error on the standard error, And we'll get a sigpipe when trying to write
 * to the pipe.
 */
/* I'm almost sure that the printing'll be broken under windows ;-) */
#ifndef _WIN32
  signal (SIGPIPE, SIG_IGN);
#endif /* _WIN32 */

#ifndef _WIN32
  _name = g_strdup (argv[0]);
  _pid = getpid ();

/* Maybe we can catch anything ;-) */
  setup_signal_handler ();
#endif /* _WIN32 */

/* Initialise some variables */
  katoob_init (&config, &katoob);

  katoob_load_all_config ();

  recent_init ();

/* Parse the command line arguments */
  parse_options (argc, argv);

#ifndef _WIN32
  if (config->disable_xft)
    {
      setenv ("GDK_USE_XFT", "0", 1);
    }

  if (config->xkb)
    {
      xkb_init (katoob);
    }
#endif /* _WIN32 */

  if (config->emulator)
    {
      katoob_emulator_init ();
    }

  if (!recent_load ())
    {
      katoob_debug ("Couldn't get recent documents file");
    }

  if (!exec_cmd_load ())
    {
      katoob_debug ("Couldn't load exec_cmd table!");
    }

  encodings_init ();
  tmp_str = g_strdup_printf
    ("Default encoding(%i): %s \"%s\"", config->defenc,
     katoob_encodings_get_by_number (config->defenc),
     katoob_encodings_get_name_by_number (config->defenc));
  katoob_debug (tmp_str);
  g_free (tmp_str);

#ifdef HAVE_SPELL
/* This should be here first cause we'll need config->dicts when getting the default_dict */
  config->dicts = katoob_spell_get_available_dicts ();

  if (!config->default_dict)
    {
      config->default_dict = g_strdup (katoob_spell_get_default_dict ());
    }

  tmp_str = g_strdup_printf ("The default dict. is: %s", config->default_dict);

  katoob_debug (tmp_str);
  g_free (tmp_str);

  create_mispelled_color ();
#endif /* HAVE_SPELL */

#ifdef ENABLE_HIGHLIGHT
  katoob->hl_manager = gtk_source_languages_manager_new ();
#endif /* ENABLE_HIGHLIGHT */

  katoob->win = katoob_window_new ();

  gtk_widget_show_all (katoob->win);

/* NOTE: katoob_update_main_interface() should be called after 
 * gtk_widget_show_all() to hide all the widgets that the user doesn't want to 
 * be visible! 
 */
  katoob_update_main_interface (KATOOB_WINDOW (katoob->win));

/* Setup the DnD stuff. */
  katoob_dnd_init ();

  if (config->emulator)
    {
      g_signal_connect (G_OBJECT (katoob->win), "key-press-event",
			G_CALLBACK (katoob_text_view_emulator_insert), NULL);
    }

  g_signal_connect_after (G_OBJECT (KATOOB_WINDOW (katoob->win)->notebook),
			  "switch-page",
			  G_CALLBACK (katoob_switch_page_handler), NULL);

  if (config->files_toopen)
    {
      GSList *list = config->files_toopen;
      while (list)
	{
	  file = (gchar *) list->data;
	  if (!file)
	    {
	      list = g_slist_next (list);
	      continue;
	    }
	  if (!strcmp (file, "-"))
	    {
	      katoob_debug ("Reading from stdin.");
	      tmp_file = katoob_create_temp_file (&tmp);
	      if (!tmp_file)
		{
		  /*		  g_free (tmp); */
		  katoob_error (_("Could not create temporary file"));
		  continue;
		}
	      katoob_debug (tmp);

	      printf ("%s: Reading from stdin...\n", PACKAGE);
	      fflush (stdout);

	      while ((c = getchar ()) != EOF)
		{
		  fputc (c, tmp_file);
		}
	      fclose (tmp_file);
	      katoob_create_stdin_doc (tmp);
	      unlink (tmp);
	      g_free (tmp);
	      /* fd is closed when we fclose() cause we are using fdopen() */
	      /*                      close (fd); */
	    }
	  else
	    {
#ifndef _WIN32
	      realpath (file, file2);
#else /* _WIN32 */
	      win32_realpath (file, file2);
#endif /* _WIN32 */
	      if (file2)
		{
		  katoob_create_doc_from_file (file2, TRUE, config->defenc);
		}
	    }
	  list = g_slist_next (list);
	}
      g_slist_foreach (config->files_toopen, (GFunc) g_free, NULL);
      g_slist_free (config->files_toopen);
    }
  else
    {
      katoob_create_doc ();
    }

#ifndef _WIN32
  if ((config->xkb) && (!katoob->xkb_grps))
    {
      xkb_error ();
    }
#endif /* _WIN32 */

/* We should grab the active document focus. */
  katoob_document_grab_focus (katoob_get_active_doc ());

  gtk_main ();
  katoob_debug ("main() Exiting.");
  return 0;
}

gboolean
katoob_exit ()
{
  KatoobDocument *doc = NULL;
  gint x;
  KATOOB_DEBUG_FUNCTION;

  while ((doc = katoob_get_active_doc ()) != NULL)
    {
      if (!katoob_close_doc (doc))
	{
	  return TRUE;
	}
    }

  katoob_save_all_config ();

  recent_save ();

  exec_cmd_save();

  gtk_main_quit ();

  katoob_free_all_config ();

  g_datalist_clear (&katoob->recent_files);

#ifndef _WIN32
  for (x = 0; x < XkbNumKbdGroups; x++)
    {
      if (katoob->xkb_grps[x])
	{
	  XFree (katoob->xkb_grps[x]);
	}
    }

  g_free (_name);

#endif /* _WIN32 */

  return FALSE;
}

#ifndef _WIN32
static void
setup_signal_handler ()
{
  KATOOB_DEBUG_FUNCTION;

/* Invalid memory reference */
  signal (SIGSEGV, signal_handler);

/* Interrupt from keyboard */
  signal (SIGINT, signal_handler);

/* Quit from keyboard */
  signal (SIGQUIT, signal_handler);

/* Floating point exception */
  signal (SIGFPE, signal_handler);

/* Bus error (bad memory access) */
  signal (SIGBUS, signal_handler);

/* Termination signal */
  signal (SIGTERM, signal_handler);

/* Illegal Instruction */
  signal (SIGILL, signal_handler);
}

void
signal_handler (const int sig)
{
  static volatile unsigned long _new = 0;

  if (_new)
    {
      return;
    }
  ++_new;

  KATOOB_DEBUG_FUNCTION;

  switch (sig)
    {
    case SIGSEGV:
    case SIGFPE:
    case SIGBUS:
    case SIGILL:
      {
	katoob_save_open ();
	katoob_init_debug (sig);
	break;
      }
    case SIGTERM:
      {
	katoob_save_open ();
	katoob_save_all_config ();
	exit (255);
	break;
      }
    case SIGQUIT:
    case SIGINT:
      {
	katoob_save_open ();
	exit (255);
	break;
      }
    }
}
#endif /* _WIN32 */
