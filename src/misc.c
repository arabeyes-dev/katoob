/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"

#include <gtk/gtk.h>
#ifndef _WIN32
#include <gdk/gdkx.h>		/* for GDK_DISPLAY() */
#endif /* _WIN32 */
#include <gdk-pixbuf/gdk-pixbuf.h>
#ifndef _WIN32
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#endif /* _WIN32 */
#include <stdio.h>
#include <stdlib.h>		/* atoi() */
/* WIN32: This should be reenabled */
#include <string.h>		/* strcmp() */
#include <ctype.h>		/* isdigit() */
#include <stdarg.h>
#include <unistd.h>
#include "misc.h"
#include "open.h"		/* real_open() */
#include "encodings.h"
#include "katoobdocument.h"
#include "katoobwindow.h"
#include "katoobstatusbar.h"
#include "katooblabel.h"
#include "mdi.h"

/* FIXME: THIS IS BAD :( They are not suppsed to be converted to header files and included, but i want to release and i'm lazy ;) */
#include "thanks.h"
#include "copying.h"

static void katoob_usage ();
static void katoob_help ();
static void katoob_version ();
static void about_destroy ();
static void _about_destroy ();

GtkWidget *about;

void
parse_options (int argc, char *argv[])
{
  extern conf *config;
  gint x = 1;

  KATOOB_DEBUG_FUNCTION;

  while (argv[x])
    {
      katoob_debug (argv[x]);
      if (!strcmp (argv[x], "--help") || !strcmp (argv[x], "-h"))
	{
	  katoob_help ();
	  exit (0);
	}
      else if (!strcmp (argv[x], "--usage") || !strcmp (argv[x], "-u"))
	{
	  katoob_usage ();
	  exit (0);
	}
      else if (!strcmp (argv[x], "--version") || !strcmp (argv[x], "-v"))
	{
	  katoob_version ();
	  exit (0);
	}
      else
	{
	  config->files_toopen =
	    g_slist_append (config->files_toopen, g_strdup (argv[x]));
	}
      ++x;
    }
}
#if 0
gboolean
katoob_about_load_file(GtkTextView *textview, gchar *file)
{
  GError *error = NULL;
  if (!access (file, F_OK))
    {
      /* Try to read it. */
      gchar *contents = NULL;
      if (g_file_get_contents (file, &contents, NULL, &error))
	{
	  gtk_text_buffer_set_text (textview->buffer, contents, -1);
	  g_free (contents);
	  return TRUE;
	}
      else {
	  g_error_free (error);
	  return FALSE;
      }
    }
  else {
    return FALSE;
  }
}

void
katoob_about_load_license_file (GtkTextView * textview)
{
  KATOOB_DEBUG_FUNCTION;
  /* <prefix/share/doc/katoob-ver/LICENSE  */
  gchar *tmp = g_strdup_printf ("%s%sshare%sdoc%s%s-%s%sLICENSE", PREFIX,
				G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S,
				G_DIR_SEPARATOR_S,
				PACKAGE, VERSION, G_DIR_SEPARATOR_S);
  katoob_debug (tmp);
  if (!katoob_about_load_file(textview, tmp))
    {
      /* <prefix/share/doc/katoob/copyright  */
      g_free(tmp);
      tmp =
	g_strdup_printf ("%s%sshare%sdoc%s%s%scopyright", PREFIX,
			 G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S,
			 G_DIR_SEPARATOR_S, PACKAGE, G_DIR_SEPARATOR_S);
      katoob_debug (tmp);
      if (!katoob_about_load_file(textview, tmp))
	{
	  /* Debian compressed. */
	  g_free (tmp);
	  tmp =
	    g_strdup_printf ("%s%sshare%sdoc%s%s%scopyright.gz", PREFIX,
			     G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S,
			     G_DIR_SEPARATOR_S, PACKAGE, G_DIR_SEPARATOR_S);
	  katoob_debug (tmp);
	}
      /*      if (!katoob_about_load_file(textview, tmp))
	{
      */	  /* /usr/share/common-licenses/GPL-2 */
      /*  g_free (tmp);
	  tmp =
	    g_strdup ("/usr/share/common-licenses/GPL-2");
	  katoob_debug (tmp);

	}*/
    }
  g_free (tmp);
  /* Can't find the file. */

}

void
katoob_about_load_thanks_file (GtkTextView * textview)
{
  KATOOB_DEBUG_FUNCTION;
  /* <prefix/share/doc/katoob-ver/THANKS  */
  gchar *tmp = g_strdup_printf ("%s%sshare%sdoc%s%s-%s%sTHANKS", PREFIX,
				G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S,
				G_DIR_SEPARATOR_S,
				PACKAGE, VERSION, G_DIR_SEPARATOR_S);
  katoob_debug (tmp);
  if (!katoob_about_load_file(textview, tmp))
    {
      /* <prefix/share/doc/katoob/THANKS  */
      g_free(tmp);
      tmp =
	g_strdup_printf ("%s%sshare%sdoc%s%s%sTHANKS", PREFIX,
			 G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S,
			 G_DIR_SEPARATOR_S, PACKAGE, G_DIR_SEPARATOR_S);
      katoob_debug (tmp);
      if (!katoob_about_load_file(textview, tmp))
	{
	  /* Debian compressed. */
	  g_free (tmp);
	  tmp =
	    g_strdup_printf ("%s%sshare%sdoc%s%s%sTHANKS.gz", PREFIX,
			     G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S,
			     G_DIR_SEPARATOR_S, PACKAGE, G_DIR_SEPARATOR_S);
	  katoob_debug (tmp);

	}
    }
  g_free (tmp);
  /* Can't find the file. */
}
#endif

void
katoob_about ()
{
  extern UI *katoob;
  GtkWidget *dialog_vbox;
  /*  GtkWidget *vbox; */
  GtkStyle *Style;
  GtkWidget *developers;
  GtkWidget *viewport;
  GtkWidget *copyright;
  GtkWidget *url;
  GtkWidget *dialog_action_area;
  GtkWidget *close;
  GtkAccelGroup *accel_group;
  GtkWidget *notebook, *main_container, *license_container,
    *credits_container;
  GtkWidget *main_label, *license_label, *credits_label;
  GtkWidget *scrolledwin, *textview;

  gchar *tmp;

  KATOOB_DEBUG_FUNCTION;

  if (about)
    {
      return;
    }

  accel_group = gtk_accel_group_new ();
  about = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (about), _("About Katoob"));
  gtk_window_set_resizable (GTK_WINDOW (about), TRUE);
  gtk_window_set_position (GTK_WINDOW (about), GTK_WIN_POS_CENTER);

  dialog_vbox = GTK_DIALOG (about)->vbox;
  gtk_container_set_border_width (GTK_CONTAINER (dialog_vbox), 5);

  /*  vbox = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (dialog_vbox), vbox, TRUE, TRUE, 0);
  */
  gtk_widget_realize (about);
  Style = gtk_widget_get_style (about);
  tmp =
    g_strdup_printf ("%s%skatoob.png", PACKAGE_DATA_DIR, G_DIR_SEPARATOR_S);
  gtk_box_pack_start (GTK_BOX (dialog_vbox), gtk_image_new_from_file (tmp), TRUE,
		      TRUE, 0);
  g_free (tmp);
  notebook = gtk_notebook_new ();
  gtk_box_pack_start (GTK_BOX (dialog_vbox), notebook, TRUE, TRUE, 0);

  /* Main page. */

  main_container = gtk_vbox_new (FALSE, 0);
  main_label = gtk_label_new (_("Main"));

  developers = gtk_frame_new (_("Developers"));
  gtk_box_pack_start (GTK_BOX (main_container), developers, FALSE, FALSE, 0);

  viewport = gtk_label_new ("Mohammed Sameer\n");
  gtk_container_add (GTK_CONTAINER (developers), viewport);

  tmp =
    g_strdup_printf
    ("\nKatoob %s Copyright (c) 2002-2004 Arabeyes, Mohammed Sameer\n",
     VERSION);
  copyright = gtk_label_new (tmp);
  g_free (tmp);
  gtk_box_pack_start (GTK_BOX (main_container), copyright, FALSE, FALSE, 0);

  url = gtk_label_new ("http://www.arabeyes.org/project.php?proj=Katoob\n");
  gtk_box_pack_start (GTK_BOX (main_container), url, FALSE, FALSE, 0);

  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), main_container,
			    main_label);

  /* Credits */
  credits_container = gtk_vbox_new (FALSE, 0);
  credits_label = gtk_label_new (_("Credits"));

  scrolledwin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				  (scrolledwin),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW
				       (scrolledwin), GTK_SHADOW_IN);
  textview = gtk_text_view_new ();
  gtk_text_view_set_left_margin (GTK_TEXT_VIEW (textview), 10);
  gtk_text_view_set_right_margin (GTK_TEXT_VIEW (textview), 10);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (textview), FALSE);
  gtk_container_add (GTK_CONTAINER (scrolledwin), textview);
  gtk_box_pack_start (GTK_BOX (credits_container), scrolledwin, TRUE, TRUE,
		      0);
  gtk_text_buffer_set_text (GTK_TEXT_VIEW(textview)->buffer, THANKS, -1);
  /*  katoob_about_load_thanks_file (GTK_TEXT_VIEW (textview)); */
  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), credits_container,
			    credits_label);

  /* License */
  license_container = gtk_vbox_new (FALSE, 0);
  license_label = gtk_label_new (_("License"));

  scrolledwin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				  (scrolledwin),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW
				       (scrolledwin), GTK_SHADOW_IN);
  textview = gtk_text_view_new ();
  gtk_text_view_set_left_margin (GTK_TEXT_VIEW (textview), 10);
  gtk_text_view_set_right_margin (GTK_TEXT_VIEW (textview), 10);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (textview), FALSE);
  gtk_container_add (GTK_CONTAINER (scrolledwin), textview);
  gtk_box_pack_start (GTK_BOX (license_container), scrolledwin, TRUE, TRUE,
		      0);
  gtk_text_buffer_set_text (GTK_TEXT_VIEW(textview)->buffer, COPYING, -1);
  /*  katoob_about_load_license_file (GTK_TEXT_VIEW (textview)); */
  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), license_container,
			    license_label);

  /* the rest of the dialog... */
  dialog_action_area = GTK_DIALOG (about)->action_area;
  gtk_container_set_border_width (GTK_CONTAINER (dialog_action_area), 10);

  close = gtk_button_new_with_mnemonic (_("_Close"));

  gtk_box_pack_start (GTK_BOX (dialog_action_area), close, FALSE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS (close, GTK_CAN_DEFAULT);

  gtk_widget_grab_default (close);
  gtk_window_add_accel_group (GTK_WINDOW (about), accel_group);

/*  g_signal_connect(G_OBJECT(about), "response", G_CALLBACK(_about_destroy),
		   NULL); */
  g_signal_connect (G_OBJECT (about), "destroy", G_CALLBACK (about_destroy),
		    NULL);
  g_signal_connect (G_OBJECT (close), "clicked", G_CALLBACK (_about_destroy),
		    NULL);

  gtk_widget_show_all (about);
  gtk_window_set_transient_for (GTK_WINDOW (about), GTK_WINDOW (katoob->win));
}

static void
about_destroy ()
{
  gtk_widget_destroyed (about, &about);
}

static void
_about_destroy ()
{
  g_signal_emit_by_name (G_OBJECT (about), "destroy");
}

void
katoob_copy ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return;
    }
  katoob_document_copy (doc);
}

void
katoob_cut ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return;
    }
  katoob_document_cut (doc);
}

void
katoob_delete ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return;
    }
  katoob_document_delete (doc);
}

void
katoob_select_all ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return;
    }
  katoob_document_select_all (doc);
}

void
katoob_paste ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return;
    }
  katoob_document_paste (doc);
}

gint
create_messagedialog (gchar * message)
{
  gint result;
  GtkWidget *messagedialog;
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  messagedialog = gtk_message_dialog_new (NULL,
					  GTK_DIALOG_DESTROY_WITH_PARENT,
					  GTK_MESSAGE_QUESTION,
					  GTK_BUTTONS_NONE, message);
  gtk_dialog_add_buttons (GTK_DIALOG (messagedialog),
			  GTK_STOCK_YES, GTK_RESPONSE_YES,
			  GTK_STOCK_NO, GTK_RESPONSE_NO,
			  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);
  gtk_window_set_position (GTK_WINDOW (messagedialog), GTK_WIN_POS_CENTER);
  gtk_window_set_transient_for (GTK_WINDOW (messagedialog),
				GTK_WINDOW (katoob->win));
  result = gtk_dialog_run (GTK_DIALOG (messagedialog));
  gtk_widget_destroy (messagedialog);
  return result;
}

/* Format strings implementation grabbed from wnews
     http://www.kerneled.com/projects/wnews/
*/
void
katoob_error (gchar * err, ...)
{
  GtkWidget *dialog;
  extern UI *katoob;
  gchar *message, *s;
  va_list ap;
  int i, n = 0, len = 0;

  KATOOB_DEBUG_FUNCTION;

  if (!err)
    {
      return;
    }

  s = strstr (err, "%s");

  while (s)
    {
      s += 2;
      ++n;
      s = strstr (s, "%s");
    }

  va_start (ap, err);

  for (i = 0; i < n; i++)
    {
      s = va_arg (ap, char *);

      if (!s)
	{
	  return;
	}

      len += strlen (s);
    }

  va_end (ap);
  len += strlen (err) - 2 * n + 1;

  va_start (ap, err);

  message = (gchar *) g_malloc (len);
  vsprintf (message, err, ap);

  dialog =
    gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
			    GTK_BUTTONS_CLOSE, message);

  g_signal_connect (G_OBJECT (dialog),
		    "response", G_CALLBACK (gtk_widget_destroy), NULL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("ERROR"));
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
  gtk_widget_show (dialog);
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (katoob->win));
  g_free (message);
}

void
katoob_info (gchar * info)
{
  GtkWidget *dialog;
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  dialog =
    gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_INFO,
			    GTK_BUTTONS_CLOSE, info);

  g_signal_connect (G_OBJECT (dialog),
		    "response", G_CALLBACK (gtk_widget_destroy), NULL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Information"));
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
  gtk_widget_show (dialog);
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (katoob->win));
}

#ifndef _WIN32
void
katoob_switch_xkb (gint n)
{
  extern UI *katoob;

  KATOOB_DEBUG_FUNCTION;

  if ((katoob->xkb_grp == n) && (!katoob->emulator))
    {
      katoob_debug ("No need for us, Returning.");
      return;
    }
  katoob->emulator = FALSE;
  XkbLockGroup (GDK_DISPLAY (), XkbUseCoreKbd, n);
  katoob->xkb_grp = n;
  gtk_button_set_label (GTK_BUTTON (katoob->xkb), katoob->xkb_grps[n]);
}

GdkFilterReturn
event_filter (GdkXEvent * xevent, GdkEvent * event, void *sia)
{
  extern UI *katoob;
  XEvent *ev;
  XkbEvent *e;

  /* gdkevents.h:
   * NOTE:
   * GdkXEvent  Can be cast to window system specific
   * even type, XEvent on X11, MSG on Win32.
   */
  ev = (XEvent *) xevent;
  if (ev->type == katoob->xkbEventType)
    {
      e = (XkbEvent *) xevent;
      if (e->any.xkb_type == XkbStateNotify)
	{
	  XkbStateRec state;
	  if (Success == XkbGetState (GDK_DISPLAY (), XkbUseCoreKbd, &state))
	    {
	      if (katoob->xkb_grp != state.locked_group)
		{
		  katoob->xkb_grp = state.locked_group;
		  if (!katoob->emulator)
		    {
		      gtk_button_set_label (GTK_BUTTON (katoob->xkb),
					    katoob->xkb_grps[katoob->
							     xkb_grp]);
		    }
		}
	    }
	  return GDK_FILTER_REMOVE;
	}
    }
  return GDK_FILTER_CONTINUE;
}
#endif /* _WIN32 */

void
katoob_toggle_document_wrap (GtkCheckMenuItem * checkmenuitem)
{
  KATOOB_DEBUG_FUNCTION;

  if (checkmenuitem->active)
    {
      katoob_document_enable_text_wrap (katoob_get_active_doc ());
    }
  else
    {
      katoob_document_disable_text_wrap (katoob_get_active_doc ());
    }
}

static void
katoob_version ()
{
  KATOOB_DEBUG_FUNCTION;

  g_print ("%s %s\n", PACKAGE, VERSION);
}

static void
katoob_usage ()
{
  KATOOB_DEBUG_FUNCTION;

  g_print ("usage: %s", PACKAGE);
  g_print (" [--help] [--version] [--usage] [file1 file2 file3... ]\n");
}

static void
katoob_help ()
{
  KATOOB_DEBUG_FUNCTION;

  g_print ("usage: %s  [OPTIONS] [FILES_TO_OPEN]\n", PACKAGE);
  g_print ("  -h, --help       Show this help message\n");
  g_print ("  -v, --version    Display version information\n");
  g_print ("  -u, --usage      Display brief usage message\n");
}

#ifdef DEBUG
void
_katoob_debug (gchar * str)
{
  fprintf (stdout, "%s\n", str);
  fflush (stdout);
}

void
_katoob_udebug (guchar * str)
{
  fprintf (stdout, "%s\n", str);
  fflush (stdout);
}
#endif /* DEBUG */

void
katoob_toggle_statusbar (GtkCheckMenuItem * checkmenuitem)
{
  extern UI *katoob;
  extern conf *config;
  KATOOB_DEBUG_FUNCTION;

  if (checkmenuitem->active)
    {
      katoob_statusbar_show (KATOOB_STATUSBAR
			     (KATOOB_WINDOW (katoob->win)->statusbar));
    }
  else
    {
      katoob_statusbar_hide (KATOOB_STATUSBAR
			     (KATOOB_WINDOW (katoob->win)->statusbar));
    }
  config->statusbar = checkmenuitem->active;
}

#ifndef _WIN32
void
xkb_error ()
{
  extern conf *config;
  KATOOB_DEBUG_FUNCTION;

  if (config->xkb_err_dlg)
    {
      GtkWidget *dialog;
      GtkWidget *dialog_vbox;
      GtkWidget *vbox;
      GtkWidget *hbox;
      GtkWidget *image;
      GtkWidget *label;
      GtkWidget *checkbutton;
      GtkWidget *dialog_action_area;
      GtkWidget *okbutton;

      dialog = gtk_dialog_new ();
      gtk_window_set_title (GTK_WINDOW (dialog),
			    _("Error initializing Arabic keyboard"));
      gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
      dialog_vbox = GTK_DIALOG (dialog)->vbox;
      vbox = gtk_vbox_new (FALSE, 4);
      gtk_box_pack_start (GTK_BOX (dialog_vbox), vbox, TRUE, TRUE, 0);
      hbox = gtk_hbox_new (FALSE, 5);
      gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);
      image =
	gtk_image_new_from_stock ("gtk-dialog-info", GTK_ICON_SIZE_DIALOG);
      gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
      label =
	gtk_label_new (_
		       ("An error has occured while trying to initialize your Arabic keyboard, Katoob'll try to use the built in Arabic keyboard emulator."));
      gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
      gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
      checkbutton =
	gtk_check_button_new_with_mnemonic (_
					    ("_Show this warning every time this problem occures"));
      gtk_box_pack_start (GTK_BOX (vbox), checkbutton, FALSE, FALSE, 0);
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbutton), TRUE);
      dialog_action_area = GTK_DIALOG (dialog)->action_area;
      gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area),
				 GTK_BUTTONBOX_END);
      okbutton = gtk_button_new_from_stock ("gtk-ok");
      gtk_dialog_add_action_widget (GTK_DIALOG (dialog), okbutton,
				    GTK_RESPONSE_OK);
      GTK_WIDGET_SET_FLAGS (okbutton, GTK_CAN_DEFAULT);
      gtk_widget_show (dialog_vbox);
      gtk_widget_show (vbox);
      gtk_widget_show (hbox);
      gtk_widget_show (image);
      gtk_widget_show (label);
      gtk_widget_show (checkbutton);
      gtk_widget_show (dialog_action_area);
      gtk_widget_show (okbutton);
      gtk_dialog_run (GTK_DIALOG (dialog));
      config->xkb_err_dlg =
	gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (checkbutton));
      gtk_widget_destroy (dialog);
    }
}
#endif /* _WIN32 */

gint
katoob_create_question (gchar * q)
{
  GtkWidget *dialog;
  gint result;
  extern UI *katoob;

  KATOOB_DEBUG_FUNCTION;

  dialog =
    gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION,
			    GTK_BUTTONS_YES_NO, q);
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (katoob->win));
  result = gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (GTK_WIDGET (dialog));
  return result;
}

void
katoob_update_active_interface ()
{
/* TODO: Implement me. */
}

void
katoob_goto_line ()
{
  extern UI *katoob;
  KatoobDocument *doc;
  GtkWidget *dialog;
  GtkWidget *dialog_vbox;
  GtkWidget *hbox;
  GtkWidget *line;
  GtkWidget *to;
  GtkWidget *dialog_action_area;
  GtkWidget *cancelbutton;
  GtkWidget *ok;
  GtkWidget *alignment;
  GtkWidget *hbox1;
  GtkWidget *image;
  GtkWidget *oklabel;
  gint res, i;
  gchar *l;
  KATOOB_DEBUG_FUNCTION;

  doc = katoob_get_active_doc ();
  if (!doc)
    {
      return;
    }
  dialog = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (dialog), _("Goto Line"));

  dialog_vbox = GTK_DIALOG (dialog)->vbox;
  gtk_widget_show (dialog_vbox);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox);
  gtk_box_pack_start (GTK_BOX (dialog_vbox), hbox, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);

  line = gtk_label_new (_("Line:"));
  gtk_widget_show (line);
  gtk_box_pack_start (GTK_BOX (hbox), line, TRUE, TRUE, 0);
  gtk_label_set_justify (GTK_LABEL (line), GTK_JUSTIFY_LEFT);

  to = gtk_entry_new ();
  gtk_widget_show (to);
  gtk_box_pack_start (GTK_BOX (hbox), to, FALSE, FALSE, 0);
  gtk_entry_set_activates_default (GTK_ENTRY (to), TRUE);

  dialog_action_area = GTK_DIALOG (dialog)->action_area;
  gtk_widget_show (dialog_action_area);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area),
			     GTK_BUTTONBOX_END);
  cancelbutton = gtk_button_new_from_stock ("gtk-cancel");
  gtk_widget_show (cancelbutton);
  gtk_dialog_add_action_widget (GTK_DIALOG (dialog), cancelbutton,
				GTK_RESPONSE_CANCEL);
  GTK_WIDGET_SET_FLAGS (cancelbutton, GTK_CAN_DEFAULT);

  ok = gtk_button_new ();
  gtk_widget_show (ok);
  gtk_dialog_add_action_widget (GTK_DIALOG (dialog), ok, GTK_RESPONSE_OK);
  GTK_WIDGET_SET_FLAGS (ok, GTK_CAN_DEFAULT);

  alignment = gtk_alignment_new (0.5, 0.5, 0, 0);
  gtk_widget_show (alignment);
  gtk_container_add (GTK_CONTAINER (ok), alignment);

  hbox1 = gtk_hbox_new (FALSE, 2);
  gtk_widget_show (hbox1);
  gtk_container_add (GTK_CONTAINER (alignment), hbox1);
  image = gtk_image_new_from_stock ("gtk-jump-to", GTK_ICON_SIZE_BUTTON);
  gtk_widget_show (image);
  gtk_box_pack_start (GTK_BOX (hbox1), image, FALSE, FALSE, 0);

  oklabel = gtk_label_new_with_mnemonic (_("_Goto"));
  gtk_widget_show (oklabel);
  gtk_box_pack_start (GTK_BOX (hbox1), oklabel, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (oklabel), GTK_JUSTIFY_LEFT);

  gtk_widget_grab_default (ok);
  g_signal_connect (G_OBJECT (to), "insert_text",
		    G_CALLBACK (filter_goto), NULL);
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (katoob->win));
  res = gtk_dialog_run (GTK_DIALOG (dialog));
  l = g_strdup (gtk_entry_get_text (GTK_ENTRY (to)));
  gtk_widget_destroy (dialog);

  switch (res)
    {
    case GTK_RESPONSE_OK:
      {
	if ((l == NULL) || (l[0] == 0))
	  {
	    return;
	  }
	i = atoi (l);
	i--;
	katoob_document_goto_line (doc, i);
	break;
      }
    default:
      {
	break;
      }
    }
  g_free (l);
}

gboolean
filter_goto (GtkEntry * entry,
	     const gchar * text, gint length, gint * position, gpointer data)
{
  GtkEditable *editable = GTK_EDITABLE (entry);
  int i, count = 0;
  gchar *result = NULL;
  static gboolean immediate_return;

  if (immediate_return)
    {
      immediate_return = FALSE;
      katoob_debug ("immediate return!");
      return TRUE;
    }

  result = (gchar *) g_malloc (length + 1);

  for (i = 0; i < length; i++)
    {
      if (!isdigit (text[i]))
	continue;
      result[count++] = text[i];
    }

  result[count] = '\0';

  g_signal_stop_emission_by_name (G_OBJECT (entry), "insert_text");

  if (count > 0)
    {
      KATOOB_DEBUG_FUNCTION;
      immediate_return = TRUE;
      gtk_editable_insert_text (editable, result, count, position);
      immediate_return = FALSE;
    }
  g_free (result);
  return TRUE;
}

void
extended_toolbar_goto_line (GtkEntry * entry, gpointer user_data)
{
  KatoobDocument *doc;
  gint line;
  gchar *txt;

  KATOOB_DEBUG_FUNCTION;

  doc = katoob_get_active_doc ();
  if (!doc)
    {
      gtk_entry_set_text (entry, "");
      return;
    }
  txt = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
  if ((!txt) || (txt[0] == 0))
    {
      if (txt)
	{
	  g_free (txt);
	}
      gtk_entry_set_text (entry, "");
      return;
    }
  line = atoi (txt);
  g_free (txt);
  --line;
  katoob_document_goto_line (doc, line);
  gtk_entry_set_text (entry, "");
  return;
}

gchar *
katoob_text_to_utf8 (gchar * text, KatoobDocument * doc)
{
  gchar *utf_buf = NULL;

  KATOOB_DEBUG_FUNCTION;

  if (!g_utf8_validate (text, -1, NULL))
    {
      utf_buf =
	katoob_encoding_convert (katoob_encodings_get_by_number
				 (katoob_document_get_encoding (doc)),
				 "UTF-8", text, strlen (text));
      if (!utf_buf)
	{
	  return NULL;
	}
      return utf_buf;
    }
  else
    {
      return g_strdup (text);
    }
}

void
katoob_show_close_buttons (KatoobDocument * doc)
{
  GtkWidget *label = katoob_document_get_label (doc);
  KATOOB_DEBUG_FUNCTION;

  katoob_label_show_close_button (KATOOB_LABEL (label), doc);
}

void
katoob_hide_close_buttons (KatoobDocument * doc)
{
  GtkWidget *label = katoob_document_get_label (doc);
  KATOOB_DEBUG_FUNCTION;

  katoob_label_hide_close_button (KATOOB_LABEL (label), doc);
}

void
katoob_set_custom_font (KatoobDocument * doc)
{
  extern conf *config;
  PangoFontDescription *fd =
    pango_font_description_from_string (config->font);

  KATOOB_DEBUG_FUNCTION;

  katoob_document_set_font (doc, fd);
  pango_font_description_free (fd);
}

void
katoob_set_default_font (KatoobDocument * doc)
{
  KATOOB_DEBUG_FUNCTION;

  katoob_document_set_font (doc, NULL);
}

void
katoob_show_line_numbers (KatoobDocument * doc)
{
  KATOOB_DEBUG_FUNCTION;

  if (!katoob_document_get_linenumbers (doc))
    {
      katoob_document_enable_line_numbers (doc);
    }
}

void
katoob_hide_line_numbers (KatoobDocument * doc)
{
  KATOOB_DEBUG_FUNCTION;

  if (katoob_document_get_linenumbers (doc))
    {
      katoob_document_disable_line_numbers (doc);
    }
}

void
katoob_toggle_document_line_numbers (GtkCheckMenuItem * checkmenuitem)
{
  KATOOB_DEBUG_FUNCTION;

  if (checkmenuitem->active)
    {
      katoob_document_enable_line_numbers (katoob_get_active_doc ());
    }
  else
    {
      katoob_document_disable_line_numbers (katoob_get_active_doc ());
    }
}

void
katoob_switch_page_handler (GtkNotebook * notebook)
{
  extern UI *katoob;
  extern conf *config;
  gchar *file = NULL;

#ifdef HAVE_SPELL
  gchar *tmp = NULL;
#endif /* HAVE_SPELL */

  gint *col = (gint *) g_malloc (sizeof (gint));
  gint *lin = (gint *) g_malloc (sizeof (gint));

  KatoobDocument *doc = katoob_get_active_doc ();

  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      katoob_debug ("No documents!");
      katoob_window_set_title (katoob->win, NULL);
      katoob_statusbar_set_modified (KATOOB_STATUSBAR
				     (KATOOB_WINDOW (katoob->win)->statusbar),
				     FALSE);
      katoob_statusbar_set_overwrite (KATOOB_STATUSBAR
				      (KATOOB_WINDOW (katoob->win)->
				       statusbar), FALSE);
      katoob_statusbar_set_cursor_position (KATOOB_STATUSBAR
					    (KATOOB_WINDOW (katoob->win)->
					     statusbar), 0, 0);
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (katoob->wrap_text),
				      config->textwrap);
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->line_numbers),
				      config->linenumbers);
      katoob_statusbar_set_encoding (KATOOB_STATUSBAR
				     (KATOOB_WINDOW (katoob->win)->statusbar),
				     config->defenc);
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->encodings[config->defenc]),
				      TRUE);
#ifdef HAVE_SPELL
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->spell_check),
				      config->spell_check);
      gtk_option_menu_set_history (GTK_OPTION_MENU (katoob->dicts_menu),
				   g_slist_position (config->dicts,
						     g_slist_find_custom
						     (config->dicts,
						      config->default_dict,
						      (GCompareFunc)
						      strcmp)));
#endif /* HAVE_SPELL */

#ifdef ENABLE_HIGHLIGHT
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				      (katoob->hl_none), TRUE);
#endif /* ENABLE_HIGHLIGHT */
      return;
    }

  katoob_debug ("Found documents!");
  katoob_statusbar_set_modified (KATOOB_STATUSBAR
				 (KATOOB_WINDOW (katoob->win)->statusbar),
				 katoob_document_get_modified (doc));
  katoob_statusbar_set_overwrite (KATOOB_STATUSBAR
				  (KATOOB_WINDOW (katoob->win)->statusbar),
				  katoob_document_get_overwrite (doc));
  katoob_document_get_cursor_position (doc, col, lin);
  katoob_statusbar_set_cursor_position (KATOOB_STATUSBAR
					(KATOOB_WINDOW (katoob->win)->
					 statusbar), *col + 1, *lin + 1);
  g_free (col);
  g_free (lin);

  file = katoob_document_get_file (doc);

  if (file)
    {
      katoob_window_set_title (katoob->win, file);
    }
  else
    {
      katoob_window_set_title (katoob->win,
			       (gchar *)
			       katoob_document_get_label_text (doc));
    }

  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (katoob->wrap_text),
				  katoob_document_get_textwrap (doc));
  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (katoob->line_numbers),
				  katoob_document_get_linenumbers (doc));
  katoob_statusbar_set_encoding (KATOOB_STATUSBAR
				 (KATOOB_WINDOW (katoob->win)->statusbar),
				 katoob_document_get_encoding (doc));
  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				  (katoob->
				   encodings[katoob_document_get_encoding
					     (doc)]), TRUE);
#ifdef HAVE_SPELL
  tmp = g_strdup_printf
    ("Document state: %i", katoob_document_get_spell_checker (doc));
  katoob_debug (tmp);
  g_free (tmp);

  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (katoob->spell_check),
				  katoob_document_get_spell_checker (doc));
  gtk_option_menu_set_history (GTK_OPTION_MENU (katoob->dicts_menu),
			       katoob_document_get_dictionary (doc));
#endif /* HAVE_SPELL */

#ifdef ENABLE_HIGHLIGHT
  switch (katoob_document_get_highlight_language (doc))
    {
    case KATOOB_HIGHLIGHT_NONE:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_none), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_ADA:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_ada), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_C:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_c), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_CPP:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_cpp), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_DESKTOP:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_desktop), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_DIFF:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_diff), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_HTML:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_html), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_IDL:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_idl), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_JAVA:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_java), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_LATEX:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_latex), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_PERL:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_perl), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_PO:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_po), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_PYTHON:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_python), TRUE);
	break;
      }
    case KATOOB_HIGHLIGHT_XML:
      {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
					(katoob->hl_xml), TRUE);
	break;
      }
    }
#endif /* ENABLE_HIGHLIGHT */
}

void
katoob_buffer_modified_handler (KatoobDocument * doc, gboolean modified)
{
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  katoob_statusbar_set_modified (KATOOB_STATUSBAR
				 (KATOOB_WINDOW (katoob->win)->
				  statusbar), modified);
}

void
katoob_encoding_changed_handler (KatoobDocument * doc, gint encoding)
{
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  katoob_statusbar_set_encoding (KATOOB_STATUSBAR
				 (KATOOB_WINDOW (katoob->win)->
				  statusbar), encoding);
}

void
katoob_file_changed_handler (KatoobDocument * doc, gchar * file)
{
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  katoob_window_set_title (GTK_WIDGET (katoob->win), file);
}

void
katoob_cursor_moved_handler (KatoobDocument * doc, gint col, gint row)
{
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  katoob_statusbar_set_cursor_position (KATOOB_STATUSBAR
					(KATOOB_WINDOW (katoob->win)->
					 statusbar), col, row);
}

gchar *
katoob_changed_text_encoding (gchar * text, gint enc)
{
  extern conf *config;
  gchar *buff = NULL;
  gchar *new_buff = NULL;
  KATOOB_DEBUG_FUNCTION;

  buff =
    katoob_encoding_convert ("UTF-8",
			     katoob_encodings_get_by_number (config->defenc),
			     text, strlen (text));
  if (!buff)
    {
      return NULL;
    }

  katoob_debug ("Done first conversion.");
  new_buff =
    katoob_encoding_convert (katoob_encodings_get_by_number (enc), "UTF-8",
			     buff, strlen (buff));
  g_free (buff);
  katoob_debug ("Done second conversion.");
  if (!new_buff)
    {
      return NULL;
    }
  return new_buff;
}

GtkWidget *
katoob_get_statusbar ()
{
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  return KATOOB_WINDOW (katoob->win)->statusbar;
}

gboolean
string_with_suffix (gchar * str, gchar * suffix, gint str_len,
		    gint suffix_len)
{
/* From g_str_with_suffix() THANKS. */
  KATOOB_DEBUG_FUNCTION;

  if (str_len < suffix_len)
    {
      return FALSE;
    }
  return (strcmp (str + str_len - suffix_len, suffix) == 0);
}

#ifdef HAVE_SPELL
void
katoob_toggle_spellcheck (GtkCheckMenuItem * checkmenuitem)
{
  KatoobDocument *doc;
  gchar *tmp;

  KATOOB_DEBUG_FUNCTION;
  doc = katoob_get_active_doc ();
  if (!doc)
    {
      return;
    }

  if (!katoob_document_get_speller (doc))
    {
      katoob_error (_
		    ("There is no spellchecking dictionary available, Please make sure that you have installed at least one."));
      return;
    }

  tmp = g_strdup_printf
    ("MenuItem state: %i\tdoc state: %i", checkmenuitem->active,
     katoob_document_get_spell_checker (katoob_get_active_doc ()));

  katoob_debug (tmp);
  g_free (tmp);

  if (checkmenuitem->active)
    {
      katoob_debug ("Enabling");
      katoob_document_enable_spell_checker (doc);
    }
  else
    {
      katoob_debug ("Disabling");
      katoob_document_disable_spell_checker (doc);
    }
}

void
katoob_toggle_spell_cb (KatoobDocument * doc, gboolean state)
{
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;
  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (katoob->spell_check),
				  state);
}

void
create_mispelled_color ()
{
  extern conf *config;
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  katoob->mispelled_color.red = config->mispelled_red;
  katoob->mispelled_color.blue = config->mispelled_blue;
  katoob->mispelled_color.green = config->mispelled_green;
}

#endif /* HAVE_SPELL */

void
katoob_toggle_overwrite_cb (gboolean overwrite)
{
  extern UI *katoob;
  KatoobStatusbar *sbar =
    KATOOB_STATUSBAR (KATOOB_WINDOW (katoob->win)->statusbar);
  katoob_statusbar_set_overwrite (sbar, overwrite);
}

#if 0
#ifdef DEBUG
unsigned long mem_usage = 0;

void *
_k_malloc (unsigned long len, gchar * file, gchar * func, int line)
{
  void *ret;
  ret = malloc (len);
  if (!ret)
    {
      g_warning ("Ran out of memory, Current usage: %ld", mem_usage);
      abort ();
    }
  mem_usage += len;
  fprintf (stderr,
	   "%s:%s():%d Malloc'ed \033[35m%d\033[m Bytes, Current usage: \033[31m%ld\033[m\n",
	   file, func, line, len, mem_usage);
}

void *
_k_strdup (gchar * str, gchar * file, gchar * func, int line)
{
  unsigned long size;
  void *ret;
  ret = malloc (size + 1);
  if (!ret)
    {
      g_warning ("Ran out of memory, Current usage: %ld", mem_usage);
      abort ();
    }
  mem_usage += size;
  fprintf (stderr,
	   "%s:%s():%d Strdup'ed\033[35m%d\033[m Bytes, Current usage: \033[31m%ld\033[m\n",
	   file, func, line, len, mem_usage);
  strcpy (ret, str);
  return ret;
}

void
_k_free (void *buf, gchar * file, gchar * func, int line)
{
  unsigned long size = strlen (buf);
  if (!buf)
    {
      katoob_debug (g_strdup_printf
		    ("%s:%s():%d Tried to free NULL", file, func, line));
      return;
    }
  free (buf);
  mem_usage -= size;
  fprintf (stderr,
	   "%s:%s():%d Free'ed \033[35m%d\033[m Bytes, Current usage: \033[31m%ld\033[m\n",
	   file, func, line, size, mem_usage);
}
#endif /* DEBUG */
#endif
