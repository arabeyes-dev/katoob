/* Katoob
 * Copyright (c) 2002-2004 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef __THANKS_H__
#define __THANKS_H__
gchar *THANKS =
"* Jens Seidel <jensseidel@users.sf.net>\n"
" - German PO file corrections (Debian bug #314022)\n"
"\n"
"* Islam Motab Amer know as phaeron <greatrambo at yahoo dot com>\n"
" - Helped me debugging the lam-alef handling code\n"
" - Helped me discovering the multiple lines paste bug.\n"
" - Helped me discovering the replace all endless loop bug.\n"
"\n"
"* Mostafa Hussein <mostafa at linux dash egypt dot org>\n"
"  - New Icons for katoob :-)\n"
"  - Switching of the line numbers to the right side into the RTL mode idea.\n"
"\n"
"* Thanks Leonardo Boshell <leonardop at gentoo dot org>\n"
"  - Pathches to fix compilation errors when disabling the spell checker and \n"
"    when freetype andfreetype2 are present.\n"
"\n"
"* Pav Lucistnik <pav at oook dot cz> & Samy Al Bahra <samy at kerneled dot com>\n"
"  - Fixes segmentation fault on start-up with several systems.\n"
"\n"
"* Samy Al Bahra <samy at kerneled dot com>\n"
"  - Several Code cleanups and optimisations, Fix many potential buffer \n"
"    overflows. Probably he deserves more than a thanks ;-)\n"
"\n"
"* Mohammed Elzubeir < elzubeir at arabeyes dot org>\n"
"  - Hosting, Supporting the project\n"
"\n"
"* Hicham AMAOUI <amaoui at altern dot org>\n"
"  - Redhat 9.0 segfault patch.\n"
"\n"
"* Youcef Rabah Rahal <yrrahal42 at hotmail dot com>\n"
"* Isam Bayazidi <bayazidi at arabeyes dot org>\n"
"  - Helping me with the Arabic translation.\n"
"\n"
"* Munzir Taha Obeid <munzirtaha at myrealbox dot com>\n"
"  - Helped me crushing bugs, iproving the UI, Fixing many issues. By his many \n"
"    suggestions and bug reports. \n"
"    He deserve more than a THANK YOU, But this is what i can offer :-(\n"
"\n"
"* Alaa Abd-El Fatah <alaa at annosoor dot org>\n"
"  - Help in debugging katoob 0.2\n"
"  - Multiple encodings support idea\n"
"  - Helping in fixing the segmentation fault while saving files with non valid \n"
"    ISO characters \"Actually he was the one who discovered it ;)\"\n"
"  - The complete Arabic emulator file\n"
"  - The idea of HTML numerical reference saving\n"
"  - His numerous ideas for improving the code, and efficiency.\n"
"  - Modifying the emulator code to get rid of the switch statement.\n"
" \n"
"* Arabeyes Developer mailing list <developer at arabeyes dot org>\n"
"  - Our main development mailing list, ideas and support\n"
"\n"
"* Anatoly Asviyan <arsen at alice dot nc dot huji dot ac dot il> <aanatoly at linuxmail dot org>\n"
"  - For writing \"gkrellxkb\", I borrowed the main idea and some code from it ;)\n"
"\n"
"* Jens Askengren <jensus at linux dot nu>\n"
" - GNOME human interface idea.\n"
" - a patch that changes the text direction buttons to use radio buttons.\n"
"\n"
"* Krzysztof Krzyzaniak <eloy at transilvania dot eu dot org>\n"
" - The Polish translation for version 0.3.0\n"
"\n"
"* Mohammed Yousif <mhdyousif at gmx dot net>\n"
" - Slack package for 0.3 \"slack 8.1\"\n"
"\n"
"* Roozbeh Pournader <roozbeh at sharif dot edu>\n"
" - Providing the IRAN SYSTEM table\n"
"\n"
"* Arash Zeini <a.zeini at farsikde dot org>\n"
" - Pointing me to \"Shabredo\" where i borrowed some code ;)\n"
"\n"
"* Han Boetes <han at linux-mandrake dot com>\n"
" - Helping me with the Mandrake spec file, And maintaining Mandrake package.\n"
"\n"
"* UTUMI Hirosi <utuhiro78 at yahoo dot co dot jp>\n"
" - Japanese translation and japanese encodings code, And a screenshot :-)\n"
"\n"
"* Diego Iastrubni <elcuco at kdemail dot net>\n"
" - Several patches that fixed some segfaults.\n"
" - The hebrew emulator file.\n"
" - Hebrew to Logical Hebrew patch.\n"
"\n"
"*  Michelle Konzack <linux4michelle at freenet dot de>\n"
" - The 2nd bug hunting feast, Many thanks. ;)\n"
" - German manpage.\n"
"\n"
"* fribidi Authors\n"
"\n"
"* gtkspell Author\n"
"\n"
"TRANSLATIONS\n"
"============\n"
"Arabic:		Mohammed Sameer <uniball at linux-egypt dot org>\n"
"		* All versions.\n"
"\n"
"Polish:		Krzysztof Krzyzaniak <eloy at transilvania dot eu dot org>\n"
"		0.2.1\n"
"\n"
"Swedish:	Jens Askengren <jensus at linux dot nu>\n"
"		0.3.1\n"
"\n"
"Hebrew:		GNU/Linux Kinneret project (http://www.linux-kinneret.org/).\n"
"\n"
"Japanese:	UTUMI Hirosi <utuhiro78 at yahoo dot co dot jp>\n"
"\n"
"German:		Michelle Konzack <linux4michelle at freenet dot de>\n"
"\n"
"French:		Emmanuel Beffara <manu at beffara dot org>\n"
"		0.3.8\n"
"Czech:		Jan Grmela <mail at grmela dot com>\n"
;
#endif /* __THANKS_H__ */
