/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_SPELL

#include "katoob.h"
#include <glib.h>
#include <string.h>		/* strncmp() */
#include "spell-private.h"
#include "spell.h"
#include "mdi.h"
#include "misc.h"

gboolean
spell_menu_item_activate (GtkMenuItem * menuitem, gpointer user_data);

#define SUFFIX "-aspell.pwli"

GSList *
katoob_spell_get_available_dicts ()
{
  GSList *available_languages = NULL;
#ifdef HAVE_NEW_SPELL
/* From aspell examples, Thanks! */
  AspellConfig *config = NULL;
  AspellDictInfoList *dlist = NULL;
  AspellDictInfoEnumeration *dels = NULL;
  const AspellDictInfo *entry = NULL;

  config = new_aspell_config ();
  dlist = get_aspell_dict_info_list (config);
  delete_aspell_config (config);
  dels = aspell_dict_info_list_elements (dlist);
  while ((entry = aspell_dict_info_enumeration_next (dels)) != 0)
    {
      available_languages =
	g_slist_append (available_languages, g_strdup (entry->name));
    }
  delete_aspell_dict_info_enumeration (dels);
  return available_languages;
#else /* HAVE_OLD_SPELL */
  extern conf *config;
  GDir *dir = NULL;
  GError *err = NULL;
  gchar *file;
  gchar *tmp = NULL;
  gchar *_tmp;
  gint suffix_len, file_len;

  if (!config->dicts_dir)
    {
      config->dicts_dir = g_strdup (DICTS_DIR);
    }
  dir = g_dir_open (config->dicts_dir, 0, &err);
  if (!dir)
    {
      g_warning (err->message);
      g_error_free (err);
    }
  else
    {
/* scan for files */
      suffix_len = strlen (SUFFIX);
      while ((file = (gchar *) g_dir_read_name (dir)) != NULL)
	{
	  file_len = strlen (file);

	  if (string_with_suffix (file, SUFFIX, file_len, suffix_len))
	    {
/* Now we want to remove some characters from the end */
	      tmp = g_strndup (file, (file_len - suffix_len));
	      available_languages = g_slist_append (available_languages, tmp);
	      _tmp = g_strdup_printf ("Adding dict. %s to the list.", tmp);
	      katoob_debug (_tmp);
	      g_free (_tmp);
	    }
	}
      g_dir_close (dir);
    }

  return available_languages;

#endif /* HAVE_OLD_SPELL */
}

GtkWidget *
katoob_spell_generate_option_menu ()
{
  extern conf *config;
  gint h = 0, x = 0;
  GtkWidget *menu;
  GtkWidget *_menu;
  GtkWidget *menu_item;
  GSList *tmp = config->dicts;

  KATOOB_DEBUG_FUNCTION;

  menu = gtk_option_menu_new ();
  _menu = gtk_menu_new ();

while (tmp)
{
      if (!strcmp (config->default_dict, (gchar *)tmp->data))
        {
          h = x;
        }

      menu_item = gtk_menu_item_new_with_label ((gchar *)tmp->data);
      gtk_container_add (GTK_CONTAINER (_menu), menu_item);
      g_signal_connect (G_OBJECT (menu_item), "activate",
                        G_CALLBACK (spell_menu_item_activate), NULL);
++x;
tmp = g_slist_next(tmp);
}

  gtk_option_menu_set_menu (GTK_OPTION_MENU (menu), _menu);
  gtk_option_menu_set_history (GTK_OPTION_MENU (menu), h);
  return menu;
}

gchar *
katoob_spell_get_default_dict ()
{
  extern conf *config;
  GSList *tmp =
    g_slist_find_custom (config->dicts, "en", (GCompareFunc) strcmp);
  KATOOB_DEBUG_FUNCTION;
  if (tmp)
    {
      return (gchar *)tmp->data;
    }
  else
    {
      return (gchar *)g_slist_nth_data (config->dicts, 0);
    }

/*  gchar *lang = (gchar *)g_getenv ("LANG");

  if (lang)
    {
      if (!strncasecmp (lang, "C", 1))
	{
	  lang = NULL;
	}
    }
return (lang) ? lang : "en";
*/
}

void
spell_replace_word (GtkWidget * menuitem, gpointer _doc)
{
  char *oldword;
  const char *newword;
  GtkTextIter start, end;
  gchar *_tmp;

  KatoobDocument *doc = (KatoobDocument *) _doc;
  GtkTextBuffer *buffer = katoob_document_get_buffer (doc);
  AspellSpeller *speller = katoob_document_get_speller (doc);

  gtk_text_buffer_get_iter_at_mark (buffer, &start,
				    gtk_text_buffer_get_insert (buffer));
  if (!gtk_text_iter_starts_word (&start))
    {
      gtk_text_iter_backward_word_start (&start);
    }
  end = start;
  if (gtk_text_iter_inside_word (&end))
    {
      gtk_text_iter_forward_word_end (&end);
    }
  oldword = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
  newword = gtk_label_get_text (GTK_LABEL (GTK_BIN (menuitem)->child));

  _tmp = g_strdup_printf
    ("%s\t%s\t%i\t%i", newword, oldword,
     gtk_text_iter_get_offset (&start), gtk_text_iter_get_offset (&end));
  katoob_debug (_tmp);
  g_free (_tmp);

  gtk_text_buffer_delete (buffer, &start, &end);

  _tmp = g_strdup_printf ("%i", gtk_text_iter_get_offset (&start));
  katoob_debug (_tmp);
  g_free (_tmp);

  gtk_text_buffer_insert (buffer, &start, newword, -1);

  _tmp = g_strdup_printf ("%i", gtk_text_iter_get_offset (&start));

  katoob_debug (_tmp);
  g_free (_tmp);

  aspell_speller_store_replacement (speller,
				    oldword, strlen (oldword),
				    newword, strlen (newword));

  g_free (oldword);
/* TODO: undo/redo */
}

void
spell_add_to_dictionary (GtkWidget * menuitem, gpointer _doc)
{
  char *word;
  GtkTextIter start, end;
  KatoobDocument *doc = (KatoobDocument *) _doc;
  GtkTextBuffer *buffer = katoob_document_get_buffer (doc);
  AspellSpeller *speller = katoob_document_get_speller (doc);

  gtk_text_buffer_get_iter_at_mark (buffer, &start,
				    gtk_text_buffer_get_insert (buffer));
  if (!gtk_text_iter_starts_word (&start))
    {
      gtk_text_iter_backward_word_start (&start);
    }
  end = start;
  if (gtk_text_iter_inside_word (&end))
    {
      gtk_text_iter_forward_word_end (&end);
    }

  word = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

  aspell_speller_add_to_personal (speller, word, strlen (word));
  gtk_text_buffer_remove_tag (buffer, katoob_document_get_highlight_tag (doc),
			      &start, &end);
  g_free (word);
}

gboolean
spell_menu_item_activate (GtkMenuItem * menuitem, gpointer user_data)
{
  KatoobDocument *doc = katoob_get_active_doc ();

  KATOOB_DEBUG_FUNCTION;

  if (doc)
    {
      if (katoob_document_set_dictionary
	  (doc,
	   (gchar *)
	   gtk_label_get_text (GTK_LABEL (GTK_BIN (menuitem)->child))))
	{
/* Enable the spell checker if it's disabled */
	  if (!katoob_document_get_spell_checker (doc))
	    {
	      katoob_document_enable_spell_checker (doc);
	    }
	  else
	    {
/* We should recheck the whole document */
	      katoob_document_recheck_spell (doc);
	    }
	}
      else
	{
/* We failed to set the dictionary. */
	  katoob_document_disable_spell_checker (doc);
	}
      return FALSE;
    }
  return TRUE;
}
#endif /* HAVE_SPELL */
