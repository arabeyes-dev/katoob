/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"

#include <gtk/gtk.h>
#include <string.h>
#include "katoobwindow.h"
#include "prefs-dlg.h"
#include "prefs.h"
#include "misc.h"
#include "dict.h"
#include "exec-cmd.h"

#ifdef ENABLE_PRINT
#include "print.h"
#include "print-gui.h"
GtkWidget *advanced;
GtkWidget *advanced_frame;
GtkWidget *dpi_x;
GtkWidget *dpi_y;

#endif /* ENABLE_PRINT */

GtkWidget *prefs = NULL;
GtkWidget *notebook;
GtkWidget *undo;
GtkWidget *undono;
GtkWidget *toolbar;
GtkWidget *extended_toolbar;
GtkWidget *statusbar;
GtkWidget *recent;
GtkWidget *recentno;
GtkWidget *toolbartype;
GtkWidget *showclose;
GtkWidget *showtabs;
GtkWidget *tabsmenu;
GtkWidget *scrolltabs;
GtkWidget *tabspos;
GtkWidget *text_dir;
GtkWidget *textwrap;
GtkWidget *savewinpos;
GtkWidget *saveonexit;
GtkWidget *x;
GtkWidget *y;
GtkWidget *w;
GtkWidget *h;
GtkWidget *linenumbers;
#ifndef _WIN32
GtkWidget *disable_xft;
#endif /* _WIN32 */
GtkWidget *locale_enc;
#ifndef _WIN32
GtkWidget *special_enc;
#endif /* _WIN32 */
GtkWidget *encodings_combo;
GtkWidget *font_label;
GtkWidget *default_font;

#ifdef HAVE_SPELL
GtkWidget *spell_check;
GtkWidget *da;
GtkWidget *default_dict;
#ifdef HAVE_OLD_SPELL
GtkWidget *dicts_dir;
#endif /* HAVE_OLD_SPELL */
#endif /* HAVE_SPELL */

#ifdef ENABLE_HIGHLIGHT
GtkWidget *highlight;
#endif /* ENABLE_HIGHLIGHT */

GtkWidget *backup;
GtkWidget *backup_in_same_dir;
GtkWidget *backup_ext;
GtkWidget *backup_dir;

#ifdef ENABLE_PRINT
extern PConfig *PConf;
#endif /* ENABLE_PRINT */

GtkWidget *dict;
GtkWidget *dict_host;
GtkWidget *dict_port;
GtkWidget *dict_db_combo;
GtkWidget *dict_timeout;
GtkTreeModel *dicts_model;
GtkWidget *treeview;
GtkWidget *exec_cmd_size;

extern conf *config;
extern UI *katoob;

typedef struct
{
  gint number;
  gchar *section;
}
Prefs;

enum
{
  COLUMN_NUMBER,
  COLUMN_SECTION,
  NUM_COLUMNS
};

Prefs data[] = {
  {0, N_("General")},
  {1, N_("Interface")},
  {2, N_("Tabs")},
  {3, N_("Editor")},
  {4, N_("Encodings")},
  {5, N_("File Save")},
  {6, N_("Spell Checker")},
  {7, N_("Print")},
  {8, N_("Dictionary")},
  {9, N_("Advanced")}
};

/* Should be called in the same order as above. */
static GtkWidget *prefs_create_general_page ();
static GtkWidget *prefs_create_interface_page ();
static GtkWidget *prefs_create_tabs_page ();
static GtkWidget *prefs_create_editor_page ();
static GtkWidget *prefs_create_encodings_page ();
static GtkWidget *prefs_create_file_save_page ();
static GtkWidget *prefs_create_spell_checker_page ();
static GtkWidget *prefs_create_print_page ();
static GtkWidget *prefs_create_dict_page ();
static GtkWidget *prefs_create_advanced_page ();

static void add_columns (GtkTreeView * treeview);
static GtkTreeModel *create_model ();
static gchar *katoob_get_default_font ();

#ifdef ENABLE_PRINT
void print_enable_advanced_options (GtkToggleButton * button, gpointer data);
void print_set_font (GtkWidget * widget, gpointer data);
#endif /* ENABLE_PRINT */

#ifdef HAVE_SPELL
static void button_press_callback (GtkWidget * widget,
				   GdkEventExpose * event, gpointer data);
#endif /* HAVE_SPELL */

static void browse_clicked_callback (gchar * title, gpointer data);
static void backup_browse_clicked_callback (GtkWidget * widget,
					    gpointer data);

#ifdef HAVE_OLD_SPELL
static void dicts_browse_clicked_callback (GtkWidget * widget, gpointer data);
#endif /* HAVE_OLD_SPELL */

static void katoob_prefs_list_available_dicts ();

void tree_selection_changed (GtkTreeSelection * selection, gpointer data);
void destroy_prefs ();
void on_cancelbutton_clicked ();
void on_applybutton_clicked ();
void on_okbutton_clicked ();
void katoob_prefs_process_options ();
void katoob_create_font_selection_dialog ();

#ifdef ENABLE_PRINT
void
print_enable_advanced_options (GtkToggleButton * button, gpointer data)
{
  gtk_widget_set_sensitive (advanced_frame, button->active);
}

void
print_set_font (GtkWidget * widget, gpointer data)
{
  katoob_print_set_fonts (data);
}
#endif /* ENABLE_PRINT */

#ifdef HAVE_SPELL
static void
button_press_callback (GtkWidget * widget, GdkEventExpose * event,
		       gpointer data)
{
  GtkWidget *dialog;
  GtkColorSelection *colorsel;
  gint response;
  GdkColor color;
  KATOOB_DEBUG_FUNCTION;

  dialog = gtk_color_selection_dialog_new (_("Mispelled words color"));
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (katoob->win));

  colorsel =
    GTK_COLOR_SELECTION (GTK_COLOR_SELECTION_DIALOG (dialog)->colorsel);
  gtk_color_selection_set_previous_color (colorsel, &katoob->mispelled_color);
  gtk_color_selection_set_current_color (colorsel, &katoob->mispelled_color);
  gtk_color_selection_set_has_palette (colorsel, TRUE);

  response = gtk_dialog_run (GTK_DIALOG (dialog));

  if (response == GTK_RESPONSE_OK)
    {
      gtk_color_selection_get_current_color (colorsel, &color);
      gtk_widget_modify_bg (da, GTK_STATE_NORMAL, &color);
    }
  gtk_widget_destroy (dialog);
}
#endif /* HAVE_SPELL */

static void
backup_browse_clicked_callback (GtkWidget * widget, gpointer data)
{
  browse_clicked_callback (_("Please select the backup directory"), data);
}

#ifdef HAVE_OLD_SPELL
static void
dicts_browse_clicked_callback (GtkWidget * widget, gpointer data)
{
  browse_clicked_callback (_("Please select the dictionaries directory"),
			   data);
}
#endif /* HAVE_OLD_SPELL */

static void
browse_clicked_callback (gchar * title, gpointer data)
{
  GtkWidget *dlg;
  gint response;
  gchar *txt = NULL;

  KATOOB_DEBUG_FUNCTION;

  dlg = gtk_file_selection_new (title);

  txt = (gchar *) gtk_entry_get_text (GTK_ENTRY (data));

  if (txt)
    {
      gtk_file_selection_set_filename (GTK_FILE_SELECTION (dlg), txt);
    }

  response = gtk_dialog_run (GTK_DIALOG (dlg));

  if (response == GTK_RESPONSE_OK)
    {
      txt =
	g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (dlg)));
      if (txt)
	{
/* Check that it's a directory and that it exists */
	  if (g_file_test (txt, G_FILE_TEST_IS_DIR))
	    {
	      gtk_entry_set_text (GTK_ENTRY (data), txt);
	    }
	  else if (!g_file_test (txt, G_FILE_TEST_EXISTS))
	    {
	      katoob_error (_("This file doesn't exist."));
	    }
	  else
	    {
	      katoob_error (_("This is not a directory."));
	    }
	}
      if (txt)
	{
	  g_free (txt);
	}
    }
  gtk_widget_destroy (dlg);
}

static void
katoob_prefs_list_available_dicts ()
{
  GSList *dicts, *tmp = NULL;
  gchar *host;
  gint port;
  gint timeout;
  gint x = 0;

  KATOOB_DEBUG_FUNCTION;
  host = (gchar *) gtk_entry_get_text (GTK_ENTRY (dict_host));
  if (!host)
    {
      return;
    }

  port = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dict_port));
  timeout = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dict_timeout));

  dicts = katoob_dict_list_dicts (host, port, timeout);

  if (!dicts)
    {
      return;
    }

  gtk_list_store_clear (GTK_LIST_STORE (dicts_model));
  tmp = dicts;
  while (tmp)
    {
      GtkTreeIter iter;
      struct ld_dbs *entry = (struct ld_dbs *) tmp->data;
      gtk_list_store_append (GTK_LIST_STORE (dicts_model), &iter);
      gtk_list_store_set (GTK_LIST_STORE (dicts_model), &iter, 0,
			  entry->ld_dbname, 1, entry->ld_dbdesc, -1);
      g_free (entry->ld_dbname);
      g_free (entry->ld_dbdesc);
      g_free (entry);
      ++x;
      tmp = g_slist_next (tmp);
    }

  g_slist_free (dicts);

  dicts = NULL;

  if (x == 1)
    {
      GtkTreeIter iter;
      gtk_tree_model_get_iter_first (GTK_TREE_MODEL (dicts_model), &iter);
      gtk_tree_selection_select_iter (gtk_tree_view_get_selection
				      (GTK_TREE_VIEW (treeview)), &iter);
    }
  return;
}

static GtkWidget *
prefs_create_general_page ()
{
  extern conf *config;
  GtkWidget *genaral_page;
  GtkWidget *hbox;
  GtkWidget *undo_label;
  GtkObject *undono_adj;
  GtkWidget *hbox2;
  GtkObject *exec_adj;
  GtkWidget *exec_label;

  genaral_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (genaral_page), 10);

  undo = gtk_check_button_new_with_mnemonic (_("_Enable undo, redo\t"));
  gtk_box_pack_start (GTK_BOX (genaral_page), undo, FALSE, FALSE, 0);

  hbox = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (genaral_page), hbox, FALSE, FALSE, 0);

  undo_label = gtk_label_new (_("Undo history size\n(0 for unlimited)"));
  gtk_box_pack_start (GTK_BOX (hbox), undo_label, FALSE, FALSE, 0);

  undono_adj = gtk_adjustment_new (1, 0, 100, 1, 10, 10);
  undono = gtk_spin_button_new (GTK_ADJUSTMENT (undono_adj), 1, 0);
  gtk_box_pack_start (GTK_BOX (hbox), undono, FALSE, FALSE, 0);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (undo), config->undo);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (undono), config->undono);

  hbox2 = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (genaral_page), hbox2, FALSE, FALSE, 0);

  exec_label = gtk_label_new (_("Executed commands history size:"));
  gtk_box_pack_start (GTK_BOX (hbox2), exec_label, FALSE, FALSE, 0);

  exec_adj = gtk_adjustment_new (config->exec_cmd_size, 0, 100, 1, 10, 10);
  exec_cmd_size = gtk_spin_button_new (GTK_ADJUSTMENT (exec_adj), 1, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), exec_cmd_size, FALSE, FALSE, 0);

  return genaral_page;
}

static GtkWidget *
prefs_create_interface_page ()
{
  GtkWidget *interface_page;
  GtkWidget *hbox2;
  GtkWidget *hbox3;
  GtkWidget *recentno_label;
  GtkObject *recentno_adj;
  GtkWidget *toolbartype_label;
  GtkWidget *toolbartype_menu;
  GtkWidget *text;
  GtkWidget *icons;
  GtkWidget *both;
  GtkWidget *both_horiz;

  interface_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (interface_page), 10);

  toolbar = gtk_check_button_new_with_mnemonic (_("Show the _Main Toolbar"));
  gtk_box_pack_start (GTK_BOX (interface_page), toolbar, FALSE, FALSE, 0);

  extended_toolbar =
    gtk_check_button_new_with_mnemonic (_("Show the _Extended Toolbar"));
  gtk_box_pack_start (GTK_BOX (interface_page), extended_toolbar, FALSE,
		      FALSE, 0);

  statusbar = gtk_check_button_new_with_mnemonic (_("Show the _Statusbar"));
  gtk_box_pack_start (GTK_BOX (interface_page), statusbar, FALSE, FALSE, 0);

  recent = gtk_check_button_new_with_mnemonic (_("Enable re_cent menu"));
  gtk_box_pack_start (GTK_BOX (interface_page), recent, FALSE, FALSE, 0);

  hbox2 = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (interface_page), hbox2, FALSE, FALSE, 0);

  recentno_label =
    gtk_label_new (_("No. of recent entries\n(0 for unlimited)"));
  gtk_box_pack_start (GTK_BOX (hbox2), recentno_label, FALSE, FALSE, 0);
  gtk_misc_set_padding (GTK_MISC (recentno_label), 5, 0);

  recentno_adj = gtk_adjustment_new (1, 0, 100, 1, 10, 10);
  recentno = gtk_spin_button_new (GTK_ADJUSTMENT (recentno_adj), 1, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), recentno, FALSE, FALSE, 0);

  hbox3 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (interface_page), hbox3, FALSE, FALSE, 0);
  toolbartype_label = gtk_label_new (_("Show the toolbar as"));
  gtk_box_pack_start (GTK_BOX (hbox3), toolbartype_label, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (toolbartype_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (toolbartype_label), 0.49, 0.5);
  gtk_misc_set_padding (GTK_MISC (toolbartype_label), 5, 0);

  toolbartype = gtk_option_menu_new ();
  gtk_box_pack_start (GTK_BOX (hbox3), toolbartype, FALSE, FALSE, 0);
  toolbartype_menu = gtk_menu_new ();

  text = gtk_menu_item_new_with_mnemonic (_("Text only"));
  gtk_container_add (GTK_CONTAINER (toolbartype_menu), text);

  icons = gtk_menu_item_new_with_mnemonic (_("Icons only"));
  gtk_container_add (GTK_CONTAINER (toolbartype_menu), icons);

  both = gtk_menu_item_new_with_mnemonic (_("Both"));
  gtk_container_add (GTK_CONTAINER (toolbartype_menu), both);

  both_horiz = gtk_menu_item_new_with_mnemonic (_("Text beside icons"));
  gtk_container_add (GTK_CONTAINER (toolbartype_menu), both_horiz);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (toolbartype), toolbartype_menu);

  showclose =
    gtk_check_button_new_with_mnemonic (_
					("Show the _close button on each tab label"));

  gtk_box_pack_start (GTK_BOX (interface_page), showclose, FALSE, FALSE, 0);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toolbar), config->toolbar);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (extended_toolbar),
				config->extended_toolbar);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (statusbar),
				config->statusbar);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (recent), config->recent);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (recentno), config->recentno);

  if (!strcmp (config->toolbartype, "text"))
    {
      gtk_option_menu_set_history (GTK_OPTION_MENU (toolbartype), 0);
    }
  else if (!strcmp (config->toolbartype, "icons"))
    {
      gtk_option_menu_set_history (GTK_OPTION_MENU (toolbartype), 1);
    }
  else if (!strcmp (config->toolbartype, "both_horiz"))
    {
      gtk_option_menu_set_history (GTK_OPTION_MENU (toolbartype), 3);
    }
  else
    {
      gtk_option_menu_set_history (GTK_OPTION_MENU (toolbartype), 2);
    }

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (showclose),
				config->showclose);
  return interface_page;
}

static GtkWidget *
prefs_create_tabs_page ()
{
  GtkWidget *tabs_page;
  GtkWidget *hbox4;
  GtkWidget *tabspos_label;
  GtkWidget *tabspos_menu;
  GtkWidget *top;
  GtkWidget *top_img;
  GtkWidget *bottom;
  GtkWidget *bottom_img;
  GtkWidget *right;
  GtkWidget *right_img;
  GtkWidget *left;
  GtkWidget *left_img;

  tabs_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (tabs_page), 10);

  showtabs = gtk_check_button_new_with_mnemonic (_("_Show tabs"));
  gtk_box_pack_start (GTK_BOX (tabs_page), showtabs, FALSE, TRUE, 0);

  tabsmenu = gtk_check_button_new_with_mnemonic (_("_Enable tabs menu"));
  gtk_box_pack_start (GTK_BOX (tabs_page), tabsmenu, FALSE, TRUE, 0);

  scrolltabs = gtk_check_button_new_with_mnemonic (_("Scro_ll tabs"));
  gtk_box_pack_start (GTK_BOX (tabs_page), scrolltabs, FALSE, TRUE, 0);

  hbox4 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (tabs_page), hbox4, TRUE, FALSE, 0);

  tabspos_label = gtk_label_new (_("Show the tabs at the"));
  gtk_box_pack_start (GTK_BOX (hbox4), tabspos_label, FALSE, FALSE, 10);
  gtk_label_set_justify (GTK_LABEL (tabspos_label), GTK_JUSTIFY_LEFT);

  tabspos = gtk_option_menu_new ();
  gtk_box_pack_start (GTK_BOX (hbox4), tabspos, TRUE, TRUE, 0);

  tabspos_menu = gtk_menu_new ();
  top = gtk_image_menu_item_new_with_mnemonic (_("Top"));
  gtk_container_add (GTK_CONTAINER (tabspos_menu), top);

  top_img = gtk_image_new_from_stock ("gtk-goto-top", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (top), top_img);

  bottom = gtk_image_menu_item_new_with_mnemonic (_("Bottom"));
  gtk_container_add (GTK_CONTAINER (tabspos_menu), bottom);

  bottom_img =
    gtk_image_new_from_stock ("gtk-goto-bottom", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (bottom), bottom_img);

  right = gtk_image_menu_item_new_with_mnemonic (_("Right"));
  gtk_container_add (GTK_CONTAINER (tabspos_menu), right);

  right_img = gtk_image_new_from_stock ("gtk-goto-last", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (right), right_img);

  left = gtk_image_menu_item_new_with_mnemonic (_("Left"));
  gtk_container_add (GTK_CONTAINER (tabspos_menu), left);

  left_img = gtk_image_new_from_stock ("gtk-goto-first", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (left), left_img);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (tabspos), tabspos_menu);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (showtabs),
				config->showtabs);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (tabsmenu),
				config->tabsmenu);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (scrolltabs),
				config->scrolltabs);

  switch (config->tabspos)
    {
    case TABS_POS_BOTTOM:
      gtk_option_menu_set_history (GTK_OPTION_MENU (tabspos), 1);
    case TABS_POS_RIGHT:
      gtk_option_menu_set_history (GTK_OPTION_MENU (tabspos), 2);
    case TABS_POS_LEFT:
      gtk_option_menu_set_history (GTK_OPTION_MENU (tabspos), 3);
    default:
      gtk_option_menu_set_history (GTK_OPTION_MENU (tabspos), 0);
    }

  return tabs_page;
}

static GtkWidget *
prefs_create_editor_page ()
{
  GtkWidget *editor_page;
  GtkWidget *hbox5;
  GtkWidget *text_dir_menu;
  GtkWidget *aut;
  GtkWidget *rtl;
  GtkWidget *ltr;
  GtkWidget *text_dir_label;
  GtkWidget *frame4;
  GtkWidget *font_vbox;
  GtkWidget *font_button;

  gchar *markup;

  editor_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (editor_page), 10);
#ifndef GTK_2_4
  hbox5 = gtk_hbox_new (FALSE, 0);
  text_dir = gtk_option_menu_new ();
  text_dir_menu = gtk_menu_new ();
  gtk_box_pack_start (GTK_BOX (editor_page), hbox5, FALSE, TRUE, 0);
  aut = gtk_menu_item_new_with_mnemonic (_("_Automatic"));
  gtk_container_add (GTK_CONTAINER (text_dir_menu), aut);
  rtl = gtk_menu_item_new_with_mnemonic (_("_Right to left"));
  gtk_container_add (GTK_CONTAINER (text_dir_menu), rtl);
  ltr = gtk_menu_item_new_with_mnemonic (_("_Left to right"));
  gtk_container_add (GTK_CONTAINER (text_dir_menu), ltr);
  gtk_option_menu_set_menu (GTK_OPTION_MENU (text_dir), text_dir_menu);
  text_dir_label = gtk_label_new (_("Default text direction:"));
  gtk_box_pack_start (GTK_BOX (hbox5), text_dir_label, FALSE, FALSE, 10);
  gtk_box_pack_start (GTK_BOX (hbox5), text_dir, FALSE, FALSE, 0);
#endif
  textwrap =
    gtk_check_button_new_with_mnemonic (_("Text _Wrapping is on by default"));
  gtk_box_pack_start (GTK_BOX (editor_page), textwrap, FALSE, FALSE, 0);

  linenumbers =
    gtk_check_button_new_with_mnemonic (_("Show _Line numbers by default"));
  gtk_box_pack_start (GTK_BOX (editor_page), linenumbers, FALSE, FALSE, 0);

  frame4 = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (editor_page), frame4, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frame4), 5);
  font_label = gtk_label_new (_("Font"));
  gtk_frame_set_label_widget (GTK_FRAME (frame4), font_label);
  gtk_label_set_justify (GTK_LABEL (font_label), GTK_JUSTIFY_LEFT);

  font_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame4), font_vbox);
  gtk_container_set_border_width (GTK_CONTAINER (font_vbox), 5);

  default_font =
    gtk_check_button_new_with_mnemonic (_("Use the default _font"));

  if (!config->font)
    {
      config->font = katoob_get_default_font ();
    }

  font_button = gtk_button_new ();
  font_label = gtk_label_new ("");

  markup = g_strdup_printf ("<span font_desc=\"%s\">%s</span>",
			    config->font, config->font);
  gtk_label_set_markup (GTK_LABEL (font_label), markup);
  g_free (markup);
  gtk_container_add (GTK_CONTAINER (font_button), font_label);
  gtk_box_pack_start (GTK_BOX (font_vbox), default_font, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (font_vbox), font_button, FALSE, FALSE, 0);
#ifndef GTK_2_4
  switch (config->text_dir)
    {
    case KATOOB_BIDI_LTR:
      {
	gtk_option_menu_set_history (GTK_OPTION_MENU (text_dir), 1);
	break;
      }
    case KATOOB_BIDI_RTL:
      {
	gtk_option_menu_set_history (GTK_OPTION_MENU (text_dir), 2);
	break;
      }
    default:
      {
	gtk_option_menu_set_history (GTK_OPTION_MENU (text_dir), 0);
	break;
      }
    }
#endif

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (textwrap),
				config->textwrap);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (linenumbers),
				config->linenumbers);

  g_signal_connect (G_OBJECT (font_button), "clicked",
		    G_CALLBACK (katoob_create_font_selection_dialog), NULL);

#ifdef ENABLE_HIGHLIGHT
  highlight =
    gtk_check_button_new_with_mnemonic (_("_Highlight known file types"));
  gtk_box_pack_start (GTK_BOX (editor_page), highlight, FALSE, FALSE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (highlight),
				config->highlight);
#endif /* ENABLE_HIGHLIGHT */

  return editor_page;
}

static GtkWidget *
prefs_create_encodings_page ()
{
  GtkWidget *encodings_page;
  GtkWidget *frame3;
  GtkWidget *default_encoding;
  GtkWidget *encodings_vbox;
  GSList *egp = NULL;

  encodings_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (encodings_page), 10);

  frame3 = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (encodings_page), frame3, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frame3), 5);
  default_encoding = gtk_label_new (_("Default Encoding"));
  gtk_frame_set_label_widget (GTK_FRAME (frame3), default_encoding);
  gtk_label_set_justify (GTK_LABEL (default_encoding), GTK_JUSTIFY_LEFT);

  encodings_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame3), encodings_vbox);
  gtk_container_set_border_width (GTK_CONTAINER (encodings_vbox), 5);

#ifndef _WIN32
  locale_enc =
    gtk_radio_button_new_with_mnemonic (NULL, _("_Use the locale encoding"));
  gtk_box_pack_start (GTK_BOX (encodings_vbox), locale_enc, FALSE, FALSE, 0);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (locale_enc), egp);
  egp = gtk_radio_button_get_group (GTK_RADIO_BUTTON (locale_enc));

  special_enc =
    gtk_radio_button_new_with_mnemonic (NULL,
					_("_Manually choose the encoding"));
  gtk_box_pack_start (GTK_BOX (encodings_vbox), special_enc, FALSE, FALSE, 0);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (special_enc), egp);
  egp = gtk_radio_button_get_group (GTK_RADIO_BUTTON (special_enc));
#else
  locale_enc = gtk_label_new (_("Choose the encoding."));
  gtk_box_pack_start (GTK_BOX (encodings_vbox), locale_enc, FALSE, FALSE, 0);
#endif /* _WIN32 */

  encodings_combo = gtk_combo_new ();
  gtk_combo_set_popdown_strings (GTK_COMBO (encodings_combo), config->enc);
  gtk_combo_set_value_in_list (GTK_COMBO (encodings_combo), TRUE, FALSE);
  gtk_combo_set_case_sensitive (GTK_COMBO (encodings_combo), TRUE);
  gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (encodings_combo)->entry),
		      katoob_encodings_get_name_by_number (config->defenc));
  gtk_box_pack_start (GTK_BOX (encodings_vbox), encodings_combo, FALSE, FALSE,
		      0);
#ifndef _WIN32
  if (config->locale_enc)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (locale_enc), TRUE);
    }
  else if (config->special_enc)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (special_enc), TRUE);
    }
#endif /* _WIN32 */

  return encodings_page;
}

static GtkWidget *
prefs_create_file_save_page ()
{
  GtkWidget *file_save_page;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *hbox2;
  GtkWidget *label2;
  GtkWidget *button;

  file_save_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (file_save_page), 10);

  backup =
    gtk_check_button_new_with_mnemonic (_("_Backup files before saving."));
  gtk_box_pack_start (GTK_BOX (file_save_page), backup, FALSE, FALSE, 0);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (backup), config->backup);

  backup_in_same_dir =
    gtk_check_button_new_with_mnemonic (_
					("Create backup in the original file directory."));
  gtk_box_pack_start (GTK_BOX (file_save_page), backup_in_same_dir, FALSE,
		      FALSE, 0);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (backup_in_same_dir),
				config->backup_in_same_dir);

  hbox = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (file_save_page), hbox, FALSE, FALSE, 0);

  label = gtk_label_new (_("Backup extension."));
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

  backup_ext = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (hbox), backup_ext, FALSE, FALSE, 0);
  gtk_entry_set_text (GTK_ENTRY (backup_ext), config->backup_ext);

  hbox2 = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (file_save_page), hbox2, FALSE, FALSE, 0);

  label2 = gtk_label_new (_("Backup directory."));
  gtk_box_pack_start (GTK_BOX (hbox2), label2, FALSE, FALSE, 0);

  backup_dir = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (hbox2), backup_dir, FALSE, FALSE, 0);
  gtk_entry_set_text (GTK_ENTRY (backup_dir), config->backup_dir);

  button = gtk_button_new_with_mnemonic (_("_Browse"));
  gtk_box_pack_start (GTK_BOX (hbox2), button, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (button), "clicked",
		    G_CALLBACK (backup_browse_clicked_callback), backup_dir);
  return file_save_page;
}

static GtkWidget *
prefs_create_spell_checker_page ()
{
  GtkWidget *spell_page;
#ifdef HAVE_SPELL
  GtkWidget *frame;
  GtkWidget *hbox;
  GtkWidget *hbox2;
#ifdef HAVE_OLD_SPELL
  GtkWidget *hbox3;
  GtkWidget *browse;
  GtkWidget *label3;
#endif /* HAVE_OLD_SPELL */
  GtkWidget *label2;
#endif /* HAVE_SPELL */
  GtkWidget *label;

  spell_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (spell_page), 10);

#ifdef HAVE_SPELL

  spell_check =
    gtk_check_button_new_with_mnemonic (_("Enable the _spell checker"));
  gtk_box_pack_start (GTK_BOX (spell_page), spell_check, FALSE, FALSE, 0);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (spell_check),
				config->spell_check);

  hbox = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (spell_page), hbox, FALSE, FALSE, 0);

  label = gtk_label_new (_("Mispelled words color"));
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  da = gtk_drawing_area_new ();

  gtk_widget_set_size_request (da, 20, 20);
  gtk_widget_modify_bg (da, GTK_STATE_NORMAL, &katoob->mispelled_color);
  gtk_widget_set_events (da, GDK_BUTTON_PRESS_MASK);

  g_signal_connect (G_OBJECT (da), "button-press-event",
		    G_CALLBACK (button_press_callback), NULL);

  gtk_container_add (GTK_CONTAINER (frame), da);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);

  hbox2 = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (spell_page), hbox2, FALSE, FALSE, 0);

  label2 = gtk_label_new (_("Choose the default dictionary"));
  gtk_box_pack_start (GTK_BOX (hbox2), label2, FALSE, FALSE, 0);
  default_dict = katoob_spell_generate_option_menu ();
  gtk_box_pack_start (GTK_BOX (hbox2), default_dict, FALSE, FALSE, 0);

#ifdef HAVE_OLD_SPELL
  hbox3 = gtk_hbox_new (FALSE, 3);
  gtk_box_pack_start (GTK_BOX (spell_page), hbox3, FALSE, FALSE, 0);

  label3 = gtk_label_new (_("Dictionaries directory"));
  gtk_box_pack_start (GTK_BOX (hbox3), label3, FALSE, FALSE, 0);

  dicts_dir = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (hbox3), dicts_dir, FALSE, FALSE, 0);
  gtk_entry_set_text (GTK_ENTRY (dicts_dir), config->dicts_dir);
  browse = gtk_button_new_with_mnemonic (_("_Browse"));
  gtk_box_pack_start (GTK_BOX (hbox3), browse, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (browse), "clicked",
		    G_CALLBACK (dicts_browse_clicked_callback), dicts_dir);
#endif /* HAVE_OLD_SPELL */
#else /* ! HAVE_SPELL */
  label = gtk_label_new (_("Not enabled at compile time!"));
  gtk_box_pack_start (GTK_BOX (spell_page), label, TRUE, TRUE, 0);
#endif /* HAVE_SPELL */

  return spell_page;
}

static GtkWidget *
prefs_create_print_page ()
{
  GtkWidget *print_page;
#ifdef ENABLE_PRINT
  GtkWidget *hbox;
  GtkWidget *print_font_label;
  GtkWidget *print_font_button;
  GtkWidget *print_font_button_label;
  GtkWidget *table;
  GtkWidget *dpi_x_label;
  GtkWidget *dpi_y_label;
  GtkObject *dpi_x_adj;
  GtkObject *dpi_y_adj;
  gchar *markup;
#else /* ! ENABLE_PRINT */
  GtkWidget *label;
#endif /* ENABLE_PRINT */

  print_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (print_page), 10);

#ifdef ENABLE_PRINT
  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (print_page), hbox, FALSE, FALSE, 0);

  print_font_label = gtk_label_new ("Printing font:");
  gtk_box_pack_start (GTK_BOX (hbox), print_font_label, FALSE, FALSE, 10);

  print_font_button = gtk_button_new ();
  print_font_button_label = gtk_label_new ("");
  gtk_box_pack_start (GTK_BOX (hbox), print_font_button, FALSE, FALSE, 10);

  markup = g_strdup_printf
    ("<span font_desc=\"%s %s %s\">%s %s %s</span>",
     PConf->font_family, PConf->font_style,
     PConf->font_scale, PConf->font_family,
     PConf->font_style, PConf->font_scale);
  gtk_label_set_markup (GTK_LABEL (print_font_button_label), markup);
  g_free (markup);

  gtk_container_add (GTK_CONTAINER (print_font_button),
		     print_font_button_label);
  g_signal_connect (G_OBJECT (print_font_button), "clicked",
		    G_CALLBACK (print_set_font), print_font_button_label);

  advanced =
    gtk_check_button_new_with_mnemonic (_("_Enable advanced options"));
  gtk_box_pack_start (GTK_BOX (print_page), advanced, FALSE, FALSE, 0);

  advanced_frame = gtk_frame_new ("");
  gtk_box_pack_start (GTK_BOX (print_page), advanced_frame, FALSE, FALSE, 0);

  table = gtk_table_new (2, 2, FALSE);
  gtk_container_add (GTK_CONTAINER (advanced_frame), table);

  dpi_x_adj = gtk_adjustment_new (PConf->dpi_x, 150, 1200, 1, 10, 10);
  dpi_x = gtk_spin_button_new (GTK_ADJUSTMENT (dpi_x_adj), 1, 0);
  dpi_y_adj = gtk_adjustment_new (PConf->dpi_y, 150, 1200, 1, 10, 10);
  dpi_y = gtk_spin_button_new (GTK_ADJUSTMENT (dpi_y_adj), 1, 0);
  dpi_x_label = gtk_label_new ("X Dot Per Inch");
  dpi_y_label = gtk_label_new ("Y Dot Per Inch");

  gtk_table_attach_defaults (GTK_TABLE (table), dpi_x_label, 0, 1, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE (table), dpi_x, 1, 2, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE (table), dpi_y_label, 0, 1, 1, 2);
  gtk_table_attach_defaults (GTK_TABLE (table), dpi_y, 1, 2, 1, 2);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (advanced),
				PConf->gui_show_advanced);

  gtk_widget_set_sensitive (advanced_frame, PConf->gui_show_advanced);

  g_signal_connect (G_OBJECT (advanced), "toggled",
		    G_CALLBACK (print_enable_advanced_options), NULL);

#else /* ! ENABLE_PRINT */
  label = gtk_label_new (_("Not enabled at compile time!"));
  gtk_box_pack_start (GTK_BOX (print_page), label, TRUE, TRUE, 0);
#endif /* ENABLE_PRINT */

  return print_page;
}

static GtkWidget *
prefs_create_dict_page ()
{
  GtkWidget *dict_page;
  GtkObject *dict_port_adj;
  GtkObject *dict_timeout_adj;
  GtkWidget *table;
  GtkWidget *label1;
  GtkWidget *label2;
  GtkWidget *label3;
  GtkWidget *label4;
  GtkWidget *q_button;
  GtkWidget *sw;
  GtkTreeSelection *select;
  GtkTreeViewColumn *column;
  GtkCellRenderer *renderer;
  GtkTreeIter iter;

/*
  GtkWidget *label5;
  GtkWidget *label6;
  GtkWidget *user;
  GtkWidget *pass;
  GtkWidget *auth_type;
*/

  dict_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (dict_page), 10);
  dict = gtk_check_button_new_with_mnemonic (_("_Enable the dictionary."));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dict), config->dict);

  gtk_container_add (GTK_CONTAINER (dict_page), dict);
  table = gtk_table_new (6, 3, FALSE);
  gtk_container_add (GTK_CONTAINER (dict_page), table);
  gtk_container_set_border_width (GTK_CONTAINER (table), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table), 5);
  gtk_table_set_row_spacings (GTK_TABLE (table), 5);

/* Host: */
  label1 = gtk_label_new (_("Host:"));
  gtk_table_attach (GTK_TABLE (table), label1, 0, 1, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  dict_host = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table), dict_host, 1, 2, 0, 1,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  if (config->dict_host)
    {
      gtk_entry_set_text (GTK_ENTRY (dict_host), config->dict_host);
    }


  q_button = gtk_button_new_with_mnemonic (_("_List dictionaries"));
  gtk_table_attach (GTK_TABLE (table), q_button, 2, 3, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

/* Port: */
  label2 = gtk_label_new (_("Port:"));
  gtk_table_attach (GTK_TABLE (table), label2, 0, 1, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  dict_port_adj = gtk_adjustment_new (config->dict_port, 0, 65535, 1, 10, 10);
  dict_port = gtk_spin_button_new (GTK_ADJUSTMENT (dict_port_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (table), dict_port, 1, 2, 1, 2,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

/* Timeout: */
  label3 = gtk_label_new (_("Timeout:"));
  gtk_table_attach (GTK_TABLE (table), label3, 0, 1, 2, 3,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  dict_timeout_adj =
    gtk_adjustment_new (config->dict_timeout, 1, 100, 1, 10, 10);
  dict_timeout =
    gtk_spin_button_new (GTK_ADJUSTMENT (dict_timeout_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (table), dict_timeout, 1, 2, 2, 3,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

/* Dictionary: */
  label4 = gtk_label_new (_("Dictionary:"));
  gtk_table_attach (GTK_TABLE (table), label4, 0, 1, 3, 4,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  dicts_model =
    (GtkTreeModel *) gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				  (sw),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  treeview = gtk_tree_view_new_with_model (dicts_model);
  g_object_unref (G_OBJECT (dicts_model));
  dicts_model = gtk_tree_view_get_model (GTK_TREE_VIEW (treeview));

  gtk_widget_set_size_request (GTK_WIDGET (treeview), -1, 75);

  gtk_container_add (GTK_CONTAINER (sw), treeview);
  gtk_table_attach (GTK_TABLE (table), sw, 1, 2, 3, 4,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Dictionary"),
						     renderer,
						     "text", 0, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Description"),
						     renderer,
						     "text", 1, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

  select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
  gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);

  if (config->dict_db)
    {
      gtk_list_store_append (GTK_LIST_STORE (dicts_model), &iter);
      gtk_list_store_set (GTK_LIST_STORE (dicts_model), &iter, 0,
			  config->dict_db, 1, _("Not Available."), -1);
      gtk_tree_selection_select_iter (gtk_tree_view_get_selection
				      (GTK_TREE_VIEW (treeview)), &iter);
    }
  g_signal_connect (G_OBJECT (q_button), "clicked",
		    G_CALLBACK (katoob_prefs_list_available_dicts), NULL);

/* TODO: To be implemented. */
/*
  label5 = gtk_label_new (_("Username:"));
  gtk_table_attach (GTK_TABLE (table), label5, 0, 1, 4, 5,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  label6 = gtk_label_new (_("Password:"));
  gtk_table_attach (GTK_TABLE (table), label6, 0, 1, 5, 6,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  user = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table), user, 1, 2, 4, 5,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  pass = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table), pass, 1, 2, 5, 6,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);   
  gtk_entry_set_visibility (GTK_ENTRY (pass), FALSE);
  
  optionmenu1 = gtk_option_menu_new ();
  gtk_widget_show (optionmenu1);
  gtk_table_attach (GTK_TABLE (table), optionmenu1, 2, 3, 5, 6,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);


*/

  return dict_page;
}

static GtkWidget *
prefs_create_advanced_page ()
{
  GtkWidget *advanced_page;
  GtkWidget *frame1;
  GtkWidget *table1;
  GtkObject *x_adj;
  GtkObject *y_adj;
  GtkWidget *left_dim;
  GtkWidget *top_dim;
  GtkWidget *windowposition;
  GtkWidget *frame2;
  GtkWidget *table2;
  GtkObject *w_adj;
  GtkObject *h_adj;
  GtkWidget *width_dim;
  GtkWidget *height_dim;
  GtkWidget *windowsize;

  advanced_page = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (advanced_page), 10);

  frame1 = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (advanced_page), frame1, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frame1), 5);

  table1 = gtk_table_new (2, 2, TRUE);
  gtk_container_add (GTK_CONTAINER (frame1), table1);
  gtk_container_set_border_width (GTK_CONTAINER (table1), 5);
  gtk_table_set_row_spacings (GTK_TABLE (table1), 5);
  x_adj = gtk_adjustment_new (1, 0, 100, 1, 10, 10);
  x = gtk_spin_button_new (GTK_ADJUSTMENT (x_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (table1), x, 1, 2, 0, 1,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_range (GTK_SPIN_BUTTON (x), 0, 1000000);
  y_adj = gtk_adjustment_new (1, 0, 100, 1, 10, 10);
  y = gtk_spin_button_new (GTK_ADJUSTMENT (y_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (table1), y, 1, 2, 1, 2,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_range (GTK_SPIN_BUTTON (y), 0, 1000000);
  left_dim = gtk_label_new (_("Left"));
  gtk_table_attach (GTK_TABLE (table1), left_dim, 0, 1, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (left_dim), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (left_dim), 0, 0.5);
  gtk_misc_set_padding (GTK_MISC (left_dim), 10, 0);

  top_dim = gtk_label_new (_("Top"));
  gtk_table_attach (GTK_TABLE (table1), top_dim, 0, 1, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (top_dim), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (top_dim), 0, 0.5);
  gtk_misc_set_padding (GTK_MISC (top_dim), 10, 0);

  windowposition = gtk_label_new (_("Window position"));
  gtk_frame_set_label_widget (GTK_FRAME (frame1), windowposition);
  gtk_label_set_justify (GTK_LABEL (windowposition), GTK_JUSTIFY_LEFT);

  frame2 = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (advanced_page), frame2, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frame2), 5);

  table2 = gtk_table_new (2, 2, TRUE);
  gtk_container_add (GTK_CONTAINER (frame2), table2);
  gtk_container_set_border_width (GTK_CONTAINER (table2), 5);
  gtk_table_set_row_spacings (GTK_TABLE (table2), 5);

  w_adj = gtk_adjustment_new (1, 0, 100, 1, 10, 10);
  w = gtk_spin_button_new (GTK_ADJUSTMENT (w_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (table2), w, 1, 2, 0, 1,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_range (GTK_SPIN_BUTTON (w), 0, 1000000);
  h_adj = gtk_adjustment_new (1, 0, 100, 1, 10, 10);

  h = gtk_spin_button_new (GTK_ADJUSTMENT (h_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (table2), h, 1, 2, 1, 2,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_range (GTK_SPIN_BUTTON (h), 0, 1000000);
  width_dim = gtk_label_new (_("Width"));
  gtk_table_attach (GTK_TABLE (table2), width_dim, 0, 1, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (width_dim), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (width_dim), 0, 0.5);
  gtk_misc_set_padding (GTK_MISC (width_dim), 10, 0);

  height_dim = gtk_label_new (_("Height"));
  gtk_table_attach (GTK_TABLE (table2), height_dim, 0, 1, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (height_dim), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (height_dim), 0, 0.5);
  gtk_misc_set_padding (GTK_MISC (height_dim), 10, 0);

  windowsize = gtk_label_new (_("Window size"));
  gtk_frame_set_label_widget (GTK_FRAME (frame2), windowsize);
  gtk_label_set_justify (GTK_LABEL (windowsize), GTK_JUSTIFY_LEFT);

  savewinpos =
    gtk_check_button_new_with_mnemonic (_
					("Save the main window _attributes on exit"));
  gtk_box_pack_start (GTK_BOX (advanced_page), savewinpos, FALSE, FALSE, 0);

  saveonexit =
    gtk_check_button_new_with_mnemonic (_
					("Save all the program _settings on exit"));
  gtk_box_pack_start (GTK_BOX (advanced_page), saveonexit, FALSE, FALSE, 0);
#ifndef _WIN32
  disable_xft =
    gtk_check_button_new_with_mnemonic (_("_Disable Antialiasing"));
  gtk_box_pack_start (GTK_BOX (advanced_page), disable_xft, FALSE, FALSE, 0);
#endif /* _WIN32 */
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (savewinpos),
				config->savewinpos);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (saveonexit),
				config->saveonexit);
#ifndef _WIN32
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (disable_xft),
				config->disable_xft);
#endif /* _WIN32 */
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (default_font),
				config->default_font);

  gtk_spin_button_set_value (GTK_SPIN_BUTTON (x), config->x);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (y), config->y);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (w), config->w);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (h), config->h);

  return advanced_page;
}

/* TODO: How to get the actual font used by the widget ?
 * I have "tahoma 12" in my ~/.gtkrc-2.0 but this function returns "sans 10"
 */
static gchar *
katoob_get_default_font ()
{
  gchar *fnt = NULL;
  GtkWidget *foo = gtk_text_view_new ();
  fnt = pango_font_description_to_string (foo->style->font_desc);
  gtk_widget_destroy (foo);
  return fnt;
}

static GtkTreeModel *
create_model ()
{
  guint i = 0;
  GtkListStore *store;
  GtkTreeIter iter;

  store = gtk_list_store_new (NUM_COLUMNS, G_TYPE_UINT, G_TYPE_STRING);
  for (i = 0; i < G_N_ELEMENTS (data); i++)
    {

      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter,
			  COLUMN_NUMBER, data[i].number,
			  COLUMN_SECTION, data[i].section, -1);
    }
  return GTK_TREE_MODEL (store);
}

static void
add_columns (GtkTreeView * treeview)
{
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkTreeModel *model;
  model = gtk_tree_view_get_model (treeview);

  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("#",
						     renderer,
						     "text",
						     COLUMN_NUMBER, NULL);
  gtk_tree_view_append_column (treeview, column);
  gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (column), FALSE);
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Options for",
						     renderer,
						     "text",
						     COLUMN_SECTION, NULL);
  gtk_tree_view_append_column (treeview, column);
}

void
katoob_create_prefs_dlg ()
{
  GtkWidget *cancelbutton;
  GtkWidget *applybutton;
  GtkWidget *okbutton;
  GtkWidget *hbox;
  GtkWidget *sw;
  GtkTreeModel *model;
  GtkTreeSelection *select;
  GtkWidget *treeview;
  GtkWidget *dialog_vbox;

  if (prefs)
    {
      gtk_window_present (GTK_WINDOW (prefs));
      return;
    }

  prefs = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (prefs), _("Preferences"));
  gtk_window_set_position (GTK_WINDOW (prefs), GTK_WIN_POS_CENTER);
  gtk_window_set_transient_for (GTK_WINDOW (prefs), GTK_WINDOW (katoob->win));

  cancelbutton = gtk_button_new_from_stock ("gtk-cancel");
  gtk_dialog_add_action_widget (GTK_DIALOG (prefs), cancelbutton,
				GTK_RESPONSE_CANCEL);
  GTK_WIDGET_SET_FLAGS (cancelbutton, GTK_CAN_DEFAULT);

  applybutton = gtk_button_new_from_stock ("gtk-apply");
  gtk_dialog_add_action_widget (GTK_DIALOG (prefs), applybutton,
				GTK_RESPONSE_APPLY);
  GTK_WIDGET_SET_FLAGS (applybutton, GTK_CAN_DEFAULT);

  okbutton = gtk_button_new_from_stock ("gtk-ok");
  gtk_dialog_add_action_widget (GTK_DIALOG (prefs), okbutton,
				GTK_RESPONSE_OK);
  GTK_WIDGET_SET_FLAGS (okbutton, GTK_CAN_DEFAULT);

  dialog_vbox = GTK_DIALOG (prefs)->vbox;
  hbox = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (dialog_vbox), hbox, TRUE, TRUE, 0);

  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
				       GTK_SHADOW_ETCHED_IN);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
				  GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX (hbox), sw, TRUE, TRUE, 0);

  model = create_model ();
  treeview = gtk_tree_view_new_with_model (model);
  gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (treeview), FALSE);
  g_object_unref (G_OBJECT (model));
  gtk_container_add (GTK_CONTAINER (sw), treeview);
  add_columns (GTK_TREE_VIEW (treeview));

  select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
  gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);

  notebook = gtk_notebook_new ();
  gtk_widget_show (notebook);
  gtk_box_pack_start (GTK_BOX (hbox), notebook, TRUE, TRUE, 0);
  GTK_WIDGET_UNSET_FLAGS (notebook, GTK_CAN_FOCUS);
  gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook), FALSE);

  gtk_container_add (GTK_CONTAINER (notebook), prefs_create_general_page ());
  gtk_container_add (GTK_CONTAINER (notebook),
		     prefs_create_interface_page ());
  gtk_container_add (GTK_CONTAINER (notebook), prefs_create_tabs_page ());
  gtk_container_add (GTK_CONTAINER (notebook), prefs_create_editor_page ());
  gtk_container_add (GTK_CONTAINER (notebook),
		     prefs_create_encodings_page ());
  gtk_container_add (GTK_CONTAINER (notebook),
		     prefs_create_file_save_page ());
  gtk_container_add (GTK_CONTAINER (notebook),
		     prefs_create_spell_checker_page ());
  gtk_container_add (GTK_CONTAINER (notebook), prefs_create_print_page ());
  gtk_container_add (GTK_CONTAINER (notebook), prefs_create_dict_page ());
  gtk_container_add (GTK_CONTAINER (notebook), prefs_create_advanced_page ());

  g_signal_connect (G_OBJECT (select), "changed",
		    G_CALLBACK (tree_selection_changed), NULL);
  g_signal_connect (G_OBJECT (prefs), "destroy",
		    G_CALLBACK (destroy_prefs), NULL);
  g_signal_connect (G_OBJECT (cancelbutton), "clicked",
		    G_CALLBACK (on_cancelbutton_clicked), NULL);
  g_signal_connect (G_OBJECT (applybutton), "clicked",
		    G_CALLBACK (on_applybutton_clicked), NULL);
  g_signal_connect (G_OBJECT (okbutton), "clicked",
		    G_CALLBACK (on_okbutton_clicked), NULL);

  gtk_widget_show_all (prefs);
}

void
tree_selection_changed (GtkTreeSelection * selection, gpointer data)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  gint page;

  if (!gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      return;
    }
  gtk_tree_model_get (model, &iter, COLUMN_NUMBER, &page, -1);
  gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), page);
}

void
destroy_prefs ()
{
  KATOOB_DEBUG_FUNCTION;
  gtk_widget_destroyed (prefs, &prefs);
}

void
on_cancelbutton_clicked ()
{
  KATOOB_DEBUG_FUNCTION;
  g_signal_emit_by_name (G_OBJECT (prefs), "destroy");
}

void
on_applybutton_clicked ()
{
  KATOOB_DEBUG_FUNCTION;
  katoob_prefs_process_options ();
  katoob_update_main_interface (KATOOB_WINDOW (katoob->win));
}

void
on_okbutton_clicked ()
{
  KATOOB_DEBUG_FUNCTION;
  katoob_prefs_process_options ();
  g_signal_emit_by_name (G_OBJECT (prefs), "destroy");
  katoob_update_main_interface (KATOOB_WINDOW (katoob->win));
}

void
katoob_prefs_process_options ()
{
  gint i;
  KATOOB_DEBUG_FUNCTION;

  config->undo = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (undo));
  config->undono =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (undono));
  config->toolbar =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toolbar));
  config->extended_toolbar =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (extended_toolbar));
  config->statusbar =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (statusbar));
  config->recent = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (recent));
  config->recentno =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (recentno));
  i = gtk_option_menu_get_history (GTK_OPTION_MENU (toolbartype));

  if (config->toolbartype)
    {
      g_free (config->toolbartype);
    }

  switch (i)
    {
    case 0:
      config->toolbartype = g_strdup ("text");
      break;
    case 1:
      config->toolbartype = g_strdup ("icons");
      break;
    case 3:
      config->toolbartype = g_strdup ("both_horiz");
      break;
    default:
      config->toolbartype = g_strdup ("both");
      break;
    }

  i = gtk_option_menu_get_history (GTK_OPTION_MENU (text_dir));
#ifndef GTK_2_4
  switch (i)
    {
    case 0:
      config->text_dir = KATOOB_BIDI_AUT;
      break;
    case 1:
      config->text_dir = KATOOB_BIDI_RTL;
      break;
    case 2:
      config->text_dir = KATOOB_BIDI_LTR;
      break;
    }
#endif

  config->showclose =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (showclose));
  config->showtabs =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (showtabs));
  config->tabsmenu =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (tabsmenu));
  config->scrolltabs =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (scrolltabs));
  i = gtk_option_menu_get_history (GTK_OPTION_MENU (tabspos));
  switch (i)
    {
    case 1:
      config->tabspos = TABS_POS_BOTTOM;
      break;
    case 2:
      config->tabspos = TABS_POS_RIGHT;
      break;
    case 3:
      config->tabspos = TABS_POS_LEFT;
      break;
    default:
      config->tabspos = TABS_POS_TOP;
      break;
    }

  config->textwrap =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (textwrap));
  config->linenumbers =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (linenumbers));
#ifdef HAVE_SPELL
  config->spell_check =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (spell_check));

/* TODO: Modify existing documents */
  config->mispelled_red = da->style->bg[GTK_STATE_NORMAL].red;
  config->mispelled_blue = da->style->bg[GTK_STATE_NORMAL].blue;
  config->mispelled_green = da->style->bg[GTK_STATE_NORMAL].green;

/* default dict */
  if (config->default_dict)
    {
      g_free (config->default_dict);
    }
  config->default_dict =
    g_strdup ((gchar *) g_slist_nth_data
	      (config->dicts,
	       gtk_option_menu_get_history (GTK_OPTION_MENU (default_dict))));
#ifdef HAVE_OLD_SPELL
/* TODO: rebuild the dicts. menu */
  if (config->dicts_dir)
    {
      g_free (config->dicts_dir);
    }
  config->dicts_dir =
    g_strdup ((gchar *) gtk_entry_get_text (GTK_ENTRY (dicts_dir)));
#endif /* HAVE_OLD_SPELL */
#endif /* HAVE_SPELL */
  config->saveonexit =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (saveonexit));
  config->savewinpos =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (savewinpos));
#ifndef _WIN32
  config->disable_xft =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (disable_xft));
#endif /* _WIN32 */
  config->x = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (x));
  config->y = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (y));
  config->w = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (w));
  config->h = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (h));

#ifndef _WIN32
  config->locale_enc =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (locale_enc));
  config->special_enc =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (special_enc));
  if (config->special_enc)
    {
#endif /* _WIN32 */

      config->defenc =
	katoob_encodings_get_name_from_string ((gchar *) gtk_entry_get_text
					       (GTK_ENTRY
						(GTK_COMBO (encodings_combo)->
						 entry)));
#ifndef _WIN32
    }
#endif /* _WIN32 */

  config->default_font =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (default_font));

  if (config->font)
    {
      g_free (config->font);
    }
  config->font = g_strdup (gtk_label_get_text (GTK_LABEL (font_label)));
#ifdef ENABLE_HIGHLIGHT
  config->highlight =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (highlight));
#endif /* ENABLE_HIGHLIGHT */

  if (config->backup_ext)
    {
      g_free (config->backup_ext);
    }
  config->backup_ext =
    g_strdup ((gchar *) gtk_entry_get_text (GTK_ENTRY (backup_ext)));

  if (config->backup_dir)
    {
      g_free (config->backup_dir);
    }
  config->backup_dir =
    g_strdup ((gchar *) gtk_entry_get_text (GTK_ENTRY (backup_dir)));

  config->backup = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (backup));
  config->backup_in_same_dir =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (backup_in_same_dir));

#ifdef ENABLE_PRINT
  PConf->gui_show_advanced =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (advanced));
  PConf->dpi_x = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dpi_x));
  PConf->dpi_y = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dpi_y));
#endif /* ENABLE_PRINT */

  config->dict = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dict));

  if (gtk_entry_get_text (GTK_ENTRY (dict_host)))
    {
      g_free (config->dict_host);
      config->dict_host =
	g_strdup ((gchar *) gtk_entry_get_text (GTK_ENTRY (dict_host)));
    }
  config->dict_port =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dict_port));
  config->dict_timeout =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dict_timeout));
  {
    GtkTreeIter iter;
    GtkTreeSelection *selection;
    selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
    if (gtk_tree_selection_get_selected (selection, &dicts_model, &iter))
      {
	if (config->dict_db)
	  {
	    g_free (config->dict_db);
	  }
	gtk_tree_model_get (dicts_model, &iter, 0, &config->dict_db, -1);
      }
  }
  config->exec_cmd_size =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (exec_cmd_size));
  katoob_exec_cmd_set_size (config->exec_cmd_size);
}

void
katoob_create_font_selection_dialog ()
{
  GtkWidget *dlg;
  gchar *font = NULL;
  KATOOB_DEBUG_FUNCTION;

  dlg = gtk_font_selection_dialog_new (_("Select Font"));

  gtk_font_selection_dialog_set_font_name (GTK_FONT_SELECTION_DIALOG (dlg),
					   config->font);

  gtk_font_selection_dialog_set_preview_text (GTK_FONT_SELECTION_DIALOG (dlg),
					      PREVIEW_TEXT);

/*
gtk_widget_show(dlg);
*/
  if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_OK)
    {
/* Get the font name, update the button. */
      font =
	gtk_font_selection_dialog_get_font_name (GTK_FONT_SELECTION_DIALOG
						 (dlg));
      if (font)
	{
	  gchar *markup = g_strdup_printf
	    ("<span font_desc=\"%s\">%s</span>", font,
	     font);

	  gtk_label_set_markup (GTK_LABEL (font_label), markup);
	  g_free (markup);
	  g_free (font);
	}
    }
  gtk_widget_destroy (dlg);
}
