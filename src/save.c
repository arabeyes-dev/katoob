/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include <string.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <errno.h>
#include "mdi.h"
#include "file.h"
#include "save.h"
#include "misc.h"
#include "recent.h"

static gboolean
katoob_real_save (KatoobDocument * doc, gchar * title, KatoobSaveType type);

gboolean
katoob_save ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return FALSE;
    }
  if (katoob_document_get_readonly (doc))
    {
      return katoob_real_save (doc, _("Save As..."), KATOOB_SAVE_AS);
    }
  else
    {
      return katoob_real_save (doc, _("Save As..."), KATOOB_SAVE);
    }
}

gboolean
katoob_save_copy ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return FALSE;
    }
  return katoob_real_save (doc, _("Save A Copy..."), KATOOB_SAVE_COPY);
}

gboolean
katoob_save_as ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return FALSE;
    }
  return katoob_real_save (doc, _("Save As..."), KATOOB_SAVE_AS);
}

static gboolean
katoob_real_save (KatoobDocument * doc, gchar * title, KatoobSaveType type)
{
  extern conf *config;
  extern UI *katoob;

  gint result;
  gchar *c;
  FILE *fp;
  gboolean x, r;
  File *f = NULL;
  gchar *_tmp;

  KATOOB_DEBUG_FUNCTION;

  if ((type != KATOOB_SAVE)
      || ((type == KATOOB_SAVE) && (!katoob_document_get_file (doc))))
    {
      f = katoob_create_file_selection (title, FALSE);
      switch (f->result)
	{
	case GTK_RESPONSE_OK:
	  break;
	default:
	  g_strfreev (f->file);
	  g_free (f);
	  return FALSE;
	}
    }
  else
    {
      f = (File *) g_malloc (sizeof (File));
      f->encoding = katoob_document_get_encoding (doc);
      f->file = g_new (gchar *, 2);
      f->file[0] = g_strdup (katoob_document_get_file (doc));
      f->file[1] = NULL;
    }

  if (((x = g_file_test (f->file[0], G_FILE_TEST_EXISTS)) == TRUE)
      && (!katoob_document_get_file (doc) || (type == KATOOB_SAVE_AS)
	  || (type == KATOOB_SAVE_COPY)))
    {
      _tmp =
	g_strdup_printf (_
			 ("Are you sure you want to overwrite the file %s ?"),
			 f->file[0]);
      result = create_messagedialog (_tmp);
      g_free (_tmp);
      switch (result)
	{
	case GTK_RESPONSE_YES:
	  break;
	case GTK_RESPONSE_NO:
	  g_strfreev (f->file);
	  g_free (f);
	  switch (type)
	    {
	    case KATOOB_SAVE:
	      {
		return katoob_save ();
		break;
	      }
	    case KATOOB_SAVE_AS:
	      {
		return katoob_save_as ();
		break;
	      }
	    case KATOOB_SAVE_COPY:
	      {
		return katoob_save_copy ();
		break;
	      }
	    }
	default:
	  g_strfreev (f->file);
	  g_free (f);
	  return FALSE;
	}
    }

  if (config->backup)
    {
      gchar *tmp;
      gint i;

      katoob_debug (" Creating backup ");
      if (config->backup_in_same_dir)
	{
	  katoob_debug("In the same dir.");
	  tmp = g_strdup_printf ("%s%s", f->file[0], config->backup_ext);
	}
      else
	{
	  katoob_debug("Not in the same dir.");
	  tmp =
	    g_strdup_printf ("%s%s%s%s", config->backup_dir,
			     G_DIR_SEPARATOR_S,
			     g_path_get_basename (f->file[0]),
			     config->backup_ext);
	}
      katoob_debug (tmp);
      i = rename (f->file[0], tmp);
      g_free (tmp);
      if ((i != 0) && (i != -1))	/* error */
	{
	  i =
	    katoob_create_question (_
				    (" Couldn 't create a backup, Continue ?"));
	  switch (i)
	    {
	    case GTK_RESPONSE_YES:
	      break;
	    case GTK_RESPONSE_NO:
	      {
		g_strfreev (f->file);
		g_free (f);
		return FALSE;
	      }
	    }
	}
    }

  if ((fp = fopen (f->file[0], "w+")) == NULL)
    {
      katoob_error (_("The requested file couldn' t be opened for saving "));
      g_strfreev (f->file);
      g_free (f);
      return FALSE;
    }

  c = katoob_document_get_text (doc);
  r = katoob_write (c, f->encoding, fp, f->file[0]);
/* DON'T close fp cause katoob_real_write() close it */
  g_free (c);

  if (!r)
    {
      katoob_debug (" Failed to write ! ");
      g_strfreev (f->file);
      g_free (f);
      return FALSE;
    }
/* enum */
  switch (type)
    {
    case KATOOB_SAVE:
    case KATOOB_SAVE_AS:
      {
	katoob_document_set_modified (doc, FALSE);
	if ((!katoob_document_get_file (doc))
	    || (strcmp (katoob_document_get_file (doc), f->file[0])))
	  {
	    _tmp = g_strdup_printf ("Changing file name to %s", f->file[0]);
	    katoob_debug (_tmp);
	    g_free (_tmp);

	    katoob_document_set_file (doc, f->file[0]);
	    recent_append (f->file[0]);

/* We need to unset the readonly stuff. */
	    katoob_document_unset_readonly (doc);
/* 
 * If the user selects a new encoding for the document then we should save 
 * the new one. 
 */
	    if (katoob_document_get_encoding (doc) != f->encoding)
	      {
		katoob_document_set_encoding (doc, f->encoding);
/* Change the encodings menu. */
		gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
						(katoob->
						 encodings[f->encoding]),
						TRUE);
	      }
	  }
	/* If the user choosed the same file while saving, and changed the encoding, then we should update it. */
	else if
	  (!strcmp (katoob_document_get_file (doc), f->file[0]) && (katoob_document_get_encoding (doc) != f->encoding))
	  {
		katoob_document_set_encoding (doc, f->encoding);
/* Change the encodings menu. */
		gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
						(katoob->
						 encodings[f->encoding]),
						TRUE);
	  }
	break;
      }
    case KATOOB_SAVE_COPY:
      {
	break;
      }
    }
  g_strfreev (f->file);
  g_free (f);
  return TRUE;
}
