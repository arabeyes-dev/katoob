/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "katoob.h"
#include "fribidi.h"
#include "katoobdocument.h"
#include "bidi.h"
#include "katoob.h"

FriBidiCharType
katoob_bidi_get_line_dir (gunichar * ch, glong len)
{
  FriBidiCharType t;
  glong x = 0;
  KATOOB_DEBUG_FUNCTION;

  for (x = 0; x < len; ++x)
    {
      t = fribidi_get_type (ch[x]);

      if (t == FRIBIDI_TYPE_LTR)
	{
	  katoob_debug ("LTR DIR");
	  return FRIBIDI_TYPE_LTR;
	}
      else if ((t == FRIBIDI_TYPE_RTL) || (t == FRIBIDI_TYPE_AL))
	{
	  katoob_debug ("RTL DIR");
	  return FRIBIDI_TYPE_RTL;
	}
      else
	{
	  continue;
	}
    }
  return FRIBIDI_TYPE_LTR;
}

#ifndef GTK_2_4
/* TODO: Rewrite. */
void
katoob_bidi_modify_line_dir_from_utf8_string (KatoobDocument * doc,
					      gchar * text, GtkTextIter start,
					      GtkTextIter end)
{
  GtkTextBuffer *buffer = katoob_document_get_buffer (doc);
  GtkTextTag *tag_ltr = katoob_document_get_ltr_tag (doc);
  GtkTextTag *tag_rtl = katoob_document_get_rtl_tag (doc);

  glong l;
  FriBidiCharType dir = FRIBIDI_TYPE_WL;
  gunichar *ch = NULL;
  KATOOB_DEBUG_FUNCTION;

  ch = g_utf8_to_ucs4_fast (text, -1, &l);
  dir = katoob_bidi_get_line_dir (ch, l);
  g_free (ch);
  gtk_text_buffer_remove_tag (buffer, tag_rtl, &start, &end);
  gtk_text_buffer_remove_tag (buffer, tag_ltr, &start, &end);

  if (dir == FRIBIDI_TYPE_RTL)
    {
      gtk_text_buffer_apply_tag (buffer, tag_rtl, &start, &end);
    }
  else if (dir == FRIBIDI_TYPE_LTR)
    {
      gtk_text_buffer_apply_tag (buffer, tag_ltr, &start, &end);
    }
}
#endif /* GTK_2_4 */
