/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include "katoobwindow.h"
#include "katoobstatusbar.h"
#include "toolbar.h"
#include "menubar.h"

static void katoob_window_class_init (KatoobWindowClass * klass);
static void katoob_window_init (KatoobWindow * win);

/*
gboolean katoob_window_destroy_handler(GtkWidget * widget);
*/
gboolean katoob_window_delete_event_handler (GtkWidget * widget);

static gpointer parent_class;

GType
katoob_window_get_type (void)
{
  static GType katoob_window_type = 0;
  KATOOB_DEBUG_FUNCTION;

  if (!katoob_window_type)
    {
      static const GTypeInfo katoob_window_info = {
	sizeof (KatoobWindowClass),
	NULL,			/* base_init */
	NULL,			/* base_finalize */
	(GClassInitFunc) katoob_window_class_init,
	NULL,			/* class_finalize */
	NULL,			/* class_data */
	sizeof (KatoobWindow),
	0,			/* n_preallocs */
	(GInstanceInitFunc) katoob_window_init,
      };

      katoob_window_type =
	g_type_register_static (GTK_TYPE_WINDOW, "KatoobWindow",
				&katoob_window_info, (GTypeFlags)0);
    }

  return katoob_window_type;
}

static void
katoob_window_class_init (KatoobWindowClass * klass)
{
  KATOOB_DEBUG_FUNCTION;

  parent_class = g_type_class_peek_parent (klass);
}

static void
katoob_window_init (KatoobWindow * win)
{
  gchar *_tmp;
  GtkWidget *menubar;
  GdkPixbuf *icon;
  GtkWidget *ev;
  GtkWidget *box;
  extern conf *config;
  extern UI *katoob;
  KATOOB_DEBUG_FUNCTION;

  g_signal_connect (win,
		    "delete_event",
		    G_CALLBACK (katoob_window_delete_event_handler), NULL);

/*
  g_signal_connect (win,
                    "destroy",
                    G_CALLBACK (katoob_window_destroy_handler), NULL);
*/
  _tmp = g_strdup_printf ("%s%skatoob-small.png", PACKAGE_DATA_DIR, G_DIR_SEPARATOR_S);
katoob_debug(_tmp);
  icon = gdk_pixbuf_new_from_file (_tmp, NULL);
  g_free (_tmp);

  if (icon)
    {
      gtk_window_set_icon (GTK_WINDOW (win), icon);
    }
  else
    {
      g_warning ("Couldn't load the icon for the main window");
    }

  katoob_window_set_title (GTK_WIDGET (win), NULL);
/*
  gtk_window_set_position (GTK_WINDOW (win), GTK_WIN_POS_CENTER_ALWAYS);
*/
/* NOTE: The problem with this is that the window manager can ignore it! */
  gtk_window_move (GTK_WINDOW (win), config->x, config->y);
  gtk_window_set_default_size (GTK_WINDOW (win), config->w, config->h);

  gtk_window_set_wmclass (GTK_WINDOW (win), "main_window", PACKAGE);

  box = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (win), box);

  menubar = katoob_menubar_new (GTK_WIDGET (win));
  gtk_box_pack_start (GTK_BOX (box), menubar, FALSE, FALSE, 0);

  ev = gtk_event_box_new ();
  gtk_box_pack_start (GTK_BOX (box), ev, FALSE, FALSE, 0);

  win->toolbar = katoob_toolbar_new (GTK_WIDGET (win));
  gtk_container_add (GTK_CONTAINER (ev), win->toolbar);
  win->extended_toolbar = gtk_toolbar_new ();
  gtk_box_pack_start (GTK_BOX (box), win->extended_toolbar, FALSE, FALSE, 0);

  katoob_populate_extended_toolbar (win->extended_toolbar, GTK_WIDGET (win));

  win->notebook = gtk_notebook_new ();
  gtk_box_pack_start (GTK_BOX (box), win->notebook, TRUE, TRUE, 0);

  win->statusbar = katoob_statusbar_new ();
  gtk_box_pack_start (GTK_BOX (box), win->statusbar, FALSE, FALSE, 0);
  katoob->clipboard = gtk_clipboard_get (GDK_NONE);

  g_signal_connect (G_OBJECT (ev), "event",
		    G_CALLBACK (katoob_popup_toolbar_menu), NULL);
}

GtkWidget *
katoob_window_new ()
{
  KATOOB_DEBUG_FUNCTION;

  return (GtkWidget *)g_object_new (KATOOB_TYPE_WINDOW, NULL);
}

/*
gboolean
katoob_window_destroy_handler(GtkWidget * widget)
{
  return katoob_exit ();
}
*/

gboolean
katoob_window_delete_event_handler (GtkWidget * widget)
{
  KATOOB_DEBUG_FUNCTION;

  return katoob_exit ();
}

void
katoob_window_set_title (GtkWidget * win, gchar * title)
{
  gchar *_tmp;

  KATOOB_DEBUG_FUNCTION;

  if (!title)
    {
      _tmp = g_strdup_printf ("%s %s", PACKAGE, VERSION);
      gtk_window_set_title (GTK_WINDOW (win), _tmp);
      g_free (_tmp);
    }
  else
    {
      _tmp = g_strdup_printf ("%s - %s %s", title, PACKAGE, VERSION);
      gtk_window_set_title (GTK_WINDOW (win), _tmp);
      g_free (_tmp);
    }
}
