/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include <gtk/gtk.h>
#include "katooblabel.h"
#include "mdi.h"

extern conf *config;

struct _KatoobLabelPrivate
{
  GtkWidget *label;
  GtkWidget *button;
  GtkWidget *close;
  gboolean modified;
  gboolean show_close;
};

static void katoob_label_class_init (KatoobLabelClass * klass);
static void katoob_label_init (KatoobLabel * label);

static GtkHBoxClass *parent_class;

GType
katoob_label_get_type (void)
{
  static GType label_type = 0;
  KATOOB_DEBUG_FUNCTION;

  if (!label_type)
    {
      static const GTypeInfo label_info = {
	sizeof (KatoobLabelClass),
	NULL,
	NULL,
	(GClassInitFunc) katoob_label_class_init,
	NULL,
	NULL,
	sizeof (KatoobLabel),
	0,
	(GInstanceInitFunc) katoob_label_init,
      };
      label_type =
	g_type_register_static (GTK_TYPE_HBOX, "KatoobLabel", &label_info, (GTypeFlags)0);
    }
  return label_type;
}

static void
katoob_label_class_init (KatoobLabelClass * klass)
{
  KATOOB_DEBUG_FUNCTION;
  parent_class = (GtkHBoxClass *)g_type_class_peek_parent (klass);
}

static void
katoob_label_init (KatoobLabel * label)
{
  KATOOB_DEBUG_FUNCTION;

  label->priv = g_new0 (KatoobLabelPrivate, 1);

  label->priv->modified = FALSE;
  label->priv->show_close = config->showclose;

  label->priv->label = gtk_label_new ("");
  gtk_box_pack_start (GTK_BOX (label), label->priv->label, FALSE, FALSE, 0);
  if (label->priv->show_close)
    {
      katoob_label_create_close_button (label);
    }
}

void
katoob_label_create_close_button (KatoobLabel * label)
{
  gchar *_tmp;
  KATOOB_DEBUG_FUNCTION;

  label->priv->button = gtk_button_new ();
  gtk_widget_set_size_request (label->priv->button, 15, 15);
  gtk_button_set_relief (GTK_BUTTON (label->priv->button), GTK_RELIEF_NONE);
  gtk_box_pack_start (GTK_BOX (label), label->priv->button, FALSE, FALSE, 0);
  _tmp = g_strdup_printf ("%s%sclose.png", PACKAGE_DATA_DIR, G_DIR_SEPARATOR_S);
  label->priv->close = gtk_image_new_from_file (_tmp);
  g_free (_tmp);
  gtk_container_add (GTK_CONTAINER (label->priv->button), label->priv->close);
  gtk_widget_show_all (label->priv->button);
  label->priv->show_close = TRUE;
}

void
katoob_label_destroy_close_button (KatoobLabel * label)
{
  KATOOB_DEBUG_FUNCTION;

  gtk_widget_destroy (label->priv->close);
  gtk_widget_destroy (label->priv->button);
  label->priv->show_close = FALSE;
}

GtkWidget *
katoob_label_new (gchar * str)
{
  KatoobLabel *label = (KatoobLabel *)g_object_new (KATOOB_TYPE_LABEL, NULL);
  KATOOB_DEBUG_FUNCTION;

  katoob_label_set_text (label, str);
  return GTK_WIDGET (label);
}

const gchar *
katoob_label_get_text (KatoobLabel * label)
{
  KATOOB_DEBUG_FUNCTION;

  return gtk_label_get_text (GTK_LABEL (label->priv->label));
}

void
katoob_label_set_text (KatoobLabel * label, gchar * str)
{
  KATOOB_DEBUG_FUNCTION;

  gtk_label_set_text (GTK_LABEL (label->priv->label), str);
}

void
katoob_label_set_modified (KatoobLabel * label, gboolean mod)
{
  KATOOB_DEBUG_FUNCTION;

  if (mod)
    {
      gchar *str = g_strdup_printf ("<span foreground=\"red\">%s</span>",
				    katoob_label_get_text (label));
      gtk_label_set_markup (GTK_LABEL (label->priv->label), str);
      g_free (str);
    }
  else
    {
      gchar *str = g_strdup (katoob_label_get_text (label));
      gtk_label_set_text (GTK_LABEL (label->priv->label), str);
      g_free (str);
    }
}

gboolean
katoob_label_get_modified (KatoobLabel * label)
{
  KATOOB_DEBUG_FUNCTION;

  return label->priv->modified;
}

GtkWidget *
katoob_label_get_close_button (KatoobLabel * label)
{
  KATOOB_DEBUG_FUNCTION;

  return label->priv->button;
}

void
katoob_label_show_close_button (KatoobLabel * label, KatoobDocument * doc)
{
  KATOOB_DEBUG_FUNCTION;

  if (!GTK_IS_WIDGET (label->priv->button))
    {
      katoob_label_create_close_button (label);
      g_signal_connect (G_OBJECT
			(label->priv->button),
			"clicked", G_CALLBACK (katoob_close_button_handler),
			doc);
    }
  gtk_widget_show (label->priv->button);
}

void
katoob_label_hide_close_button (KatoobLabel * label, KatoobDocument * doc)
{
  KATOOB_DEBUG_FUNCTION;

  if (!GTK_IS_WIDGET (label->priv->button))
    {
      katoob_label_create_close_button (label);
      g_signal_connect (G_OBJECT
			(label->priv->button),
			"clicked", G_CALLBACK (katoob_close_button_handler),
			doc);

    }
  gtk_widget_hide (label->priv->button);
}
