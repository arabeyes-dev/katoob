/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef __KATOOB_DOCUMENT_H__
#define __KATOOB_DOCUMENT_H__

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#include <gtk/gtk.h>
#ifdef HAVE_SPELL
#include "spell.h"
#include "spell-private.h"
#endif				/* HAVE_SPELL */

#ifdef ENABLE_HIGHLIGHT
#include <gtksourceview/gtksourcelanguagesmanager.h>
#endif				/* ENABLE_HIGHLIGHT */

#define KATOOB_TYPE_DOCUMENT                  (katoob_document_get_type ())
#define KATOOB_DOCUMENT(obj)                  (GTK_CHECK_CAST ((obj), KATOOB_TYPE_DOCUMENT, KatoobDocument))
#define KATOOB_DOCUMENT_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), KATOOB_TYPE_DOCUMENT, KatoobDocumentClass))
#define KATOOB_IS_DOCUMENT(obj)               (GTK_CHECK_TYPE ((obj), KATOOB_TYPE_DOCUMENT))
#define KATOOB_IS_DOCUMENT_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), KATOOB_TYPE_DOCUMENT))
#define KATOOB_DOCUMENT_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), KATOOB_TYPE_DOCUMENT, KatoobDocumentClass))

  typedef struct _KatoobDocument KatoobDocument;
  typedef struct _KatoobDocumentClass KatoobDocumentClass;
  typedef struct _KatoobDocumentPrivate KatoobDocumentPrivate;

  struct _KatoobDocument
  {
    GtkVBox vbox;
    KatoobDocumentPrivate *priv;
  };

  struct _KatoobDocumentClass
  {
    GtkVBoxClass parent_class;

    /* Signal handlers */
    void (*file_changed) (KatoobDocument * document, gchar * file);
    void (*buffer_modified) (KatoobDocument * document, gboolean modified);
    void (*cursor_moved) (KatoobDocument * document, gint col, gint row);
    void (*readonly_set) (KatoobDocument * document);
    void (*readonly_unset) (KatoobDocument * document);
    void (*encoding_changed) (KatoobDocument * document, gint enc);
    void (*text_dir_changed) (KatoobDocument * document, gint text_dir);
    void (*match_case_changed) (KatoobDocument * document,
				gboolean match_case);
    void (*beginning_changed) (KatoobDocument * document, gboolean beginning);
    void (*can_undo) (KatoobDocument * document, gboolean state);
    void (*can_redo) (KatoobDocument * document, gboolean state);

#ifdef HAVE_SPELL
    void (*spell_toggled) (KatoobDocument * document, gboolean state);
#endif				/* HAVE_SPELL */
  };

  typedef enum
  {
    KATOOB_UNDO_TYPE_INSERT,
    KATOOB_UNDO_TYPE_DELETE,
    KATOOB_UNDO_TYPE_REPLACE
  }
  KatoobUndoType;

#ifdef ENABLE_HIGHLIGHT
  typedef enum
  {
    KATOOB_HIGHLIGHT_NONE,
    KATOOB_HIGHLIGHT_ADA,
    KATOOB_HIGHLIGHT_C,
    KATOOB_HIGHLIGHT_CPP,
    KATOOB_HIGHLIGHT_DESKTOP,
    KATOOB_HIGHLIGHT_DIFF,
    KATOOB_HIGHLIGHT_HTML,
    KATOOB_HIGHLIGHT_IDL,
    KATOOB_HIGHLIGHT_JAVA,
    KATOOB_HIGHLIGHT_LATEX,
    KATOOB_HIGHLIGHT_PERL,
    KATOOB_HIGHLIGHT_PO,
    KATOOB_HIGHLIGHT_PYTHON,
    KATOOB_HIGHLIGHT_XML
  }
  KatoobHighlightType;

#endif				/* ENABLE_HIGHLIGHT */

  typedef enum
  {
    KATOOB_DOCUMENT_TEXT,
    KATOOB_DOCUMENT_CODE
  }
  KatoobDocumentType;

  GType katoob_document_get_type (void) G_GNUC_CONST;
  GtkWidget *katoob_document_new (gchar * title);
  GtkWidget *katoob_document_new_from_file (gchar * file);
  void katoob_document_set_file (KatoobDocument * doc, gchar * file);
  gchar *katoob_document_get_file (KatoobDocument * doc);
  GtkWidget *katoob_document_new_from_text (gchar * text, gchar * title);
  void katoob_document_set_title (KatoobDocument * doc, gchar * title);
  void katoob_document_set_readonly (KatoobDocument * doc);
  void katoob_document_unset_readonly (KatoobDocument * doc);
  gboolean katoob_document_get_readonly (KatoobDocument * doc);
  void katoob_document_set_bidi (KatoobDocument * doc, gint text_dir);
  GtkTextBuffer *katoob_document_get_buffer (KatoobDocument * doc);
  GtkTextTag *katoob_document_get_ltr_tag (KatoobDocument * doc);
  GtkTextTag *katoob_document_get_rtl_tag (KatoobDocument * doc);
  GtkWidget *katoob_document_get_menu (KatoobDocument * doc);
  void katoob_document_set_text (KatoobDocument * doc, gchar * text);
  void katoob_document_set_text_with_undo (KatoobDocument * doc,
					   gchar * text);
  void katoob_document_set_encoding (KatoobDocument * doc, gint enc);
  gint katoob_document_get_encoding (KatoobDocument * doc);
  GtkWidget *katoob_document_get_label (KatoobDocument * doc);
  const gchar *katoob_document_get_label_text (KatoobDocument * doc);
  void katoob_document_set_modified (KatoobDocument * doc, gboolean modified);
  gboolean katoob_document_get_modified (KatoobDocument * doc);
  gchar *katoob_document_get_text (KatoobDocument * doc);
  GList *katoob_document_get_undo (KatoobDocument * doc);
  GList *katoob_document_get_redo (KatoobDocument * doc);
  void katoob_document_add_undo (KatoobDocument * doc, KatoobUndoType type,
				 gchar * text, gchar * text2, gint start);
  void katoob_document_undo (KatoobDocument * doc);
  void katoob_document_redo (KatoobDocument * doc);
  gboolean katoob_document_has_focus (KatoobDocument * doc);
  GtkTextIter katoob_document_get_iter_at_insertion_mark (KatoobDocument *
							  doc);
  void katoob_document_insert_text (KatoobDocument * doc, GtkTextIter iter,
				    gchar * text, gint len);
  void katoob_document_cut (KatoobDocument * doc);
  void katoob_document_copy (KatoobDocument * doc);
  void katoob_document_delete (KatoobDocument * doc);
  void katoob_document_paste (KatoobDocument * doc);
  void katoob_document_select_all (KatoobDocument * doc);
  void katoob_document_goto_line (KatoobDocument * doc, gint n);
  void katoob_document_reset_undo_redo (KatoobDocument * doc);
  void katoob_document_grab_focus (KatoobDocument * doc);
  void katoob_document_enable_line_numbers (KatoobDocument * doc);
  void katoob_document_disable_line_numbers (KatoobDocument * doc);
  void katoob_document_enable_text_wrap (KatoobDocument * doc);
  void katoob_document_disable_text_wrap (KatoobDocument * doc);
  gboolean katoob_document_get_textwrap (KatoobDocument * doc);
  gboolean katoob_document_get_linenumbers (KatoobDocument * doc);
  void katoob_document_get_cursor_position (KatoobDocument * doc, gint * col,
					    gint * lin);
  gboolean katoob_document_search (KatoobDocument * doc);
  gint katoob_document_replace_all (KatoobDocument * doc);
  gboolean katoob_document_replace (KatoobDocument * doc);
  gchar *katoob_document_get_last_searched (KatoobDocument * doc);
  gchar *katoob_document_get_last_replaced (KatoobDocument * doc);
  gboolean katoob_document_get_match_case (KatoobDocument * doc);
  gboolean katoob_document_get_beginning (KatoobDocument * doc);
  void katoob_document_set_last_searched (KatoobDocument * doc,
					  gchar * last_searched);
  void katoob_document_set_last_replaced (KatoobDocument * doc,
					  gchar * last_replaced);
  void katoob_document_set_match_case (KatoobDocument * doc,
				       gboolean match_case);
  void katoob_document_set_beginning (KatoobDocument * doc,
				      gboolean beginning);
  gboolean katoob_document_get_selection_bounds (KatoobDocument * doc,
						 GtkTextIter * start,
						 GtkTextIter * end);
  void katoob_document_set_font (KatoobDocument * doc,
				 PangoFontDescription * fd);
  gboolean katoob_document_get_overwrite (KatoobDocument * doc);

  gboolean katoob_document_get_can_undo (KatoobDocument * doc);
  gboolean katoob_document_get_can_redo (KatoobDocument * doc);

  GtkTextView *katoob_document_get_text_view (KatoobDocument * doc);

#ifdef HAVE_SPELL
  AspellSpeller *katoob_document_get_speller (KatoobDocument * doc);
  GtkTextTag *katoob_document_get_highlight_tag (KatoobDocument * doc);
  GtkTextMark *katoob_document_get_mark (KatoobDocument * doc);
  gboolean katoob_document_set_dictionary (KatoobDocument * doc,
					   gchar * lang);
  void katoob_document_enable_spell_checker (KatoobDocument * doc);
  void katoob_document_disable_spell_checker (KatoobDocument * doc);
  gboolean katoob_document_get_spell_checker (KatoobDocument * doc);
  gint katoob_document_get_dictionary (KatoobDocument * doc);
  void katoob_document_recheck_spell (KatoobDocument * doc);
#endif				/* HAVE_SPELL */

#ifdef ENABLE_HIGHLIGHT
  void katoob_document_set_highlight (KatoobDocument * doc,
				      GtkSourceLanguagesManager * manager,
				      KatoobHighlightType t);
  KatoobHighlightType katoob_document_get_highlight_language (KatoobDocument *
							      doc);
#endif				/* ENABLE_HIGHLIGHT */

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* __KATOOB_DOCUMENT_H__ */
