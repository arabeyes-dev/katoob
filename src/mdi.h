/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "katoobdocument.h"

#ifndef __MDI_H__
#define __MDI_H__

KatoobDocument *katoob_get_active_doc ();
void katoob_create_doc_from_file (gchar * file,
				  gboolean addtorecent, gint encoding);
void katoob_create_doc ();
void katoob_create_stdin_doc (gchar * file);
gboolean katoob_close_doc (KatoobDocument * doc);
gboolean katoob_real_close_doc (KatoobDocument * doc);
void katoob_close_button_handler (GtkWidget * w, KatoobDocument * doc);
void katoob_close_active_doc ();
void katoob_save_all ();
void katoob_close_all ();

#endif /* __MDI_H__ */
