/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* TODO: Remove this soon! */
#ifdef _WIN32
int isblank(char x)
{
  if ((x == ' ') || (x == '\t'))
    {
      return 1;
    }
  else {
    return 0;
  }
}
#endif /* _WIN32 */

/*
 * Here is how we are supposed to handle configuration file loading and saving:
 * Add the code to katoob_load_all_config()
 * Simply call config_load() with 2 arguments: the name of the file to load, 
 * The file'll be read from ~/.katoob/
 * The second argument is a pointer to a function
 * that'll populate your configuration struct.
 * config_load()'ll parse the file, put all values in a hash table,
 * then it'll call the function you passed with the hash table as an argument.
 * The hash table is free'ed after the function you provided returns.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif /* _GNU_SOURCE */

#include "katoob.h"

#include <stdlib.h>
#include <stdio.h>		/* remove() */
#include <sys/stat.h>		/* mkdir() */
#include <sys/types.h>		/* mkdir() */
#include <glib.h>
#include <string.h>		/* strcmp() */
#include <ctype.h>		/* isblank() */
#include "conf.h"
#include "spell.h"
#include "misc.h"

#ifdef ENABLE_PRINT
#include "print.h"
#endif /* ENABLE_PRINT */

#define LINE_LEN 4096

char buff[LINE_LEN];

int key_len = 100;

typedef void (*KatoobConfFillFunc) (GHashTable * hash);

void katoob_fill_config_struct (GHashTable * hash);

#ifdef ENABLE_PRINT
void katoob_fill_print_struct (GHashTable * hash);
#endif /* ENABLE_PRINT */

void config_init ();
void config_free ();
void config_load (gchar * file, KatoobConfFillFunc katoob_struct_fill_func);
void config_save ();

#ifdef ENABLE_PRINT
//void print_config_load (KatoobConfFillFunc katoob_struct_fill_func);
void print_config_save ();
void print_config_init ();
void print_config_free ();
#endif /* ENABLE_PRINT */

gboolean pre_config (gchar * file);
static gchar *decode_font (gchar * font);
static gchar *encode_font (gchar * font);
static void handle_key (GHashTable * hash, char *key, char *line);
static void config_struct_foreach (gpointer _key, gpointer _value);

#ifdef ENABLE_PRINT
static void print_struct_foreach (gpointer _key, gpointer _value);
#endif /* ENABLE_PRINT */

/*
 * This function'll call *config_init() to set the hardcoded defaults
 * and it'll call config_load() for every configuration file in the
 * syntax "name = value" we know about.
 */
void
katoob_load_all_config ()
{
/* Set the default configuration options */
  config_init ();

/* Load the configuration file */
  config_load ("config", katoob_fill_config_struct);

#ifdef ENABLE_PRINT
  print_config_init ();
  config_load ("print", katoob_fill_print_struct);
  /*
     print_config_load (katoob_fill_print_struct);
   */
#endif /* ENABLE_PRINT */
}

/*
 * Takes the token after the "=", strips leading and trailing spaces
 * And puts it into the hash table.
 */
static void
handle_key (GHashTable * hash, char *key, char *line)
{
  int x = 0;

  /* We should now strip any leading and trailing spaces. */
  while (line[x])
    {
      if (!isblank (line[x]))
	{
	  g_hash_table_insert (hash, g_strdup (key), g_strdup (&line[x]));
	  /*	  printf("Parsed: key %s\tvalue %s\n", key, &line[x]); */
	  return;
	}
      ++x;
    }
}

/*
 * This function sets all the hardcoded defaults for the main configuration struct.
 */
void
config_init ()
{
  extern conf *config;
  gchar *wd = NULL;

  KATOOB_DEBUG_FUNCTION;

  config->x = 0;
  config->y = 25;
  config->w = 793;
  config->h = 514;
  config->recentno = 10;
  config->tabsmenu = TRUE;
  config->showtabs = TRUE;
  config->showclose = TRUE;
  config->toolbar = TRUE;
  config->extended_toolbar = TRUE;
  config->statusbar = TRUE;
  config->savewinpos = TRUE;
  config->saveonexit = TRUE;
  config->textwrap = TRUE;
  config->text_dir = KATOOB_BIDI_AUT;
  config->tabspos = TABS_POS_TOP;
  config->toolbartype = g_strdup ("both");
  config->recent = TRUE;
#ifndef _WIN32
  config->xkb_err_dlg = TRUE;
  config->xkb = TRUE;
  config->disable_xft = FALSE;
#endif /* _WIN32 */
  config->tabsmenu = TRUE;
  config->scrolltabs = TRUE;
  config->undo = TRUE;
  config->undono = 0;
#ifdef HAVE_SPELL
  config->spell_check = TRUE;
  config->mispelled_red = 65535;
  config->mispelled_blue = 0;
  config->mispelled_green = 0;
  config->dicts_dir = g_strdup (DICTS_DIR);
#endif /* HAVE_SPELL */
  config->emulator = TRUE;
  config->defenc = -1;
  config->default_font = TRUE;
#ifdef ENABLE_HIGHLIGHT
  config->highlight = TRUE;
#endif /* ENABLE_HIGHLIGHT */
  config->backup = FALSE;
  config->backup_in_same_dir = FALSE;
  config->backup_ext = g_strdup ("~");
  config->backup_dir = g_strdup (g_get_home_dir ());
  wd = g_get_current_dir ();
  if (!wd)
    {
      wd = g_strdup (g_get_home_dir ());
    }
  config->filesel_path = g_strconcat (wd, G_DIR_SEPARATOR_S, NULL);
  g_free (wd);
  config->dict = TRUE;
  config->dict_host = g_strdup ("dict.arabeyes.org");
  config->dict_port = 2628;
  config->dict_timeout = 1;
/*
config->dict_user = NULL;
config->dict_pw = NULL;
*/
  config->dict_db = NULL;
  config->exec_cmd = NULL;
  config->exec_cmd_in_new = TRUE;
  config->exec_cmd_size = 10;
}

/*
 * This function checks if ~/.katoob exists and is a directory,
 * If it's a file we'll remove it and create a directory instead.
 * And it'll check the existance of the configuration file passed, otherwise
 * It'll create an empty one.
 * Return FALSE if ~/.katoob is a file and can't be removed, 
 * Or if the configuration file doesn't exists and we couldn't create one.
 */
gboolean
pre_config (gchar * file)
{
  gint x;
  FILE *tmp;
  gchar *buffer;
  KATOOB_DEBUG_FUNCTION;

/**************************************************************************
 * If the user had used 0.1 before "the configuration file was ~/.katoob, *
 * Then we'll sadly erase the file ;-)                                    *
 **************************************************************************/
  buffer =
    g_strdup_printf ("%s%s.katoob", g_get_home_dir (), G_DIR_SEPARATOR_S);
  if (g_file_test (buffer, G_FILE_TEST_IS_REGULAR))
    {
      x = remove (buffer);
      if (x != 0)
	{
	  g_warning ("Can't remove old configuration file");
	  g_free (buffer);
	  return FALSE;
	}
    }
/* The first time to run, No ~/.katoob directory */
  if (!g_file_test (buffer, G_FILE_TEST_IS_DIR))
    {
/* Try to make it */
      x = mkdir (buffer, 0700);
      if (x != 0)
	{
	  g_warning ("Can't create configuration directory");
	  g_free (buffer);
	  return FALSE;
	}
      g_free (buffer);
/* Try to make an empty configuration file */
      buffer =
	g_strdup_printf ("%s%s.katoob/%s", g_get_home_dir (),
			 G_DIR_SEPARATOR_S, file);
      tmp = fopen (buffer, "w");
      g_free (buffer);
      if (!tmp)
	{
	  g_warning ("Can't create configuration file");
	  return FALSE;
	}
      fclose (tmp);
    }
  return TRUE;
}

/*
 * Reads ~/.katoob/<file>, populate a hashtable with the contents.
 * The file must be in the form: key = value
 * After populating the hash table, It calls the function passed to fill the
 * configuration struct, then destroy the hash table.
 * The function calls handle_key() to populate the hash.
 */
void
config_load (gchar * file, KatoobConfFillFunc katoob_struct_fill_func)
{
  int i;
  gchar *configfile, *a;
  FILE *prefs;
  GHashTable *hash;

  KATOOB_DEBUG_FUNCTION;

  if (!pre_config (file))
    {
      return;
    }

  configfile =
    g_strdup_printf ("%s%s.katoob%s%s", g_get_home_dir (),
		     G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S, file);

  prefs = fopen (configfile, "r");
  g_free (configfile);

  if (!prefs)
    {
      gchar *tmp =
	g_strdup_printf ("Couldn't open configuration file %s\n", file);
      katoob_debug (tmp);
      g_free (tmp);
      return;
    }

  hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

  a = (char *) g_malloc (key_len * sizeof (char));
  while (fgets (buff, LINE_LEN, prefs) != NULL)
    {
      int j;
      i = strlen (buff);

      if (buff[i - 1] != '\n')
	{
	  char c;
	  while ((c = fgetc (prefs)) != EOF)
	    {
	      if (c == '\n')
		{
		  break;
		}
	    }
	}
      else
	{
	  buff[i - 1] = '\0';
	}
      /* begin parsing routine */
      for (j = 0; j < i; j++)
	{
	  if (buff[j] == '=')
	    {
	      int x = 0;
	      char *b;
	      if (j > key_len - 1)
		{
		  a = (char *) g_realloc (a, j + 1);
		  key_len = j + 1;
		}
	      b = a;
	      for (x = 0; x < j; x++)
		{
		  if (!isblank (buff[x]))
		    {
		      *b = buff[x];
		      ++b;
		    }
		}
	      *b = '\0';
	      handle_key (hash, a, &buff[++x]);
	      break;
	    }
	}
    }
/* NOTE: Free this here only as it gets reallocated not malloced in the loop if required. */
  g_free (a);
  fclose (prefs);
  katoob_struct_fill_func (hash);
  g_hash_table_destroy (hash);
}

void
config_save ()
{
  extern conf *config;
  extern UI *katoob;
  gchar *configfile;
  FILE *prefs;
  KATOOB_DEBUG_FUNCTION;

  if (!config->saveonexit)
    {
      config_free ();
      config = (conf *) g_malloc (sizeof (conf));
      config_init ();
      config_load ("config", katoob_fill_config_struct);
      config->saveonexit = FALSE;
    }

  if (!pre_config ("config"))
    {
      return;
    }

  configfile =
    g_strdup_printf ("%s%s.katoob%sconfig", g_get_home_dir (),
		     G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S);
  prefs = fopen (configfile, "w");
  if (!prefs)
    {
      g_warning ("Can't write configuration file");
      g_free (configfile);
      return;
    }

  if (config->savewinpos)
    {
      katoob_debug ("Getting the main window position");
      gtk_window_get_position (GTK_WINDOW (katoob->win), &config->x,
			       &config->y);
      gtk_window_get_size (GTK_WINDOW (katoob->win), &config->w, &config->h);
    }

  if (config->saved_enc)
    {
      g_free (config->saved_enc);
    }

  config->saved_enc = katoob_encodings_get_by_number (config->defenc);

/*  fprintf (prefs, "ver = %s\n", VERSION); */
  fprintf (prefs, "x = %i\n", config->x);
  fprintf (prefs, "y = %i\n", config->y);
  fprintf (prefs, "w = %i\n", config->w);
  fprintf (prefs, "h = %i\n", config->h);
  fprintf (prefs, "recentno = %i\n", config->recentno);
  fprintf (prefs, "tabsmenu = %i\n", config->tabsmenu);
  fprintf (prefs, "toolbar = %i\n", config->toolbar);
  fprintf (prefs, "extended_toolbar = %i\n", config->extended_toolbar);
  fprintf (prefs, "statusbar = %i\n", config->statusbar);
  fprintf (prefs, "savewinpos = %i\n", config->savewinpos);
  fprintf (prefs, "saveonexit = %i\n", config->saveonexit);
  fprintf (prefs, "textwrap = %i\n", config->textwrap);
  fprintf (prefs, "text_dir = %i\n", config->text_dir);
  fprintf (prefs, "recent = %i\n", config->recent);
  fprintf (prefs, "tabsmenu = %i\n", config->tabsmenu);
  fprintf (prefs, "showtabs = %i\n", config->showtabs);
  fprintf (prefs, "showclose = %i\n", config->showclose);
  fprintf (prefs, "scrolltabs = %i\n", config->scrolltabs);
  fprintf (prefs, "tabspos = %i\n", config->tabspos);
  fprintf (prefs, "toolbartype = %s\n", config->toolbartype);
  fprintf (prefs, "filesno = %i\n", config->filesno);
#ifndef _WIN32
  fprintf (prefs, "xkb_err_dlg = %i\n", config->xkb_err_dlg);
  fprintf (prefs, "xkb = %i\n", config->xkb);
#endif /* _WIN32 */
  fprintf (prefs, "undo = %i\n", config->undo);
  fprintf (prefs, "undono = %i\n", config->undono);
#ifdef HAVE_SPELL
  fprintf (prefs, "spell_check = %i\n", config->spell_check);
  fprintf (prefs, "mispelled_red = %u\n", config->mispelled_red);
  fprintf (prefs, "mispelled_blue = %u\n", config->mispelled_blue);
  fprintf (prefs, "mispelled_green = %u\n", config->mispelled_green);
  if (config->dicts_dir)
    {
      fprintf (prefs, "dicts_dir = %s\n", config->dicts_dir);
    }
  if (config->default_dict)
    {
      fprintf (prefs, "default_dict = %s\n", config->default_dict);
    }
#endif /* HAVE_SPELL */
  fprintf (prefs, "linenumbers = %i\n", config->linenumbers);
  fprintf (prefs, "emulator = %i\n", config->emulator);
#ifndef _WIN32
  fprintf (prefs, "disable_xft = %i\n", config->disable_xft);
  fprintf (prefs, "locale_enc = %i\n", config->locale_enc);
#endif /* _WIN32 */
  fprintf (prefs, "special_enc = %i\n", config->special_enc);
  fprintf (prefs, "saved_enc = %s\n", config->saved_enc);
  if (config->font)
    {
      fprintf (prefs, "font = %s\n", encode_font (config->font));
    }
  fprintf (prefs, "default_font = %i\n", config->default_font);
#ifdef ENABLE_HIGHLIGHT
  fprintf (prefs, "highlight = %i\n", config->highlight);
#endif /* ENABLE_HIGHLIGHT */
  fprintf (prefs, "backup = %i\n", config->backup);
  fprintf (prefs, "backup_in_same_dir = %i\n", config->backup_in_same_dir);
  if (config->backup_ext)
    {
      fprintf (prefs, "backup_ext = %s\n", config->backup_ext);
    }
  if (config->backup_dir)
    {
      fprintf (prefs, "backup_dir = %s\n", config->backup_dir);
    }
  /* Don't remember accross the sessions. */
  /*
     fprintf (prefs, "filesel_path = %s\n", config->filesel_path);
   */
  if (config->dict_host)
    {
      fprintf (prefs, "dict_host = %s\n", config->dict_host);
    }
  fprintf (prefs, "dict = %i\n", config->dict);
  fprintf (prefs, "dict_port = %i\n", config->dict_port);
  fprintf (prefs, "dict_timeout = %i\n", config->dict_timeout);
  if (config->dict_db)
    {
      fprintf (prefs, "dict_db = %s\n", config->dict_db);
    }
  fprintf (prefs, "exec_cmd_in_new = %i\n", config->exec_cmd_in_new);
  fprintf (prefs, "exec_cmd_size = %i\n", config->exec_cmd_size);
  fclose (prefs);
  g_free (configfile);
  return;
}

/*
 * free the main configuration struct.
 */
void
config_free ()
{
  extern conf *config;
  KATOOB_DEBUG_FUNCTION;

  if (config->font)
    {
      g_free (config->font);
    }
#ifdef HAVE_SPELL
  if (config->dicts_dir)
    {
      g_free (config->dicts_dir);
    }
  if (config->default_dict)
    {
      g_free (config->default_dict);
    }
#endif /* HAVE_SPELL */

  g_free (config->toolbartype);
  g_free (config->filesel_path);
  if (config->dict_host)
    {
      g_free (config->dict_host);
    }
  if (config->dict_db)
    {
      g_free (config->dict_db);
    }
  g_free (config);
  config = NULL;
}

/*
 * ~s/ /_/
 */
static gchar *
encode_font (gchar * font)
{
  gchar *new_font = font;
  KATOOB_DEBUG_FUNCTION;
  if (!font)
    {
      return NULL;
    }
  katoob_debug (font);
  while (*new_font)
    {
      if (*new_font == ' ')
	{
	  *new_font = '_';
	}
      new_font++;
    }
  katoob_debug (font);
  return font;
}

/*
 * ~s/_/ /
 */
static gchar *
decode_font (gchar * font)
{
  gchar *new_font = font;
  KATOOB_DEBUG_FUNCTION;
  katoob_debug (font);
  while (*new_font)
    {
      if (*new_font == '_')
	{
	  *new_font = ' ';
	}
      new_font++;
    }
  katoob_debug (font);
  return font;
}

void
katoob_fill_config_struct (GHashTable * hash)
{
  KATOOB_DEBUG_FUNCTION;
  g_hash_table_foreach (hash, (GHFunc) config_struct_foreach, NULL);
}

static void
config_struct_foreach (gpointer _key, gpointer _value)
{
  extern conf *config;
  gchar *key = (gchar *) _key;
  gchar *value = (gchar *) _value;

  gchar *tmp;
  tmp = g_strdup_printf("%s: %s = %s", __FUNCTION__, key, value);
  katoob_debug(tmp);
  g_free(tmp);

  if (!strcmp (key, "x"))
    {
      config->x = atoi (value);
    }
  else if (!strcmp (key, "y"))
    {
      config->y = atoi (value);
    }
  else if (!strcmp (key, "w"))
    {
      config->w = atoi (value);
    }
  else if (!strcmp (key, "h"))
    {
      config->h = atoi (value);
    }
  else if (!strcmp (key, "recentno"))
    {
      config->recentno = atoi (value);
    }
  else if (!strcmp (key, "tabsmenu"))
    {
      config->tabsmenu = atoi (value);
    }
  else if (!strcmp (key, "toolbar"))
    {
      config->toolbar = atoi (value);
    }
  else if (!strcmp (key, "extended_toolbar"))
    {
      config->extended_toolbar = atoi (value);
    }
  else if (!strcmp (key, "statusbar"))
    {
      config->statusbar = atoi (value);
    }
  else if (!strcmp (key, "savewinpos"))
    {
      config->savewinpos = atoi (value);
    }
  else if (!strcmp (key, "saveonexit"))
    {
      config->saveonexit = atoi (value);
    }
  else if (!strcmp (key, "textwrap"))
    {
      config->textwrap = atoi (value);
    }
  else if (!strcmp (key, "text_dir"))
    {
      config->text_dir = atoi (value);
    }
  else if (!strcmp (key, "recent"))
    {
      config->recent = atoi (value);
    }
  else if (!strcmp (key, "tabsmenu"))
    {
      config->tabsmenu = atoi (value);
    }
  else if (!strcmp (key, "showtabs"))
    {
      config->showtabs = atoi (value);
    }
  else if (!strcmp (key, "showclose"))
    {
      config->showclose = atoi (value);
    }
  else if (!strcmp (key, "scrolltabs"))
    {
      config->scrolltabs = atoi (value);
    }
  else if (!strcmp (key, "tabspos"))
    {
      config->tabspos = atoi (value);
    }
  else if (!strcmp (key, "toolbartype"))
    {
      if (config->toolbartype)
	{
	  g_free (config->toolbartype);
	}
      config->toolbartype = g_strdup (value);
    }
  else if (!strcmp (key, "filesno"))
    {
      config->filesno = atoi (value);
    }
#ifndef _WIN32
  else if (!strcmp (key, "xkb_err_dlg"))
    {
      config->xkb_err_dlg = atoi (value);
    }
  else if (!strcmp (key, "xkb"))
    {
      config->xkb = atoi (value);
    }
#endif /* _WIN32 */

  else if (!strcmp (key, "undo"))
    {
      config->undo = atoi (value);
    }
  else if (!strcmp (key, "undono"))
    {
      config->undono = atoi (value);
    }

#ifdef HAVE_SPELL
  else if (!strcmp (key, "spell_check"))
    {
      config->spell_check = atoi (value);
    }
  else if (!strcmp (key, "mispelled_red"))
    {
      config->mispelled_red = atoi (value);
    }
  else if (!strcmp (key, "mispelled_blue"))
    {
      config->mispelled_blue = atoi (value);
    }
  else if (!strcmp (key, "mispelled_green"))
    {
      config->mispelled_green = atoi (value);
    }
  else if (!strcmp (key, "dicts_dir"))
    {
      if (config->dicts_dir)
	{
	  g_free (config->dicts_dir);
	}
      config->dicts_dir = g_strdup (value);
    }
  else if (!strcmp (key, "default_dict"))
    {
      if (config->default_dict)
	{
	  g_free (config->default_dict);
	}
      config->default_dict = g_strdup (value);
    }
#endif /* HAVE_SPELL */

  else if (!strcmp (key, "linenumbers"))
    {
      config->linenumbers = atoi (value);
    }
  else if (!strcmp (key, "emulator"))
    {
      config->emulator = atoi (value);
    }
#ifndef _WIN32
  else if (!strcmp (key, "disable_xft"))
    {
      config->disable_xft = atoi (value);
    }
  else if (!strcmp (key, "locale_enc"))
    {
      config->locale_enc = atoi (value);
    }
#endif /* _WIN32 */
  else if (!strcmp (key, "special_enc"))
    {
      config->special_enc = atoi (value);
    }
  else if (!strcmp (key, "saved_enc"))
    {
      if (config->saved_enc)
	{
	  g_free (config->saved_enc);
	}
      config->saved_enc = g_strdup (value);
    }
  else if (!strcmp (key, "font"))
    {
      config->font = g_strdup (decode_font (value));
    }
  else if (!strcmp (key, "default_font"))
    {
      config->default_font = atoi (value);
    }

#ifdef ENABLE_HIGHLIGHT
  else if (!strcmp (key, "highlight"))
    {
      config->highlight = atoi (value);
    }
#endif /* ENABLE_HIGHLIGHT */

  else if (!strcmp (key, "backup"))
    {
      config->backup = atoi (value);
    }
  else if (!strcmp (key, "backup_in_same_dir"))
    {
      config->backup_in_same_dir = atoi (value);
    }
  else if (!strcmp (key, "backup_ext"))
    {
      if (config->backup_ext)
	{
	  g_free (config->backup_ext);
	}
      config->backup_ext = g_strdup (value);
    }
  else if (!strcmp (key, "backup_dir"))
    {
      if (config->backup_dir)
	{
	  g_free (config->backup_dir);
	}
      config->backup_dir = g_strdup (value);
    }
  /* Don't remember across sessions. */
  /*  else if (!strcmp (key, "filesel_path"))
     {
     if (config->filesel_path)
     {
     g_free (config->filesel_path);
     }
     config->filesel_path = g_strdup (value);
     }
   */
  else if (!strcmp (key, "dict_host"))
    {
      if (config->dict_host)
	{
	  g_free (config->dict_host);
	}
      config->dict_host = g_strdup (value);
    }
  else if (!strcmp (key, "dict"))
    {
      config->dict = atoi (value);
    }
  else if (!strcmp (key, "dict_port"))
    {
      config->dict_port = atoi (value);
    }
  else if (!strcmp (key, "dict_timeout"))
    {
      config->dict_timeout = atoi (value);
    }
  else if (!strcmp (key, "dict_db"))
    {
      if (config->dict_db)
	{
	  g_free (config->dict_db);
	}
      config->dict_db = g_strdup (value);
    }
  else if (!strcmp (key, "exec_cmd_in_new"))
    {
      config->exec_cmd_in_new = atoi (value);
    }
  else if (!strcmp (key, "exec_cmd_size"))
    {
      config->exec_cmd_size = atoi (value);
    }
}

/*
 * for each config struct we have a function to save its values.
 */
void
katoob_save_all_config ()
{
#ifdef ENABLE_PRINT
/* NOTE:
 * This should be called before config_free() as we examine config->saveonexit
 */
  print_config_save ();
#endif /* ENABLE_PRINT */
  config_save ();
}

gboolean
katoob_load_strings_from_file (char *file,
			       LoadStringsCcallback callback_function)
{
  gchar buff[PATH_MAX];
  FILE *fp;
  gchar *path = NULL;

  KATOOB_DEBUG_FUNCTION;

  if (!pre_config (file))
    {
      return FALSE;
    }
  path =
    g_strdup_printf ("%s%s.katoob%s%s", g_get_home_dir (), G_DIR_SEPARATOR_S,
		     G_DIR_SEPARATOR_S, file);
  fp = fopen (path, "r");
  g_free (path);
  if (!fp)
    {
      g_warning ("Can't open file!");
      return FALSE;
    }
  while ((fgets (buff, PATH_MAX, fp)) != NULL)
    {
      gint x = strlen (buff);
      if (buff[x - 1] == '\n')
	{
      buff[x - 1] = '\0';
	}
      callback_function (buff);
    }
  fclose (fp);
  return TRUE;
}

/*
 * for each config struct we have a function to free it.
 */
void
katoob_free_all_config ()
{
#ifdef ENABLE_PRINT
  print_config_free ();
#endif /* ENABLE_PRINT */
  config_free ();
}

#ifdef ENABLE_PRINT

void
katoob_fill_print_struct (GHashTable * hash)
{
  KATOOB_DEBUG_FUNCTION;
  g_hash_table_foreach (hash, (GHFunc) print_struct_foreach, NULL);
}

static void
print_struct_foreach (gpointer _key, gpointer _value)
{
  extern PConfig *PConf;
  gchar *key = (gchar *) _key;
  gchar *value = (gchar *) _value;

  if (!strcmp (key, "font_family"))
    {
      if (PConf->font_family)
	{
	  g_free (PConf->font_family);
	}
      PConf->font_family = g_strdup (value);
    }
  else if (!strcmp (key, "font_style"))
    {
      if (PConf->font_style)
	{
	  g_free (PConf->font_style);
	}
      PConf->font_style = g_strdup (value);
    }
  else if (!strcmp (key, "font_scale"))
    {
      if (PConf->font_scale)
	{
	  g_free (PConf->font_scale);
	}
      PConf->font_scale = g_strdup (value);
    }
  else if (!strcmp (key, "custom_entry"))
    {
      if (PConf->custom_entry)
	{
	  g_free (PConf->custom_entry);
	}
      PConf->custom_entry = g_strdup (value);
    }
  else if (!strcmp (key, "file_entry"))
    {
      if (PConf->file_entry)
	{
	  g_free (PConf->file_entry);
	}
      PConf->file_entry = g_strdup (value);
    }
  else if (!strcmp (key, "pdf_entry"))
    {
      if (PConf->pdf_entry)
	{
	  g_free (PConf->pdf_entry);
	}
      PConf->pdf_entry = g_strdup (value);
    }
  else if (!strcmp (key, "printer"))
    {
      PConf->printer = atoi (value);
    }
  else if (!strcmp (key, "location"))
    {
      PConf->location = atoi (value);
    }
  else if (!strcmp (key, "paper_size"))
    {
      PConf->paper_size = atoi (value);
    }
  else if (!strcmp (key, "paper_width"))
    {
      PConf->paper_width = atoi (value);
    }
  else if (!strcmp (key, "paper_height"))
    {
      PConf->paper_height = atoi (value);
    }
  else if (!strcmp (key, "paper_size_unite"))
    {
      PConf->paper_size_unite = atoi (value);
    }
  else if (!strcmp (key, "top_margin"))
    {
      PConf->top_margin = atoi (value);
    }
  else if (!strcmp (key, "left_margin"))
    {
      PConf->left_margin = atoi (value);
    }
  else if (!strcmp (key, "right_margin"))
    {
      PConf->right_margin = atoi (value);
    }
  else if (!strcmp (key, "bottom_margin"))
    {
      PConf->bottom_margin = atoi (value);
    }
  else if (!strcmp (key, "page_orientation"))
    {
      PConf->page_orientation = atoi (value);
    }
  else if (!strcmp (key, "dpi_x"))
    {
      PConf->dpi_x = atoi (value);
    }
  else if (!strcmp (key, "dpi_y"))
    {
      PConf->dpi_y = atoi (value);
    }
  else if (!strcmp (key, "copies"))
    {
      PConf->copies = atoi (value);
    }
  else if (!strcmp (key, "gui_show_advanced"))
    {
      PConf->gui_show_advanced = atoi (value);
    }

}

/*
void
print_config_load (KatoobConfFillFunc katoob_struct_fill_func)
{
  config_load ("print", katoob_struct_fill_func);
}
*/

void
print_config_save ()
{
  extern conf *config;
  extern PConfig *PConf;

  FILE *fp;
  gchar *buffer;
  KATOOB_DEBUG_FUNCTION;

  if (!config->saveonexit)
    {
      return;
    }

  if (!pre_config ("print"))
    {
      return;
    }
  buffer =
    g_strdup_printf ("%s%s.katoob%sprint", g_get_home_dir (),
		     G_DIR_SEPARATOR_S, G_DIR_SEPARATOR_S);
  fp = fopen (buffer, "w");
  g_free (buffer);
  if (!fp)
    {
      g_warning ("Can't open the printing configuration file.");
      return;
    }
  if (PConf->font_family)
    {
  fprintf (fp, "font_family = %s\n", encode_font (PConf->font_family));
    }
  if (PConf->font_style)
    {
  fprintf (fp, "font_style = %s\n", encode_font (PConf->font_style));
    }
  fprintf (fp, "font_scale = %s\n", PConf->font_scale);
  fprintf (fp, "custom_entry = %s\n", PConf->custom_entry);
  fprintf (fp, "file_entry = %s\n", PConf->file_entry);
  fprintf (fp, "pdf_entry = %s\n", PConf->pdf_entry);
  fprintf (fp, "printer = %i\n", PConf->printer);
  fprintf (fp, "location = %i\n", PConf->location);
  fprintf (fp, "paper_size = %i\n", PConf->paper_size);
  fprintf (fp, "paper_width = %i\n", PConf->paper_width);
  fprintf (fp, "paper_height = %i\n", PConf->paper_height);
  fprintf (fp, "paper_size_unite = %i\n", PConf->paper_size_unite);
  fprintf (fp, "top_margin = %i\n", PConf->top_margin);
  fprintf (fp, "left_margin = %i\n", PConf->left_margin);
  fprintf (fp, "right_margin = %i\n", PConf->right_margin);
  fprintf (fp, "bottom_margin = %i\n", PConf->bottom_margin);
  fprintf (fp, "page_orientation = %i\n", PConf->page_orientation);
  fprintf (fp, "dpi_x = %i\n", PConf->dpi_x);
  fprintf (fp, "dpi_y = %i\n", PConf->dpi_y);
  fprintf (fp, "copies = %i\n", PConf->copies);
  fprintf (fp, "gui_show_advanced = %i\n", PConf->gui_show_advanced);
  fclose (fp);
}

void
print_config_init ()
{
  extern PConfig *PConf;

  KATOOB_DEBUG_FUNCTION;

  PConf = (PConfig *) g_malloc (sizeof (PConfig));

  PConf->font_family = g_strdup (DEFAULT_FAMILY);
  PConf->font_style = g_strdup (DEFAULT_STYLE);
  PConf->font_scale = g_strdup (DEFAULT_SIZE);
  PConf->printer = PRINTER_POSTSCRIPT;
  PConf->location = LOCATION_LPR;
  PConf->custom_entry = g_strdup ("lpr");
  PConf->file_entry =
    g_strdup_printf ("%s%skatoob.ps", g_get_home_dir (), G_DIR_SEPARATOR_S);
  PConf->pdf_entry =
    g_strdup_printf ("%s%skatoob.pdf", g_get_home_dir (), G_DIR_SEPARATOR_S);
  PConf->paper_size = 6;	/* A4 */
  PConf->paper_width = 0;
  PConf->paper_height = 0;
  PConf->paper_size_unite = PAPER_SIZE_UNITS_INCHES;
  PConf->top_margin = 1;
  PConf->left_margin = 1;
  PConf->right_margin = 1;
  PConf->bottom_margin = 1;
  PConf->page_orientation = PAGE_ORIENTATION_PORTRAIT;
  PConf->dpi_x = 150;
  PConf->dpi_y = 150;
  PConf->copies = 1;
  PConf->gui_show_advanced = 0;
}

void
print_config_free ()
{
  extern PConfig *PConf;

  KATOOB_DEBUG_FUNCTION;
  g_free (PConf->font_family);
  g_free (PConf->font_style);
  g_free (PConf->font_scale);
  g_free (PConf->custom_entry);
  g_free (PConf->file_entry);
  g_free (PConf->pdf_entry);
  g_free (PConf);
}

#endif /* ENABLE_PRINT */
