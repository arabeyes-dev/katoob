/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"

#include <string.h>		/* strlen() */
#include <glib.h>
#include "katoobdocument.h"
#include "undoredo.h"
#include "mdi.h"

void
katoob_undo ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  if (!doc)
    {
      return;
    }
  katoob_document_undo (doc);
}

void
katoob_redo ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  if (!doc)
    {
      return;
    }
  katoob_document_redo (doc);
}
