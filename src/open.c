/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"

#include <gtk/gtk.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>		/* strlen() */
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "open.h"
#include "misc.h"
#include "file.h"
#include "encodings.h"
#include "iransystem.h"
#include "mdi.h"

void
katoob_open ()
{
  File *f = NULL;
  gchar **p;

  KATOOB_DEBUG_FUNCTION;

  f =
    katoob_create_file_selection (_("Please select a file for editing."),
				  TRUE);
  switch (f->result)
    {
    case GTK_RESPONSE_OK:
      {
	p = f->file;
	while (*p)
	  {
	    katoob_create_doc_from_file (*p, TRUE, f->encoding);
	    p++;
	  }
/*
      katoob_create_doc_from_file (f->file, TRUE, f->encoding);
*/
	g_strfreev (f->file);
	g_free (f);
	break;
      }
    default:
      g_strfreev (f->file);
      g_free (f);
      return;
    }
}

void
katoob_revert ()
{
  extern UI *katoob;
  GtkWidget *dialog;
  GtkWidget *dialog_vbox;
  GtkWidget *hbox;
  GtkWidget *image;
  GtkWidget *label;
  GtkWidget *dialog_action_area;
  GtkWidget *cancelbutton;
  GtkWidget *okbutton;
  gint res;
  gchar *_tmp;
  gchar *content = NULL;
  gchar *content2 = NULL;

  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return;
    }
  if (!katoob_document_get_file (doc))
    {
      return;
    }
  dialog = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (dialog), _("Confirm Revert"));
  dialog_vbox = GTK_DIALOG (dialog)->vbox;
  gtk_widget_show (dialog_vbox);
  hbox = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox);
  gtk_box_pack_start (GTK_BOX (dialog_vbox), hbox, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);
  image =
    gtk_image_new_from_stock ("gtk-dialog-warning", GTK_ICON_SIZE_DIALOG);
  gtk_widget_show (image);
  gtk_box_pack_start (GTK_BOX (hbox), image, TRUE, TRUE, 2);
  _tmp =
    g_strdup_printf (_
		     ("This will close the current file \"%s\", Discard any changes done to the active buffer & Reload the Saved copy of the file, Are you sure you want to continue?"),
		     katoob_document_get_file (doc));
  label = gtk_label_new (_tmp);
  g_free (_tmp);
  gtk_widget_show (label);
  gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 2);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  dialog_action_area = GTK_DIALOG (dialog)->action_area;
  gtk_widget_show (dialog_action_area);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area),
			     GTK_BUTTONBOX_END);
  cancelbutton = gtk_button_new_from_stock ("gtk-cancel");
  gtk_widget_show (cancelbutton);
  gtk_dialog_add_action_widget (GTK_DIALOG (dialog), cancelbutton,
				GTK_RESPONSE_CANCEL);
  GTK_WIDGET_SET_FLAGS (cancelbutton, GTK_CAN_DEFAULT);
  okbutton = gtk_button_new_from_stock ("gtk-ok");
  gtk_widget_show (okbutton);
  gtk_dialog_add_action_widget (GTK_DIALOG (dialog), okbutton,
				GTK_RESPONSE_OK);
  GTK_WIDGET_SET_FLAGS (okbutton, GTK_CAN_DEFAULT);
  gtk_window_set_transient_for (GTK_WINDOW (dialog),
				GTK_WINDOW (katoob->win));
  res = gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
  switch (res)
    {
    case GTK_RESPONSE_OK:
      {
	if (!katoob_create_file_if_required (katoob_document_get_file (doc)))
	  {
	    return;
	  }
	if (!katoob_file_get_contents
	    (katoob_document_get_file (doc), &content))
	  {
	    return;
	  }

/* If the file is empty, content is set to NULL */

	if (!content)
	  {
	    return;
	  }
	else
	  {
	    content2 = katoob_text_to_utf8 (content, doc);
	    g_free (content);
	    if (!content2)
	      {
		return;
	      }
	  }
	katoob_document_set_text (doc, content2);
	if (content2)
	  {
	    g_free (content2);
	  }
	katoob_document_reset_undo_redo (doc);
	break;
      }
    default:
      return;
    }
}
