/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __TOOLBAR_H__
#define __TOOLBAR_H__

GtkWidget *katoob_toolbar_new (GtkWidget * win);
void katoob_toolbar_icons ();
void katoob_toolbar_text ();
void katoob_toolbar_both ();
void katoob_toolbar_bothhoriz ();
void katoob_toggle_toolbar (GtkCheckMenuItem * checkmenuitem);
void katoob_toggle_extended_toolbar (GtkCheckMenuItem * checkmenuitem);
gboolean katoob_popup_toolbar_menu (GtkWidget * widget, GdkEvent * event);
void katoob_populate_extended_toolbar (GtkWidget * toolbar, GtkWidget * win);

#endif /* __TOOLBAR_H__ */
