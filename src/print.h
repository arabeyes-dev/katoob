/* Katoob
 * Copyright (c) 2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __PRINT_H__
#define __PRINT_H__

#ifdef ENABLE_PRINT

#include <glib.h>
#include "katoobdocument.h"

#define PAGE_ORIENTATION_PORTRAIT	0x1
#define PAGE_ORIENTATION_LANDSCAPE	0x2

#define DEFAULT_FAMILY			"Sans"
#define DEFAULT_STYLE			"Regular"
#define DEFAULT_SIZE			"12"

#define PRINTER_POSTSCRIPT		0x1
#define PRINTER_PDF			0x2

#define LOCATION_FILE			0x1
#define LOCATION_LPR			0x2
#define LOCATION_CUSTOM			0x3

#define PAPER_SIZE_UNITS_INCHES 	0x1
#define PAPER_SIZE_UNITS_POINTS		0x2

#define PAGE_ORIENTATION_PORTRAIT	0x1
#define PAGE_ORIENTATION_LANDSCAPE	0x2

typedef struct _PConfig
{
  gchar *font_family;
  gchar *font_style;
  gchar *font_scale;
  gint printer;
  gint location;
  gchar *custom_entry;
  gchar *file_entry;
  gchar *pdf_entry;
  gint paper_size;		/* -1 = custom */
  gint paper_width;
  gint paper_height;
  gint paper_size_unite;
  gint top_margin;
  gint left_margin;
  gint right_margin;
  gint bottom_margin;
  gint page_orientation;
  gint dpi_x;
  gint dpi_y;
  gint copies;
  gint gui_show_advanced;
}
PConfig;

typedef struct
{
  gchar *name;
  glong width;
  glong height;
}
_Papers;

gboolean katoob_do_print (KatoobDocument * doc, gboolean selection_only);

#endif /* ENABLE_PRINT */

#endif /* __PRINT_H__ */
