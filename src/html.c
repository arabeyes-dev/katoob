/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include <string.h>		/* strlen() */
#include <stdlib.h>		/* atoi() */
#include <glib.h>
#include "html.h"
#include "katoobdocument.h"
#include "file.h"
#include "misc.h"
#include "mdi.h"

static gchar *katoob_from_html_style (gchar * buf);
static gboolean katoob_to_html_style (FILE * fp, gchar * buff, gchar * file);

/* TODO: Do we need multiple files to be opened at once here ? */
void
katoob_import_html ()
{
  extern UI *katoob;
  extern conf *config;
  GtkWidget *file_selector;
  gint result;
  gchar *file;
  file_selector =
    gtk_file_selection_new (_("Please choose a file to import"));
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (file_selector),
				   config->filesel_path);
  gtk_window_set_transient_for (GTK_WINDOW (file_selector),
				GTK_WINDOW (katoob->win));
  result = gtk_dialog_run (GTK_DIALOG (file_selector));
  file =
    g_strdup (gtk_file_selection_get_filename
	      (GTK_FILE_SELECTION (file_selector)));
  gtk_widget_destroy (file_selector);
  switch (result)
    {
    case GTK_RESPONSE_OK:
      {
	gchar *tmp;
	g_free (config->filesel_path);
	tmp = g_path_get_dirname (file);
	config->filesel_path = g_strconcat (tmp, G_DIR_SEPARATOR_S, NULL);
	g_free (tmp);
	break;
      }
    default:
      {
	g_free (file);
	return;
      }
    }
  if (katoob_create_file_if_required (file))
    {
      gchar *content;
      gchar *buff;
      if (katoob_file_get_contents (file, &content))
	{
/* content = NULL if the file is empty. */
	  if (!content)
	    {
	      katoob_error (_("Empty file."));
	      g_free (file);
	      return;
	    }
	  else
	    {
	      buff = katoob_from_html_style (content);
	      g_free (content);
	      if (!buff)
		{
		  g_free (file);
		  return;
		}
	      else
		{
		  KatoobDocument *doc;
		  katoob_create_doc ();
		  doc = katoob_get_active_doc ();
		  katoob_document_set_text (doc, buff);

		  g_free (file);
		  g_free (buff);
		  return;
		}
	    }
	}
      else
	{
	  g_free (file);
	  return;
	}
    }
  else
    {
      g_free (file);
      return;
    }

}

void
katoob_export_html ()
{
  extern UI *katoob;
  extern conf *config;
  GtkWidget *file_selector;
  gint result;
  gchar *file;
  FILE *fp;
  gchar *_tmp;

  KatoobDocument *doc = katoob_get_active_doc ();

  if (!doc)
    {
      return;
    }

  file_selector =
    gtk_file_selection_new (_("Please choose a file to export to"));
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (file_selector),
				   config->filesel_path);
  gtk_window_set_transient_for (GTK_WINDOW (file_selector),
				GTK_WINDOW (katoob->win));
  result = gtk_dialog_run (GTK_DIALOG (file_selector));
  file =
    g_strdup (gtk_file_selection_get_filename
	      (GTK_FILE_SELECTION (file_selector)));
  gtk_widget_destroy (file_selector);

  switch (result)
    {
    case GTK_RESPONSE_OK:
      {
	gchar *tmp;
	g_free (config->filesel_path);
	tmp = g_path_get_dirname (file);
	config->filesel_path = g_strconcat (tmp, G_DIR_SEPARATOR_S, NULL);
	g_free (tmp);
	break;
      }
    default:
      {
	g_free (file);
	return;
      }
    }

  if ((!katoob_document_get_modified (doc))
      && (!katoob_document_get_file (doc)))
    {
      g_free (file);
      return;
    }

  if (g_file_test (file, G_FILE_TEST_EXISTS))
    {
      _tmp =
	g_strdup_printf (_
			 ("Are you sure you want to overwrite the file %s ?"),
			 file);

      result = create_messagedialog (_tmp);
      g_free (_tmp);

      switch (result)
	{
	case GTK_RESPONSE_YES:
	  break;
	case GTK_RESPONSE_NO:
	  g_free (file);
	  katoob_export_html ();
	  return;
	default:
	  g_free (file);
	  return;
	}
    }

  if ((fp = fopen (file, "w+")) == NULL)
    {
      katoob_error (_("The requested file couldn't be opened for saving"));
      g_free (file);
      return;
    }
  katoob_to_html_style (fp, katoob_document_get_text (doc), file);

  fclose (fp);
  g_free (file);
}

static gboolean
katoob_to_html_style (FILE * fp, gchar * buff, gchar * file)
{
  gunichar *b;
  glong len;
  register glong x;
  size_t i;

  b = g_utf8_to_ucs4_fast (buff, strlen (buff), &len);
  for (x = 0; x <= len; x++)
    {
      if ((b[x] == 0x060c) || (b[x] == 0x061b) || (b[x] == 0x061f) ||
	  (b[x] == 0x200e) || (b[x] == 0x200f) || (b[x] == 0x202a) ||
	  (b[x] == 0x202b) || (b[x] == 0x202d) || (b[x] == 0x202e) ||
	  (b[x] == 0x202c) || (b[x] == 0x200b) || (b[x] == 0x200d) ||
	  (b[x] == 0x200c) ||
	  ((b[x] >= 0x0621) && (b[x] <= 0x063a)) ||
	  ((b[x] >= 0x0640) && (b[x] <= 0x0655)) ||
	  ((b[x] >= 0x0660) && (b[x] <= 0x06ed)) ||
	  ((b[x] >= 0x06f0) && (b[x] <= 0x06fe)))
	{
	  gchar *n = g_strdup_printf ("&#%d;", b[x]);
	  i = fprintf (fp, "%s", n);
	  if (i != strlen (n))
	    {
	      katoob_error (_
			    ("An error has occured while writing to file %s"),
			    file);
	      g_free (b);
	      g_free (n);
	      return FALSE;
	    }
	  g_free (n);
	}
      else if (b[x] != 0x0)
	{
	  i = fprintf (fp, "%s", (char *) &b[x]);
	  if (i != 1)
	    {
	      katoob_error (_
			    ("An error has occured while writing to file %s"),
			    file);
	      g_free (b);
	      return FALSE;
	    }
	}
    }
  g_free (b);
  return TRUE;
}

/* NOTE: To be revised. */
static gchar *
katoob_from_html_style (gchar * buf)
{
  gunichar *ch = NULL;
  gunichar *tch = NULL;
  gchar *html_buf = NULL;
  glong x, i, buf_size = 0;
  glong len = 0;
  gchar *tmp = (gchar *) g_malloc (sizeof (gchar) * 11);
  GError *er = NULL;
  KATOOB_DEBUG_FUNCTION;

  ch = (gunichar *) g_malloc (sizeof (gunichar) * strlen (buf) * 2);
  if (!ch)
    {
      return NULL;
    }

  tch = g_utf8_to_ucs4_fast (buf, -1, &len);
  for (x = 0; x <= len; x++)
    {
      if ((tch[x] == '&') && (tch[x + 1] == '#'))
	{
	  ++x;
	  ++x;
	  i = 0;
	  while (buf[x] != ';')
	    {
	      tmp[i] = tch[x];
	      ++i;
	      ++x;
	    }
	  tmp[i] = '\0';
	  ch[buf_size] = atoi (tmp);
	  ++buf_size;
	}
      else
	{
	  ch[buf_size] = tch[x];
	  ++buf_size;
	}
    }

  buf_size--;
  ch[buf_size] = 0;
  g_free (tmp);
  html_buf = g_ucs4_to_utf8 (ch, -1, NULL, NULL, &er);
  g_free (ch);
  g_free (tch);
  if (!html_buf)
    {
      katoob_error ("%s", er->message);
      g_error_free (er);
      return NULL;
    }
  if (!g_utf8_validate (html_buf, -1, NULL))
    {
      katoob_error (_("That's not a valid UTF-8 file"));
      g_free (html_buf);
      return NULL;
    }
  return html_buf;
}
