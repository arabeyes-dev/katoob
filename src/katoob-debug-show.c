/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h> /* exit() */
#include <unistd.h>
#include <fcntl.h>
#include <string.h> /* strlen() */
#include <gtk/gtk.h>

#ifdef ENABLE_NLS
#  include <locale.h>
#  include <libintl.h>
#  define _(String) gettext (String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define bind_textdomain_codeset(Domain,Codeset) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif /* ENABLE_NLS */

void katoob_debug_save ();

GtkWidget *window;
GtkTextBuffer *buffer;

gint
katoob_create_question (gchar * q)
{
  GtkWidget *dialog;
  gint result;

  dialog =
    gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION,
			    GTK_BUTTONS_YES_NO, q);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (window));
  result = gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (GTK_WIDGET (dialog));
  return result;
}

void
katoob_error (gchar * err)
{
  GtkWidget *dialog;
  dialog =
    gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
			    GTK_BUTTONS_CLOSE, err);
  g_signal_connect (G_OBJECT (dialog),
		    "response", G_CALLBACK (gtk_widget_destroy), NULL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("ERROR"));
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
  gtk_widget_show (dialog);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (window));
}

static void
add_debug_text (gchar * buff)
{
  gtk_text_buffer_insert_at_cursor (buffer, buff, -1);
}

void
destroy_cb ()
{
  exit (0);
}

void
close_cb ()
{
  exit (0);
}

void
create_crash_dialog ()
{
  GtkWidget *view, *label, *vbox, *hbox, *save_button, *close_button,
    *scrolledwin;
  gchar *tmp = NULL;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_CENTER);
  gtk_window_set_title (GTK_WINDOW (window), _("Katoob debugging output."));

  gtk_widget_set_size_request (window, 400, 300);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  tmp = g_strdup_printf
    (_
     ("Katoob has crashed, Please email the following debugging output to developer@arabeyes.org and tell them exactly what you were doing."));

  label = gtk_label_new (tmp);
  g_free (tmp);

  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_CENTER);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);

  scrolledwin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				  (scrolledwin),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW
				       (scrolledwin), GTK_SHADOW_IN);

  view = gtk_text_view_new ();
  gtk_text_view_set_editable (GTK_TEXT_VIEW (view), FALSE);
  gtk_container_add (GTK_CONTAINER (scrolledwin), view);
  gtk_box_pack_start (GTK_BOX (vbox), scrolledwin, TRUE, TRUE, 0);

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));

  hbox = gtk_hbox_new (TRUE, 10);

  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

  save_button = gtk_button_new_from_stock (GTK_STOCK_SAVE);
  gtk_box_pack_start (GTK_BOX (hbox), save_button, TRUE, TRUE, 0);

  close_button = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
  gtk_box_pack_start (GTK_BOX (hbox), close_button, TRUE, TRUE, 0);

  g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (destroy_cb),
		    NULL);
  g_signal_connect (G_OBJECT (close_button), "clicked",
		    G_CALLBACK (close_cb), NULL);
  g_signal_connect (G_OBJECT (save_button), "clicked",
		    G_CALLBACK (katoob_debug_save), NULL);

  tmp =
    g_strdup_printf ("Katoob version: %s\n Compiled in features:", VERSION);
  add_debug_text (tmp);
  g_free (tmp);
}

void
show_crash_dialog ()
{
  gtk_widget_show_all (window);
  return;
}

void
katoob_debug_save ()
{
  GtkWidget *file_selector;
  file_selector = gtk_file_selection_new (_("Save"));
  if (gtk_dialog_run (GTK_DIALOG (file_selector)) == GTK_RESPONSE_OK)
    {
      GtkTextIter start, end;
      gchar *buff;
      gchar *file;
      FILE *f;
      file =
	g_strdup (gtk_file_selection_get_filename
		  (GTK_FILE_SELECTION (file_selector)));
      gtk_widget_destroy (file_selector);

      if (!file)
	{
	  return;
	}
/* If the file exists, Prompt the user to overwrite. */
      if (g_file_test (file, G_FILE_TEST_EXISTS))
	{
	  gint result;
	  gchar *_tmp =
	    g_strdup_printf (_
			     ("Are you sure you want to overwrite the file %s ?"),
file);
	  result = katoob_create_question (_tmp);
	  g_free (_tmp);
	  switch (result)
	    {
	    case GTK_RESPONSE_YES:
	      break;
	    case GTK_RESPONSE_NO:
	      g_free (file);
	      return;
	      break;
	    }
	}

      f = fopen (file, "w+");

      if (!f)
	{
	  katoob_error (_
			("The requested file couldn't be opened for saving"));
	  g_free (file);
	  return;
	}

      gtk_text_buffer_get_bounds (buffer, &start, &end);
      buff = gtk_text_buffer_get_text (buffer, &start, &end, TRUE);
      fwrite (buff, strlen (buff), 1, f);
      fclose (f);
      g_free (buffer);
      g_free (file);

    }
  exit (0);
}

int
main (int argc, char *argv[])
{
  char buff[1024];
  int x;

#ifdef ENABLE_NLS
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (PACKAGE, "UTF-8");
  textdomain (PACKAGE);
#endif

  gtk_set_locale ();

  gtk_init (&argc, &argv);

  create_crash_dialog ();
  show_crash_dialog ();
  fcntl (0, F_SETFL, O_NONBLOCK);
  while (1)
    {
      x = read (0, buff, 1023);
      if (x == 0)
	{
	  break;
	}
      if (x == -1)
	{
	  sleep (1);
	  continue;
	}
      buff[x] = '\0';
      add_debug_text (buff);
    }
  gtk_main ();
  return 0;
}
