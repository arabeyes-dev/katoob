/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "katoobdocument.h"

#ifndef __KATOOB_LABEL_H__
#define __KATOOB_LABEL_H__

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#include <gtk/gtk.h>

#define KATOOB_TYPE_LABEL                  (katoob_label_get_type ())
#define KATOOB_LABEL(obj)                  (GTK_CHECK_CAST ((obj), KATOOB_TYPE_LABEL, KatoobLabel))
#define KATOOB_LABEL_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), KATOOB_TYPE_LABEL, KatoobLabelClass))
#define KATOOB_IS_LABEL(obj)               (GTK_CHECK_TYPE ((obj), KATOOB_TYPE_LABEL))
#define KATOOB_IS_LABEL_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), KATOOB_TYPE_LABEL))
#define KATOOB_LABEL_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), KATOOB_TYPE_LABEL, KatoobLabelClass))

  typedef struct _KatoobLabel KatoobLabel;
  typedef struct _KatoobLabelClass KatoobLabelClass;
  typedef struct _KatoobLabelPrivate KatoobLabelPrivate;

  struct _KatoobLabel
  {
    GtkHBox hbox;
    KatoobLabelPrivate *priv;
  };

  struct _KatoobLabelClass
  {
    GtkHBoxClass parent_class;
  };

  GType katoob_label_get_type (void) G_GNUC_CONST;
  GtkWidget *katoob_label_new (gchar * str);
  const gchar *katoob_label_get_text (KatoobLabel * label);
  void katoob_label_set_text (KatoobLabel * label, gchar * str);
  void katoob_label_set_modified (KatoobLabel * label, gboolean mod);
  gboolean katoob_label_get_modified (KatoobLabel * label);
  GtkWidget *katoob_label_get_close_button (KatoobLabel * label);
  void katoob_label_create_close_button (KatoobLabel * label);
  void katoob_label_destroy_close_button (KatoobLabel * label);
  GtkWidget *katoob_label_get_close_button (KatoobLabel * label);
  void katoob_label_show_close_button (KatoobLabel * label,
				       KatoobDocument * doc);
  void katoob_label_hide_close_button (KatoobLabel * label,
				       KatoobDocument * doc);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* __KATOOB_LABEL_H__ */
