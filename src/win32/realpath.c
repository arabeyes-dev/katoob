/* Katoob
 * Copyright (c) 2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* WIN32 implementation of realpath() */

#include <ctype.h>
#include <limits.h>
#include <stdio.h>

char *win32_realpath (char *path, char full_path[]);

/*
int
main (int argc, char *argv[])
{
  char foo[PATH_MAX];
  win32_realpath (argv[1], foo);
  printf ("%s\n", foo);
}
*/

char *
win32_realpath (char *path, char full_path[])
{
  char new_path[PATH_MAX];
  int x;
  char *_path = path;

/*  printf ("START\n");
  printf ("%s\n", path); */
/* 
Perhaps the passed path length is greater than PATH_MAX, But after resolving 
it'll be less!
if (strlen(path) > PATH_MAX)
{
return NULL;
}
*/
  if ((strlen (path) >= 3) && isascii (path[0]) && isalpha (path[0])
      && !isdigit (path[0]) && (path[1] == ':') && path[2] == '\\')
/*
  if (path[0] == '/') */
    {
/* Probably an absolute path. */
      x = 0;
      while (*_path != '\0')
	{
	  if (*_path == '\\')
	    {
	      if (_path[1] == '\\')
		{
		  full_path[x] = *_path;
		  ++x;
		  ++_path;
		  ++_path;
		  continue;
		}
	      else if ((_path[1] == '.') && (_path[2] == '.')
		       && (_path[3] == '\\'))
		{
		  while (full_path[x] != '\\')
		    {
		      if (full_path[x] == ':')
			{
			  return NULL;
			}
		      --x;
		    }
		  ++_path;
		  ++_path;
		  ++_path;
		  continue;

		}
	      else if ((_path[1] == '.') && (_path[2] == '\\'))
		{
		  ++_path;
		  ++_path;
		  continue;
		}
	      full_path[x] = *_path;
	      ++x;
	      ++_path;
	      continue;
	    }
	  else
	    {
	      full_path[x] = *_path;
	      ++x;
	      ++_path;
	      continue;
	    }
	}
      full_path[x] = '\0';
      return full_path;
    }
  else
    {
/* Probably Not an absolute path. */

      if (!getcwd (new_path, PATH_MAX))
	{
	  return NULL;
	}
      x = strlen (new_path);

      if (x >= PATH_MAX)
	{
	  return NULL;
	}
      if (new_path[x] != '\\' && path[0] != '\\')
	{
	  new_path[x] = '\\';
	  ++x;
	}

      strcat (&new_path[x], path);
      return win32_realpath (new_path, full_path);
    }
}
