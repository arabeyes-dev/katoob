/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* Some of the code are taken from Shabredo */

/***************
 * @(#) Shabredo 0.8
 * @author Feature enhancements by Madhav & Saeed for Shabdiz
 * @version 0.8 (alpha)
 *
 *  Chapar Shabdiz
 *  Arash Zeini
 *  info@chaparshabdiz.com
 *  http://www.chaparshabdiz.com
 *  Enhancements for Zarnegar support by K Madhav Das (kmadhavd@yahoo.com)
 *  Enhancements for Vazhe Negar support by Saeed Nadjariun (nadjariun@att.net)
 *
 *  Chapar Shabdiz, hereby disclaims all copyright interest in the program
 *  `Shabredo' (which makes passes at compilers)
 *  enhancements added by K Madhav Das.
 *   
 *  Chapar Shabdiz, 2 February 2002
 *  Arash Zeini, Managing Director of Chapar Shabdiz.
 *
 *******************/

/*********************************************************************************
 * NOTE: THIS IS NOT TESTED, If it work, That's OK.                              *
 * But if it didn't work and the produced files are mangled, It's not my problem *
 * I've requested anyone to test it but no one responded, I even kept fighting   *
 * till i have got the IRAN SYSTEM charmap.                                      *
 * I didn't understand it, So i had to rep this code "modified for katoob" to    *
 * implement this.                                                               *
 *********************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include <string.h>		/* strlen() */
#include <glib.h>
#include "iransystem.h"
#include "katoobdocument.h"
#include "file.h"
#include "misc.h"
#include "mdi.h"

static glong unicode_to_iran_system (glong before, glong uni, glong after);
gchar *katoob_to_iran_system (gchar * buf);
gchar *katoob_from_iran_system (gchar * buf);

gunichar iran_system[] = {
  0x0000, 0x0001, 0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x0007,
  0x0008, 0x0009, 0x000A, 0x000B, 0x000C, 0x000D, 0x000E, 0x000F,
  0x0010, 0x0011, 0x0012, 0x0013, 0x0014, 0x0015, 0x0016, 0x0017,
  0x0018, 0x0019, 0x001A, 0x001B, 0x001C, 0x001D, 0x001E, 0x001F,
  0x0020, 0x0021, 0x0022, 0x0023, 0x0024, 0x0025, 0x0026, 0x0027,
  0x0028, 0x0029, 0x002A, 0x002B, 0x002C, 0x002D, 0x002E, 0x002F,
  0x0030, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037,
  0x0038, 0x0039, 0x003A, 0x003B, 0x003C, 0x003D, 0x003E, 0x003F,
  0x0040, 0x0041, 0x0042, 0x0043, 0x0044, 0x0045, 0x0046, 0x0047,
  0x0048, 0x0049, 0x004A, 0x004B, 0x004C, 0x004D, 0x004E, 0x004F,
  0x0050, 0x0051, 0x0052, 0x0053, 0x0054, 0x0055, 0x0056, 0x0057,
  0x0058, 0x0059, 0x005A, 0x005B, 0x005C, 0x005D, 0x005E, 0x005F,
  0x0060, 0x0061, 0x0062, 0x0063, 0x0064, 0x0065, 0x0066, 0x0067,
  0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D, 0x006E, 0x006F,
  0x0070, 0x0071, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077,
  0x0078, 0x0079, 0x007A, 0x007B, 0x007C, 0x007D, 0x007E, 0x007F,
  0x06F0, 0x06F1, 0x06F2, 0x06F3, 0x06F4, 0x06F5, 0x06F6, 0x06F7,
  0x06F8, 0x06F9, 0x060C, 0x0640, 0x061F, 0x0622, 0x0626, 0x0621,
  0x0627, 0x0627, 0x0628, 0x0628, 0x067E, 0x067E, 0x062A, 0x062A,
  0x062B, 0x062B, 0x062C, 0x062C, 0x0686, 0x0686, 0x062D, 0x062D,
  0x062E, 0x062E, 0x062F, 0x0630, 0x0631, 0x0632, 0x0633, 0x0633,
  0x0634, 0x0634, 0x0635, 0x0635, 0x0636, 0x0636, 0x0637, 0x2591,
  0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556, 0x2555,
  0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510, 0x2514,
  0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F, 0x255A,
  0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567, 0x2568,
  0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B, 0x256A,
  0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580, 0x0638,
  0x0639, 0x0639, 0x0639, 0x0639, 0x063A, 0x063A, 0x063A, 0x063A,
  0x0641, 0x0641, 0x0642, 0x0642, 0x06A9, 0x06A9, 0x06AF, 0x06AF,
  0x0644, 0x0644, 0x0644, 0x0645, 0x0645, 0x0646, 0x0646, 0x0648,
  0x0647, 0x0647, 0x0647, 0x06CC, 0x06CC, 0x06CC, 0x00A0
};

gchar *
katoob_to_iran_system (gchar * buf)
{
  glong l = 1;
  gchar *ibuf = (gchar *) g_malloc (strlen (buf) + 1);
  ibuf[strlen (buf)] = '\0';

  ibuf[0] = unicode_to_iran_system (0x0, buf[0], buf[1]);

  while (buf[l] != 0x0)
    {
      ibuf[l] = unicode_to_iran_system (buf[l - 1], buf[l], buf[l + 1]);
      ++l;
    }
  return ibuf;
}

static glong
unicode_to_iran_system (glong before, glong uni, glong after)
{
  gboolean isolated = FALSE;
  gboolean finall = FALSE;
  gboolean medial = FALSE;
  gboolean initial = FALSE;
  gboolean before_is_letter = FALSE;
  gboolean after_is_letter = FALSE;

  if (uni < 0x0600)
    {
      return uni;
    }

  if ((uni >= 0x6F0) && (uni <= 0x6F9))
    {
      return (uni - 0x6f0 + 0x80);
    }

  if ((before >= 0x0620) && (before <= 0x06D0))
    {
      before_is_letter = TRUE;
    }
  if ((after >= 0x0620) && (after <= 0x06D0))
    {
      after_is_letter = TRUE;
    }

  if (before_is_letter && after_is_letter)
    {
      medial = TRUE;
    }
  else if (before_is_letter && !after_is_letter)
    {
      finall = TRUE;
    }
  else if (!before_is_letter && after_is_letter)
    {
      initial = TRUE;
    }
  else
    {
      isolated = TRUE;
    }

  if (uni == 0x060C)
    return 0x8A;
  if (uni == 0x0640)
    return 0x8B;
  if (uni == 0x061F)
    return 0x8C;

  if (uni == 0x0622)
    return 0x8D;
  if (uni == 0x0626)
    return 0x8E;
  if (uni == 0x0621)
    return 0x8f;

  if (uni == 0x0627 && (isolated || initial))
    return 0x90;
  if (uni == 0x0627 && (medial || finall))
    return 0x91;
  if (uni == 0x0623 && (isolated || initial))
    return 0x90;
  if (uni == 0x0623 && (medial || finall))
    return 0x91;
  if (uni == 0x0625 && (isolated || initial))
    return 0x90;
  if (uni == 0x0625 && (medial || finall))
    return 0x91;
  if (uni == 0x0628 && (finall || isolated))
    return 0x92;
  if (uni == 0x0628 && (initial || medial))
    return 0x93;
  if (uni == 0x067E && (finall || isolated))
    return 0x94;
  if (uni == 0x067E && (initial || medial))
    return 0x95;
  if (uni == 0x062A && (finall || isolated))
    return 0x96;
  if (uni == 0x062A && (initial || medial))
    return 0x97;
  if (uni == 0x062B && (finall || isolated))
    return 0x98;
  if (uni == 0x062B && (initial || medial))
    return 0x99;
  if (uni == 0x062C && (finall || isolated))
    return 0x9A;
  if (uni == 0x062C && (initial || medial))
    return 0x9B;
  if (uni == 0x0686 && (finall || isolated))
    return 0x9C;
  if (uni == 0x0686 && (initial || medial))
    return 0x9D;
  if (uni == 0x062D && (finall || isolated))
    return 0x9E;
  if (uni == 0x062D && (initial || medial))
    return 0x9F;
  if (uni == 0x062E && (finall || isolated))
    return 0xA0;
  if (uni == 0x062E && (initial || medial))
    return 0xA1;
  if (uni == 0x062F)
    return 0xA2;
  if (uni == 0x0630)
    return 0xA3;
  if (uni == 0x0631)
    return 0xA4;
  if (uni == 0x0632)
    return 0xA5;
  if (uni == 0x0698)
    return 0xA6;
  if (uni == 0x0633 && (finall || isolated))
    return 0xA7;
  if (uni == 0x0633 && (initial || medial))
    return 0xA8;
  if (uni == 0x0634 && (finall || isolated))
    return 0xA9;
  if (uni == 0x0634 && (initial || medial))
    return 0xAA;
  if (uni == 0x0635 && (finall || isolated))
    return 0xAB;
  if (uni == 0x0635 && (initial || medial))
    return 0xAC;
  if (uni == 0x0636 && (finall || isolated))
    return 0xAD;
  if (uni == 0x0636 && (initial || medial))
    return 0xAE;
  if (uni == 0x0637)
    return 0xAF;

  if (uni == 0x0638)
    return 0xE0;

  if (uni == 0x0639 && isolated)
    return 0xE1;
  if (uni == 0x0639 && finall)
    return 0xE2;
  if (uni == 0x0639 && medial)
    return 0xE3;
  if (uni == 0x0639 && initial)
    return 0xE4;
  if (uni == 0x063A && isolated)
    return 0xE5;
  if (uni == 0x063A && finall)
    return 0xE6;
  if (uni == 0x063A && medial)
    return 0xE7;
  if (uni == 0x063A && initial)
    return 0xE8;

  if (uni == 0x0641 && (finall || isolated))
    return 0xE9;
  if (uni == 0x0641 && (initial || medial))
    return 0xEA;
  if (uni == 0x0642 && (finall || isolated))
    return 0xEB;
  if (uni == 0x0642 && (initial || medial))
    return 0xEC;
  if (uni == 0x06A9 && (finall || isolated))
    return 0xED;
  if (uni == 0x06A9 && (initial || medial))
    return 0xEE;
  if (uni == 0x0643 && (finall || isolated))
    return 0xED;
  if (uni == 0x0643 && (initial || medial))
    return 0xEE;

  if (uni == 0x06AF && (finall || isolated))
    return 0xEF;
  if (uni == 0x06AF && (initial || medial))
    return 0xF0;
  if (uni == 0x0644 && (finall || isolated))
    return 0xF1;
  if (uni == 0x0644 && after == 0x0627)
    return 0xF2;

  if (uni == 0x0644 && (initial || medial))
    return 0xF3;

  if (uni == 0x0645 && (finall || isolated))
    return 0xF4;
  if (uni == 0x0645 && (initial || medial))
    return 0xF5;
  if (uni == 0x0646 && (finall || isolated))
    return 0xF6;
  if (uni == 0x0646 && (initial || medial))
    return 0xF7;

  if (uni == 0x0648)
    return 0xF8;
  if (uni == 0x0647 && (finall || isolated))
    return 0xF9;
  if (uni == 0x0647 && medial)
    return 0xFA;
  if (uni == 0x0647 && initial)
    return 0xFB;

  if (uni == 0x06CC && finall)
    return 0xFC;
  if (uni == 0x06CC && isolated)
    return 0xFD;
  if (uni == 0x06CC && (initial || medial))
    return 0xFE;
  if (uni == 0x0649 && finall)
    return 0xFC;
  if (uni == 0x0649 && isolated)
    return 0xFD;
  if (uni == 0x0649 && (initial || medial))
    return 0xFE;

  if (uni == 0x064A && finall)
    return 0xFC;
  if (uni == 0x064A && isolated)
    return 0xFD;
  if (uni == 0x064A && (initial || medial))
    return 0xFE;

  if (uni == 0xA0)
    return 0xFF;

  return uni;
}

gchar *
katoob_from_iran_system (gchar * buf)
{
  glong len = strlen (buf);
  gunichar *ibuf = (gunichar *) g_malloc (sizeof (gunichar) * (len + 1));
  gchar *ibuf2 = NULL;
  gint x = 0;

  KATOOB_DEBUG_FUNCTION;

  for (x = 0; x < len; x++)
    {
      if (buf[x] != 0x7F)
	{
	  ibuf[x] = iran_system[buf[x] & 0x00FF];
	}
      else
	{
	  ibuf[x] = buf[x];
	}
    }

  ibuf2 = g_ucs4_to_utf8 (ibuf, --len, NULL, NULL, NULL);
  g_free (ibuf);
  return ibuf2;
}

/* TODO: Do we need multiple files to be opened at once here ? */
void
katoob_import_iran_system ()
{
  extern UI *katoob;
  extern conf *config;
  GtkWidget *file_selector;
  gint result;
  gchar *file;
  file_selector =
    gtk_file_selection_new (_("Please choose a file to import"));
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (file_selector),
				   config->filesel_path);
  gtk_window_set_transient_for (GTK_WINDOW (file_selector),
				GTK_WINDOW (katoob->win));
  result = gtk_dialog_run (GTK_DIALOG (file_selector));
  file =
    g_strdup (gtk_file_selection_get_filename
	      (GTK_FILE_SELECTION (file_selector)));
  gtk_widget_destroy (file_selector);

  switch (result)
    {
    case GTK_RESPONSE_OK:
      {
	gchar *tmp;
	g_free (config->filesel_path);
	tmp = g_path_get_dirname (file);
	config->filesel_path = g_strconcat (tmp, G_DIR_SEPARATOR_S, NULL);
	g_free (tmp);
	break;
      }
    default:
      {
	g_free (file);
	return;
      }
    }
  if (katoob_create_file_if_required (file))
    {
      gchar *content;
      if (katoob_file_get_contents (file, &content))
	{
/* content = NULL if the file is empty. */
	  if (!content)
	    {
	      katoob_error (_("Empty file."));
	      g_free (file);
	      return;
	    }
	  else
	    {
	      KatoobDocument *doc;
	      katoob_create_doc ();
	      doc = katoob_get_active_doc ();
	      katoob_document_set_text (doc,
					katoob_from_iran_system (content));
	      g_free (file);
	      g_free (content);
	      return;
	    }
	}
      else
	{
	  g_free (file);
	  return;
	}
    }
  else
    {
      g_free (file);
      return;
    }
}

void
katoob_export_iran_system ()
{
  extern UI *katoob;
  extern conf *config;
  GtkWidget *file_selector;
  gint result;
  gchar *file;
  FILE *fp;
  gchar *_tmp;

  KatoobDocument *doc = katoob_get_active_doc ();

  if (!doc)
    {
      return;
    }
  file_selector =
    gtk_file_selection_new (_("Please choose a file to export to"));
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (file_selector),
				   config->filesel_path);
  gtk_window_set_transient_for (GTK_WINDOW (file_selector),
				GTK_WINDOW (katoob->win));
  result = gtk_dialog_run (GTK_DIALOG (file_selector));
  file =
    g_strdup (gtk_file_selection_get_filename
	      (GTK_FILE_SELECTION (file_selector)));
  gtk_widget_destroy (file_selector);

  switch (result)
    {
    case GTK_RESPONSE_OK:
      {
	gchar *tmp;
	g_free (config->filesel_path);
	tmp = g_path_get_dirname (file);
	config->filesel_path = g_strconcat (tmp, G_DIR_SEPARATOR_S, NULL);
	g_free (tmp);
	break;
      }
    default:
      {
	g_free (file);
	return;
      }
    }

  if ((!katoob_document_get_modified (doc))
      && (!katoob_document_get_file (doc)))
    {
      g_free (file);
      return;
    }
  if (g_file_test (file, G_FILE_TEST_EXISTS))
    {
      _tmp =
	g_strdup_printf (_
			 ("Are you sure you want to overwrite the file %s ?"),
			 file);
      result = create_messagedialog (_tmp);
      g_free (_tmp);
      switch (result)
	{
	case GTK_RESPONSE_YES:
	  break;
	case GTK_RESPONSE_NO:
	  g_free (file);
	  katoob_export_iran_system ();
	  return;
	default:
	  g_free (file);
	  return;
	}
    }

  if ((fp = fopen (file, "w+")) == NULL)
    {
      katoob_error (_("The requested file couldn't be opened for saving"));
      g_free (file);
      return;
    }
  katoob_real_write
    (katoob_to_iran_system (katoob_document_get_text (doc)), fp, file);

  g_free (file);
}
