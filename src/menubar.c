/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "recent.h"
#include "mdi.h"
#include "open.h"
#include "misc.h"
#include "bidi.h"
#include "save.h"
#include "toolbar.h"
#include "undoredo.h"
#include "search.h"
#include "spell.h"
#include "prefs-dlg.h"
#include "katoobdocument.h"
#include "iransystem.h"
#include "html.h"
#include "exec-cmd.h"

#ifdef ENABLE_PRINT
#include "print-gui.h"
#endif /* ENABLE_PRINT */

#ifdef HAVE_SPELL
#include "spelldlg.h"
#endif /* HAVE_SPELL */

#ifdef ENABLE_HIGHLIGHT
#include "highlight.h"
#endif /* ENABLE_HIGHLIGHT */

static void katoob_create_file_menu (GtkWidget * menubar,
				     GtkAccelGroup * accel_group);
static void katoob_create_edit_menu (GtkWidget * menubar,
				     GtkAccelGroup * accel_group);
static void katoob_create_search_menu (GtkWidget * menubar,
				       GtkAccelGroup * accel_group);
static void katoob_create_view_menu (GtkWidget * menubar,
				     GtkAccelGroup * accel_group);
static void katoob_create_tools_menu (GtkWidget * menubar,
				      GtkAccelGroup * accel_group);
static void katoob_create_documents_menu (GtkWidget * menubar,
					  GtkAccelGroup * accel_group);
static void katoob_create_help_menu (GtkWidget * menubar,
				     GtkAccelGroup * accel_group);

/*
void katoob_menu_item_set_statusbar_hint(GtkWidget *item, gchar *hint);
*/

GtkWidget *
katoob_menubar_new (GtkWidget * win)
{
  GtkWidget *menubar;
  GtkAccelGroup *accel_group;

  accel_group = gtk_accel_group_new ();

  menubar = gtk_menu_bar_new ();

  katoob_create_file_menu (menubar, accel_group);
  katoob_create_edit_menu (menubar, accel_group);
  katoob_create_search_menu (menubar, accel_group);
  katoob_create_view_menu (menubar, accel_group);
  katoob_create_tools_menu (menubar, accel_group);
  katoob_create_documents_menu (menubar, accel_group);
  katoob_create_help_menu (menubar, accel_group);

  gtk_window_add_accel_group (GTK_WINDOW (win), accel_group);

  return menubar;
}

static void
katoob_create_file_menu (GtkWidget * menubar, GtkAccelGroup * accel_group)
{
  extern conf *config;
  extern UI *katoob;
  GtkWidget *file;
  GtkWidget *file_menu;
  GtkWidget *image;
  GtkWidget *new_item;		/* Was new, but renamed to new_item because it's a C++ keyword. */
  GtkWidget *open;
  GtkWidget *save;
  GtkWidget *save_as;
  GtkWidget *revert;
  GtkWidget *separator;
  GtkWidget *separator1;
  GtkWidget *save_copy;
  GtkWidget *close;
  GtkWidget *quit;
#ifdef ENABLE_PRINT
  GtkWidget *print;
  GtkWidget *separator2;
#endif /* ENABLE_PRINT */

  GtkWidget *import_item;
  GtkWidget *export_item;	/* Was export, C++ stuff */
  GtkWidget *import_menu;
  GtkWidget *import_html;
  GtkWidget *import_iran;
  GtkWidget *export_menu;
  GtkWidget *export_html;
  GtkWidget *export_iran;
  GtkWidget *separator3;

  file = gtk_menu_item_new_with_mnemonic (_("_File"));
  gtk_container_add (GTK_CONTAINER (menubar), file);

  file_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (file), file_menu);

  new_item = gtk_image_menu_item_new_with_mnemonic (_("_New"));
  image = gtk_image_new_from_stock ("gtk-new", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (new_item), image);
  gtk_container_add (GTK_CONTAINER (file_menu), new_item);
  gtk_widget_add_accelerator (new_item, "activate", accel_group,
			      GDK_n, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  open = gtk_image_menu_item_new_with_mnemonic (_("_Open..."));
  image = gtk_image_new_from_stock ("gtk-open", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (open), image);
  gtk_container_add (GTK_CONTAINER (file_menu), open);
  gtk_widget_add_accelerator (open, "activate", accel_group,
			      GDK_o, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  katoob->recent = gtk_image_menu_item_new_with_mnemonic (_("Recent"));
  gtk_container_add (GTK_CONTAINER (file_menu), katoob->recent);
  katoob->recent_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (katoob->recent),
			     katoob->recent_menu);
  if (config->recent_files)
    {
      GtkWidget *tmp;
      gint j = 0;
      GSList *list = config->recent_files;

      while (list)
	{
	  if ((j == config->recentno) && (config->recentno != 0))
	    {
	      break;
	    }

	  ++katoob->recentno;
	  tmp = gtk_menu_item_new_with_label ((gchar *) list->data);
	  g_datalist_set_data (&katoob->recent_files, (gchar *) list->data,
			       tmp);
	  gtk_widget_show (tmp);
	  gtk_container_add (GTK_CONTAINER (katoob->recent_menu), tmp);
	  g_signal_connect (G_OBJECT (tmp), "activate",
			    G_CALLBACK (recent_open), NULL);
	  j++;
	  list = g_slist_next (list);
	}
    }

  g_slist_foreach (config->recent_files, (GFunc) g_free, NULL);
  g_slist_free (config->recent_files);

  separator = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (file_menu), separator);
  gtk_widget_set_sensitive (separator, FALSE);
  save = gtk_image_menu_item_new_with_mnemonic (_("_Save"));
  image = gtk_image_new_from_stock ("gtk-save", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (save), image);
  gtk_container_add (GTK_CONTAINER (file_menu), save);
  gtk_widget_add_accelerator (save, "activate", accel_group,
			      GDK_s, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  save_as = gtk_image_menu_item_new_with_mnemonic (_("Save _As..."));
  image = gtk_image_new_from_stock ("gtk-save-as", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (save_as), image);
  gtk_container_add (GTK_CONTAINER (file_menu), save_as);
  gtk_widget_add_accelerator (save_as, "activate", accel_group,
			      GDK_s, (GdkModifierType)(GDK_CONTROL_MASK | GDK_SHIFT_MASK),
 		      (GtkAccelFlags)GTK_ACCEL_VISIBLE);

  save_copy = gtk_image_menu_item_new_with_mnemonic (_("Sa_ve Copy..."));
  gtk_container_add (GTK_CONTAINER (file_menu), save_copy);


  revert = gtk_image_menu_item_new_with_mnemonic (_("_Revert"));
  image = gtk_image_new_from_stock ("gtk-refresh", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (revert), image);
  gtk_container_add (GTK_CONTAINER (file_menu), revert);

  separator3 = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (file_menu), separator3);
  gtk_widget_set_sensitive (separator3, FALSE);

  import_item = gtk_menu_item_new_with_mnemonic (_("_Import"));
  gtk_container_add (GTK_CONTAINER (file_menu), import_item);
  import_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (import_item), import_menu);
  import_html =
    gtk_menu_item_new_with_mnemonic (_("_HTML Character Reference..."));
  import_iran = gtk_menu_item_new_with_mnemonic (_("_Iran System..."));
  gtk_container_add (GTK_CONTAINER (import_menu), import_html);
  gtk_container_add (GTK_CONTAINER (import_menu), import_iran);

  export_item = gtk_menu_item_new_with_mnemonic (_("_Export"));
  gtk_container_add (GTK_CONTAINER (file_menu), export_item);
  export_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (export_item), export_menu);
  export_html =
    gtk_menu_item_new_with_mnemonic (_("_HTML Character Reference..."));
  export_iran = gtk_menu_item_new_with_mnemonic (_("_Iran System..."));

  gtk_container_add (GTK_CONTAINER (export_menu), export_html);
  gtk_container_add (GTK_CONTAINER (export_menu), export_iran);

  separator1 = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (file_menu), separator1);
  gtk_widget_set_sensitive (separator1, FALSE);

#ifdef ENABLE_PRINT
  print = gtk_image_menu_item_new_with_mnemonic (_("_Print..."));
  image = gtk_image_new_from_stock ("gtk-print", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (print), image);
  gtk_container_add (GTK_CONTAINER (file_menu), print);
  gtk_widget_add_accelerator (print, "activate", accel_group,
			      GDK_p, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  separator2 = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (file_menu), separator2);
  gtk_widget_set_sensitive (separator2, FALSE);
#endif /* ENABLE_PRINT */

  close = gtk_image_menu_item_new_with_mnemonic (_("_Close"));
  image = gtk_image_new_from_stock ("gtk-close", GTK_ICON_SIZE_MENU);

  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (close), image);
  gtk_container_add (GTK_CONTAINER (file_menu), close);
  gtk_widget_add_accelerator (close, "activate", accel_group,
			      GDK_w, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  quit = gtk_image_menu_item_new_with_mnemonic (_("_Quit"));
  image = gtk_image_new_from_stock ("gtk-quit", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (quit), image);
  gtk_container_add (GTK_CONTAINER (file_menu), quit);
  gtk_widget_add_accelerator (quit, "activate", accel_group,
			      GDK_q, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  g_signal_connect (G_OBJECT (new_item), "activate",
		    G_CALLBACK (katoob_new), NULL);
  g_signal_connect (G_OBJECT (open), "activate",
		    G_CALLBACK (katoob_open), NULL);
  g_signal_connect (G_OBJECT (save), "activate",
		    G_CALLBACK (katoob_save), NULL);
  g_signal_connect (G_OBJECT (save_as), "activate",
		    G_CALLBACK (katoob_save_as), NULL);
  g_signal_connect (G_OBJECT (save_copy), "activate",
		    G_CALLBACK (katoob_save_copy), NULL);
  g_signal_connect (G_OBJECT (quit), "activate",
		    G_CALLBACK (katoob_exit), NULL);
  g_signal_connect (G_OBJECT (close), "activate",
		    G_CALLBACK (katoob_close_active_doc), NULL);
  g_signal_connect (G_OBJECT (revert), "activate",
		    G_CALLBACK (katoob_revert), NULL);

#ifdef ENABLE_PRINT
  g_signal_connect (G_OBJECT (print), "activate",
		    G_CALLBACK (katoob_create_print_dialog), NULL);
#endif /* ENABLE_PRINT */

  g_signal_connect (G_OBJECT (import_iran), "activate",
		    G_CALLBACK (katoob_import_iran_system), NULL);
  g_signal_connect (G_OBJECT (export_iran), "activate",
		    G_CALLBACK (katoob_export_iran_system), NULL);

  g_signal_connect (G_OBJECT (import_html), "activate",
		    G_CALLBACK (katoob_import_html), NULL);
  g_signal_connect (G_OBJECT (export_html), "activate",
		    G_CALLBACK (katoob_export_html), NULL);
}

static void
katoob_create_edit_menu (GtkWidget * menubar, GtkAccelGroup * accel_group)
{
  extern UI *katoob;
  GtkWidget *edit;
  GtkWidget *edit_menu;
  GtkWidget *cut;
  GtkWidget *copy;
  GtkWidget *paste;
  GtkWidget *delete_item; /* Was delete, g++! */
  GtkWidget *separator;
  GtkWidget *select_all;
  GtkWidget *separator1;
  GtkWidget *separator2;
  GtkWidget *preferences;
  GtkWidget *image;

  edit = gtk_menu_item_new_with_mnemonic (_("_Edit"));
  gtk_container_add (GTK_CONTAINER (menubar), edit);

  edit_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (edit), edit_menu);

  katoob->undo = gtk_image_menu_item_new_with_mnemonic (_("_Undo"));
  image = gtk_image_new_from_stock ("gtk-undo", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (katoob->undo), image);
  gtk_container_add (GTK_CONTAINER (edit_menu), katoob->undo);
  gtk_widget_add_accelerator (katoob->undo, "activate", accel_group,
			      GDK_z, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  katoob->redo = gtk_image_menu_item_new_with_mnemonic (_("_Redo"));
  image = gtk_image_new_from_stock ("gtk-redo", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (katoob->redo), image);
  gtk_container_add (GTK_CONTAINER (edit_menu), katoob->redo);
  gtk_widget_add_accelerator (katoob->redo, "activate", accel_group,
			      GDK_z, (GdkModifierType)(GDK_CONTROL_MASK | GDK_SHIFT_MASK),
			      GTK_ACCEL_VISIBLE);

  separator = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (edit_menu), separator);
  gtk_widget_set_sensitive (separator, FALSE);

  cut = gtk_image_menu_item_new_with_mnemonic (_("Cu_t"));
  image = gtk_image_new_from_stock ("gtk-cut", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (cut), image);
  gtk_container_add (GTK_CONTAINER (edit_menu), cut);
  gtk_widget_add_accelerator (cut, "activate", accel_group,
			      GDK_x, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  copy = gtk_image_menu_item_new_with_mnemonic (_("_Copy"));
  image = gtk_image_new_from_stock ("gtk-copy", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (copy), image);
  gtk_container_add (GTK_CONTAINER (edit_menu), copy);
  gtk_widget_add_accelerator (copy, "activate", accel_group,
			      GDK_c, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  paste = gtk_image_menu_item_new_with_mnemonic (_("_Paste"));
  image = gtk_image_new_from_stock ("gtk-paste", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (paste), image);
  gtk_container_add (GTK_CONTAINER (edit_menu), paste);
  gtk_widget_add_accelerator (paste, "activate", accel_group,
			      GDK_v, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
  delete_item = gtk_image_menu_item_new_with_mnemonic (_("_Delete"));
  image = gtk_image_new_from_stock ("gtk-delete", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (delete_item), image);
  gtk_container_add (GTK_CONTAINER (edit_menu), delete_item);
  gtk_widget_add_accelerator (delete_item, "activate", accel_group,
			      GDK_d, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  separator1 = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (edit_menu), separator1);
  gtk_widget_set_sensitive (separator1, FALSE);

  select_all = gtk_image_menu_item_new_with_mnemonic (_("_Select All"));
  gtk_container_add (GTK_CONTAINER (edit_menu), select_all);
  gtk_widget_add_accelerator (select_all, "activate", accel_group,
			      GDK_a, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  separator2 = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (edit_menu), separator2);
  gtk_widget_set_sensitive (separator2, FALSE);

  preferences = gtk_image_menu_item_new_with_mnemonic (_("Pr_eferences..."));
  image = gtk_image_new_from_stock ("gtk-preferences", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (preferences), image);
  gtk_container_add (GTK_CONTAINER (edit_menu), preferences);

  g_signal_connect (G_OBJECT (cut), "activate",
		    G_CALLBACK (katoob_cut), NULL);
  g_signal_connect (G_OBJECT (copy), "activate",
		    G_CALLBACK (katoob_copy), NULL);
  g_signal_connect (G_OBJECT (paste), "activate",
		    G_CALLBACK (katoob_paste), NULL);
  g_signal_connect (G_OBJECT (delete_item), "activate",
		    G_CALLBACK (katoob_delete), NULL);
  g_signal_connect (G_OBJECT (select_all), "activate",
		    G_CALLBACK (katoob_select_all), NULL);
  g_signal_connect (G_OBJECT (katoob->undo), "activate",
		    G_CALLBACK (katoob_undo), NULL);
  g_signal_connect (G_OBJECT (katoob->redo), "activate",
		    G_CALLBACK (katoob_redo), NULL);
  g_signal_connect (G_OBJECT (preferences), "activate",
		    G_CALLBACK (katoob_create_prefs_dlg), NULL);
}

static void
katoob_create_search_menu (GtkWidget * menubar, GtkAccelGroup * accel_group)
{
  GtkWidget *search;
  GtkWidget *search_menu;
  GtkWidget *find;
  GtkWidget *find_next;
  GtkWidget *replace;
  GtkWidget *separator;
  GtkWidget *goto_line;
  GtkWidget *image;

  search = gtk_menu_item_new_with_mnemonic (_("_Search"));
  gtk_container_add (GTK_CONTAINER (menubar), search);

  search_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (search), search_menu);

  find = gtk_image_menu_item_new_with_mnemonic (_("_Find..."));
  image = gtk_image_new_from_stock ("gtk-find", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (find), image);
  gtk_container_add (GTK_CONTAINER (search_menu), find);
  gtk_widget_add_accelerator (find, "activate", accel_group,
			      GDK_f, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  find_next = gtk_image_menu_item_new_with_mnemonic (_("Find Ne_xt"));
  gtk_container_add (GTK_CONTAINER (search_menu), find_next);
  gtk_widget_add_accelerator (find_next, "activate", accel_group,
			      GDK_g, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  replace = gtk_image_menu_item_new_with_mnemonic (_("_Replace..."));
  image =
    gtk_image_new_from_stock ("gtk-find-and-replace", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (replace), image);
  gtk_container_add (GTK_CONTAINER (search_menu), replace);
  gtk_widget_add_accelerator (replace, "activate", accel_group,
			      GDK_r, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  separator = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (search_menu), separator);
  gtk_widget_set_sensitive (separator, FALSE);

  goto_line = gtk_image_menu_item_new_with_mnemonic (_("Goto _Line..."));
  image = gtk_image_new_from_stock ("gtk-jump-to", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (goto_line), image);
  gtk_container_add (GTK_CONTAINER (search_menu), goto_line);

  g_signal_connect (G_OBJECT (find), "activate",
		    G_CALLBACK (katoob_find), NULL);
  g_signal_connect (G_OBJECT (find_next), "activate",
		    G_CALLBACK (katoob_findnext), NULL);
  g_signal_connect (G_OBJECT (replace), "activate",
		    G_CALLBACK (katoob_replace), NULL);
  g_signal_connect (G_OBJECT (goto_line), "activate",
		    G_CALLBACK (katoob_goto_line), NULL);
}

static void
katoob_create_view_menu (GtkWidget * menubar, GtkAccelGroup * accel_group)
{
  extern conf *config;
  extern UI *katoob;
  GtkWidget *view;
  GtkWidget *view_menu;
  GtkWidget *toolbar;
  GtkWidget *encodingw;
  GtkWidget *separator;
#ifdef ENABLE_HIGHLIGHT
  GtkWidget *highlighters;
  GtkWidget *highlighters_menu;
  GSList *hl = NULL;
#endif /* ENABLE_HIGHLIGHT */
  GSList *tb = NULL;

  view = gtk_menu_item_new_with_mnemonic (_("_View"));
  gtk_container_add (GTK_CONTAINER (menubar), view);

  view_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (view), view_menu);

  katoob->show_statusbar =
    gtk_check_menu_item_new_with_mnemonic (_("_Statusbar"));
  gtk_widget_show (katoob->show_statusbar);
  gtk_container_add (GTK_CONTAINER (view_menu), katoob->show_statusbar);
  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				  (katoob->show_statusbar),
				  config->statusbar);

  katoob->line_numbers =
    gtk_check_menu_item_new_with_mnemonic (_("_Line Numbers"));
  gtk_widget_show (katoob->line_numbers);
  gtk_container_add (GTK_CONTAINER (view_menu), katoob->line_numbers);
  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (katoob->line_numbers),
				  config->linenumbers);

  katoob->wrap_text = gtk_check_menu_item_new_with_mnemonic (_("_Wrap Text"));
  gtk_widget_show (katoob->wrap_text);
  gtk_container_add (GTK_CONTAINER (view_menu), katoob->wrap_text);

  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (katoob->wrap_text),
				  config->textwrap);

  separator = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (view_menu), separator);
  gtk_widget_set_sensitive (separator, FALSE);

  toolbar = gtk_menu_item_new_with_mnemonic (_("Toolbars"));
  gtk_container_add (GTK_CONTAINER (view_menu), toolbar);

  katoob->toolbar_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (toolbar), katoob->toolbar_menu);

  katoob->extended_toolbar =
    gtk_check_menu_item_new_with_mnemonic (_("_Extended Toolbar"));
  gtk_widget_show (katoob->extended_toolbar);
  gtk_container_add (GTK_CONTAINER (katoob->toolbar_menu),
		     katoob->extended_toolbar);

  katoob->show_toolbar =
    gtk_check_menu_item_new_with_mnemonic (_("_Main Toolbar"));
  gtk_widget_show (katoob->show_toolbar);
  gtk_container_add (GTK_CONTAINER (katoob->toolbar_menu),
		     katoob->show_toolbar);

  separator = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (katoob->toolbar_menu), separator);
  gtk_widget_set_sensitive (separator, FALSE);

  katoob->iconsonly =
    gtk_radio_menu_item_new_with_label (tb, _("Icons only"));
  gtk_widget_show (katoob->iconsonly);
  gtk_container_add (GTK_CONTAINER (katoob->toolbar_menu), katoob->iconsonly);
  tb =
    gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->iconsonly));

  katoob->textonly = gtk_radio_menu_item_new_with_label (tb, _("Text only"));
  gtk_widget_show (katoob->textonly);
  gtk_container_add (GTK_CONTAINER (katoob->toolbar_menu), katoob->textonly);
  tb = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->textonly));

  katoob->bothhoriz =
    gtk_radio_menu_item_new_with_label (tb, _("Text beside icons"));
  gtk_widget_show (katoob->bothhoriz);
  gtk_container_add (GTK_CONTAINER (katoob->toolbar_menu), katoob->bothhoriz);
  tb =
    gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->bothhoriz));

  katoob->both = gtk_radio_menu_item_new_with_label (tb, _("Both"));
  gtk_widget_show (katoob->both);
  gtk_container_add (GTK_CONTAINER (katoob->toolbar_menu), katoob->both);
  tb = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->both));

  encodingw = gtk_menu_item_new_with_mnemonic (_("_Encoding"));
  gtk_container_add (GTK_CONTAINER (view_menu), encodingw);

  gtk_menu_item_set_submenu (GTK_MENU_ITEM (encodingw),
			     katoob->encodings_menu);
  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				  (katoob->encodings[config->defenc]), TRUE);

#ifdef ENABLE_HIGHLIGHT
  highlighters = gtk_menu_item_new_with_mnemonic (_("_Highlight"));
  gtk_container_add (GTK_CONTAINER (view_menu), highlighters);

  highlighters_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (highlighters), highlighters_menu);

  katoob->hl_none = gtk_radio_menu_item_new_with_label (hl, _("None"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_none));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_none);
  g_signal_connect (G_OBJECT (katoob->hl_none), "activate",
		    G_CALLBACK (katoob_set_highlight), GINT_TO_POINTER(KATOOB_HIGHLIGHT_NONE));

  katoob->hl_ada = gtk_radio_menu_item_new_with_label (hl, _("Ada"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_ada));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_ada);
  g_signal_connect (G_OBJECT (katoob->hl_ada), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER( KATOOB_HIGHLIGHT_ADA));

  katoob->hl_c = gtk_radio_menu_item_new_with_label (hl, _("C"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_c));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_c);
  g_signal_connect (G_OBJECT (katoob->hl_c), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER( KATOOB_HIGHLIGHT_C));

  katoob->hl_cpp = gtk_radio_menu_item_new_with_label (hl, _("C++"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_cpp));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_cpp);
  g_signal_connect (G_OBJECT (katoob->hl_cpp), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER( KATOOB_HIGHLIGHT_CPP));

  katoob->hl_desktop = gtk_radio_menu_item_new_with_label (hl, _(".desktop"));
  hl =
    gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_desktop));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_desktop);
  g_signal_connect (G_OBJECT (katoob->hl_desktop), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER(KATOOB_HIGHLIGHT_DESKTOP));

  katoob->hl_diff = gtk_radio_menu_item_new_with_label (hl, _("Diff"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_diff));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_diff);
  g_signal_connect (G_OBJECT (katoob->hl_diff), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER( KATOOB_HIGHLIGHT_DIFF));

  katoob->hl_html = gtk_radio_menu_item_new_with_label (hl, _("HTML"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_html));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_html);
  g_signal_connect (G_OBJECT (katoob->hl_html), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER( KATOOB_HIGHLIGHT_HTML));

  katoob->hl_idl = gtk_radio_menu_item_new_with_label (hl, _("IDL"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_idl));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_idl);
  g_signal_connect (G_OBJECT (katoob->hl_idl), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER( KATOOB_HIGHLIGHT_IDL));

  katoob->hl_java = gtk_radio_menu_item_new_with_label (hl, _("Java"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_java));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_java);
  g_signal_connect (G_OBJECT (katoob->hl_java), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER(KATOOB_HIGHLIGHT_JAVA));

  katoob->hl_latex = gtk_radio_menu_item_new_with_label (hl, _("Latex"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_latex));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_latex);
  g_signal_connect (G_OBJECT (katoob->hl_latex), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER(KATOOB_HIGHLIGHT_LATEX));

  katoob->hl_perl = gtk_radio_menu_item_new_with_label (hl, _("Perl"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_perl));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_perl);
  g_signal_connect (G_OBJECT (katoob->hl_perl), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER(KATOOB_HIGHLIGHT_PERL));

  katoob->hl_po =
    gtk_radio_menu_item_new_with_label (hl, _("gettext translation"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_po));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_po);
  g_signal_connect (G_OBJECT (katoob->hl_po), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER(KATOOB_HIGHLIGHT_PO));

  katoob->hl_python = gtk_radio_menu_item_new_with_label (hl, _("Python"));
  hl =
    gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_python));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_python);
  g_signal_connect (G_OBJECT (katoob->hl_python), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER(KATOOB_HIGHLIGHT_PYTHON));

  katoob->hl_xml = gtk_radio_menu_item_new_with_label (hl, _("XML"));
  hl = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (katoob->hl_xml));
  gtk_container_add (GTK_CONTAINER (highlighters_menu), katoob->hl_xml);
  g_signal_connect (G_OBJECT (katoob->hl_xml), "activate",
		    G_CALLBACK (katoob_set_highlight),
		    GINT_TO_POINTER(KATOOB_HIGHLIGHT_XML));

#endif /* ENABLE_HIGHLIGHT */

  g_signal_connect (G_OBJECT (katoob->line_numbers), "activate",
		    G_CALLBACK (katoob_toggle_document_line_numbers), NULL);
  g_signal_connect (G_OBJECT (katoob->extended_toolbar), "activate",
		    G_CALLBACK (katoob_toggle_extended_toolbar), NULL);
  g_signal_connect (G_OBJECT (katoob->show_toolbar), "activate",
		    G_CALLBACK (katoob_toggle_toolbar), NULL);
  g_signal_connect (G_OBJECT (katoob->show_statusbar), "activate",
		    G_CALLBACK (katoob_toggle_statusbar), NULL);
  g_signal_connect (G_OBJECT (katoob->iconsonly), "activate",
		    G_CALLBACK (katoob_toolbar_icons), NULL);
  g_signal_connect (G_OBJECT (katoob->textonly), "activate",
		    G_CALLBACK (katoob_toolbar_text), NULL);
  g_signal_connect (G_OBJECT (katoob->bothhoriz), "activate",
		    G_CALLBACK (katoob_toolbar_bothhoriz), NULL);
  g_signal_connect (G_OBJECT (katoob->both), "activate",
		    G_CALLBACK (katoob_toolbar_both), NULL);
  g_signal_connect (G_OBJECT (katoob->wrap_text), "activate",
		    G_CALLBACK (katoob_toggle_document_wrap), NULL);
}

static void
katoob_create_tools_menu (GtkWidget * menubar, GtkAccelGroup * accel_group)
{

  GtkWidget *tools, *tools_menu;
  GtkWidget *exec_cmd;
  GtkWidget *separator;

#ifdef HAVE_SPELL
  extern conf *config;
  extern UI *katoob;
  GtkWidget *spelldlg, *image;
#endif /* HAVE_SPELL */

  tools = gtk_menu_item_new_with_mnemonic (_("_Tools"));
  gtk_container_add (GTK_CONTAINER (menubar), tools);

  tools_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (tools), tools_menu);
  exec_cmd = gtk_menu_item_new_with_mnemonic (_("_Execute Command On Buffer..."));
  gtk_container_add (GTK_CONTAINER (tools_menu), exec_cmd);
  gtk_widget_add_accelerator (exec_cmd, "activate", accel_group,
			      GDK_e, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  separator = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (tools_menu), separator);
  gtk_widget_set_sensitive (separator, FALSE);

  g_signal_connect (G_OBJECT (exec_cmd), "activate",
		    G_CALLBACK (exec_cmd_cb), NULL);

#ifdef HAVE_SPELL
  spelldlg = gtk_image_menu_item_new_with_mnemonic (_("_Spell Check..."));
  image = gtk_image_new_from_stock ("gtk-spell-check", GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (spelldlg), image);
  gtk_container_add (GTK_CONTAINER (tools_menu), spelldlg);
  gtk_widget_add_accelerator (spelldlg, "activate", accel_group,
			      GDK_F7, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  katoob->spell_check =
    gtk_check_menu_item_new_with_mnemonic (_("_Autocheck Spelling"));
  gtk_container_add (GTK_CONTAINER (tools_menu), katoob->spell_check);
  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				  (katoob->spell_check), config->spell_check);
  g_signal_connect (G_OBJECT (spelldlg), "activate",
		    G_CALLBACK (katoob_create_spelldlg), NULL);
  g_signal_connect (G_OBJECT (katoob->spell_check), "activate",
		    G_CALLBACK (katoob_toggle_spellcheck), NULL);

#endif /* HAVE_SPELL */
}

static void
katoob_create_documents_menu (GtkWidget * menubar,
			      GtkAccelGroup * accel_group)
{
  extern UI *katoob;
  GtkWidget *documents;
  GtkWidget *save_all;
  GtkWidget *close_all;
  GtkWidget *separator;

  documents = gtk_menu_item_new_with_mnemonic (_("_Documents"));
  gtk_container_add (GTK_CONTAINER (menubar), documents);

  katoob->documents_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (documents),
			     katoob->documents_menu);

  save_all = gtk_image_menu_item_new_with_mnemonic (_("Save _All"));
  gtk_container_add (GTK_CONTAINER (katoob->documents_menu), save_all);

  close_all = gtk_image_menu_item_new_with_mnemonic (_("Clos_e All"));
  gtk_container_add (GTK_CONTAINER (katoob->documents_menu), close_all);
  gtk_widget_add_accelerator (close_all, "activate", accel_group,
			      GDK_w, (GdkModifierType)(GDK_CONTROL_MASK | GDK_SHIFT_MASK),
			      GTK_ACCEL_VISIBLE);

  separator = gtk_menu_item_new ();
  gtk_container_add (GTK_CONTAINER (katoob->documents_menu), separator);
  gtk_widget_set_sensitive (separator, FALSE);

  g_signal_connect (G_OBJECT (close_all), "activate",
		    G_CALLBACK (katoob_close_all), NULL);
  g_signal_connect (G_OBJECT (save_all), "activate",
		    G_CALLBACK (katoob_save_all), NULL);
}

static void
katoob_create_help_menu (GtkWidget * menubar, GtkAccelGroup * accel_group)
{
  GtkWidget *help, *help_menu, *about;

  help = gtk_menu_item_new_with_mnemonic (_("_Help"));
  gtk_container_add (GTK_CONTAINER (menubar), help);

  help_menu = gtk_menu_new ();
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (help), help_menu);

  about = gtk_image_menu_item_new_with_mnemonic (_("Abou_t"));
  gtk_container_add (GTK_CONTAINER (help_menu), about);
  gtk_widget_add_accelerator (about, "activate", accel_group,
			      GDK_t, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  g_signal_connect (G_OBJECT (about), "activate",
		    G_CALLBACK (katoob_about), NULL);
}
