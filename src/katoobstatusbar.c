/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include <gtk/gtk.h>
#include "katoobstatusbar.h"

extern conf *config;

struct _KatoobStatusbarPrivate
{
  GtkWidget *label;
  GtkWidget *encoding_label;
  GtkWidget *statusbar;
  GtkWidget *overwrite_statusbar;
  GtkWidget *red;
  GtkWidget *green;
  gboolean visible;
};

static void katoob_statusbar_class_init (KatoobStatusbarClass * klass);
static void katoob_statusbar_init (KatoobStatusbar * sbar);

static GtkHBoxClass *parent_class;

GType
katoob_statusbar_get_type (void)
{
  static GType statusbar_type = 0;
  KATOOB_DEBUG_FUNCTION;

  if (!statusbar_type)
    {
      static const GTypeInfo statusbar_info = {
	sizeof (KatoobStatusbarClass),
	NULL,
	NULL,
	(GClassInitFunc) katoob_statusbar_class_init,
	NULL,
	NULL,
	sizeof (KatoobStatusbar),
	0,
	(GInstanceInitFunc) katoob_statusbar_init,
      };
      statusbar_type =
	g_type_register_static (GTK_TYPE_HBOX, "KatoobStatusbar",
				&statusbar_info, (GTypeFlags)0);
    }
  return statusbar_type;
}

static void
katoob_statusbar_class_init (KatoobStatusbarClass * klass)
{
  KATOOB_DEBUG_FUNCTION;

  parent_class = (GtkHBoxClass *)g_type_class_peek_parent (klass);
}

static void
katoob_statusbar_init (KatoobStatusbar * sbar)
{
  gchar *_tmp;
  KATOOB_DEBUG_FUNCTION;

  sbar->priv = g_new0 (KatoobStatusbarPrivate, 1);

  gtk_widget_set_direction (GTK_WIDGET (sbar), GTK_TEXT_DIR_LTR);

  sbar->priv->statusbar = gtk_statusbar_new ();
  sbar->priv->label = gtk_label_new ("");
  sbar->priv->encoding_label = gtk_label_new ("");
  _tmp = g_strdup_printf ("%s%sred.png", PACKAGE_DATA_DIR, G_DIR_SEPARATOR_S);
  sbar->priv->red = gtk_image_new_from_file (_tmp);
  g_free (_tmp);

  _tmp =
    g_strdup_printf ("%s%sgreen.png", PACKAGE_DATA_DIR, G_DIR_SEPARATOR_S);
  sbar->priv->green = gtk_image_new_from_file (_tmp);
  g_free (_tmp);

  sbar->priv->overwrite_statusbar = gtk_statusbar_new ();
  gtk_statusbar_set_has_resize_grip (GTK_STATUSBAR
				     (sbar->priv->overwrite_statusbar),
				     FALSE);

  gtk_box_pack_start (GTK_BOX (sbar), sbar->priv->green, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (sbar), sbar->priv->red, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (sbar), sbar->priv->label, TRUE, TRUE, 10);
  gtk_box_pack_start (GTK_BOX (sbar), sbar->priv->encoding_label, FALSE,
		      FALSE, 5);
  gtk_box_pack_start (GTK_BOX (sbar), sbar->priv->overwrite_statusbar, FALSE,
		      FALSE, 0);
  gtk_box_pack_start (GTK_BOX (sbar), sbar->priv->statusbar, FALSE, FALSE, 0);

  gtk_widget_set_size_request (sbar->priv->encoding_label, 150, -1);
  gtk_widget_set_size_request (GTK_WIDGET (sbar->priv->overwrite_statusbar),
			       75, -1);
  gtk_widget_set_size_request (sbar->priv->statusbar, 150, -1);
  katoob_statusbar_set_cursor_position (sbar, 1, 1);
  katoob_statusbar_set_overwrite (sbar, FALSE);
}

GtkWidget *
katoob_statusbar_new ()
{
  KATOOB_DEBUG_FUNCTION;

  return (GtkWidget *)g_object_new (KATOOB_TYPE_STATUSBAR, NULL);
}

void
katoob_statusbar_set_overwrite (KatoobStatusbar * sbar, gboolean overwrite)
{
  gchar *tmp;
  KATOOB_DEBUG_FUNCTION;
  if (overwrite)
    {
      tmp = g_strdup (_(" OVR "));
    }
  else
    {
      tmp = g_strdup (_(" INS "));
    }

  katoob_debug (tmp);

  gtk_statusbar_pop (GTK_STATUSBAR (sbar->priv->overwrite_statusbar), 0);
  gtk_statusbar_push (GTK_STATUSBAR (sbar->priv->overwrite_statusbar), 0,
		      tmp);
  g_free (tmp);
}

void
katoob_statusbar_set_modified (KatoobStatusbar * sbar, gboolean modified)
{
  KATOOB_DEBUG_FUNCTION;

  if (modified)
    {
      gtk_widget_hide (sbar->priv->green);
      gtk_widget_show (sbar->priv->red);
    }
  else
    {
      gtk_widget_hide (sbar->priv->red);
      gtk_widget_show (sbar->priv->green);
    }
}

void
katoob_statusbar_set_cursor_position (KatoobStatusbar * sbar, gint col,
				      gint row)
{
  gchar *tmp;

  KATOOB_DEBUG_FUNCTION;

  tmp = g_strdup_printf (_("  L: %d, C: %d"), row, col);

  gtk_statusbar_pop (GTK_STATUSBAR (sbar->priv->statusbar), 0);
  gtk_statusbar_push (GTK_STATUSBAR (sbar->priv->statusbar), 0, tmp);
  g_free (tmp);
}

void
katoob_statusbar_show_hint (KatoobStatusbar * sbar, gchar * text)
{
  KATOOB_DEBUG_FUNCTION;
/* gtk_label_set_text(GTK_LABEL(sbar->label), text); */
/* TODO: implement me */

}

void
katoob_statusbar_hide_hint (KatoobStatusbar * sbar)
{
  KATOOB_DEBUG_FUNCTION;
/* TODO: implement me */
}

void
katoob_statusbar_show (KatoobStatusbar * sbar)
{
  KATOOB_DEBUG_FUNCTION;
  gtk_widget_show (GTK_WIDGET (sbar));
}

void
katoob_statusbar_hide (KatoobStatusbar * sbar)
{
  KATOOB_DEBUG_FUNCTION;
  gtk_widget_hide (GTK_WIDGET (sbar));
}

void
katoob_statusbar_set_encoding (KatoobStatusbar * sbar, gint encoding)
{
  KATOOB_DEBUG_FUNCTION;
  gtk_label_set_label (GTK_LABEL (sbar->priv->encoding_label),
		       katoob_encodings_get_name_by_number (encoding));
}
