/* Katoob
 * Copyright (c) 2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __PRINT_PRIVATE_H__
#define __PRINT_PRIVATE_H__

#ifdef ENABLE_PRINT

#include <glib.h>

/* *INDENT-OFF* */

/* from: http://astronomy.swin.edu.au/~pbourke/geomformats/postscript/ */
const _Papers Papers[] = {
			 {"Letter", 612 , 792 },
			 {"Legal" , 612 , 1008},
			 {"A0"    , 2384, 3370},
			 {"A1"    , 1684, 2384},
			 {"A2"    , 1191, 1684},
			 {"A3"    , 842 , 1191},
			 {"A4"    , 595 , 842 },
			 {"A5"    , 420 , 595 },
			 {"A6"    , 297 , 420 },
			 {"A7"    , 210 , 297 },
			 {"A8"    , 148 , 210 },
			 {"A9"    , 105 , 148 },
			 {"B0"    , 2920, 4127},
			 {"B1"    , 2064, 2920},
			 {"B2"    , 1460, 2064},
			 {"B3"    , 1032, 1460},
			 {"B4"    , 729 , 1032},
			 {"B5"    , 516 , 729 },
			 {"B6"    , 363 , 516 },
			 {"B7"    , 258 , 363 },
			 {"B8"    , 181 , 258 },
			 {"B9"    , 127 , 181 },
			 {"B10"   , 91  , 127 },
			 {NULL    , 0x0 , 0x0 }
			 };
/* *INDENT-ON* */

#endif /* ENABLE_PRINT */

#endif /* __PRINT_PRIVATE_H__ */
