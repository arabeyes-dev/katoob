/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef ENABLE_PRINT

#include "katoob.h"
#include <gtk/gtk.h>
#include "print.h"
#include "print-gui.h"
#include "mdi.h"
#include "katoobdocument.h"
#include "misc.h"

/* FIXME: This is ugly :( */
gchar *browse_dialog_title = NULL;

extern _Papers Papers[];
extern PConfig *PConf;

GtkWidget *dialog = NULL;

GtkWidget *all_radio;
GtkWidget *selection_radio;
GtkWidget *spinbutton;
GtkWidget *printer_menu;
GtkWidget *location_menu;
GtkWidget *width_spin;
GtkWidget *height_spin;
GtkWidget *custom_check;
GtkWidget *orientation_menu;
GtkWidget *top_margin_spin;
GtkWidget *left_margin_spin;
GtkWidget *right_margin_spin;
GtkWidget *bottom_margin_spin;
GtkWidget *optionmenu;
GtkWidget *paper_menu;
GtkWidget *width_label;
GtkWidget *height_label;
GtkWidget *browse_button;
GtkWidget *location_entry;
GtkWidget *portrait_img;
GtkWidget *landscape_img;

void pdf_activate (GtkWidget * widget, gpointer data);
void ps_activate (GtkWidget * widget, gpointer data);

void file_activate (GtkWidget * widget, gpointer data);
void lpr_activate (GtkWidget * widget, gpointer data);
void custom_activate (GtkWidget * widget, gpointer data);

void portrait_activate (GtkWidget * widget, gpointer data);
void landscape_activate (GtkWidget * widget, gpointer data);

void inches_activate (GtkWidget * widget, gpointer data);
void points_activate (GtkWidget * widget, gpointer data);

void custom_check_toggled (GtkToggleButton * button, gpointer data);

void generate_print_config ();

void preview_button_clicked ();
void cancel_button_clicked ();
void print_button_clicked (GtkWidget * w, gpointer user_data);
void browse_button_clicked ();

void
katoob_create_print_dialog ()
{
  GtkWidget *dialog_vbox;
  GtkWidget *notebook;
  GtkWidget *job_vbox;
  GtkWidget *range_frame;
  GtkWidget *range_vbox;
  GSList *all_radio_group = NULL;
  GtkWidget *range_label;
  GtkWidget *copies_frame;
  GtkWidget *copies_hbox;
  GtkWidget *copies_label;
  GtkObject *spinbutton_adj;
  GtkWidget *copies_frame_label;
  GtkWidget *job_label;
  GtkWidget *printer_frame;
  GtkWidget *printer_table;
  GtkWidget *printer_label;
  GtkWidget *location_label;
  GtkWidget *menu1;
  GtkWidget *generic_postscript;
  GtkWidget *create_a_pdf_document;
  GtkWidget *menu2;
  GtkWidget *file;
  GtkWidget *lpr;
  GtkWidget *custom;
  GtkWidget *printer_frame_label;
  GtkWidget *printer_tab_label;
  GtkWidget *paper_hbox;
  GtkWidget *paper_frame;
  GtkWidget *table4;
  GtkWidget *size_label;
  GtkWidget *orientation_label;
  GtkObject *width_spin_adj;
  GtkObject *height_spin_adj;
  GtkWidget *menu3;
  GtkWidget *points;
  GtkWidget *inches;
  GtkWidget *menu4;
  GtkWidget *portrait;
  GtkWidget *landscape;
  GtkWidget *paper_label;
  GtkWidget *preview_frame;
  GtkWidget *preview_label;
  GtkWidget *frame5;
  GtkWidget *margins_table;
  GtkWidget *top_margin_label;
  GtkWidget *left_margin_label;
  GtkWidget *right_margin_label;
  GtkWidget *bottom_margin_label;
  GtkObject *top_margin_spin_adj;
  GtkObject *left_margin_spin_adj;
  GtkObject *right_margin_spin_adj;
  GtkObject *bottom_margin_spin_adj;
  GtkWidget *margins_label;
  GtkWidget *paper_tab_label;
  GtkWidget *dialog_action_area1;
  GtkWidget *preview_button;
  GtkWidget *cancel_button;
  GtkWidget *print_button;
  GtkWidget *menu, *menuitem;
  GtkWidget *box;
  GtkTextIter start, end;
  gchar *_file;
  KatoobDocument *doc;

  if (dialog)
    {
      gtk_window_present (GTK_WINDOW (dialog));
      return;
    }

  doc = katoob_get_active_doc ();

  if (!doc)
    {
      return;
    }

  dialog = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (dialog), _("Print"));
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);

  dialog_vbox = GTK_DIALOG (dialog)->vbox;

  notebook = gtk_notebook_new ();
  gtk_box_pack_start (GTK_BOX (dialog_vbox), notebook, TRUE, TRUE, 0);

/* Job tab */
  job_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (notebook), job_vbox);
  gtk_notebook_set_tab_label_packing (GTK_NOTEBOOK (notebook), job_vbox,
				      FALSE, FALSE, GTK_PACK_START);

  range_frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (job_vbox), range_frame, FALSE, FALSE, 0);

  range_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (range_frame), range_vbox);

  all_radio = gtk_radio_button_new_with_mnemonic (NULL, _("_All"));
  gtk_box_pack_start (GTK_BOX (range_vbox), all_radio, FALSE, FALSE, 0);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (all_radio), all_radio_group);
  all_radio_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (all_radio));

  selection_radio =
    gtk_radio_button_new_with_mnemonic (NULL, _("_Selection"));
  gtk_box_pack_start (GTK_BOX (range_vbox), selection_radio, FALSE, FALSE, 0);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (selection_radio),
			      all_radio_group);
  all_radio_group =
    gtk_radio_button_get_group (GTK_RADIO_BUTTON (selection_radio));

  range_label = gtk_label_new (_("Print range"));
  gtk_frame_set_label_widget (GTK_FRAME (range_frame), range_label);
  gtk_label_set_justify (GTK_LABEL (range_label), GTK_JUSTIFY_LEFT);

  copies_frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (job_vbox), copies_frame, FALSE, FALSE, 0);

  copies_hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (copies_frame), copies_hbox);
  gtk_container_set_border_width (GTK_CONTAINER (copies_hbox), 5);

  copies_label = gtk_label_new_with_mnemonic (_("N_umber of copies"));
  gtk_box_pack_start (GTK_BOX (copies_hbox), copies_label, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (copies_label), GTK_JUSTIFY_LEFT);

  spinbutton_adj = gtk_adjustment_new (PConf->copies, 1, 100, 1, 10, 10);
  spinbutton = gtk_spin_button_new (GTK_ADJUSTMENT (spinbutton_adj), 1, 0);
  gtk_box_pack_start (GTK_BOX (copies_hbox), spinbutton, FALSE, FALSE, 0);

  copies_frame_label = gtk_label_new (_("Copies"));
  gtk_frame_set_label_widget (GTK_FRAME (copies_frame), copies_frame_label);
  gtk_label_set_justify (GTK_LABEL (copies_frame_label), GTK_JUSTIFY_LEFT);

  job_label = gtk_label_new (_("Job"));
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook),
			      gtk_notebook_get_nth_page (GTK_NOTEBOOK
							 (notebook), 0),
			      job_label);
  gtk_label_set_justify (GTK_LABEL (job_label), GTK_JUSTIFY_LEFT);

/* Printer tab */
  printer_frame = gtk_frame_new (NULL);
  gtk_container_add (GTK_CONTAINER (notebook), printer_frame);

  printer_table = gtk_table_new (2, 4, FALSE);
  gtk_container_add (GTK_CONTAINER (printer_frame), printer_table);

  printer_label = gtk_label_new_with_mnemonic (_("Pr_inter:"));
  gtk_table_attach (GTK_TABLE (printer_table), printer_label, 0, 1, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (printer_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (printer_label), 0, 0.5);

  location_label = gtk_label_new_with_mnemonic (_("_Location:"));
  gtk_table_attach (GTK_TABLE (printer_table), location_label, 0, 1, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (location_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (location_label), 0, 0.5);

  printer_menu = gtk_option_menu_new ();
  gtk_table_attach (GTK_TABLE (printer_table), printer_menu, 1, 2, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  menu1 = gtk_menu_new ();

  generic_postscript =
    gtk_menu_item_new_with_mnemonic (_("Generic Postscript"));
  gtk_container_add (GTK_CONTAINER (menu1), generic_postscript);

  create_a_pdf_document =
    gtk_menu_item_new_with_mnemonic (_("Create a PDF document"));
  gtk_container_add (GTK_CONTAINER (menu1), create_a_pdf_document);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (printer_menu), menu1);

  location_menu = gtk_option_menu_new ();
  gtk_table_attach (GTK_TABLE (printer_table), location_menu, 1, 2, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_option_menu_set_history (GTK_OPTION_MENU (location_menu), 1);

  menu2 = gtk_menu_new ();

  file = gtk_menu_item_new_with_mnemonic (_("File"));
  gtk_container_add (GTK_CONTAINER (menu2), file);

  lpr = gtk_menu_item_new_with_mnemonic (_("lpr"));
  gtk_container_add (GTK_CONTAINER (menu2), lpr);

  custom = gtk_menu_item_new_with_mnemonic (_("Custom"));
  gtk_container_add (GTK_CONTAINER (menu2), custom);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (location_menu), menu2);

  location_entry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (printer_table), location_entry, 2, 3, 1, 2,
		    (GtkAttachOptions) (0), (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_activates_default (GTK_ENTRY (location_entry), TRUE);

  browse_button = gtk_button_new_with_mnemonic (_("_Browse"));
  gtk_table_attach (GTK_TABLE (printer_table), browse_button, 3, 4, 1, 2,
		    (GtkAttachOptions) (0), (GtkAttachOptions) (0), 0, 0);

  printer_frame_label = gtk_label_new (_("Printer:"));
  gtk_frame_set_label_widget (GTK_FRAME (printer_frame), printer_frame_label);
  gtk_label_set_justify (GTK_LABEL (printer_frame_label), GTK_JUSTIFY_LEFT);

  printer_tab_label = gtk_label_new (_("Printer"));
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook),
			      gtk_notebook_get_nth_page (GTK_NOTEBOOK
							 (notebook), 1),
			      printer_tab_label);
  gtk_label_set_justify (GTK_LABEL (printer_tab_label), GTK_JUSTIFY_LEFT);

/* Paper tab */
  paper_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (notebook), paper_hbox);

  paper_frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (paper_hbox), paper_frame, TRUE, TRUE, 0);

  table4 = gtk_table_new (4, 4, FALSE);
  gtk_container_add (GTK_CONTAINER (paper_frame), table4);

  size_label = gtk_label_new_with_mnemonic (_("Paper _size:"));
  gtk_table_attach (GTK_TABLE (table4), size_label, 0, 1, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (size_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (size_label), 0, 0.5);

  orientation_label = gtk_label_new_with_mnemonic (_("Page _orientation:"));
  gtk_table_attach (GTK_TABLE (table4), orientation_label, 0, 1, 3, 4,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (orientation_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (orientation_label), 0, 0.5);

  width_spin_adj =
    gtk_adjustment_new (PConf->paper_width, 1, 10000, 0.1, 10, 10);
  width_spin = gtk_spin_button_new (GTK_ADJUSTMENT (width_spin_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (table4), width_spin, 2, 3, 1, 2,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  height_spin_adj =
    gtk_adjustment_new (PConf->paper_height, 1, 1000, 0.1, 10, 10);
  height_spin = gtk_spin_button_new (GTK_ADJUSTMENT (height_spin_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (table4), height_spin, 2, 3, 2, 3,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  optionmenu = gtk_option_menu_new ();
  gtk_table_attach (GTK_TABLE (table4), optionmenu, 3, 4, 1, 2,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  menu3 = gtk_menu_new ();

  inches = gtk_menu_item_new_with_mnemonic (_("Inches"));
  gtk_container_add (GTK_CONTAINER (menu3), inches);

  points = gtk_menu_item_new_with_mnemonic (_("Points"));
  gtk_container_add (GTK_CONTAINER (menu3), points);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu), menu3);

  paper_menu = gtk_option_menu_new ();
  gtk_table_attach (GTK_TABLE (table4), paper_menu, 1, 2, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  menu = gtk_menu_new ();
  {
    _Papers *P = Papers;
    while (P->name)
      {
	menuitem = gtk_menu_item_new_with_mnemonic (P->name);
	gtk_container_add (GTK_CONTAINER (menu), menuitem);
	P++;
      }
  }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (paper_menu), menu);

  width_label = gtk_label_new_with_mnemonic (_("_Width:"));
  gtk_table_attach (GTK_TABLE (table4), width_label, 1, 2, 1, 2,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (width_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (width_label), 0, 0.5);

  height_label = gtk_label_new_with_mnemonic (_("_Height:"));
  gtk_table_attach (GTK_TABLE (table4), height_label, 1, 2, 2, 3,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (height_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (height_label), 0, 0.5);

  custom_check = gtk_check_button_new_with_mnemonic (_("Cus_tom size"));
  gtk_table_attach (GTK_TABLE (table4), custom_check, 3, 4, 0, 1,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  orientation_menu = gtk_option_menu_new ();
  gtk_table_attach (GTK_TABLE (table4), orientation_menu, 1, 2, 3, 4,
		    (GtkAttachOptions) (GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  menu4 = gtk_menu_new ();

  portrait = gtk_menu_item_new_with_mnemonic (_("Portrait"));
  gtk_container_add (GTK_CONTAINER (menu4), portrait);

  landscape = gtk_menu_item_new_with_mnemonic (_("Landscape"));
  gtk_container_add (GTK_CONTAINER (menu4), landscape);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (orientation_menu), menu4);

  paper_label = gtk_label_new (_("Paper and layout"));
  gtk_frame_set_label_widget (GTK_FRAME (paper_frame), paper_label);
  gtk_label_set_justify (GTK_LABEL (paper_label), GTK_JUSTIFY_LEFT);

  preview_frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (paper_hbox), preview_frame, TRUE, TRUE, 0);
  box = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (preview_frame), box);

  _file = g_strdup_printf ("%s/portrait.png", PACKAGE_DATA_DIR);
  portrait_img = gtk_image_new_from_file (_file);
  g_free (_file);

  gtk_box_pack_start (GTK_BOX (box), portrait_img, TRUE, TRUE, 0);

  _file = g_strdup_printf ("%s/landscape.png", PACKAGE_DATA_DIR);
  landscape_img = gtk_image_new_from_file (_file);
  g_free (_file);

  gtk_box_pack_start (GTK_BOX (box), landscape_img, TRUE, TRUE, 0);

  preview_label = gtk_label_new (_("Preview"));
  gtk_frame_set_label_widget (GTK_FRAME (preview_frame), preview_label);
  gtk_label_set_justify (GTK_LABEL (preview_label), GTK_JUSTIFY_LEFT);

  frame5 = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (paper_hbox), frame5, TRUE, TRUE, 0);

  margins_table = gtk_table_new (4, 2, TRUE);
  gtk_container_add (GTK_CONTAINER (frame5), margins_table);

  top_margin_label = gtk_label_new (_("Top"));
  gtk_table_attach (GTK_TABLE (margins_table), top_margin_label, 0, 1, 0, 1,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (top_margin_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (top_margin_label), 0, 0.5);

  left_margin_label = gtk_label_new (_("Left"));
  gtk_table_attach (GTK_TABLE (margins_table), left_margin_label, 0, 1, 1, 2,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (left_margin_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (left_margin_label), 0, 0.5);

  right_margin_label = gtk_label_new (_("Right"));
  gtk_table_attach (GTK_TABLE (margins_table), right_margin_label, 0, 1, 2, 3,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (right_margin_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (right_margin_label), 0, 0.5);

  bottom_margin_label = gtk_label_new (_("Bottom"));
  gtk_table_attach (GTK_TABLE (margins_table), bottom_margin_label, 0, 1, 3,
		    4, (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (bottom_margin_label), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (bottom_margin_label), 0, 0.5);

  top_margin_spin_adj =
    gtk_adjustment_new (PConf->top_margin, 0, 100, 0.1, 1, 10);
  top_margin_spin =
    gtk_spin_button_new (GTK_ADJUSTMENT (top_margin_spin_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (margins_table), top_margin_spin, 1, 2, 0, 1,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  left_margin_spin_adj =
    gtk_adjustment_new (PConf->left_margin, 0, 100, 0.1, 10, 10);
  left_margin_spin =
    gtk_spin_button_new (GTK_ADJUSTMENT (left_margin_spin_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (margins_table), left_margin_spin, 1, 2, 1, 2,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  right_margin_spin_adj =
    gtk_adjustment_new (PConf->right_margin, 0, 100, 0.1, 10, 10);
  right_margin_spin =
    gtk_spin_button_new (GTK_ADJUSTMENT (right_margin_spin_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (margins_table), right_margin_spin, 1, 2, 2, 3,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  bottom_margin_spin_adj =
    gtk_adjustment_new (PConf->bottom_margin, 0, 100, 0.1, 10, 10);
  bottom_margin_spin =
    gtk_spin_button_new (GTK_ADJUSTMENT (bottom_margin_spin_adj), 1, 0);
  gtk_table_attach (GTK_TABLE (margins_table), bottom_margin_spin, 1, 2, 3, 4,
		    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		    (GtkAttachOptions) (0), 0, 0);

  margins_label = gtk_label_new (_("Margins (inch)"));
  gtk_frame_set_label_widget (GTK_FRAME (frame5), margins_label);
  gtk_label_set_justify (GTK_LABEL (margins_label), GTK_JUSTIFY_LEFT);

  paper_tab_label = gtk_label_new (_("Paper"));
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook),
			      gtk_notebook_get_nth_page (GTK_NOTEBOOK
							 (notebook), 2),
			      paper_tab_label);
  gtk_label_set_justify (GTK_LABEL (paper_tab_label), GTK_JUSTIFY_LEFT);

  dialog_action_area1 = GTK_DIALOG (dialog)->action_area;
  gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area1),
			     GTK_BUTTONBOX_END);

  preview_button = gtk_button_new_from_stock ("gtk-print-preview");

/* TODO: This should be enabled after implementing the preview code. */
/*  gtk_dialog_add_action_widget (GTK_DIALOG (dialog), preview_button,
				GTK_RESPONSE_APPLY);
  GTK_WIDGET_SET_FLAGS (preview_button, GTK_CAN_DEFAULT);
*/

  cancel_button = gtk_button_new_from_stock ("gtk-cancel");
  gtk_dialog_add_action_widget (GTK_DIALOG (dialog), cancel_button,
				GTK_RESPONSE_CANCEL);
  GTK_WIDGET_SET_FLAGS (cancel_button, GTK_CAN_DEFAULT);

  print_button = gtk_button_new_from_stock ("gtk-print");
  gtk_dialog_add_action_widget (GTK_DIALOG (dialog), print_button,
				GTK_RESPONSE_OK);
  GTK_WIDGET_SET_FLAGS (print_button, GTK_CAN_DEFAULT);

  gtk_label_set_mnemonic_widget (GTK_LABEL (copies_label), spinbutton);
  gtk_label_set_mnemonic_widget (GTK_LABEL (printer_label), printer_menu);
  gtk_label_set_mnemonic_widget (GTK_LABEL (location_label), location_menu);
  gtk_label_set_mnemonic_widget (GTK_LABEL (size_label), paper_menu);
  gtk_label_set_mnemonic_widget (GTK_LABEL (orientation_label),
				 orientation_menu);
  gtk_label_set_mnemonic_widget (GTK_LABEL (width_label), width_spin);
  gtk_label_set_mnemonic_widget (GTK_LABEL (height_label), height_spin);

  gtk_widget_grab_default (print_button);

  gtk_option_menu_set_history (GTK_OPTION_MENU (optionmenu),
			       PConf->paper_size_unite - 1);

/* Signals */
  g_signal_connect (G_OBJECT (generic_postscript), "activate",
		    G_CALLBACK (ps_activate), location_menu);
  g_signal_connect (G_OBJECT (create_a_pdf_document), "activate",
		    G_CALLBACK (pdf_activate), location_menu);

  g_signal_connect (G_OBJECT (file), "activate", G_CALLBACK (file_activate),
		    NULL);
  g_signal_connect (G_OBJECT (lpr), "activate", G_CALLBACK (lpr_activate),
		    NULL);

  g_signal_connect (G_OBJECT (custom), "activate",
		    G_CALLBACK (custom_activate), NULL);

  g_signal_connect (G_OBJECT (portrait), "activate",
		    G_CALLBACK (portrait_activate), preview_frame);
  g_signal_connect (G_OBJECT (landscape), "activate",
		    G_CALLBACK (landscape_activate), preview_frame);

  g_signal_connect (G_OBJECT (inches), "activate",
		    G_CALLBACK (inches_activate), NULL);
  g_signal_connect (G_OBJECT (points), "activate",
		    G_CALLBACK (points_activate), NULL);

  g_signal_connect (G_OBJECT (custom_check), "toggled",
		    G_CALLBACK (custom_check_toggled), NULL);

/* Our buttons. */
  g_signal_connect (G_OBJECT (preview_button), "clicked",
		    G_CALLBACK (preview_button_clicked), NULL);
  g_signal_connect (G_OBJECT (cancel_button), "clicked",
		    G_CALLBACK (cancel_button_clicked), NULL);
  g_signal_connect (G_OBJECT (print_button), "clicked",
		    G_CALLBACK (print_button_clicked), doc);

  g_signal_connect (G_OBJECT (browse_button), "clicked",
		    G_CALLBACK (browse_button_clicked), NULL);

/* Now adjust the dialog */
  if (katoob_document_get_selection_bounds (doc, &start, &end))
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (selection_radio),
				    TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (all_radio), TRUE);
    }

  gtk_option_menu_set_history (GTK_OPTION_MENU (printer_menu),
			       PConf->printer - 1);

  gtk_option_menu_set_history (GTK_OPTION_MENU (location_menu),
			       PConf->location - 1);

  if (PConf->location == LOCATION_FILE)
    {
      file_activate (NULL, NULL);
    }
  else if (PConf->location == LOCATION_CUSTOM)
    {
      custom_activate (NULL, NULL);
    }
  else
    {
      lpr_activate (NULL, NULL);
    }

  if (PConf->paper_size == -1)
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (custom_check), TRUE);
    }
  else
    {
      gtk_option_menu_set_history (GTK_OPTION_MENU (paper_menu),
				   PConf->paper_size);
    }

  gtk_option_menu_set_history (GTK_OPTION_MENU (orientation_menu),
			       PConf->page_orientation - 1);

  custom_check_toggled (GTK_TOGGLE_BUTTON (custom_check), NULL);

  gtk_widget_show_all (dialog);

  if (PConf->page_orientation == PAGE_ORIENTATION_PORTRAIT)
    {
      portrait_activate (NULL, NULL);
    }
  else
    {
      landscape_activate (NULL, NULL);
    }

  if (PConf->printer == PRINTER_POSTSCRIPT)
    {
      ps_activate (NULL, location_menu);
    }
  else
    {
      pdf_activate (NULL, location_menu);
    }

  g_signal_connect (G_OBJECT (dialog), "destroy",
		    G_CALLBACK (gtk_widget_destroyed), &dialog);
}

void
browse_button_clicked ()
{
  GtkWidget *dlg;
  gint response;
  gchar *txt = NULL;
  dlg = gtk_file_selection_new (browse_dialog_title);

  txt = (gchar *) gtk_entry_get_text (GTK_ENTRY (location_entry));

  if (txt)
    {
      gtk_file_selection_set_filename (GTK_FILE_SELECTION (dlg), txt);
    }

  response = gtk_dialog_run (GTK_DIALOG (dlg));

  if (response == GTK_RESPONSE_OK)
    {
      txt =
	g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (dlg)));

      if (txt)
	{
/* Check that it's not a directory */
	  if (g_file_test (txt, G_FILE_TEST_IS_DIR))
	    {
	      katoob_error (_("This is a directory."));
	      g_free (txt);
	    }
	  else
	    {
	      gtk_entry_set_text (GTK_ENTRY (location_entry), txt);
	      g_free (txt);
	    }
	}
    }

  gtk_widget_destroy (dlg);
}

void
preview_button_clicked ()
{
  generate_print_config ();
//katoob_do_preview();
}

void
cancel_button_clicked ()
{
  gtk_widget_destroy (dialog);
  dialog = NULL;
}

void
print_button_clicked (GtkWidget * w, gpointer user_data)
{
  KatoobDocument *doc = (KatoobDocument *) user_data;

  generate_print_config ();
  if (katoob_do_print
      (doc,
       gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (selection_radio))))
    {
      gtk_widget_destroy (dialog);
      dialog = NULL;
    }

}

void
pdf_activate (GtkWidget * widget, gpointer data)
{
  GtkWidget *w = (GtkWidget *) data;
  gtk_widget_set_sensitive (w, FALSE);
  gtk_widget_set_sensitive (location_entry, TRUE);
  gtk_widget_set_sensitive (browse_button, TRUE);
  gtk_entry_set_text (GTK_ENTRY (location_entry), PConf->pdf_entry);
}

void
ps_activate (GtkWidget * widget, gpointer data)
{
  gint x;
  GtkWidget *w = (GtkWidget *) data;
  gtk_widget_set_sensitive (w, TRUE);
  x = gtk_option_menu_get_history (GTK_OPTION_MENU (location_menu));
  if (x == 0)
    {
/* File */
      file_activate (NULL, NULL);
    }
  else if (x == 1)
    {
/* LPR */
      lpr_activate (NULL, NULL);
    }
  else if (x == 2)
    {
/* Custom */
      custom_activate (NULL, NULL);
    }
}

void
file_activate (GtkWidget * widget, gpointer data)
{
  gtk_widget_set_sensitive (location_entry, TRUE);
  gtk_widget_set_sensitive (browse_button, TRUE);
  gtk_entry_set_text (GTK_ENTRY (location_entry), PConf->file_entry);
  browse_dialog_title = _("Please select a file to print to.");
}

void
lpr_activate (GtkWidget * widget, gpointer data)
{
  gtk_widget_set_sensitive (location_entry, FALSE);
  gtk_widget_set_sensitive (browse_button, FALSE);
  gtk_entry_set_text (GTK_ENTRY (location_entry), "");
}

void
custom_activate (GtkWidget * widget, gpointer data)
{
  gtk_widget_set_sensitive (location_entry, TRUE);
  gtk_widget_set_sensitive (browse_button, TRUE);
  gtk_entry_set_text (GTK_ENTRY (location_entry), PConf->custom_entry);
  browse_dialog_title = _("Please select an application.");
}

void
portrait_activate (GtkWidget * widget, gpointer data)
{
  gtk_widget_show (portrait_img);
  gtk_widget_hide (landscape_img);
}

void
landscape_activate (GtkWidget * widget, gpointer data)
{
  gtk_widget_show (landscape_img);
  gtk_widget_hide (portrait_img);
}

/* We need custom adjustement for each of points/inches */
void
inches_activate (GtkWidget * widget, gpointer data)
{
/* 1 inch = 72 points. */
  gdouble v;
  v = gtk_spin_button_get_value (GTK_SPIN_BUTTON (width_spin)) / 72;
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (width_spin), v);

  v = gtk_spin_button_get_value (GTK_SPIN_BUTTON (height_spin)) / 72;
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (height_spin), v);
}

void
points_activate (GtkWidget * widget, gpointer data)
{
/* 1 inch = 72 points. */
  gdouble v;
  v = gtk_spin_button_get_value (GTK_SPIN_BUTTON (width_spin)) * 72;
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (width_spin), v);

  v = gtk_spin_button_get_value (GTK_SPIN_BUTTON (height_spin)) * 72;
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (height_spin), v);
}

void
custom_check_toggled (GtkToggleButton * button, gpointer data)
{
  if (button->active)
    {
      gtk_widget_set_sensitive (paper_menu, FALSE);
      gtk_widget_set_sensitive (width_spin, TRUE);
      gtk_widget_set_sensitive (height_spin, TRUE);
      gtk_widget_set_sensitive (optionmenu, TRUE);
      gtk_widget_set_sensitive (width_label, TRUE);
      gtk_widget_set_sensitive (height_label, TRUE);
    }
  else
    {
      gtk_widget_set_sensitive (paper_menu, TRUE);
      gtk_widget_set_sensitive (width_spin, FALSE);
      gtk_widget_set_sensitive (height_spin, FALSE);
      gtk_widget_set_sensitive (optionmenu, FALSE);
      gtk_widget_set_sensitive (width_label, FALSE);
      gtk_widget_set_sensitive (height_label, FALSE);
    }
}

void
generate_print_config ()
{
  gchar *tmp = NULL;

  PConf->printer =
    gtk_option_menu_get_history (GTK_OPTION_MENU (printer_menu)) + 1;
  PConf->location =
    gtk_option_menu_get_history (GTK_OPTION_MENU (location_menu)) + 1;
  if (PConf->location == LOCATION_FILE)
    {
      tmp = (gchar *) gtk_entry_get_text (GTK_ENTRY (location_entry));
      if (tmp)
	{
	  g_free (PConf->file_entry);
	  PConf->file_entry = g_strdup (tmp);
	}
      g_free (tmp);
      tmp = NULL;
    }
  else if (PConf->printer == PRINTER_PDF)
    {
      tmp = (gchar *) gtk_entry_get_text (GTK_ENTRY (location_entry));
      if (tmp)
	{
	  g_free (PConf->pdf_entry);
	  PConf->pdf_entry = g_strdup (tmp);
	}
      g_free (tmp);
      tmp = NULL;
    }
  else if (PConf->location == LOCATION_CUSTOM)
    {
      tmp = (gchar *) gtk_entry_get_text (GTK_ENTRY (location_entry));
      if (tmp)
	{
	  g_free (PConf->custom_entry);
	  PConf->custom_entry = g_strdup (tmp);
	}
      g_free (tmp);
      tmp = NULL;
    }
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (custom_check)))
    {
      PConf->paper_size = -1;
      PConf->paper_width =
	gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (width_spin));
      PConf->paper_height =
	gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (height_spin));
      PConf->paper_size_unite =
	gtk_option_menu_get_history (GTK_OPTION_MENU (optionmenu)) + 1;
    }
  else
    {
      PConf->paper_size =
	gtk_option_menu_get_history (GTK_OPTION_MENU (paper_menu));
    }

  PConf->page_orientation = gtk_option_menu_get_history (GTK_OPTION_MENU
							 (orientation_menu)) +
    1;

  PConf->top_margin =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (top_margin_spin));
  PConf->left_margin =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (left_margin_spin));
  PConf->right_margin =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (right_margin_spin));
  PConf->bottom_margin =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (bottom_margin_spin));
  PConf->copies =
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (spinbutton));
}

#endif /* ENABLE_PRINT */
