/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "katoobdocument.h"

#ifndef __MISC_H__
#define __MISC_H__

#ifndef _WIN32
void xkb_error ();
#endif /* _WIN32 */
gint katoob_create_question (gchar * q);
void katoob_update_active_interface ();
void katoob_goto_line ();
void katoob_error (gchar * err, ...);
void katoob_info (gchar * info);
void katoob_toggle_document_wrap (GtkCheckMenuItem * checkmenuitem);
void katoob_toggle_document_line_numbers (GtkCheckMenuItem * checkmenuitem);
void katoob_switch_page_handler (GtkNotebook * notebook);
void katoob_buffer_modified_handler (KatoobDocument * doc, gboolean modified);
void katoob_encoding_changed_handler (KatoobDocument * doc, gint encoding);
void katoob_file_changed_handler (KatoobDocument * doc, gchar * file);
void katoob_cursor_moved_handler (KatoobDocument * doc, gint col, gint row);
gboolean filter_goto (GtkEntry * entry, const gchar * text, gint length,
		gint * position, gpointer data);
gchar *katoob_text_to_utf8 (gchar * text, KatoobDocument * doc);
gchar *katoob_changed_text_encoding (gchar * text, gint enc);

void katoob_show_close_buttons (KatoobDocument * doc);
void katoob_hide_close_buttons (KatoobDocument * doc);
void katoob_show_line_numbers (KatoobDocument * doc);
void katoob_hide_line_numbers (KatoobDocument * doc);
void katoob_set_custom_font (KatoobDocument * doc);
void katoob_set_default_font (KatoobDocument * doc);
void extended_toolbar_goto_line (GtkEntry * entry, gpointer user_data);
gboolean string_with_suffix (gchar * str, gchar * suffix, gint str_len,
			     gint suffix_len);

#ifdef HAVE_SPELL
void katoob_toggle_spellcheck (GtkCheckMenuItem * checkmenuitem);
void katoob_toggle_spell_cb (KatoobDocument * doc, gboolean state);
void create_mispelled_color ();
#endif /* HAVE_SPELL */

void katoob_cut ();
void katoob_copy ();
void katoob_delete ();
void katoob_select_all ();
void katoob_paste ();
GtkWidget *katoob_get_statusbar ();
void katoob_toggle_statusbar (GtkCheckMenuItem * checkmenuitem);
void parse_options (int argc, char *argv[]);
#ifndef _WIN32
void katoob_switch_xkb (gint x);
GdkFilterReturn event_filter (GdkXEvent * xevent,
			      GdkEvent * event, void *sia);
#endif /* _WIN32 */
gint create_messagedialog (gchar * message);
void katoob_about ();
void katoob_toggle_overwrite_cb (gboolean overwrite);

#endif /* __MISC_H__ */
