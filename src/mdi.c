/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"

#include <unistd.h>
#include <string.h>
#include <gtk/gtk.h>

#include "mdi.h"
#include "open.h"
#include "emulator.h"
#include "misc.h"		/* katoob_update_active_interface() */
#include "encodings.h"
#include "undoredo.h"
#include "file.h"
#include "katoobdocument.h"
#include "recent.h"
#include "katoobwindow.h"
#include "save.h"

#ifdef ENABLE_HIGHLIGHT
#include "highlight.h"
#endif /* ENABLE_HIGHLIGHT */

static void katoob_doc_finalise (KatoobDocument * doc);
static void katoob_display_doc (GtkMenuItem * menuitem);

KatoobDocument *
katoob_get_active_doc ()
{
  extern UI *katoob;
  gint x;
  KATOOB_DEBUG_FUNCTION;

  if (!GTK_IS_WIDGET (katoob->win))
    {
      return NULL;
    }

  x = gtk_notebook_get_current_page
    (GTK_NOTEBOOK (KATOOB_WINDOW (katoob->win)->notebook));

  if (x == -1)
    {
      return NULL;
    }

  return
    KATOOB_DOCUMENT (gtk_notebook_get_nth_page
		     (GTK_NOTEBOOK (KATOOB_WINDOW (katoob->win)->notebook),
		      x));
}

void
katoob_create_doc_from_file (gchar * file,
			     gboolean addtorecent, gint encoding)
{
  extern conf *config;

#ifdef ENABLE_HIGHLIGHT
  extern UI *katoob;
#endif /* ENABLE_HIGHLIGHT */

  GtkWidget *doc = NULL;
  gchar *content = NULL, *utf_buf = NULL;

#ifdef ENABLE_HIGHLIGHT
  KatoobHighlightType t = katoob_get_highlight_type (file);
#endif /* ENABLE_HIGHLIGHT */

  KATOOB_DEBUG_FUNCTION;

  if (!katoob_create_file_if_required (file))
    {
      return;
    }

  if (!katoob_file_get_contents (file, &content))
    {
      return;
    }

/* If the file is empty, content is set to NULL */
  if (!content)
    {
      doc = katoob_document_new_from_file (file);
#ifdef ENABLE_HIGHLIGHT
      if (config->highlight)
	{
	  katoob_document_set_highlight (KATOOB_DOCUMENT (doc),
					 katoob->hl_manager, t);
	}
#endif /* ENABLE_HIGHLIGHT */
    }
  else
    {
      if (!g_utf8_validate (content, -1, NULL))
	{
#if 0
/* If both the encoding and the default encoding are UTF-8, We fall here and 
   display an error message */
	  if (encoding ==
	      katoob_encodings_get_by_name ("UTF-8") == config->defenc)
	    {
	      katoob_error (_
			    ("Can't determine the encoding for that file, Please choose one."));
	      g_free (content);
	      return;
	    }
#endif
	  if ((encoding == katoob_encodings_get_by_name ("UTF-8")) && (encoding == config->defenc))
	    {
	      katoob_debug("encoding = defenc = UTF-8");
	      encoding = katoob_encodings_get_by_name("WINDOWS-1256");
	    }
	  /* Convert, create doc, insert */
	  utf_buf =
	    katoob_encoding_convert (katoob_encodings_get_by_number
				     (encoding), "UTF-8", content,
				     strlen (content));
	  if (!utf_buf)
	    {
	      g_free (content);
	      return;
	    }
	  doc = katoob_document_new_from_file (file);
#ifdef ENABLE_HIGHLIGHT
	  if (config->highlight)
	    {
	      katoob_document_set_highlight (KATOOB_DOCUMENT (doc),
					     katoob->hl_manager, t);
	    }
#endif /* ENABLE_HIGHLIGHT */
	  katoob_document_set_text (KATOOB_DOCUMENT (doc), utf_buf);
	  g_free (utf_buf);
	  g_free (content);
	}
      else
	{
/* create doc, insert */
	  doc = katoob_document_new_from_file (file);
#ifdef ENABLE_HIGHLIGHT
	  if (config->highlight)
	    {
	      katoob_document_set_highlight (KATOOB_DOCUMENT (doc),
					     katoob->hl_manager, t);
	    }
#endif /* ENABLE_HIGHLIGHT */
	  katoob_document_set_text (KATOOB_DOCUMENT (doc), content);
	  g_free (content);
/* NOTE:
 * The document was valid UTF-8, We'll set the document encoding to UTF-8 
 * regardless of what the user wants ;-)
 */
	  encoding = katoob_encodings_get_by_name ("UTF-8");
	}
    }
/* set encoding */
  katoob_document_set_encoding (KATOOB_DOCUMENT (doc), encoding);
  if ((config->recent) && (addtorecent))
    {
      recent_append (file);
    }
  ++config->filesno;
  katoob_doc_finalise (KATOOB_DOCUMENT (doc));
  katoob_document_set_modified (KATOOB_DOCUMENT (doc), FALSE);
  if (!katoob_check_file_access (file))
    {				/* write permission denied */
      katoob_document_set_readonly (KATOOB_DOCUMENT (doc));
    }
}

void
katoob_create_doc ()
{
  extern conf *config;
  GtkWidget *doc;
  gchar *title = g_strdup_printf ("Untitled New %i", ++config->filesno);
  KATOOB_DEBUG_FUNCTION;

  doc = katoob_document_new (title);
  g_free (title);
  katoob_doc_finalise (KATOOB_DOCUMENT (doc));
}

void
katoob_create_stdin_doc (gchar * file)
{
  extern conf *config;
  GtkWidget *doc;
  gchar *content = NULL;
  gchar *utf_buf = NULL;
  gchar *_tmp;

  KATOOB_DEBUG_FUNCTION;

/*****************************************************************************
 * Function Analysis:                                                        *
 * We have the name of the file, We open it, Get its contents.               *
 * We have 2 cases:                                                          *
 * 1 - The file content is valid UTF-8, Then we create a document, insert it *
 *     using katoob_document_set_text()                                      *
 * 2 - If it's not, We have 2 cases                                          *
 *	a - The default encoding is not UTF-8, convert, create, insert.      *
 *	b - The default encoding is UTF-8, Return an error to the user.      *
 *****************************************************************************/

  if (!katoob_file_get_contents (file, &content))
    {
      return;
    }
  if (!content)
    {
/* An empty file! */
      _tmp = g_strdup_printf ("Untitled New %i", ++config->filesno);
      doc = katoob_document_new (_tmp);
      g_free (_tmp);
    }
  else
    {
      if (!g_utf8_validate (content, -1, NULL))
	{
	  if (katoob_encodings_get_by_name ("UTF-8") == config->defenc)
	    {
	      katoob_error (_("Can't determine the encoding for the text."));
	      g_free (content);
	      return;
	    }
/* Convert, create doc, insert */
	  utf_buf =
	    katoob_encoding_convert (katoob_encodings_get_by_number
				     (config->defenc), "UTF-8", content,
				     strlen (content));
	  if (!utf_buf)
	    {
	      g_free (content);
	      return;
	    }
	  _tmp = g_strdup_printf ("Untitled New %i", ++config->filesno);
	  doc = katoob_document_new (_tmp);
	  g_free (_tmp);
	  katoob_document_set_text (KATOOB_DOCUMENT (doc), utf_buf);
	  g_free (utf_buf);
	  g_free (content);
	}			/* Valid UTF-8 */
      else
	{
/* create doc, insert */
	  _tmp = g_strdup_printf ("Untitled New %i", ++config->filesno);
	  doc = katoob_document_new (_tmp);
	  g_free (_tmp);
	  katoob_document_set_text (KATOOB_DOCUMENT (doc), content);
	  g_free (content);
	}
    }
/* set encoding */
  katoob_document_set_encoding (KATOOB_DOCUMENT (doc), config->defenc);
  katoob_doc_finalise (KATOOB_DOCUMENT (doc));
}

/* FIXME: to be removed */
static void
katoob_doc_finalise (KatoobDocument * doc)
{
  extern UI *katoob;
  GtkWidget *notebook = KATOOB_WINDOW (katoob->win)->notebook;
  GtkWidget *menu = katoob_document_get_menu (doc);
  KATOOB_DEBUG_FUNCTION;

  g_signal_connect (G_OBJECT (menu), "activate",
		    G_CALLBACK (katoob_display_doc), NULL);
  gtk_menu_shell_append (GTK_MENU_SHELL (katoob->documents_menu), menu);
  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), GTK_WIDGET (doc),
			    katoob_document_get_label (doc));
  gtk_notebook_set_menu_label_text (GTK_NOTEBOOK (notebook),
				    GTK_WIDGET (doc),
				    katoob_document_get_label_text (doc));
  gtk_widget_show_all (GTK_WIDGET (doc));
  gtk_widget_show_all (menu);
  gtk_widget_show_all (katoob_document_get_label (doc));
  gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), -1);

  g_signal_connect (G_OBJECT (doc), "buffer-modified",
		    G_CALLBACK (katoob_buffer_modified_handler), NULL);
  g_signal_connect (G_OBJECT (doc), "encoding_changed",
		    G_CALLBACK (katoob_encoding_changed_handler), NULL);
  g_signal_connect (G_OBJECT (doc), "file-changed",
		    G_CALLBACK (katoob_file_changed_handler), NULL);
  g_signal_connect (G_OBJECT (doc), "cursor-moved",
		    G_CALLBACK (katoob_cursor_moved_handler), NULL);
  g_signal_connect (G_OBJECT (doc), "encoding_changed",
		    G_CALLBACK (katoob_encoding_changed_cb), NULL);
}

void
katoob_close_button_handler (GtkWidget * w, KatoobDocument * doc)
{
  KATOOB_DEBUG_FUNCTION;

  katoob_close_doc (doc);
}

gboolean
katoob_real_close_doc (KatoobDocument * doc)
{
  extern UI *katoob;
  extern conf *config;

  KATOOB_DEBUG_FUNCTION;

  gtk_notebook_remove_page (GTK_NOTEBOOK
			    (KATOOB_WINDOW (katoob->win)->notebook),
			    gtk_notebook_page_num (GTK_NOTEBOOK
						   (KATOOB_WINDOW
						    (katoob->win)->notebook),
						   GTK_WIDGET (doc)));
/*  gtk_widget_destroy (GTK_WIDGET (doc)); */
  config->filesno--;

/****************************************************************************
 * FIXME: Here we are calling the function directly cause the "switch-page" *
 * signal handler doesn't work when this is the last page to destroy        *
 ****************************************************************************/
  if (config->filesno == 0)
    {
      katoob_switch_page_handler (GTK_NOTEBOOK
				  (KATOOB_WINDOW (katoob->win)->notebook));
    }
  return TRUE;
}

gboolean
katoob_close_doc (KatoobDocument * doc)
{
  gchar *_tmp;
  gint i;
  gchar *file;
  KATOOB_DEBUG_FUNCTION;

  if (katoob_document_get_modified (doc))
    {
      file = katoob_document_get_file (doc);
      if (file)
	{
	  _tmp =
	    g_strdup_printf (_("The file %s is not saved, Save first?"),
			     file);
	  i = create_messagedialog (_tmp);
	  g_free (_tmp);
	}
      else
	{
	  _tmp =
	    g_strdup_printf (_("\"%s\" is not saved, Save first?"),
			     katoob_document_get_label_text (doc));
	  i = create_messagedialog (_tmp);
	  g_free (_tmp);
	}
      switch (i)
	{
	case GTK_RESPONSE_YES:
	  if (katoob_document_get_readonly (doc))
	    {
	      if (!katoob_save_as ())
		{
		  return FALSE;
		}
	      else
		{
		  return katoob_real_close_doc (doc);
		}
	    }
	  else
	    {
	      if (!katoob_save ())
		{
		  return FALSE;
		}
	      else
		{
		  return katoob_real_close_doc (doc);
		}
	    }
	case GTK_RESPONSE_NO:
	  return katoob_real_close_doc (doc);
	default:
	  return FALSE;
	}
    }
  else
    {
      return katoob_real_close_doc (doc);
    }
}

static void
katoob_display_doc (GtkMenuItem * menuitem)
{
  extern UI *katoob;
  GtkWidget *w = gtk_widget_get_parent (GTK_WIDGET (menuitem));
  GList *children = GTK_MENU_SHELL (w)->children;
  gint x = g_list_position (children, g_list_find (children, menuitem));
  KATOOB_DEBUG_FUNCTION;

  x -= 3;
  gtk_notebook_set_current_page (GTK_NOTEBOOK
				 (KATOOB_WINDOW (katoob->win)->notebook), x);
}

void
katoob_close_active_doc ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return;
    }
  katoob_close_doc (doc);
}

void
katoob_save_all ()
{
  extern UI *katoob;
  extern conf *config;
  guint x;
  KatoobDocument *doc = NULL;
  KATOOB_DEBUG_FUNCTION;

  for (x = 0; x < config->filesno; x++)
    {
      doc =
	KATOOB_DOCUMENT (gtk_notebook_get_nth_page
			 (GTK_NOTEBOOK
			  (KATOOB_WINDOW (katoob->win)->notebook), x));
      if (katoob_document_get_modified (doc))
	{
	  if (katoob_document_get_readonly (doc))
	    {
	      katoob_save_as ();
	    }
	  else
	    {
	      katoob_save ();
	    }
	}
    }
}

void
katoob_close_all ()
{
  KatoobDocument *doc = NULL;
  KATOOB_DEBUG_FUNCTION;

  while ((doc = katoob_get_active_doc ()) != NULL)
    {
      if (!katoob_close_doc (doc))
	{
	  return;
	}
    }
}
