/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* TODO: This should be handled by an AM_CONDITIONAL() */
#ifndef _WIN32

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define PAGER "katoob-debug-show"
#define CRASH_NAME "katoob-XXXXXX"

#include "katoob.h"
#include "crash.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#ifndef _WIN32
#include <sys/wait.h>
#endif
#include <unistd.h>
#include <gtk/gtk.h>
#include "misc.h"
#include "file.h"
#include "mdi.h"

#define DEBUG_COMMANDS "bt\nq"

/* Called when we catch a signal. */
void
katoob_init_debug (gint sig)
{
  extern gchar *_name;
  extern unsigned long _pid;
  gchar *command;
  gint fd;
  gchar *file;
  gchar buff[100];
  FILE *fl, *pager;

  KATOOB_DEBUG_FUNCTION;

  file = g_strdup_printf ("%s/%sXXXXXX", g_get_tmp_dir (), PACKAGE);

  fd = g_mkstemp (file);

  if (fd == -1)
    {
      g_warning ("Couldn't create temp file");
      g_free (file);
      exit (1);
    }
  fl = fopen (file, "wb");
  if (!fl)
    {
      g_warning ("Couldn't write gdb script.");
      g_free (file);
      exit (1);
    }
  if (!fwrite (DEBUG_COMMANDS, strlen (DEBUG_COMMANDS), 1, fl))
    {
      fclose (fl);
      g_free (file);
      g_warning ("Couldn't write to temp file");
      exit (1);
    }
  fclose (fl);

/*
 * gdb: -x <file> -batch -n
 */

  command = g_strdup_printf ("gdb -n -batch -x %s %s %ld", file, _name, _pid);

  fl = popen (command, "r");

  if (!fl)
    {
      g_warning ("Couldn't execute gdb");
      g_free (command);
      unlink (file);
      g_free (file);
      exit (1);
    }

  g_free (command);

  pager = popen (PAGER, "w");
  if (!pager)
    {
      g_warning ("Couldn't execute our pager");
      pclose (fl);
      unlink (file);
      g_free (file);
      exit (1);
    }

#ifdef EXPERIMENTAL
  fprintf (pager, " EXPERIMENTAL");
#endif /* EXPERIMENTAL */

#ifdef DEBUG
  fprintf (pager, " DEBUG");
#endif /* DEBUG */

#ifdef ENABLE_PRINT
  fprintf (pager, " PRINT");
#endif /* ENABLE_PRINT */

#ifdef ENABLE_HIGHLIGHT
  fprintf (pager, " HIGHLIGHT");
#endif /* ENABLE_HIGHLIGHT */

#ifdef HAVE_NEW_SPELL
  fprintf (pager, " NEW_SPELL");
#endif /* HAVE_NEW_SPELL */

#ifdef HAVE_OLD_SPELL
  fprintf (pager, " OLD_SPELL");
#endif /* HAVE_OLD_SPELL */

  fprintf (pager, "\n\n");

  while (fgets (buff, 100, fl))
    {
      fprintf (pager, "%s", buff);
    }

  pclose (fl);

  unlink (file);
  g_free (file);
  exit (255);
}

void
katoob_save_open ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  while (doc)
    {
      gint fd;
      char *file =
	g_strdup_printf ("%s%s%s", g_get_home_dir (), G_DIR_SEPARATOR_S,
			 CRASH_NAME);
      gchar *buff = NULL;
      fd = g_mkstemp (file);
      if (fd == -1)
	{
	  g_free (file);
	  continue;
	}
      buff = katoob_document_get_text (doc);
      if (strlen (buff) != 0)
	{
	  write (fd, buff, strlen (buff));
	  g_free (buff);
	  close (fd);
	}
      else
	{
/* Buffer is empty, Don't save. */
	  close (fd);
	  unlink (file);
	}
      katoob_real_close_doc (doc);
      doc = katoob_get_active_doc ();
      g_free (file);
    }

}
#endif /* _WIN32 */
