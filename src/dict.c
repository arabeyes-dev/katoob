/* Katoob
 * Copyright (c) 2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include "katoob.h"
#include "dict.h"
#include "misc.h"

#define LDVERSION "0.9"

static struct ld_conn *katoob_dict_init_conn (gchar * host, glong port,
					      gint timeout);
void katoob_dict_end_conn (struct ld_conn *conn);

static struct ld_conn *
katoob_dict_init_conn (gchar * host, glong port, gint timeout)
{
  gchar *client;
  gint lderrno;

  struct ld_conn *conn = NULL;
  KATOOB_DEBUG_FUNCTION;

  client =
    g_strdup_printf ("%s Version %s, Using libdict version %s.", PACKAGE,
		     VERSION, LDVERSION);

  conn = ld_newconn (host, port, timeout, client,
#ifdef DEBUG
		     LD_True
#else /* ! DEBUG */
		     LD_False
#endif /* DEBUG */
    );

  g_free (client);

  if ((lderrno = ld_geterrno (conn)) != LDOK)
    {
      g_warning ("Error connecting to dict server, %s.",
		 ld_strerror (lderrno));
      ld_freeconn (conn);
      conn = NULL;
      return NULL;
    }

  return conn;
}

void
katoob_dict_end_conn (struct ld_conn *conn)
{
  ld_closeconn (conn);
  ld_freeconn (conn);
  conn = NULL;
}

GSList *
katoob_dict_lookup_word (gchar * word)
{
  extern conf *config;
  struct ld_defanswer **answers;
  struct ld_conn *conn = NULL;
  gint lderrno;
  gint x = 0;
  GSList *ans = NULL;

  KATOOB_DEBUG_FUNCTION;

  if (strlen (word) == 0)
    {
      return NULL;
    }

  conn =
    katoob_dict_init_conn (config->dict_host, config->dict_port,
			   config->dict_timeout);

  if (!conn)
    {
      return NULL;
    }

  if (config->dict_db)
    {
      ld_setdb (conn, config->dict_db);
    }

  answers = ld_define (conn, word);

  if ((lderrno = ld_geterrno (conn)) != LDOK)
    {
      g_warning ("Error searching for word, %s.", ld_strerror (lderrno));
      katoob_dict_end_conn (conn);
      return NULL;
    }


  if (!answers)
    {
      katoob_dict_end_conn (conn);
      return NULL;
    }

  while (answers[x])
    {
      ans = g_slist_append (ans, g_strdup (answers[x]->ld_ansdef));
      x++;
    }

  katoob_dict_end_conn (conn);

  return ans;
}

GSList *
katoob_dict_list_dicts (gchar * host, glong port, gint timeout)
{
  struct ld_conn *conn;
  struct ld_dbs **dbs;
  GSList *dicts = NULL;
  gint lderrno;
  gint x = 0;

  KATOOB_DEBUG_FUNCTION;

  conn = katoob_dict_init_conn (host, port, timeout);

  if (!conn)
    {
      katoob_dict_end_conn (conn);
      return NULL;
    }

  dbs = ld_getdbs (conn);

  if ((lderrno = ld_geterrno (conn)) != LDOK)
    {
      g_warning ("Error getting databases, %s.", ld_strerror (lderrno));
      katoob_dict_end_conn (conn);
      return NULL;
    }

  if (!dbs)
    {
      katoob_dict_end_conn (conn);
      return NULL;
    }

  while (dbs[x])
    {
      struct ld_dbs *entry =
	(struct ld_dbs *) g_malloc (sizeof (struct ld_dbs));
      entry->ld_dbname = g_strdup (dbs[x]->ld_dbname);
      entry->ld_dbdesc = g_strdup (dbs[x]->ld_dbdesc);
      dicts = g_slist_append (dicts, entry);
      x++;
    }

  katoob_dict_end_conn (conn);

  return dicts;
}
