/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "katoobdocument.h"
#include "search.h"
#include "emulator.h"
#include "mdi.h"
#include "misc.h"

void katoob_destroy_find ();
void closeb_clicked ();
gboolean katoob_replace_cb ();
void katoob_replace_cb2 ();
gboolean katoob_find_cb ();
void katoob_replace_all_cb ();
void filter_text ();
void katoob_find_dialog (gboolean replace);

GtkWidget *find_dialog = NULL;
GtkWidget *beginning;
GtkWidget *cursor_position;
/* TODO: Get rid of the following 2 global variables! */
GtkWidget *entry;
GtkWidget *entry2;
GtkWidget *find_button;
GtkWidget *replace_button;
GtkWidget *match_case;

void
katoob_find ()
{
  KATOOB_DEBUG_FUNCTION;
  katoob_find_dialog (FALSE);
}

void
katoob_replace ()
{
  KATOOB_DEBUG_FUNCTION;
  katoob_find_dialog (TRUE);
}

/* Display the search/replace dialog */
void
katoob_find_dialog (gboolean replace)
{
  extern conf *config;
  extern UI *katoob;

  GtkWidget *dialog_vbox;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *what;
  GtkWidget *with;
  GSList *sf = NULL;		/* Search From */
  GtkWidget *dialog_action_area1;
  GtkWidget *hbox2;
  GtkWidget *closeb;
  GtkWidget *replaceall = NULL;
  KatoobDocument *doc;
  gchar *tmp = NULL;
  KATOOB_DEBUG_FUNCTION;

  doc = katoob_get_active_doc ();

  if (!doc)
    {
      return;
    }

  if (find_dialog)
    {
      gtk_window_present (GTK_WINDOW (find_dialog));
      return;
    }

  find_dialog = gtk_dialog_new ();
  gtk_container_set_border_width (GTK_CONTAINER (find_dialog), 5);

  if (replace)
    {
      gtk_window_set_title (GTK_WINDOW (find_dialog), _("Replace"));
    }
  else
    {
      gtk_window_set_title (GTK_WINDOW (find_dialog), _("Find"));
    }

  gtk_window_set_resizable (GTK_WINDOW (find_dialog), FALSE);
  gtk_window_set_position (GTK_WINDOW (find_dialog), GTK_WIN_POS_CENTER);
  dialog_vbox = GTK_DIALOG (find_dialog)->vbox;

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (dialog_vbox), vbox, TRUE, TRUE, 0);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

  what = gtk_label_new (_("Find what:"));
  gtk_box_pack_start (GTK_BOX (hbox), what, FALSE, FALSE, 0);
  gtk_misc_set_padding (GTK_MISC (what), 15, 0);

  entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);

  if (replace)
    {
      with = gtk_label_new (_("Replace with:"));
      gtk_box_pack_start (GTK_BOX (hbox), with, FALSE, FALSE, 0);
      gtk_misc_set_padding (GTK_MISC (with), 15, 0);

      entry2 = gtk_entry_new ();
      gtk_box_pack_start (GTK_BOX (hbox), entry2, TRUE, TRUE, 0);
    }

  match_case = gtk_check_button_new_with_mnemonic (_("Match _case"));
  gtk_box_pack_start (GTK_BOX (vbox), match_case, FALSE, FALSE, 0);

  if (katoob_document_get_match_case (doc))
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (match_case), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (match_case), FALSE);
    }

  beginning =
    gtk_radio_button_new_with_mnemonic (sf,
					_
					("Search from the _beginning of the document"));
  sf = gtk_radio_button_get_group (GTK_RADIO_BUTTON (beginning));
  gtk_box_pack_start (GTK_BOX (vbox), beginning, FALSE, FALSE, 0);

  cursor_position =
    gtk_radio_button_new_with_mnemonic (sf,
					_
					("Search from the cur_sor position"));
  sf = gtk_radio_button_get_group (GTK_RADIO_BUTTON (cursor_position));
  if (katoob_document_get_beginning (doc))
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (beginning), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cursor_position),
				    TRUE);
    }

  gtk_box_pack_start (GTK_BOX (vbox), cursor_position, FALSE, FALSE, 0);

  dialog_action_area1 = GTK_DIALOG (find_dialog)->action_area;
  g_object_set_data (G_OBJECT (find_dialog), "dialog_action_area1",
		     dialog_action_area1);
  gtk_container_set_border_width (GTK_CONTAINER (dialog_action_area1), 10);

  hbox2 = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (dialog_action_area1), hbox2, TRUE, TRUE, 0);

  find_button = gtk_button_new_from_stock (GTK_STOCK_FIND);
  GTK_WIDGET_SET_FLAGS (find_button, GTK_CAN_DEFAULT);
  gtk_widget_set_sensitive (find_button, FALSE);
  gtk_box_pack_start (GTK_BOX (hbox2), find_button, FALSE, FALSE, 0);
  gtk_widget_set_size_request (GTK_WIDGET (find_button), 100, -1);
  gtk_widget_grab_default (find_button);

  if (replace)
    {
      replace_button = gtk_button_new_with_mnemonic (_("_Replace"));
      gtk_widget_set_sensitive (replace_button, FALSE);
      gtk_box_pack_start (GTK_BOX (hbox2), replace_button, FALSE, FALSE, 0);
      gtk_widget_set_size_request (GTK_WIDGET (replace_button), 100, -1);

      replaceall = gtk_button_new_with_mnemonic (_("Replace _all"));
      gtk_box_pack_start (GTK_BOX (hbox2), replaceall, FALSE, FALSE, 0);
      gtk_widget_set_size_request (GTK_WIDGET (replaceall), 100, -1);
    }

  closeb = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
  gtk_box_pack_start (GTK_BOX (hbox2), closeb, FALSE, FALSE, 0);
  gtk_widget_set_size_request (GTK_WIDGET (closeb), 100, -1);

  gtk_widget_show_all (find_dialog);
  gtk_window_set_transient_for (GTK_WINDOW (find_dialog),
				GTK_WINDOW (katoob->win));

  g_signal_connect (G_OBJECT (closeb), "clicked",
		    G_CALLBACK (closeb_clicked), NULL);
  g_signal_connect (G_OBJECT (find_dialog), "destroy",
		    G_CALLBACK (katoob_destroy_find), NULL);

  if (replace)
    {
      g_signal_connect (G_OBJECT (entry), "activate",
			G_CALLBACK (katoob_replace_cb), NULL);
      g_signal_connect (G_OBJECT (entry2), "activate",
			G_CALLBACK (katoob_replace_cb), NULL);
      g_signal_connect (G_OBJECT (find_button), "clicked",
			G_CALLBACK (katoob_replace_cb), NULL);
    }
  else
    {
      g_signal_connect (G_OBJECT (entry), "activate",
			G_CALLBACK (katoob_find_cb), NULL);
      g_signal_connect (G_OBJECT (find_button), "clicked",
			G_CALLBACK (katoob_find_cb), NULL);
    }

  if (replace)
    {
      g_signal_connect (G_OBJECT (replace_button), "clicked",
			G_CALLBACK (katoob_replace_cb2), NULL);
      g_signal_connect (G_OBJECT (replaceall), "clicked",
			G_CALLBACK (katoob_replace_all_cb), NULL);
    }
/* Our emulator signals */
  if (config->emulator)
    {
      g_signal_connect (G_OBJECT (entry), "insert_text",
			G_CALLBACK (katoob_emulator_text_entry_insert), NULL);

      if (replace)
	{
	  g_signal_connect (G_OBJECT (entry2), "insert_text",
			    G_CALLBACK
			    (katoob_emulator_text_entry_insert), NULL);
	}
    }

/* enable/disable find button */
  g_signal_connect (G_OBJECT (entry), "insert_text",
		    G_CALLBACK (filter_text), NULL);
  g_signal_connect (G_OBJECT (entry), "delete_text",
		    G_CALLBACK (filter_text), NULL);

  tmp = katoob_document_get_last_searched (doc);
  if (tmp)
    {
      gtk_entry_set_text (GTK_ENTRY (entry), tmp);
      gtk_widget_set_sensitive (find_button, TRUE);
      /* Don't free(), that's the copy used internally by the widget! */
      /*      g_free (tmp);
	      tmp = NULL; */
    }

  tmp = katoob_document_get_last_replaced (doc);
  if ((tmp) && (replace))
    {
      gtk_entry_set_text (GTK_ENTRY (entry2), tmp);
      /* Don't free(), that's the copy used internally by the widget! */
      /*      g_free (tmp);
	      tmp = NULL; */
    }
}

void
katoob_destroy_find ()
{
  KATOOB_DEBUG_FUNCTION;
  gtk_widget_destroyed (find_dialog, &find_dialog);
}

void
closeb_clicked ()
{
  KATOOB_DEBUG_FUNCTION;
  g_signal_emit_by_name (G_OBJECT (find_dialog), "destroy");
}

/* Menubar handlers */

void
katoob_findnext ()
{
/*****************************************************************************
 * Set the search to be from the cursor position, If it's from the beginning *
 * Search, If no matches DON'T set the search to be from the beginning       *
 *****************************************************************************/
  KatoobDocument *doc;
  KATOOB_DEBUG_FUNCTION;

  doc = katoob_get_active_doc ();

  if (!doc)
    {
      return;
    }

  if (!katoob_document_get_last_searched (doc))
    {
      gdk_beep ();
      return;
    }

  katoob_document_set_beginning (doc, FALSE);
  if (!katoob_document_search (doc))
    {
      katoob_error (_("No matches found!"));
    }
}

void
filter_text (GtkEntry * entry,
	     const gchar * text, gint length, gint * position, gpointer data)
{
  if (position)
    {
      if (*position == 0)
	{
	  gtk_widget_set_sensitive (find_button, TRUE);
	}
    }
  else
    {
      if (length == 1)
	{
	  gtk_widget_set_sensitive (find_button, FALSE);
	}
    }
}

/***************************************************************************
 * This function searches the text, Enables the replace button if matches, *
 * Disable it if no matches.                                               *
 ***************************************************************************/
gboolean
katoob_replace_cb ()
{
  KatoobDocument *doc;
  gchar *tmp;
  KATOOB_DEBUG_FUNCTION;

  doc = katoob_get_active_doc ();

  if (!doc)
    {
      return FALSE;
    }

  tmp = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry2)));

  if (!tmp)
    {
      gdk_beep ();
      return FALSE;
    }

  katoob_document_set_last_replaced (doc, tmp);

  g_free (tmp);

  tmp = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));

  if (!tmp)
    {
      gdk_beep ();
      return FALSE;
    }

  katoob_document_set_last_searched (doc, tmp);
  g_free (tmp);

  katoob_document_set_beginning (doc,
				 gtk_toggle_button_get_active
				 (GTK_TOGGLE_BUTTON (beginning)));
  katoob_document_set_match_case (doc,
				  gtk_toggle_button_get_active
				  (GTK_TOGGLE_BUTTON (match_case)));

  if (!katoob_document_search (doc))
    {
      katoob_error (_("No matches found!"));
/* Don't disable the replace button, As we should have some highlighted text. */
/*
  gtk_widget_set_sensitive (replace_button, FALSE);
*/
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (beginning), TRUE);
      katoob_document_set_beginning (doc, TRUE);
      return FALSE;
    }

/* We should search from the cursor position */
  katoob_document_set_beginning (doc, FALSE);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cursor_position), TRUE);
  katoob_document_set_beginning (doc, FALSE);

/* Enable the replace button */
  gtk_widget_set_sensitive (replace_button, TRUE);
  return TRUE;
}

/* The actual replace */
void
katoob_replace_cb2 ()
{
  KatoobDocument *doc = katoob_get_active_doc ();
  KATOOB_DEBUG_FUNCTION;

  if (!doc)
    {
      return;
    }
  katoob_document_replace (doc);
}

gboolean
katoob_find_cb ()
{
  KatoobDocument *doc;
  gchar *tmp;
  KATOOB_DEBUG_FUNCTION;

  doc = katoob_get_active_doc ();
  if (!doc)
    {
      return FALSE;
    }

  tmp = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));

  if (!tmp)
    {
      gdk_beep ();
      return FALSE;
    }

  katoob_document_set_last_searched (doc, tmp);
  katoob_document_set_beginning (doc,
				 gtk_toggle_button_get_active
				 (GTK_TOGGLE_BUTTON (beginning)));
  katoob_document_set_match_case (doc,
				  gtk_toggle_button_get_active
				  (GTK_TOGGLE_BUTTON (match_case)));
  g_free (tmp);

  if (!katoob_document_search (doc))
    {
      katoob_error (_("No matches found!"));
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (beginning), TRUE);
      katoob_document_set_beginning (doc, TRUE);
      return FALSE;
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cursor_position),
				    TRUE);
      katoob_document_set_beginning (doc, FALSE);
      return TRUE;
    }
}

void
katoob_replace_all_cb ()
{
  KatoobDocument *doc;
  gchar *tmp = NULL;
  gint x = 0;
  KATOOB_DEBUG_FUNCTION;

  doc = katoob_get_active_doc ();

  if (!doc)
    {
      return;
    }

  tmp = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
  if (!tmp)
    {
      gdk_beep ();
      return;
    }

  katoob_document_set_last_searched (doc, tmp);

  g_free (tmp);
  tmp = NULL;

  tmp = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry2)));

  if (!tmp)
    {
      gdk_beep ();
      return;
    }

  katoob_document_set_last_replaced (doc, tmp);

  g_free (tmp);
  tmp = NULL;

  katoob_document_set_beginning (doc,
				 gtk_toggle_button_get_active
				 (GTK_TOGGLE_BUTTON (beginning)));
  katoob_document_set_match_case (doc,
				  gtk_toggle_button_get_active
				  (GTK_TOGGLE_BUTTON (match_case)));

  x = katoob_document_replace_all (doc);

  if (x == 0)
    {
      katoob_error (_("No matches found"));
    }
  else if (x == -1)
    {
      katoob_error (_("An error has occured"));
    }
  else
    {
/* Total matches found */
      katoob_info (g_strdup_printf (_("Replaced %d occurences"), x));
    }
}

void
extended_toolbar_search (GtkEntry * entry, gpointer user_data)
{
  KatoobDocument *doc;
  gchar *txt;
  gboolean match_case;

  KATOOB_DEBUG_FUNCTION;

  doc = katoob_get_active_doc ();
  if (!doc)
    {
      return;
    }
  txt = g_strdup (gtk_entry_get_text (entry));
  if (!txt)
    {
      return;
    }
  katoob_document_set_last_searched (doc, txt);
  g_free (txt);

  /* Searching from the extended toolbar is better to be case insensitive. */
  /* Let's get the state. */
  match_case = katoob_document_get_match_case(doc);
  katoob_document_set_match_case(doc, FALSE);

  if (!katoob_document_search (doc))
    {
      katoob_error (_("No matches found!"));
      katoob_document_set_beginning (doc, TRUE);
    }
  else
    {
      katoob_document_set_beginning (doc, FALSE);
    }

  katoob_document_set_match_case(doc, match_case);
}
