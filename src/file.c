/* Katoob
 * Copyright (c) 2002,2003 Arabeyes, Mohammed Sameer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "katoob.h"
#include <stdio.h>		/* This must be before file.h "FILE" */
#include <stdlib.h>		/* atoi() */
#include <unistd.h>		/* access() */
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>		/* strlen() */
#include <errno.h>
#include "file.h"
#include "encodings.h"
#include "misc.h"

#define DEFAULT_SAVE_ENCODING "UTF-8"

File *
katoob_create_file_selection (gchar * title, gboolean is_open)
{
  extern UI *katoob;
  extern conf *config;
  File *f;
  GtkWidget *file_selector;
  GtkWidget *ehbox;
  GtkWidget *encodingl;
  GtkWidget *cbox;
  GtkFileSelection *fs;
  gchar *tmp;

  KATOOB_DEBUG_FUNCTION;

  f = (File *) g_malloc (sizeof (File));

  file_selector = gtk_file_selection_new (title);
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (file_selector),
				   config->filesel_path);
  fs = GTK_FILE_SELECTION (file_selector);
  ehbox = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (ehbox);
  gtk_box_pack_end (GTK_BOX (fs->action_area), ehbox, TRUE, TRUE, 0);
  encodingl = gtk_label_new (_("Encoding"));
  gtk_widget_show (encodingl);
  gtk_box_pack_start (GTK_BOX (ehbox), encodingl, FALSE, FALSE, 0);
  gtk_misc_set_padding (GTK_MISC (encodingl), 20, 0);

  cbox = gtk_combo_new ();
  gtk_combo_set_popdown_strings (GTK_COMBO (cbox), config->enc);
  gtk_combo_set_value_in_list (GTK_COMBO (cbox), TRUE, FALSE);
  gtk_combo_set_case_sensitive (GTK_COMBO (cbox), TRUE);

  if (!is_open)
    {
      gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (cbox)->entry),
			  DEFAULT_SAVE_ENCODING);
    }
  else
    {
      gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (cbox)->entry),
			  katoob_encodings_get_name_by_number (config->
							       defenc));
    }

  gtk_widget_show (cbox);

  gtk_box_pack_start (GTK_BOX (ehbox), cbox, TRUE, TRUE, 0);

  gtk_window_set_transient_for (GTK_WINDOW (file_selector),
				GTK_WINDOW (katoob->win));

  if (is_open)
    {
      gtk_file_selection_set_select_multiple (fs, TRUE);
    }

  f->result = gtk_dialog_run (GTK_DIALOG (file_selector));

  f->encoding =
    katoob_encodings_get_name_from_string ((gchar *) gtk_entry_get_text
					   (GTK_ENTRY
					    (GTK_COMBO (cbox)->entry)));

  f->file =
    gtk_file_selection_get_selections (GTK_FILE_SELECTION (file_selector));

  /* When we are saving, If the user wrote a filename in the text entry, we should use it instead. */
  if (!is_open)
  {
    if (f->file[1])
      {
	g_free(f->file[0]);
	f->file[0] = g_strdup(f->file[1]);
	g_free(f->file[1]);
	f->file[1] = NULL;
      }
  }
/*    g_strdup (gtk_file_selection_get_filename
	      (GTK_FILE_SELECTION (file_selector))); */
  gtk_widget_destroy (file_selector);

  if (f->result == GTK_RESPONSE_OK)
    {
      katoob_debug ("setting filesel_path!");
      g_free (config->filesel_path);
      tmp = g_path_get_dirname (f->file[0]);
      config->filesel_path = g_strconcat (tmp, G_DIR_SEPARATOR_S, NULL);
      g_free (tmp);
    }
  return f;
}

/* TODO: WE SHOULD OPEN THE FILE AFTER CONVERTING THE ENCODING! */
gboolean
katoob_write (gchar * buff, gint encoding, FILE * fp, gchar * file)
{
  gint e;
  KATOOB_DEBUG_FUNCTION;
  if ((e = strcmp (katoob_encodings_get_by_number (encoding), "UTF-8")) == 0)
    {
      katoob_debug ("UTF-8 encoding.");
      if (!katoob_real_write (buff, fp, file))
	{
	  katoob_debug ("Failed to write!");
	  return FALSE;
	}
      return TRUE;
    }
  else
    {
      gchar *final_buff = NULL;
      if (!katoob_utf8_strip_codes (buff, &final_buff))
	{
	  katoob_debug ("Failed to strip!");
	  return FALSE;
	}
      if ((e =
	   strcmp (katoob_encodings_get_by_number
		   (encoding), "PLAIN TEXT")) == 0)
	{
	  if (!katoob_real_write (final_buff, fp, file))
	    {
	      katoob_debug ("Failed to write!");
	      return FALSE;
	    }
	  g_free (final_buff);
	  return TRUE;
	}
      else
	{
	  gchar *b =
	    katoob_encoding_convert ("UTF-8",
				     katoob_encodings_get_by_number
				     (encoding), final_buff,
				     strlen (final_buff));
	  g_free (final_buff);
	  if (!b)
	    {
	      katoob_debug("katoob_write -> failed to convert");
	      return FALSE;
	    }
	  if (!katoob_real_write (b, fp, file))
	    {
	      g_free (b);
	      katoob_debug("katoob_write -> failed to write");
	      return FALSE;
	    }
	  g_free (b);
	  return TRUE;
	}
    }
  return FALSE;
}

gboolean
katoob_create_file_if_required (gchar * file)
{
  FILE *infile;
  gchar *_tmp = NULL;
  KATOOB_DEBUG_FUNCTION;

  infile = fopen (file, "r");
  if (!infile)
    {
      if (errno == ENOENT)
	{			/* No such file or directory */
	  gint x;
	  _tmp =
	    g_strdup_printf (_("The file %s doesn't exist, Create it?"),
			     file);
	  x = katoob_create_question (_tmp);
	  g_free (_tmp);
	  switch (x)
	    {
	    case GTK_RESPONSE_YES:
	      {
		if ((infile = fopen (file, "w+")) == NULL)
		  {
		    katoob_error (_("Couldn't create file %s, %s"), file,
				  strerror (errno));
		    return FALSE;
		  }
		break;
	      }
	    case GTK_RESPONSE_NO:
	      {
		return FALSE;
	      }
	    }
	}
      else
	{
	  katoob_error (_("Couldn't open file %s, %s"), file,
			strerror (errno));
	  g_free (_tmp);
	  return FALSE;
	}
    }
  fclose (infile);
  return TRUE;
}

gboolean
katoob_check_file_access (gchar * file)
{
  gint i = 0;
  KATOOB_DEBUG_FUNCTION;

  if ((i = access (file, W_OK)) != 0)
    {				/* write permission denied */
      return FALSE;
    }
  else
    {
      return TRUE;
    }
}

gboolean
katoob_file_get_contents (gchar * file, gchar ** content)
{
  FILE *infile;
  struct stat f_stat;
  gint i = 0;
  gchar *buf = NULL;
  *content = buf;
  KATOOB_DEBUG_FUNCTION;

  if (stat (file, &f_stat))	/* failed! return -1; */
    {
      katoob_error ("%s", (gchar *) g_strerror (errno));
      return FALSE;
    }

  if (f_stat.st_size == 0)
    {
      return TRUE;
    }

  infile = fopen (file, "r");

  if (!infile)
    {
      katoob_error ("%s", (gchar *) g_strerror (errno));
      return FALSE;
    }

  buf = (gchar *) g_malloc (f_stat.st_size + 1);

  i = fread (buf, f_stat.st_size, 1, infile);

  if (i != 1)
    {
      katoob_error (_("An error has occured while reading file %s\n%s"), file,
		    (gchar *) g_strerror (errno));
      fclose (infile);
      return FALSE;
    }

  fclose (infile);

  buf[f_stat.st_size] = '\0';

  *content = buf;

  return TRUE;
}

gboolean
katoob_real_write (gchar * buff, FILE * fp, gchar * file)
{
  KATOOB_DEBUG_FUNCTION;

  if (strlen (buff) == 0)
    {
      katoob_debug ("Empty buffer.");
      fclose (fp);
      return TRUE;
    }

  if (fwrite (buff, strlen (buff), 1, fp) < 1)
    {
      katoob_error (_("An error has occured while writing to file %s\n%s"),
		    file, g_strerror (errno));
      fclose (fp);
      return FALSE;
    }

  fclose (fp);
  return TRUE;
}

FILE *
katoob_create_temp_file (gchar ** tmp_file)
{
  FILE *fp;
  gint fd;

  KATOOB_DEBUG_FUNCTION;

  *tmp_file =
    g_strdup_printf ("%s%s%sXXXXXX", g_get_tmp_dir (), G_DIR_SEPARATOR_S,
		     PACKAGE);

  fd = g_mkstemp (*tmp_file);
  katoob_debug (*tmp_file);

  if (fd == -1)
    {
      g_free (*tmp_file);
      katoob_debug ("Could not create temporary file");
      /*      katoob_error (_("Could not create temporary file")); */
      *tmp_file = NULL;
      return NULL;
    }
  fp = fdopen (fd, "w");
  katoob_debug (*tmp_file);

  if (!fp)
    {
      unlink (*tmp_file);
      g_free (*tmp_file);
      katoob_debug ("Could not open temporary file");
      *tmp_file = NULL;
      /*      katoob_error (_("Could not open temporary file")); */
      close (fd);
      return NULL;
    }
  return fp;
}

gchar *
katoob_read_from_stream (FILE * fp)
{
  gchar *name = NULL;
  gchar *result;
  FILE *file;
  gchar c;

  KATOOB_DEBUG_FUNCTION;

  file = katoob_create_temp_file (&name);
  if (!file)
    {
      katoob_error (_("Could not create temporary file"));
      return NULL;
    }

  katoob_debug (name);

  while ((c = fgetc (fp)) != EOF)
    {
      fputc (c, file);
    }
  fclose (file);
  if (!katoob_file_get_contents (name, &result))
    {
      g_free (name);
      return NULL;
    }
  else
    {
      g_free (name);
      return result;
    }
}
