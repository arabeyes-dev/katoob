AC_DEFUN(CHECK_PSPELL_CXX_LIBS,
[
AC_MSG_CHECKING([for pspell C++ library])
pspell_libs="
/usr/lib/libpspell.so
/usr/lib/libpspell.a
/usr/lib/libpspell.la
/usr/lib/pspell/libpspell.so
/usr/lib/pspell/libpspell.a
/usr/lib/pspell/libpspell.la
/usr/local/lib/libpspell.so
/usr/local/lib/libpspell.a
/usr/local/lib/libpspell.la
/usr/local/lib/pspell/libpspell.so
/usr/local/lib/pspell/libpspell.a
/usr/local/lib/pspell/libpspell.la
"
for i in $pspell_libs; do
if test -r $i; then
spell_cxx=`ldd $i | grep stdc | tr ' ' '\n' | grep lib/`
AC_MSG_RESULT($spell_cxx)
break 2
fi
done
])

AC_DEFUN(CHECK_ASPELL_CXX_LIBS,
[
AC_MSG_CHECKING([for aspell C++ library])
aspell_libs="
/usr/lib/libaspell.so
/usr/lib/libaspell.a
/usr/lib/libaspell.la
/usr/lib/aspell/libaspell.so
/usr/lib/aspell/libaspell.a
/usr/lib/aspell/libaspell.la
/usr/local/lib/libaspell.so
/usr/local/lib/libaspell.a
/usr/local/lib/libaspell.la
/usr/local/lib/aspell/libaspell.so
/usr/local/lib/aspell/libaspell.a
/usr/local/lib/aspell/libaspell.la
"
for i in $aspell_libs; do
if test -r $i; then
spell_cxx=`ldd $i | grep stdc | tr ' ' '\n' | grep lib/`
AC_MSG_RESULT($spell_cxx)
break 2
fi
done
])

AC_DEFUN(TEST_SPELL,
[
AC_ARG_ENABLE(spell,
AC_HELP_STRING([--enable-spell], [Enable the spell checker (default: no)]),
        spell=$enableval, spell=no)

AC_ARG_WITH(spell,
AC_HELP_STRING([--with-spell=PREFIX], [Prefix where aspell or pspell is installed (optional)]),
spell_prefix="$withval", spell_prefix="")

AC_ARG_WITH(spell-libraries,
AC_HELP_STRING([--with-spell-libraries=DIR], [Directory where aspell or pspell library is installed (optional)]),
spell_libraries="$withval", spell_libraries="")

AC_ARG_WITH(spell-includes,
AC_HELP_STRING([--with-spell-includes=DIR], [Directory where aspell or pspell header files are installed (optional)]),
spell_includes="$withval", spell_includes="")

  if test "x$spell_libraries" != "x" ; then
    SPELL_LIBS="-L$spell_libraries"
  elif test "x$spell_prefix" != "x"; then
    SPELL_LIBS="-L$spell_prefix/lib"
  elif test "x$prefix" != "xNONE"; then
    SPELL_LIBS="-L$prefix/lib"
  fi

  if test "x$spell_includes" != "x" ; then
    SPELL_CFLAGS="-I$spell_includes"
  elif test "x$spell_prefix" != "x"; then
    SPELL_CFLAGS="-I$spell_prefix/include"
  elif test "x$prefix" != "xNONE"; then 
    SPELL_CFLAGS="-I$prefix/include"
  fi

if test "$spell" = "yes"; then
  aspell_l=no
  AC_CHECK_HEADER(aspell.h, aspell_h=yes, aspell_h=no)
  if test "$aspell_h" = "yes"; then
   AC_CHECK_LIB(aspell, new_aspell_speller, aspell_l=yes,aspell_l=no)
  fi
  if test "$aspell_l" = "no"; then
   AC_CHECK_HEADER(pspell/pspell.h, pspell_h=yes, pspell_h=no)
   if test "$pspell_h" = "yes"; then
    AC_CHECK_LIB(pspell, new_pspell_manager, pspell_l=yes,pspell_l=no)
   fi
  fi
 if test "$aspell_l" = "yes"; then
  AC_DEFINE(HAVE_SPELL,,[Enable the spell checker])
  AC_DEFINE(HAVE_NEW_SPELL,,[aspell >= 0.50])
  CHECK_ASPELL_CXX_LIBS
  SPELL_LIBS="$SPELL_LIBS -laspell"
  AC_MSG_NOTICE([Using aspell for the spell checker.])
 else
  if test "$pspell_l" = "yes"; then
    AC_DEFINE(HAVE_SPELL,,[Enable the spell checker])
    AC_DEFINE(HAVE_OLD_SPELL,,[aspell < 0.50])
    CHECK_PSPELL_CXX_LIBS
    SPELL_LIBS="$SPELL_LIBS -lpspell"
    AC_MSG_NOTICE([Using pspell for the spell checker.])
  else
    AC_MSG_WARN([I couldn't find either aspell or pspell, The spell checker will be disabled ])
   spell=no
  fi
 fi  
fi
])
